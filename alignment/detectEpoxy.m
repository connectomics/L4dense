function [vesselcont, nucleicont] = detectEpoxy(C, all_fns)
load('Z:\Data\boergensk\st07x2_redone\2012-09-28_ex145_07x2_ttemp.mat');
load('Z:\Data\boergensk\st07x2_redone\07x2_mag4_vesselsAndNuclei.mat');
vessels(1320:1340,120:125,760) = true;
vessels(1320:1340,120:125,759) = true;
for z_it = 1 : size(vessels, 3)
    z_it
    vessels(:,:,z_it) = imfill(vessels(:,:,z_it),'holes');
end
ttemp_shiftsN_x_y_z_xvsy = reshape(ttemp.shiftsN, 3, 3, C.P, 2);
for x = 1 : size(ttemp_shiftsN_x_y_z_xvsy, 1)
    for y = 1 : size(ttemp_shiftsN_x_y_z_xvsy, 2)
        fixed_positions(x, y, 1, 1) = (x - 1) * 3072;
        fixed_positions(x, y, 1, 2) = (y - 1) * 2048;
    end
end
ttemp_shiftsN_x_y_z_xvsy = ttemp_shiftsN_x_y_z_xvsy + repmat(fixed_positions, [1, 1, C.P, 1]);
ttemp_shiftsN_x_y_z_xvsy_extended = repmat(extendArray(ttemp_shiftsN_x_y_z_xvsy, [12, 8]), [1, 1, 1, 1, 2]);
smallPositions = calcPositions(C, all_fns);
finalPositions = smallPositions.x_y_z_xvsy_beginvsend + ttemp_shiftsN_x_y_z_xvsy_extended;
for x = 1 : size(finalPositions, 1)
    x
    for y = 1 : size(finalPositions, 2)
        for z = 1 : size(finalPositions, 3)
            cc = squeeze(ceil((finalPositions(x, y, z, :, :)) / 4));
            vesselcont(x, y, z) = sum(sum(vessels(cc(1, 1) : cc(1, 2), cc(2, 1) : cc(2, 2), ceil(z / 4))));
            nucleicont(x, y, z) = sum(sum(nuclei(cc(1, 1) : cc(1, 2), cc(2, 1) : cc(2, 2), ceil(z / 4))));
        end
    end
end
end
