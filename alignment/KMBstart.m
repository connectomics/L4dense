%% Startup
tic
'tic'
D=st07x2();
C=D.C;



C.samplestr='ex145';
C.knumberstr=[C.samplestr '_' C.stackN];
C.imdatestr='2012-09-23';

C.knumberstrshort=C.knumberstr;
C.cubefn = strcat(C.datestr,'_',C.knumberstr);
C.imfn = strcat(C.imdatestr,'_',C.samplestr);

C.rootf='D:\Git\st07x2\newcubing\LDKMB_data\';
C.matdir = [C.rootf C.cubefn '/'];
C.origin = 'Z:\Data\boergensk\st07x2_redone\';
mkdir(C.matdir);
%system(['chmod 777 ' C.matdir]);
C.cubesize = 128;

all_fns_temp=KMBmakeFileList(C,C.origin,'.tif');
all_fns_temp{3, 1, 2879} = all_fns_temp{3, 1, 2878}; %debris
all_fns_temp{3, 2, 2879} = all_fns_temp{3, 2, 2878}; %debris
all_fns = extendArray(all_fns_temp, [12, 8]);

for x=D.exclude
    all_fns(:,:,x)=all_fns(:,:,x-1);
end
for slice_it = 1
    all_fns(:,:,slice_it) = all_fns(:,:,2);
end
C.M=size(all_fns,1);
C.N=size(all_fns,2);
C.P=size(all_fns,3)-1;
C.procs=C.P/C.slPth;
C.dims=[256+64, 256+16] % first width, then height size(KMBreadTif(all_fns{round(C.M/2),round(C.N/2),1000})')*3/8;
C.joblist=C.joblistF(C.procs);
C.document_ccs = 'none';
C.dimsTODO = 1:3;
C.subslicesTODO = [1, C.M; 1, C.N; 1, C.slPth + 1];
C.preloadtifs = false;
toc
%% Measure shifts Fermat
tic
'tic'
jm = findResource('scheduler', 'configuration', 'FermatCPU');
job = createJob(jm, 'configuration', 'FermatCPU');
for ii=1:C.procs
    
    inputargs = {C,ii,all_fns(:,:,(ii-1)*C.slPth+1:ii*C.slPth+1)};
    createTask(job, @KMBmeasureShifts, 0, inputargs, 'configuration', 'FermatCPU');
end
submit(job);
toc
%% Measureshifts debug
tic
for proc_it=1:C.procs
    myslice{proc_it} = all_fns(:,:,(proc_it-1)*C.slPth+1:proc_it*C.slPth+1);
end
parfor proc_it=1:C.procs
    KMBmeasureShifts(C,proc_it,myslice{proc_it});
end
toc
%% calculate positions
[vesselcont, nucleicont] = detectEpoxy(C,all_fns);
%% Make ttemp
tic
'tic'
C.allweightsReset=true;
ttemp=update_ttemp(KMBmakeLS(C, vesselcont, nucleicont), C, all_fns);

              
%% Find problems
problems = findErrors(ttemp, true, 20, 20);
documentErrors(problems, C, all_fns, false);
%% Find problem loops
findProblemLoops(ttemp)
%% Measure inter image differences
C.maxMagMultXY=ceil((C.maxis-C.minis+1)/1024)*8;
multA=C.cubesize*C.maxMagMultXY; %defines the size of chunks processed from one subsubthread
C.maxMagMultXYHigh=[1 1];
multB=C.cubesize*C.maxMagMultXY.*C.maxMagMultXYHigh;  %same thing for high writer
C.montagewh=ceil((C.maxis-C.minis+1)./(multB)).*multB;
C.outA=[1 1;1 1;1 ceil(C.P/(C.cubesize))];
C.inA=[[1;1], C.montagewh'./multA'];
ix1=it(C.outA);
KMBparforHelper(false,@(thII)KMBcalcIID(all_fns,thII,C,ttemp),size(ix1,2));
%% Low memory tif writer
C.maxMagMultXY=ceil((C.maxis-C.minis+1)/1024)*8;
multA=C.cubesize*C.maxMagMultXY; %defines the size of chunks processed from one subsubthread
C.maxMagMultXYHigh=[1 1];
multB=C.cubesize*C.maxMagMultXY.*C.maxMagMultXYHigh;  %same thing for high writer
C.montagewh=ceil((C.maxis-C.minis+1)./(multB)).*multB;
C.outA=[1 1;1 1;1 ceil(C.P/(C.cubesize))];
C.inA=[[1;1], C.montagewh'./multA'];
ix1=it(C.outA);
C.tifdir=['D:\0\Cortex\st' C.stackN 'Top\tif\' C.cubefn];
mkdir(C.tifdir);
KMBparforHelper(false,@(thII)KMBwriteTifs(all_fns,thII,C,ttemp),size(ix1,2));
%% Preparation for cubes
tic
'tic'
C.cubedir=[C.rootf '/cubes/' C.cubefn];
C.maxMagHigh=8;
C.maxMagMultXY=ceil((C.maxis-C.minis+1)/1024)*8;
multA=C.cubesize*C.maxMagMultXY; %defines the size of chunks processed from one subsubthread
C.maxMagMultXYHigh=[1 1];
C.pixelSize=[16 16 30];
multB=C.cubesize*C.maxMagMultXY.*C.maxMagMultXYHigh;  %same thing for high writer
C.montagewh=ceil((C.maxis-C.minis+1)./(multB)).*multB;
C.outA=[1 1;1 1;1 ceil(C.P/(C.cubesize))];
C.outB=[1 1;1 1;1 ceil(C.P/(C.cubesize*C.maxMagHigh))];
C.inA=[[1;1], C.montagewh'./multA'];
for magItI=0:log2(C.maxMagHigh)
    magIt=2^magItI;
    if magIt==1
        Lmontagewh=C.montagewh;
        montagedepth= C.outA(3,2)*C.cubesize;
    else
        Lmontagewh= C.montagewh/magIt;
        montagedepth= C.outB(3,2)*C.cubesize*C.maxMagHigh/magIt;
    end
    
    KNOSSOS_writeKconfFile(strcat(C.cubedir,filesep,'mag',num2str(magIt),filesep),strcat(C.cubefn,'_mag',num2str(magIt)),[Lmontagewh,montagedepth],C.pixelSize*magIt,magIt);
end
KNOSSOS_writeKconfFile(strcat(C.cubedir,filesep,'mag3',filesep),strcat(C.cubefn,'_mag3'),[Lmontagewh,montagedepth*8],[1 1 1],1);
KNOSSOS_writeKconfFile(strcat(C.cubedir,filesep,'mag5',filesep),strcat(C.cubefn,'_mag5'),[Lmontagewh/4,montagedepth*8],[1 1 1],1);

C.inB=[[1;1], C.montagewh'./multB'];
ix1=it(C.outA);
ix2=it(C.outB);
toc
%% Normal cube writer
tic
'tic'
jm = findResource('scheduler', 'configuration', 'FermatCPU');
job = createJob(jm, 'configuration', 'FermatCPU');
for thII=1:size(ix1,2)
    
    inputargs = {all_fns,thII,C,ttemp};
    createTask(job, @KMBwriteCubes3, 0, inputargs, 'configuration', 'FermatCPU');
end
submit(job);
toc
%         KMBparforHelper(false,@(thII)KMBwriteCubes3(all_fns,thII,C,ttemp),size(ix1,2));

%% High cubes
%    KMBparforHelper(false,@(thII)KMBwriteCubesHigh(thII,C),size(ix2,2));
%% Rapid cubes
tic
'tic'
C.originRapid=[C.matdir 'rapid'];
for i=1:size(ix1,2)
    KMBwriteCubesRapid(all_fns,i,C,ttemp,true);
end
toc