% load old relaxation result
ttemp_old = load('D:\Git\st07x2\newcubing\LDKMB_translateCoords\ttemp_old.mat');
% load new relaxation result
ttemp_new = load('D:\Git\st07x2\newcubing\LDKMB_translateCoords\ttemp_new.mat');
% get shift data into nice format
shifts_old = reshape(ttemp_old.ttemp.shiftsN, 3, 3, 3424, 2);
shifts_new = reshape(ttemp_new.ttemp.shifts, 36, 24, 3424, 2);
% load skeleton
addpath('D:\Git\codeBase\skeletonClassWithAdj_v8');
mynml = nml_v8('D:\temp\SynTypComm.nml');
% copy skeleton
mynml_new = mynml;
for tree_idx = 1 : mynml.numTrees()
    % get nodes from skeleton, fix for wK coord shift
    node_coords = mynml.getNodes(tree_idx).r;
    node_coords(node_coords == 0) = 1;
    % find matching frames for old coordinates
    matching_frames_old = findMatchingFramesOld(node_coords, shifts_old, [3072, 2048]);
    % find new coordinates
    node_coords_new = findNewCoordinates(node_coords, matching_frames_old, shifts_new, shifts_old);
    % set new coordinates 
    mynml_new = mynml_new.setNodes(tree_idx, node_coords_new);
end
%set dataset
mynml_new.setDataset([mynml_new.getDataset(), '_new2'])
%write nml
mynml_new.write('D:\temp\SynTypComm_new.nml');
