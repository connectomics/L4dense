function KMBparforHelper(parld,func,procs)
if parld
    if matlabpool('size')==0
        matlabpool;
        'matlabpool'
    end
    threads=matlabpool('size');
    parfor thI=1-threads:0
        for th2I=1:ceil(procs/threads)
            ii=th2I*threads+thI;
            if ii<=procs
                func(ii);
            end
            
        end
    end
    
else
    for ii=1:procs
        func(ii);
    end
end




