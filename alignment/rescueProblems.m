function problem = rescueProblems(ttemp)
problem{1}=sqrt(sum(ttemp.errors(1 : 35 * 24 * 3424, :) .^ 2, 2));
problem{2}=sqrt(sum(ttemp.errors(35 * 24 * 3424 + 1 : 35 * 24 * 3424 + 36 * 23 * 3424, :) .^ 2, 2));
problem{3}=sqrt(sum(ttemp.errors(35 * 24 * 3424 + 36 * 23 * 3424 + 1 : end - 1, :) .^ 2, 2));

problem{1} = reshape(problem{1}, [35, 24, 3424]);
problem{2} = reshape(problem{2}, [36, 23, 3424]);
problem{3} = reshape(problem{3}, [36, 24, 3423]);