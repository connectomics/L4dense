function KMBmeasureShifts(C,currentThread,all_fns)
    function readImageSmall(ii)
        if size(at(all_fns,ii),1)>0
            if isempty(cache_pre{ceil(ii(1)/12)*12,ceil(ii(2)/8)*8,ii(3)})
                cache_pre{ceil(ii(1) / 12) * 12, ceil(ii(2) / 8) * 8, ii(3)} = imread(all_fns{ii(1), ii(2), ii(3)});
            end
            cache{ii(1), ii(2), ii(3)} = cache_pre{ceil(ii(1) / 12) * 12, ceil(ii(2) / 8) * 8, ii(3)};
            idx_width = mod(ii(1)-1,12) * 256;
            idx_height = mod(ii(2)-1,8) * 256;
            if mod(ii(1),12) ~= 1
                idx_width = idx_width - 32;
            end
            if mod(ii(1),12) == 0
                idx_width = idx_width - 32;
            end
            if mod(ii(2),8) ~= 1
                idx_height = idx_height - 8;
            end
            if mod(ii(2),8) == 0
                idx_height = idx_height - 8;
            end
            cache{ii(1),ii(2),ii(3)} = cache{ii(1),ii(2),ii(3)}(idx_height+1:idx_height+C.dims(2), idx_width+1:idx_width+C.dims(1));
        end
    end
if isempty(find(C.joblist==currentThread,1))
    return;
end
cache=cell(C.M,C.N,C.slPth+1);
cache_pre=cell(C.M,C.N,C.slPth+1);
if C.preloadtifs == true
    for i=it([[1;1;1],[C.M; C.N; C.slPth+1]])
        readImageSmall(i);
    end
end


overallProcNumberStart=(currentThread-1)*C.slPth;
preShift=zeros(C.M,C.N,C.P,2);

debugOptimizeFFT=true;

delta=cell(3,1);
weights=cell(3,1);
area=cell(3,1);
eye3=eye(3);
aya3=[1 0 1; 0 1 1; 0 0 1];
for dimIt=C.dimsTODO
    delta{dimIt}=zeros([[C.M C.N C.slPth+1]-aya3(dimIt,:) 2]);
    weights{dimIt}=zeros([C.M C.N C.slPth+1]-aya3(dimIt,:));
    area{dimIt}=zeros([C.M C.N C.slPth+1]-aya3(dimIt,:));
    if debugOptimizeFFT
        disp('start optimize fft');
        fftw('planner','exhaustive');
        t1=fft2(single(ones(C.xcorr_size(dimIt,:))));
        ifft2(t1);
        disp('end optimize fft');
    end
    for i=it([[C.subslicesTODO(1,1);C.subslicesTODO(2,1);C.subslicesTODO(3,1)],[C.subslicesTODO(1,2); C.subslicesTODO(2,2); C.subslicesTODO(3,2)]-aya3(dimIt,:)'])
        shifts = [0,0,-1];
        thisweight=1E-7;
        if size(at(all_fns,i),1)>0&&size(at(all_fns,i+eye3(:,dimIt)),1)>0
            if (dimIt==2 && mod(i(2),8)==0) || (dimIt==1 && mod(i(1),12)==0) || dimIt==3
                xcorr_sizeT=C.xcorr_size(dimIt,:);
                crop=0;
                CdimsFLR=fliplr(C.dims);
                j=i+eye3(:,dimIt);
                if isempty(at(cache,j))
                    readImageSmall(j);
                end
                if isempty(at(cache,i))
                    readImageSmall(i);
                end

                if dimIt<3
                    if dimIt==2
                        CdimsFLR=fliplr(CdimsFLR);
                        xcorr_sizeT=fliplr(xcorr_sizeT);
                    end;
                    preShiftsTransversal={[0; -32], [0;32],[0; 0]};
                    dC1=[[CdimsFLR(1)/2-xcorr_sizeT(1)/2+1 CdimsFLR(1)/2+xcorr_sizeT(1)/2];...
                        [1+crop xcorr_sizeT(2)+crop]];
                    dC2=[[CdimsFLR(1)/2-xcorr_sizeT(1)/2+1 CdimsFLR(1)/2+xcorr_sizeT(1)/2];...
                        [CdimsFLR(2)-xcorr_sizeT(2)+1-crop CdimsFLR(2)-crop]];
                    dC2=dC2+repmat(flipud(preShiftsTransversal{dimIt}),[1 2]);
                    dC1=dC1-repmat(flipud(preShiftsTransversal{dimIt}),[1 2]);
                    
                    if dimIt==2
                        dC1=flipud(dC1);
                        dC2=flipud(dC2);
                    end
                    shifts = forMH_AlignCube_measureshifts(C,...
                        cache{j(1),j(2),j(3)}(dC1(1,1):dC1(1,2),dC1(2,1):dC1(2,2)),...
                        cache{i(1),i(2),i(3)}(dC2(1,1):dC2(1,2),dC2(2,1):dC2(2,2)),dimIt,false,i+[0;0;overallProcNumberStart]);
                else
                    if true
                        dC=zeros(2,2,2);
                        dC=dC+imresize(reshape(CdimsFLR,[1,1,2]),[2,2])/2;
                        reshape(xcorr_sizeT,[1,1,2]);
                        dC=dC+imresize(cat(2,-ans/2+1,ans/2),[2,2]);
                        reshape(preShift(i(1),i(2),overallProcNumberStart+i(3),:),[1,1,2]);
                        dC=dC+imresize(cat(1,-ans/2,ans/2),[2,2]);
                        shifts = forMH_AlignCube_measureshifts(C,...
                            cache{j(1),j(2),j(3)}(dC(2,1,1):dC(2,2,1),dC(2,1,2):dC(2,2,2)),...
                            cache{i(1),i(2),i(3)}(dC(1,1,1):dC(1,2,1),dC(1,1,2):dC(1,2,2)),dimIt,false,i+[0;0;overallProcNumberStart]);
                        shifts(1)=shifts(1)-preShift(i(1),i(2),overallProcNumberStart+i(3),2);
                        shifts(2)=shifts(2)-preShift(i(1),i(2),overallProcNumberStart+i(3),1);
                    else
                        shiftsX=zeros(9,4);
                        for tI=-1:1; %change for multiple FFTs per image
                            for tJ=-1:1;
                                dC1=[[CdimsFLR(1)/2-xcorr_sizeT(1)/2 CdimsFLR(1)/2+xcorr_sizeT(1)/2-1];...
                                    [CdimsFLR(2)/2-xcorr_sizeT(2)/2 CdimsFLR(2)/2+xcorr_sizeT(2)/2-1]];
                                dC1=dC1+[tI,tI;tJ,tJ]*3500;
                                shiftsX(tI*3+3+tJ+2,:) = forMH_AlignCube_measureshifts(C,...
                                    cache{j(1),j(2),j(3)}(dC1(1,1):dC1(1,2),dC1(2,1):dC1(2,2)),...
                                    cache{i(1),i(2),i(3)}(dC1(1,1):dC1(1,2),dC1(2,1):dC1(2,2)),dimIt,false,i+[0;0;overallProcNumberStart]);
                            end
                        end
                        shifts=mean(shiftsX,1);
                    end
                end
            end
            thisweight=1;
        end
        delta{dimIt}=sat(delta{dimIt},[i;1], shifts(1));
        delta{dimIt}=sat(delta{dimIt},[i;2], shifts(2));
        weights{dimIt}=sat(weights{dimIt},i, thisweight);
        area{dimIt}=sat(area{dimIt},i, shifts(3));
    end
end
if length(C.dimsTODO) == 3 && isequal(diff(C.subslicesTODO'), size(all_fns)-1)
    save(strcat(C.matdir,C.knumberstr,'_delta_',sprintf('%u',currentThread),'.mat'),'delta','area','weights');
end
end