function shifts = cleanupShifts(shifts_unclean, ttemp)
weights = reshape(abs(ttemp.total.F') ...
    * spdiags(ttemp.total.W), [36, 24, 3424]);
conn_matrix = cleanupShifts_makeConnMatrix();
shifts = shifts_unclean;
for z = 1 : 3424
	z
    flat = @(x)x(:);
    weight = flat(weights(:, :, z));
  	mask_weight = weight >= 1;
	mask_weight_new = mask_weight;
	shifts_new_x = shifts(:, :, z, 1);
	shifts_new_y = shifts(:, :, z, 2);
	
	while any(~mask_weight_new)
        asdf
		mask_weight_new = conn_matrix * mask_weight_new
		shifts_new_x = conn_matrix * shifts_new_x;
		shifts_new_y = conn_matrix * shifts_new_y;
		updated = mask_weight_new>0 & (~mask_weight);
		%shifts(x)(updated) = shifts_new_x(updated)./mask_weigh_new(updated);
		%shifts(y)(updated) = shifts_new_y(updated)./mask_weigh_new(updated);
		mask_weight(updated) = true;
	end
end
		