function ttemp=KMBmakeLS(C, vesselcont, nucleicont)

delta_col=cell(3,1);
weight_col_temp=cell(3,1);
area_col=cell(3,1);

contrast_col=[];
for i=1:C.procs
    load(strcat(C.matdir,C.knumberstr,'_delta_',sprintf('%u',i),'.mat'));
    % contrastT=load(strcat(C.matdir,C.knumberstr,'_delta_',sprintf('%u',i),'.mat'),'contrast');
    for dimIt=1:3
        area_col{dimIt}=cat(3,area_col{dimIt},area{dimIt});
        delta_col{dimIt}=cat(3,delta_col{dimIt},delta{dimIt});
        weight_col_temp{dimIt}=cat(3,weight_col_temp{dimIt},weights{dimIt});
        
    end
    % contrast_col=cat(5,contrast_col,contrastT.contrast);
end
weight_col = fixVesselWeights(weight_col_temp, {vesselcont, nucleicont}, [1000, 1000]);


alldeltas=cell(3,1);
allweights=cell(3,1);
allareas=cell(3,1);
delta_col{3}=delta_col{3}(:,:,1:C.procs*C.slPth-1,:);
weight_col{3}=weight_col{3}(:,:,1:C.procs*C.slPth-1);
area_col{3}=area_col{3}(:,:,1:C.procs*C.slPth-1);
% weight_col{2}(4, 8, 2414) = 1E-7; %nucleus
% weight_col{2}(4, 8, 2416) = 1E-7; %nucleus
% weight_col{2}(4, 8, 2409) = 1E-7; %nucleus
% weight_col{2}(4, 8, 2417) = 1E-7; %nucleus
% weight_col{3}(34, 17, 1276) = 1E-7; % nucleus
if (~C.allweightsReset)
    weightarray=makeWA();
    for dimIt=1:3
        for i=it([[1;1;1],size(weight_col{dimIt})'])
            weight_col{dimIt}(i(1),i(2),i(3))=1E-7;
            if weightarray{dimIt}(i(2),i(1))>0
                if i(3)>weightarray{dimIt}(i(2),i(1))
                    weight_col{dimIt}(i(1),i(2),i(3))=1;
                end
            else
                if i(3)<-weightarray{dimIt}(i(2),i(1))
                    weight_col{dimIt}(i(1),i(2),i(3))=1;
                end
            end
        end
    end
end
for dimIt=1:3
    alldeltas{dimIt}=reshape(delta_col{dimIt},[],2);
    allweights{dimIt}=weight_col{dimIt}(:);
    allweights{3}=allweights{3}*4;
    allareas{dimIt}=area_col{dimIt}(:);
    allweights{dimIt}(allweights{dimIt}<1E-7)=1E-7;
    if (~C.allweightsReset)
        for i=1:size(allweights{dimIt},1)
            if (dimIt~=3 || i<5518*9*13+1||i>5519*9*13)&&(dimIt~=3 || i<5174*9*13+1||i>5175*9*13)
                
                if allweights{dimIt}(i)>=1 && allareas{dimIt}(i)>600
                    allweights{dimIt}(i)=allweights{dimIt}(i)*1E-7;
                elseif allweights{dimIt}(i)>=1 && allareas{dimIt}(i)>400
                    allweights{dimIt}(i)=allweights{dimIt}(i)*1E-3;
                elseif allweights{dimIt}(i)>=1 && allareas{dimIt}(i)>200
                    allweights{dimIt}(i)=allweights{dimIt}(i)*0.5;
                end
            end
        end
        
        
        allweights_purged=allweights{dimIt};
        allweights_purged(allweights_purged<1E-1)=0;
        allweights_purged(allweights_purged>=1E-1)=1;
        
        xax={alldeltas{dimIt}(:,1).*allweights_purged alldeltas{dimIt}(:,2).*allweights_purged};
        xaax=[xax{1}(xax{1}~=0) xax{2}(xax{1}~=0)];
        
        for ix=1:2;
            subplot(2,3,dimIt+ix*3-3);
            hist(xaax(:,ix),100);
        end
        meanxaax=mean(xaax);
        minxaax=min(xaax);
        maxxaax=max(xaax);
        stdxaax=std(xaax);
        for i=1:size(allweights{dimIt},1)
            if (dimIt~=3 || i<5518*9*13+1||i>5519*9*13)&&(dimIt~=3 || i<5174*9*13+1||i>5175*9*13)
                if max(abs(alldeltas{dimIt}(i,:)-meanxaax))>50||...
                        min(alldeltas{dimIt}(i,:)-minxaax)<5||...
                        max(alldeltas{dimIt}(i,:)-maxxaax)>-5
                    if allweights{dimIt}(i)>=1E-1
                        allweights{dimIt}(i)=1E-5*allweights{dimIt}(i);
                    elseif allweights{dimIt}(i)>=1E-4
                        allweights{dimIt}(i)=1E-4*allweights{dimIt}(i);
                    end
                end
            end
        end
    end
end
disp('start LS');
ttemp = forMH_AlignCube_LSsolution_002(C,[alldeltas{1};alldeltas{2};alldeltas{3}],[allweights{1};allweights{2};allweights{3}]);
ttemp.delta_col=delta_col;
ttemp.allweights = allweights;
%save(strcat(C.matdir,C.knumberstr,'_allShifts.mat'),'ttemp');
%bestcoeffs=fminsearch(@linear_fit,[30,-0.1],[],1:7320,cons);
