function arrayNew = extendArray(arrayOld, factor)

arrayNew = repmat(arrayOld, [ 1, 1, 1, 1, factor(1), 1]);
s = size(arrayNew);

arrayNew = permute(arrayNew, [6, 2, 3, 4, 5, 1]);
arrayNew = reshape(arrayNew, [1, s(2), s(3), s(4), s(1) * factor(1)]);
arrayNew = squeeze(permute(arrayNew, [5, 2, 3, 4, 1]));

arrayNew = repmat(arrayNew, [ 1, 1, 1, 1, factor(2) 1]);
arrayNew = permute(arrayNew, [1, 6, 3, 4, 5, 2]);
arrayNew = reshape(arrayNew, [s(1) * factor(1), 1, s(3), s(4), s(2) * factor(2)]);
arrayNew = squeeze(permute(arrayNew, [1, 5, 3, 4, 2]));
