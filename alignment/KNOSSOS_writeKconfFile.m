function KNOSSOS_writeKconfFile(kl_parname,kl_filePrefix,kl_boundary_xyz,kl_scale_xyz,kl_mag)

    mkdir(kl_parname);
    kl_fname = strcat(kl_parname,'knossos.conf');
    fid=fopen(kl_fname,'w');
    
    fprintf(fid,'experiment name \"%s\";\n',kl_filePrefix);
    fprintf(fid,'boundary x %d;\n',kl_boundary_xyz(1));
    fprintf(fid,'boundary y %d;\n',kl_boundary_xyz(2));
    fprintf(fid,'boundary z %d;\n',kl_boundary_xyz(3));    
    fprintf(fid,'scale x %.2f;\n',kl_scale_xyz(1));
    fprintf(fid,'scale y %.2f;\n',kl_scale_xyz(2));
    fprintf(fid,'scale z %.2f;\n',kl_scale_xyz(3));
    
    fprintf(fid,'magnification %d;\n',kl_mag);
    
    
    fclose(fid);




end