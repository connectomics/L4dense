function cutpattern = makeCutPattern()
cutpattern{1} = false(35, 24, 3424);
cutpattern{1}([12, 24], :, :) = true;
cutpattern{2} = false(36, 23, 3424);
cutpattern{2}(:,[8, 16], :) = true;
cutpattern{3} = true(36, 24, 3423);