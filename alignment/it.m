function y=it(x)
    function yy=aP(a)
        yy=1;
        for ii=1:size(a,2)
            yy=yy*a(ii);
        end
    end
xdiff=(x(:,2)-x(:,1)+1)';
n=size(xdiff,2);
prod=aP(xdiff);
y=zeros(prod,n);
for i=1:prod
    for j=1:n
        p=aP(xdiff(1:j-1)); 
        y(i,j)=mod(div(i-1,p),xdiff(j))+x(j,1);
    end
end
y=y';
end