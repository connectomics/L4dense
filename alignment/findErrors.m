function problems_save = findErrors(ttemp, ignoreCuts, surround, N)
problem = rescueProblems(ttemp);

if ignoreCuts
    cutpattern = makeCutPattern();
    for dimIt = 1 : 3
        problem{dimIt} = problem{dimIt} .* cutpattern{dimIt};
    end
end

problems_save = findMaximumProblem(problem, surround, N);

