function svmArchEnn2Codat( folder, forceOverwrite )
%SVMARCHENN2CODAT Creates the corresponding codat cnn object from the enn
% parameters.
% INPUT folder: string
%           Path to folder containing the enn save folder. It is assumed
%           that each folder contains the parameters of the enn cnets
%           stored in matfiles (see
%           python.synem.synconn_to_codat.get_params_multi).
%       forceOverwrite: (Optional) logical
%           Force overwrite of the cnet variable in the corresponding
%           matfile.
%           (Default: false)
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('forceOverwrite', 'var') || isempty(forceOverwrite)
    forceOverwrite = false;
end

% get all subdirectories of folder
subdirs = dir(folder);
subdirs = subdirs([subdirs.isdir]);
subdirs = subdirs(3:end);
subdirs = {subdirs.name};
subdirs = subdirs(:);
subdirsPath = cellfun(@(x)fullfile(folder, x), subdirs, 'uni', 0);

% for each subdirectly use name to create cnn object
for i = 1:length(subdirs)
    % get arch name from folder name (very hacky)
    arch = strsplit(subdirs{i}, '_');
    if length(arch) > 2 && strcmp(arch{3}, 'rec')
        % recursive cnets of the form svm_2a_rec
        arch = strjoin(arch(1:3), '_');
    elseif length(arch) > 3 && strcmp(arch{3}, 'rec') ...
            && strcmp(arch{4}(1:4), 'cont')
        % recursive cnets of the form svm_2a_rec_cont2
        arch = strjoin(arch(1:3), '_');
    else
        % non-recursive cnets of the form svm_2a or svm_2a_cont2
        arch = strjoin(arch(1:2), '_');
    end
    
    % get weights
    outfile = fullfile(subdirsPath{i}, [subdirs{i} '.mat']);

    try
        % create codat object and set the weights
        m = load(outfile);
        if ~isfield(m, 'cnet') || forceOverwrite
            cnet = Codat.CNN.Misc.setUpCNN(arch);
            cnet = Codat.CNN.Misc.setElektroNNParams(cnet, m.w, m.b); %#ok<NASGU>

            % store codat object
            Util.log('Adding Codat CNN to %s.', outfile);
            save(outfile, 'cnet', '-append');
            else
        end
    catch err
        warning('An error occured: %s', err.message);
        Util.log('Skipping file %s.', outfile);
    end
    
    clear cnet m % just to make sure
end

end

