function idx = postFeatIdx( obj )
%PREFEATIDX Logical indices of the features in the second (postsynaptic)
% segment.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

names = obj.createNameStrings();
if ~isempty(obj.selectedFeat)
    names = names(cell2mat(cellfun( ...
            @(x)x(:),obj.selectedFeat, 'UniformOutput', false)));
end
idx = regexp(names, 'v\ds2');
idx = ~cellfun(@isempty, idx);

end

