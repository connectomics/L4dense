function useDevice( fm, dev )
%USEDEVICE Switch device for computation.
% INPUT dev: string
%           'cpu' or 'gpu'
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~any(ismember({'cpu', 'gpu'}, dev))
    error('Device must be ''cpu'' or ''gpu''.');
end
fm.device = dev;
fm.featTexture = cellfun(@(x)x.useDevice(dev), fm.featTexture, 'uni', 0);

end

