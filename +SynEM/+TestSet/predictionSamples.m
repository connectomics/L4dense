function [ C, fpComs, fnComs, tpComs ] = predictionSamples( scores, ...
    useBboxSmall, excOnly, scoreT, dataFolder, mode)
%PREDICTIONSAMPLES Get the predictions samples for interface classification
% on the test set.
% INPUT scores: [Nx2] or [Nx1] double
%           The scores for the interfaces in the test set.
%       useBboxSmall: (Optional) logical
%           Restrict to interfaces with a center of mass at least 160 nm
%           away from the test set boundary.
%           see also SynEM.TestSet.loadInterfaceGT
%           (Default: false)
%       excOnly: (Optional) logical
%           Only return excitatory synapses. Inhibitory borders are deleted
%           from all outputs.
%           see also SynEM.TestSet.loadInterfaceGT
%           (Default: false)
%       scoreT: (Optional) double
%           Threshold for synapse detection. Synapses with score > scoreT
%           are classified as synaptic.
%           (Default: threshold at optimal f1 score)
%       dataFolder: (Optional) string
%           Path to SynEM data folder.
%           (Default: default paths for M01300).
%       modes: (Optional) string
%           Mode to determine scoreT if required.
%           'F1', 'OptDist'
%           (Default: 'F1')
% OUTPUT C: [2x2] cell
%           Confusion matrix containing the interface indices for the
%           corresponding confusion matrix entries.
%           see also Util.confusionMatrix
%        fpComs: [Nx3] int
%           False positive detection centroids.
%        fnComs: [Nx3] int
%           False negative detection centroids.
%        tpComs: [Nx3] int
%           True positive detection centroids.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('useBboxSmall', 'var') || isempty(useBboxSmall)
    useBboxSmall = false;
end
if ~exist('excOnly', 'var') || isempty(excOnly)
    excOnly = false;
end

if ~exist('dataFolder', 'var') || isempty(dataFolder)
    if ispc
        dataFolder = 'E:\workspace\data\backed\Synapse detection\data\';
    else
        dataFolder = ['/media/benedikt/DATA2/workspace/data/backed/' ...
            'Synapse detection/'];
    end
    if ~exist(dataFolder, 'dir')
        error('Data folder %s does not exist', dataFolder);
    end
end

if ~exist('mode', 'var') || isempty(mode)
    mode = 'F1';
end

testSet = SynEM.TestSet.loadInterfaceGT(dataFolder, useBboxSmall, excOnly);

if size(scores, 1) == length(testSet.toKeep)
    scores = scores(testSet.toKeep, :);
end

if ~exist('scoreT', 'var') || isempty(scoreT)
    [rp, t] = SynEM.Eval.interfaceRP(testSet.group, max(scores, [], 2));
    switch mode
        case 'F1'
            [~, idx] = max(harmmean(rp, 2)); % maximal f1 score
        case 'OptDist'
            [~, idx] = min(pdist2(rp, [1, 1])); % min dist to [1, 1]
        otherwise
            error('Unknown mode %s.', mode)
    end
    scoreT = t(idx);
end

pred = any(scores > scoreT, 2);

[~, ~, ~, ~, ~, ~, C] = SynEM.Util.confusionMatrix(testSet.group, pred);

% fp/fn coms
if nargout > 1
    fpComs = testSet.coms(C{2,1}, :);
    fnComs = testSet.coms(C{1,2}, :);
end

% tp coms
if nargout > 3
    tpComs = testSet.coms(C{1,1}, :);
end
    

end

