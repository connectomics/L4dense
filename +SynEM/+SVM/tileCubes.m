function tileCubes( cubeFolder, outputFolder, ...
    targetSize, borderSize )
%TILECUBES Tile cubes to make files smaller.
% INPUT cubeFolder: string
%           Path to SVM cube files.
%       outputFolder: string
%           Path where the output cubes are saved.
%       targetSize: [3x1] int
%           Size of the label cubes.
%           (see also targetSize input in Codat.CNN.Misc.tileTrainingCubes)
%       borderSize: [3x1] int
%           Border around each label cube.
%           (see also border input in Codat.CNN.Misc.tileTrainingCubes)
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

s = what(cubeFolder);
files = cellfun(@(x)fullfile(cubeFolder, x), s.mat, 'uni', 0);

count = 1;
for i = 1:length(files)
    m = load(files{i});
    [x, y] = Codat.CNN.Misc.tileTrainingCubes(m.raw, m.seg, ...
        targetSize, borderSize);
    for j = 1:length(x)
        raw = x{i};
        seg = y{i};
        save(fullfile(outputFolder, sprintf('SVM_%d.mat', count)), ...
            'raw', 'seg');
        count = count + 1;
    end
end

end

