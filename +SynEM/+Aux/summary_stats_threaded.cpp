/* summary_stats_threaded.cpp
* Written by
* Benedikt Staffler <benedikt.staffler@brain.mpg.de>
*/

#include "mex.h"
#include "matrix.h"

#include <cstring>
#include <string>
#include <algorithm>
#ifdef _WIN32
#include <ppl.h>
#endif
#include <cmath>
#include <omp.h>

#ifdef _WIN32
using namespace concurrency;
#endif

template<typename T>
void print(T toPrint)
{
	std::string str = std::to_string(toPrint);
	mexPrintf(str.c_str());
	mexPrintf("\n");
}

int numel(const mxArray* a)
{
    mwSize ndim = mxGetNumberOfDimensions(a);
    const mwSize * dims = mxGetDimensions(a);
	int N = dims[0];
	for (int i = 1; i < ndim; i++)
	{
		N = N * dims[i];
	}
    return N;
}

template<typename T>
void quantiles(T* data, T* out_data, int l)
{
	std::sort(data, data + l);
	T q[5] = {0, 0.25, 0.5, 0.75, 1};
    out_data[0] = data[0];
	for (int i = 1; i < 5; i++)
	{
		out_data[i] = data[(int)round(q[i]*(T)(l - 1))];
	}
}

template<typename T, unsigned N>
T powerSum(T* data, int l)
{
    T s = 0;
    if (N == 1)
    {
        for (int i = 0; i < l; i++)
        {
            s += data[i];
        }
    }
    else
    {
        for (int i = 0; i < l; i++)
        {
            s += pow(data[i], N);
        }
    }
    return s;
}


template<typename T>
T mean(T* data, int l)
{
    return (powerSum<T,1>(data, l) / (T)l);
}

template<typename T>
void pointwise_subtract(T* data, T m, int l)
{
    for (int i = 0; i < l; i++)
    {
        data[i] -= m;
    }
}

template<typename T, unsigned N>
void pointwise_power(T* data, int l)
{
    for (int i = 0; i < l; i++)
    {
        data[i] = pow(data[i], N);
    }
}

template<typename T>
void copy_array(T* data, T* copy, int l)
{
    for (int i = 0; i < l; i++)
    {
        copy[i] = data[i];
    }
}

template<typename T>
T sum_pointwise_product(T* x1, T* x2, int l)
{
    T s = 0;
    for (int i = 0; i < l; i++)
    {
        s += x1[i] * x2[i];
    }
    return s;
}


template<typename T>
void moments(T* data, T* out_data, int l)
{
    out_data[0] = mean(data, l);
    pointwise_subtract(data, out_data[0], l);
    T* data_sq = new T[l];
    copy_array(data, data_sq, l);
    pointwise_power<T, 2>(data_sq, l);
    out_data[1] = powerSum<T, 1>(data_sq, l);
    T s2 = out_data[1] / l;
    out_data[1] = out_data[1] / (l - 1);
    out_data[2] = sum_pointwise_product(data, data_sq, l) / l / pow(s2, (T)1.5);
    out_data[3] = sum_pointwise_product(data_sq, data_sq, l) / l / pow(s2, (T)2);
    delete data_sq;
}


template<typename T>
void calcSumStatsForCell(mxArray* cur_cell, T* cur_out_data_pos)
{
    T* data = (T*) mxGetData(cur_cell);
    int N = numel(cur_cell);
    quantiles(data, cur_out_data_pos, N);
    moments(data, cur_out_data_pos + 5, N);
}


void mexFunction(
	int nlhs, mxArray * plhs[],
	int nrhs, const mxArray * prhs[])
{
	if (mxGetClassID(prhs[0]) != mxCELL_CLASS)
	{
		mexErrMsgTxt("First input must be a cell array.");
	}

	int N = numel(prhs[0]);
    int n = 9;
    
    const mwSize * dims = mxGetDimensions(prhs[0]);

    if (mxIsSingle(mxGetCell(prhs[0], 1)))
    {
        // allocate output
        plhs[0] = mxCreateNumericMatrix(dims[0]*n, dims[1], mxSINGLE_CLASS, mxREAL);
        float* out_data = (float*) mxGetData(plhs[0]);
        
//         for (int i = 0; i < N; i++)
//         {
//             calcSumStatsForCell(mxGetCell(prhs[0], i), out_data + i*n);
//         }
        
        #ifdef __unix__
        //openmp
        #pragma omp parallel for
        for (int i = 0; i < N; i++)
        {
            calcSumStatsForCell(mxGetCell(prhs[0], i), out_data + i*n);
        }
        #endif

        #ifdef _WIN32
        //ppl
        parallel_for(0, N, [&](int i)
        {
            calcSumStatsForCell(mxGetCell(prhs[0], i), out_data + i*n);
        });
        #endif
        
    }
    else if (mxIsDouble(mxGetCell(prhs[0], 1)))
    {
        // allocate output
        plhs[0] = mxCreateNumericMatrix(dims[0]*n, dims[1], mxDOUBLE_CLASS, mxREAL);
        double* out_data = (double*) mxGetData(plhs[0]);
       
        #ifdef __unix__
        //openmp
        int np = 0;
        mexPrintf("Using openmp.\n");
        #pragma omp parallel for
        for (int i = 0; i < N; i++)
        {
            calcSumStatsForCell(mxGetCell(prhs[0], i), out_data + i*n);
            np = omp_get_num_threads();
        }
        mexPrintf("\nUsing %d threads.", np);
        #endif
        

        #ifdef _WIN32
        //ppl
        mexPrintf("Using ppl.\n");
        parallel_for(0, N, [&](int i)
        {
            calcSumStatsForCell(mxGetCell(prhs[0], i), out_data + i*n);
        });
        #endif
    
    }
    else
    {
        mexErrMsgTxt("Array must be single or double");
    }
}