function skel = confMatToWK(scores, t, labels, coms, skel, varargin)
%CONFMATTOWK Creates a skeleton with the samples from the confusion matrix
% (TP, FP, FN) for inspection in webknossos.
%
% INPUT scores: [Nx1] or [Nx2] double
%           Predicted score for each synapse.
%           If scores has two columns, then the max(scores, [], 2) is used
%           which assumes that rows correspond to the two directions of
%           SynEM interfaces.
%       t: double
%           Threshold for prediction.
%       labels: [Nx1] int or logical
%           Synapses labels which can be binary or integers to group
%           several interfaces. For each group it is enough to find one
%           member to consider the whole group found.
%           (see also SynEM.Util.confusionMatrix).
%       coms: [Nx3] double
%           Center of mass for each interface (i.e. each row in scores and
%           labels).
%       skel: skeleton object or string
%           Skeleton object containing the correct webknossos dataset
%           metadata or name of the dataset according to
%           Skeleton.setParams4Dataset.
%       varargin: name-value pairs for additional options
%           'p_group': Name of a parent group for the confusion matrix
%               sampels. (Default: root group)
%           'no_descr': Flag to suppress the default description that is
%               added to the skeleton.
%
% OUTPUT skel: skeleton object
%           A skeleton with three groups containing the TP, FP and FN
%           detections.
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

opts.p_group = NaN;
opts.no_descr = false;

if ~isempty(varargin)
    uopts = cell2struct(varargin(2:2:end), varargin(1:2:end), 2);
    opts = Util.setUserOptions(opts, uopts);
end

if size(scores, 2) == 2
    scores = max(scores, [], 2);
end

[~, p, r, ~, ~, ~, CI] = SynEM.Util.confusionMatrix(...
    labels , scores > t, scores);

if ischar(skel)
    skel = Skeleton.setParams4Dataset([], skel);
end

g_name = {'TP', 'FP', 'FN'};
colors = {[0 1 0 1]; [1 0 0 1]; [0 0 1 1]};
p_group = opts.p_group;

% confusion matrix samples to skeleton each within a separate group
for i = 1:3
    idx = skel.numTrees() + 1;
    skel = skel.addNodesAsTrees(round(coms(CI{i}, :)), ...
        arrayfun(@(x, y)sprintf('%s_%d_score_%.2f', g_name{i}, x, scores(y)), ...
        1:length(CI{i}), CI{i}(:)', 'uni', 0));
    skel.colors(idx:end) = colors(i);
    [skel, gid] = skel.addGroup(g_name{i}, p_group);
    skel = skel.addTreesToGroup(idx:skel.numTrees(), gid);
end

if ~opts.no_descr
    skel = skel.setDescription(sprintf(['Confusion matrix entries for ' ...
        'a threshold of %.3f corresponding to ' ...
        '%.3f recall and %.3f precision.'], ...
        t, p, r));
end

end

