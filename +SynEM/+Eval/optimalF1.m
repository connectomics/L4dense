function [f1, idx, opt_t] = optimalF1(rp, t)
%OPTIMALF1 Determine the optimal (= highest) F1 score for a PR curve.
% INPUT rp: [Nx2] double
%           Recall-precision value pairs for different thresholds.
%       t: (Optional) [Nx1] double
%           Decision thresholds for the corresponding rows in rp.
%           If this input is passed to the function, the third output is
%           available.
% OUTPUT f1: double
%           Optimal F1 score
%        idx: int
%           Linear index of the row in rp with the optimal F1 score.
%        opt_t: double
%           Threshold to achieve the optimal F1 score. Only available if t
%           was passed to the function as input argument.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>


f1 = harmmean(rp, 2);
[f1, idx] = max(f1);

if exist('t', 'var') && ~isempty(t)
    opt_t = t(idx);
else
    opt_t = [];
end

end

