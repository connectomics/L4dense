function auc = auc( c )
%AUC Area under curve.
% INPUT c: [Nx2] double or [Nx1] cell of [Nx2] double
%           Pairs of x-y points sorted by decreasing x-value.
% OUTPUT auc: [Nx1] double
%           Area under curve for each input cell.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

auc = cellfun(@(x)abs(trapz(x(end:-1:1,1), x(end:-1:1,2))), c);
end

