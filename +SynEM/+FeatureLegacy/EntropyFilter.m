classdef EntropyFilter < SynEM.Feature.TextureFeature
    %ENTROPYFILTER Local entropy filter.
    % PROPERTIES
    % nhood: [Nx1] array of integer specifying the size of the filter in
    %   each dimension.
    % sigma: (Optional) [Nx1] array of float specifying the standard
    %   deviation in each dimension for prior smoothing
    %   (Default: no prior smoothing)
    % f_norm: (Optional) Function handle (must be convertible to string)
    %   that is applied to the raw data before converting it to uint8.
    %   (Default: false)
    %
    % NOTE raw is cast to uint8 before calculating entropyfilt which might make
    %      the normalization important.
    %
    % Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>
    
    properties
        nhood
        sigma = [];
        f_norm = [];
    end
    
    methods
        function obj = EntropyFilter(nhood, sigma, f_norm)
            obj.name = 'Entropy';
            obj.nhood = nhood;
            if exist('sigma','var') && ~isempty(sigma)
                obj.sigma = sigma;
                obj.border = (nhood - 1) + 2.*ceil(3.*sigma);
            else
                obj.border = (nhood - 1);
            end
            if exist('f_norm','var') && ~isempty(f_norm)
                if ischar(f_norm)
                    obj.f_norm = f_norm;
                else
                    obj.f_norm = func2str(f_norm);
                end
            end
            obj.numChannels = 1;
            
        end
        
        function feat = calc(obj, raw)
            feat = obj.calculate(raw, obj.nhood, obj.sigma, obj.f_norm);
        end
    end
    
    methods (Static)
        function feat = calculate(raw, nhood, sigma, f_norm)
            if ~isempty(sigma)
                raw = SynEM.Feature.GaussFilter.calculate(raw, sigma, 3,...
                    0, 'same');
            end
            if ~isempty(f_norm)
                if ischar(f_norm)
                    f_norm = str2func(f_norm);
                end
                raw = f_norm(raw);
            end
            feat = cast(entropyfilt(uint8(raw),ones(nhood)),'like',raw);
        end
    end
    
end

