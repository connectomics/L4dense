% script for preprocessing of CNN features for synem training (on gaba)
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

% load cnet
m = load('/gaba/u/bstaffle/data/SyConn/cnn17_2_enn.mat', 'cnet');
cnet = m.cnet;

% create output dataset
outFolder = ['/tmpscratch/bstaffle/data/2012-09-28_ex145_07x2_ROI2017/' ...
    'SVM/SynEM_train_cnn17_2'];
mkdir(outFolder);
wkwInit('new', outFolder, 32, 32, 'single', cnet.featureMaps(end));
datOut = Datasets.WkDataset(outFolder);

% run prediction
jobs = SynEM.Training.precalculateCNNFeatureOnGaba(cnet, datOut);