function jobs = calculateFeaturesForTestData(runFolder, pSegNewBig, ...
    pROI2017, fm, cluster)
%CALCULATEFEATURESFORTESTDATA Calculate the specified feature map for the
% test cubes.
% INPUT runFolder: string
%           Path to training pipeline run folder. The test cubes will be
%           stored in the subfolder 'test'.
%       pSegNewBig: struct
%           Parameter struct for the ex145_07x2_segNewBig pipeline run.
%       pROI2017: struct
%           arameter struct for the ex145_07x2_ROI2017 pipeline run.
%       fm: SynEM.FeatureMap object
%           The feature map to calculate.
%       cluster: (Optional) parallel.cluster object
%           The cluster object for job submission.
%           (Default: getCluster('cpu') or calculation is done in current
%           session if the former does not return a cluster)
% OUTPUT job: job object
%           Job object for feature calculation on workers.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

outfolder = fullfile(runFolder, 'test');
if ~exist(outfolder, 'dir')
    mkdir(outfolder);
end

if ~exist('cluster', 'var')
    try
        cluster = getCluster('cpu');
    catch
        cluster = [];
        Util.log('Cluster is not available. Calculating features locally.');
    end
end

segNewBigCubes = 67;
roi2017Cubes = [1082, 1579];

jobs = cell(0, 1);
for i = 1:length(segNewBigCubes)
    inputCell = {outfolder, pSegNewBig, segNewBigCubes(i), fm};
    if ~isempty(cluster)
        jobs{end+1} = startJob(cluster, @jobWrapper, inputCell, 0); %#ok<AGROW>
    else
        jobWrapper(inputCell{:});
        jobs = [];
    end
end
for i = 1:length(roi2017Cubes)
    inputCell = {outfolder, pROI2017, roi2017Cubes(i), fm};
    if ~isempty(cluster)
        jobs{end+1} = startJob(cluster, @jobWrapper, inputCell, 0); %#ok<AGROW>
    else
        jobWrapper(inputCell{:});
    end
end
jobs = vertcat(jobs{:});

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function jobWrapper(saveFolder, p, cubeNo, fm)

% add bbox to LoadWKData features
idx = find(cellfun(@(x)isa(x, 'SynEM.Feature.LoadWkData'), ...
    fm.featTexture));
for i = 1:length(idx)
    fm.featTexture{idx(i)}.bbox = p.local(cubeNo).bboxSmall;
end

X = SynEM.Seg.cubeFeatures(p, cubeNo, fm); %#ok<NASGU>

if ~isempty(strfind(p.saveFolder, '20170217_ROI'))
    prefix = 'ex145_ROI2017';
else
    prefix = 'ex145_segNewBig';
end
outFile = fullfile(saveFolder, sprintf('%s_Cube%d.mat', prefix, cubeNo));
Util.log('Saving result to %s.', outFile);
save(outFile, 'X');
end
