% run config using the svm predictions and all paper features
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

% data and save folder
dataFolder = ['/gaba/u/bstaffle/data/SynEM/' ...
    'SynapseDetectionTrainingData_2_typeLabels'];
saveFolder = ['/tmpscratch/bstaffle/data/SynEM/Classifier/' ...
    'runShaft/cnn17_synem_trShaft'];

% feature map
m = load('/gaba/u/bstaffle/data/SyConn/cnn17_enn.mat', 'cnet');
cnet = m.cnet;
fm = SynEM.getFeatureMap('PaperAndCNN', cnet);


% training options
options.useGPU = true;
options.shaftSynapseTraining = true;
options.overwrite = true;

% cluster
cluster = getCluster('gpu', [], '-l h=gaba00[0-7]');

% start training
SynEM.Training.trainingPipeline(dataFolder, saveFolder, fm, [], [], ...
    options, cluster);
