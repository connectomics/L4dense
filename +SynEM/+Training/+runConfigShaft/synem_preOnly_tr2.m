% run config for synem using all training data and only the presynaptic
% process
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

% data and save folder
dataFolder = ['/gaba/u/bstaffle/data/SynEM/' ...
    'SynapseDetectionTrainingData_2_typeLabels'];
saveFolder = ['/tmpscratch/bstaffle/data/SynEM/Classifier/' ...
    'runShaft/synem_preOnly_tr2'];

% feature map
fm = SynEM.getFeatureMap('paper_opt');
fm.setPreOnly(true);

% training options
options.shaftSynapseTraining = false;
options.trainPredClassifier = false;

% cluster
cluster = getCluster('cpu_prio');

% start training
SynEM.Training.trainingPipeline(dataFolder, saveFolder, fm, [], [], ...
    options, cluster),
