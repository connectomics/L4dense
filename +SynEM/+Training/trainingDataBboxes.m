function [ bboxes_raw, bboxes_target ] = trainingDataBboxes( filePaths, toMat )
%TRAININGDATABBOXES Get the bounding boxes for the interface classifier
%training data.
% INPUT filePaths: string or [Nx1] cell
%           Path to the folder containing the training files or cell array
%           with path to the indiviual files.
%       toMat: (Optional) logical
%           Flag indicating that the output should be an [Nx6] array
%           containing the linearized bounding boxes in each row.
% OUTPUT bboxes_raw: [Nx1] cell
%           Cell array of bounding boxes for the raw data (bboxBig).
%        bboxes_targets: [Nx1] cell
%           Cell array of bounding boxes for the segmentation data
%           (bboxSmall).
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('toMat', 'var') || isempty(toMat)
    toMat = true;
end

if ischar(filePaths)
    s = what(filePaths);
    filePaths = cellfun(@(x)fullfile(filePaths, x), s.mat, 'uni', 0);
end

bboxes_raw = cell(length(filePaths), 1);
bboxes_target = cell(length(filePaths), 1);
for i = 1:length(filePaths)
    m = load(filePaths{i}, 'metadata');
    bboxes_raw{i} = m.metadata.bboxBig;
    bboxes_target{i} = m.metadata.bboxSmall;
end

if toMat
    bboxes_raw = cell2mat(cellfun(@(x)x(:)', bboxes_raw, 'uni', 0));
    bboxes_target = cell2mat(cellfun(@(x)x(:)', bboxes_target, 'uni', 0));
end

end

