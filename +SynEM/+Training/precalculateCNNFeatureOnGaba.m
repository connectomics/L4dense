function jobs = precalculateCNNFeatureOnGaba( cnet, datOut, ...
    options, border, cluster )
%PRECALCULATECNNFEATUREONGABA Precalculate CNN features for interface
%classification.
% INPUT cnet: Codat.cnn object
%           The cnet for prediction.
%       datOut: Datsets.WkDataset or Datasets.McWkwDataset
%           The dataset for saving the output.
%       options: (Optional) struct
%           Prediction options (see defaultOptions function below)
%       border: (Optional) [1x3] int
%           The border around the training regions to predict in addition
%           (note that predictions will automatically be aligned to [32 32
%           32] cubes).
%       cluster: (Optional) parallel.cluster
%           Cluster object for job submission.
%           (Default: getCluster('gpu'))
%
% NOTE Predictions will all be written to the same dataset now which is
%      possible because the training/test region bounding boxes do not
%      overlap. Use e.g.
%      ov = Interfaces.Calculation.bboxOverlap(bboxes_train, ...
%           bboxes_train_ROI2017);
%      to check this.
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

info = Util.runInfo(false); %#ok<NASGU>

if ~exist('border', 'var') || isempty(border)
    border = [0, 0, 0];
end

if ~exist('cluster', 'var') || isempty(cluster)
    cluster = getCluster('gpu');
end

defOpts = defaultOptions(cnet.layer);
if exist('options', 'var') && ~isempty(options)
    options = Util.setUserOptions(defOpts, options);
else
    options = defOpts;
end

% check correct channels
if isempty(options.fmL_idx)
    assert(datOut.numChannels == cnet.featureMaps(options.fmL), ...
        'Wrong number of feature maps in output dataset.');
else
    assert(datOut.numChannels == length(options.fmL_idx), ...
        'Wrong number of feature maps in output dataset.');
end

% print options
Util.log('CNN prediction settings are:');
disp(options);

% training region bboxes
bboxes_train = SynEM.Training.trainingDataBboxes(['/gaba/u/bstaffle/' ...
    'data/SynEM/SynapseDetectionTrainingDataRevised']);

% test region
p = Gaba.getSegParameters('ex145_segNewBig');
bboxes_train(end+1,:) = p.local(67).bboxSmall(:)';

% input dataset (training data revised and test)
datIn = Datasets.WkDataset(p.raw);

% get ROI2017 training & test regions
p = Gaba.getSegParameters('ex145_ROI2017');
datIn2017 = Datasets.WkDataset(p.raw);
bboxes_train_ROI2017 = SynEM.Training.trainingDataBboxes( ...
    ['/gaba/u/bstaffle/data/datasets/SynEM/Interfaces/' ...
    'SynapseDetectionTrainingData_ROI2017']);
bboxes_train_ROI2017 = cat(1, bboxes_train_ROI2017, ...
    p.local(1082).bboxSmall(:)', p.local(1579).bboxSmall(:)');

bboxes_train_all = cat(1, bboxes_train, bboxes_train_ROI2017);

% enlarge by border
bboxes_train_large = Util.addBorder(bboxes_train_all, border);
bboxes_train_large = num2cell(bboxes_train_large, 2);
bboxes_train_large = cellfun(@(x)reshape(x, 3, 2), bboxes_train_large, ...
    'uni', 0);
bboxes_train_large = cellfun( ...
    @(x)Util.alignBboxWithKnossosCubes(x, options.alignToCubesize), ...
    bboxes_train_large, 'uni', 0);
options = rmfield(options, 'alignToCubesize');
bboxes_train_large_2017 = bboxes_train_large(size(bboxes_train, 1) + 1 :end);
bboxes_train_large = bboxes_train_large(1:size(bboxes_train, 1));

% check for overlap between bboxes from different alignment
ov = Interfaces.Calculation.bboxOverlap( ...
    cell2mat(cellfun(@(x)x(:)', bboxes_train_large, 'uni', 0)), ...
    cell2mat(cellfun(@(x)x(:)', bboxes_train_large_2017, 'uni', 0)));
if any(~cellfun(@isempty, ov))
    warning('Bounding boxes from the different alignments are overlapping.');
end

% store parameters
save(fullfile(datOut.root, 'params.mat'), 'border', 'cnet', ...
    'bboxes_train_large', 'info', 'options');

% submit jobs
Util.log('Submitting %d jobs to cluster.', length(bboxes_train_large));
jobs = cellfun(@(x)Codat.CNN.Cluster.predictDataset(cnet, x, datIn, ...
    datOut, options, cluster), bboxes_train_large, 'uni', 0);
jobs_ROI2017 = cellfun(@(x)Codat.CNN.Cluster.predictDataset(cnet, x, ...
    datIn2017, datOut, options, cluster), ...
    bboxes_train_large_2017, 'uni', 0);
jobs = vertcat(jobs{:}, jobs_ROI2017{:});

end

function defOpts = defaultOptions(numLayer)
defOpts.convAlg = 'fft2';
defOpts.target_size_job = [256 256 256];
defOpts.target_size = [64 64 64];
defOpts.fmL = numLayer;
defOpts.fmL_idx = [];
defOpts.gpuDev = true;
defOpts.f_norm = func2str(@(x)single(x)./255);
% align to cubes of this size to avoid write collision bugs
% should be chosen as the cubesize for wkcube and the blockLen for wkwrap
defOpts.alignToCubesize = [32, 32, 32];
end
