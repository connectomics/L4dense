% convert the interface training cubes to nml
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

% original synem cubes & unused ones
tr_folder = 'E:\workspace\data\backed\SynapseDetectionTrainingDataRevised\';
outFolder = 'E:\workspace\tmp\IntTrainingData\';
skel = trainingDataToNmlWrapper(tr_folder, outFolder);

% roi2017 cubes
tr_folder = 'E:\workspace\data\backed\SynapseDetectionTrainingDataROI2017\';
outFolder = 'E:\workspace\tmp\IntTrainingData\';
dset = 'ex145_ROI2017';
skel_roi2017 = trainingDataToNmlWrapper(tr_folder, outFolder, ...
    'new_cubes', dset);
