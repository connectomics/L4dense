% script to add the type annotations to the training cubes
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

info = Util.runInfo(false);
outFolder = 'E:\workspace\sync\SynapseDetectionTrainingDataTypeLabels\';
overwrite = true;


%% paper cubes train

cubeFolder = 'E:\workspace\data\backed\SynapseDetectionTrainingDataRevised\';
nmlFolder = 'E:\workspace\data\backed\SynapseDetectionTrainingDataRevised\typeAnnotations\train\';
s = what(cubeFolder);
nmlFiles = dir([nmlFolder '*.nml']);
nmlFiles = {nmlFiles.name}';
wasLoaded = false(length(nmlFiles), 1);

matfiles = cellfun(@(x)fullfile(cubeFolder, x), s.mat, 'uni', 0);
count = 0;
for i = 1:length(matfiles)
    [~, mName, ~] = fileparts(matfiles{i});
    thisNmlIdx = find(~cellfun(@isempty, strfind(nmlFiles, mName)));
    if length(thisNmlIdx) == 1
        wasLoaded(thisNmlIdx) = true;
        thisFile = fullfile(nmlFolder, nmlFiles{thisNmlIdx});
        typeLabels = SynEM.TrainingData.parseTypeAnnotations(thisFile);
        typeLabels.info = info;
        m = load(matfiles{i});
        assert(all(m.interfaceLabels(typeLabels.idx) < 3 & ...
            setdiff(m.interfaceLabels, m.interfaceLabels(typeLabels.idx)) == 3), ...
            sprintf('Something went wrong for cube %s', matfiles{i}));
%         m.typeLabels = typeLabels;
%         outFile = fullfile(outFolder, [mName '.mat']);
        outFile = matfiles{i};
        if exist(outFile, 'file') && ~overwrite
            warning(['File %s already exists and will not be ' ...
                'overwritten.'], outFile);
        else
%             Util.saveStruct(outFile, m);
            save(outFile, 'typeLabels', '-append');
        end
        count = count + 1;
    elseif length(thisNmlIdx) > 1
        error('Cube %s was associated to multiple nml files', mName);
    end
end

Util.log('Loaded type annotations from %d nml files.', count);


%% paper cubes val

cubeFolder = 'E:\workspace\data\backed\SynapseDetectionTrainingDataRevised\';
nmlFolder = 'E:\workspace\data\backed\SynapseDetectionTrainingDataRevised\typeAnnotations\val\';
s = what(cubeFolder);
nmlFiles = dir([nmlFolder '*.nml']);
nmlFiles = {nmlFiles.name}';
wasLoaded = false(length(nmlFiles), 1);

matfiles = cellfun(@(x)fullfile(cubeFolder, x), s.mat, 'uni', 0);
count = 0;
for i = 1:length(matfiles)
    [~, mName, ~] = fileparts(matfiles{i});
    thisNmlIdx = find(~cellfun(@isempty, strfind(nmlFiles, mName)));
    if length(thisNmlIdx) == 1
        wasLoaded(thisNmlIdx) = true;
        thisFile = fullfile(nmlFolder, nmlFiles{thisNmlIdx});
        typeLabels = SynEM.TrainingData.parseTypeAnnotations(thisFile);
        m = load(matfiles{i});
        assert(all(m.interfaceLabels(typeLabels.idx) < 3 & ...
            setdiff(m.interfaceLabels, m.interfaceLabels(typeLabels.idx)) == 3), ...
            sprintf('Something went wrong for cube %s', matfiles{i}));
%         m.typeLabels = typeLabels;
%         outFile = fullfile(outFolder, [mName '.mat']);
        outFile = matfiles{i};
        if exist(outFile, 'file') && ~overwrite
            warning(['File %s already exists and will not be ' ...
                'overwritten.'], outFile);
        else
%             Util.saveStruct(outFile, m);
            save(outFile, 'typeLabels', '-append');
        end
        count = count + 1;
    elseif length(thisNmlIdx) > 1
        error('Cube %s was associated to multiple nml files', mName);
    end
end

Util.log('Loaded type annotations from %d nml files.', count);


%% ROI2017 cubes

cubeFolder = 'E:\workspace\data\backed\SynapseDetectionTrainingDataROI2017\';
nmlFolder = 'E:\workspace\data\backed\SynapseDetectionTrainingDataROI2017\typeAnnotations\';
s = what(cubeFolder);
nmlFiles = dir([nmlFolder '*.nml']);
nmlFiles = {nmlFiles.name}';
wasLoaded = false(length(nmlFiles), 1);

matfiles = cellfun(@(x)fullfile(cubeFolder, x), s.mat, 'uni', 0);
count = 0;
for i = 1:length(matfiles)
    [~, mName, ~] = fileparts(matfiles{i});
    thisNmlIdx = find(~cellfun(@isempty, strfind(nmlFiles, mName)));
    if length(thisNmlIdx) == 1
        wasLoaded(thisNmlIdx) = true;
        thisFile = fullfile(nmlFolder, nmlFiles{thisNmlIdx});
        typeLabels = SynEM.TrainingData.parseTypeAnnotations(thisFile);
        m = load(matfiles{i});
        assert(all(m.interfaceLabels(typeLabels.idx) < 3 & ...
            setdiff(m.interfaceLabels, m.interfaceLabels(typeLabels.idx)) == 3), ...
            sprintf('Something went wrong for cube %s', matfiles{i}));
%         m.typeLabels = typeLabels;
%         outFile = fullfile(outFolder, [mName '.mat']);
        outFile = matfiles{i};
        if exist(outFile, 'file') && ~overwrite
            warning(['File %s already exists and will not be ' ...
                'overwritten.'], outFile);
        else
%             Util.saveStruct(outFile, m);
            save(outFile, 'typeLabels', '-append');
        end
        count = count + 1;
    elseif length(thisNmlIdx) > 1
        error('Cube %s was associated to multiple nml files', mName);
    end
end

Util.log('Loaded type annotations from %d nml files.', count);

