% add type annotations to cubes and save them to new location
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

cubeFolder = '/gaba/u/bstaffle/data/SynEM/SynapseDetectionTrainingData_2/';
typeDataFolder = ['/gaba/u/bstaffle/code/pipeline/sync/' ...
    'SynapseDetectionTrainingDataTypeLabels/'];
outFolder = ['/gaba/u/bstaffle/data/SynEM/' ...
    'SynapseDetectionTrainingData_2_typeLabels/'];

s = what(cubeFolder);

for i = 1:length(s.mat)
    m = load(fullfile(cubeFolder, s.mat{i}));
    try
        m2 = load(fullfile(typeDataFolder, s.mat{i}));
        m.typeLabels = m2.typeLabels;
        outFile = fullfile(outFolder, s.mat{i});
        Util.saveStruct(outFile, m);
    catch err
        warning('An error occured for cube %s: %s', s.mat{i}, err.message);
    end
end
