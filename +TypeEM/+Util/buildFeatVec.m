function [featVec, featLabels] = buildFeatVec(feats, skipFeats)
    % BUILDFEATVEC Build feature vector from
    % a feature structre.
    %
    % feats
    %   feature structure
    % skipFeats
    %   optional list of features to skip
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    function name = toVecName(name, count)
        % to cell and
        % repeat as needed
        name = {name};
        name = repmat(name, count, 1);
        
        if count > 1
            number = arrayfun( ...
                @(x) ['(', num2str(x), ')'], (1:count)', ...
                'UniformOutput', false);
            name = cellfun( ...
                @(x, y) [x, y], name, number, ...
                'UniformOutput', false);
        end
    end
    
    if ~exist('skipFeats', 'var')
        skipFeats = {};
    end
    
    % convert to cell array
    featCells = struct2cell(feats);
    featLabels = fieldnames(feats);
    
    % skip labels
    skipMask = ismember(featLabels, skipFeats);
    featCells = featCells(~skipMask);
    featLabels = featLabels(~skipMask);
    
    % linearize each feature
    % results are column vectors
    featCells = cellfun( ...
        @(x) x(:), featCells, 'UniformOutput', false);
    
    featSizes = cellfun( ...
        @(x) {numel(x)}, featCells);
    featLabels = cellfun( ...
        @(x, y) {toVecName(x, y)}, featLabels, featSizes);
    
    % build a single column vector
    featVec = vertcat(featCells{:});
    featLabels = vertcat(featLabels{:});
    
    % to row vector
    featVec = featVec';
    featLabels = featLabels';
end

