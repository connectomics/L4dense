function feats = buildStats(inVal)
    inVal = inVal(:);
    inValNaNs = isnan(inVal);
    
    if any(inValNaNs)
        inVal = inVal(~inValNaNs);
        warning('Removed NaN values from input');
    end
    
    % moments (using SynEM's optimized routine)
    moments = TypeEM.SynEM.FeatureMap.moment(inVal, true(1, 4));
    
    feats = struct;
    feats.mean = moments(1);
    feats.var = moments(2);
    feats.skewnewss = moments(3);
    feats.kurtosis = moments(4);
    
    % quantiles
    inVal = sort(inVal);
    inValLen = numel(inVal);
    
    feats.min       = inVal(1);
    feats.prctile10 = inVal(1 + floor(0.1 * (inValLen - 1)));
    feats.prctile50 = inVal(1 + floor(0.5 * (inValLen - 1)));
    feats.prctile90 = inVal(1 + floor(0.9 * (inValLen - 1)));
    feats.max       = inVal(end);
end