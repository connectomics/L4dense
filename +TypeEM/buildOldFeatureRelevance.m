function buildOldFeatureRelevance(className)
    % At the time of writing, the new astrocyte classifier performs just as
    % good as the old version, which includes shape features. For this
    % reason, the following analysis is limited to axons and dendrites.
    cfDir = '/gaba/u/amotta/data/classifiers';
    cfFile = fullfile(cfDir, strcat(className, '.mat'));
    featFile = '/gaba/u/mberning/results/pipeline/20151111T183414/local/x0001y0001z0001/aggloFeats.mat';

    % load data
    classifier = matfile(cfFile);
    classifier = classifier.classifier;
    featRelevance = classifier.predictorImportance;
    clear classifier;
    
    featNames = matfile(featFile);
    featNames = featNames.featNames;
    
    % build table
    feats = table;
    feats.name = featNames(:);
    feats.relevance = featRelevance(:);
    feats.groupId = (1:height(feats))';
    feats.groupName = feats.name;
    
    % group
    feats = groupByNamePrefix(feats, 'texture');
    feats = groupByNamePrefix(feats, 'shape mask');
    feats = groupByNamePrefix(feats, 'shape isoSurf');
    feats = groupByNamePrefix(feats, 'shape conv');
    feats = groupByNamePrefix(feats, 'shape curvGauss');
    feats = groupByNamePrefix(feats, 'shape curvMean');
    feats = groupByNamePrefix(feats, 'shape dna');
    feats = groupByNamePrefix(feats, 'shape geoDist');
    feats = groupByNamePrefix(feats, 'shape thickDist');
    feats = groupByNamePrefix(feats, 'shape thickWeight');
    
    % calculate per-group relevance
    groups = table;
   [groups.id, uniRowOld, uniRowNew] = unique(feats.groupId);
    groups.name = feats.groupName(uniRowOld(:));
    groups.relevance = accumarray(uniRowNew, feats.relevance);
    
    % sort table
    groups = sortrows(groups, 'relevance', 'descend');
    
    % plot
    figure();
    bar(groups.relevance);
    
    % feature names
    xticklabels(groups.name);
    xtickangle(-45);
    
    % title
    title(['Feature Relevance for ', TypeEM.Util.toCamelCase(className), 's']);
end

function feats = groupByNamePrefix(feats, prefix)
    mask = strncmpi(feats.name, prefix, numel(prefix));
    feats.groupId(mask) = min(feats.groupId(mask));
    feats.groupName(mask) = repmat({prefix}, sum(mask), 1);
    
    % renumber groups
    [~, ~, feats.groupId] = unique(feats.groupId);
end