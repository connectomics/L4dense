function feat = loadFeatures(param, featSetName, segIds)
    % gt = loadFeatures(param, featFileName, segIds)
    %   Loads the features for the segments in a groundtruth.
    %
    % Input arguments
    %   param
    %     Parameter structure
    %   featSetName
    %     There may exist multiple feature sets (e.g., segment- and
    %     agglomerate-based ones). The feature values returned by this
    %     function will be loaded from `strcat(featSetName, '.mat')`.
    %   segIds
    %     List of segment IDs
    %
    % Output argument
    %   feat
    %     Feature structure with the following fields:
    %
    %     segIds
    %       Nx1 column vector with segment IDs
    %     featMat
    %       NxM matrix with feature values
    %     featNames
    %       1xM cell array with feature names
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    segIds = unique(segIds);
    
    closure = @(cubeIdx, segIds) ...
        loadFeaturesCube(param, featSetName, cubeIdx, segIds);
    feat = TypeEM.Util.forSegsByCube(param, segIds, closure);
    
    % build output features
    feat = Util.concatStructs(1, feat{:});
    feat.featNames = feat.featNames(1, :);
    
    if isfield(feat, 'gitHash')
        % within if for backward compatibility
        feat.gitHash = unique(feat.gitHash, 'rows');
    end
end

function feat = loadFeaturesCube(param, featSetName, cubeIdx, segIds)
    cubeRootDir = param.local(cubeIdx).saveFolder;
    
    featFile = strcat(featSetName, 'Features', '.mat');
    featFile = fullfile(cubeRootDir, featFile);
    feat = load(featFile);
    
    % find rows
    [found, rows] = ismember(segIds, feat.segId);
    rows = rows(found);
    
    % build output
    feat.segId = feat.segId(rows);
    feat.featMat = feat.featMat(rows, :);
end