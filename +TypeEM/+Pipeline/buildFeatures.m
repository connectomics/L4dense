function job = buildFeatures(param, classEmParam, fileName)
    % job = buildFeatures(param)
    %   Calculates and stores the segment features for each
    %   local segmentation cube.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % build job input arguments
    taskCount = numel(param.local);
    taskSeparateInputs = arrayfun(@(i) {{i}}, 1:taskCount);
    taskSharedInputs = {param, classEmParam, fileName};
    
    % configure cluster
    %
    % tested settings
    %   for single segments:
    %     successfully used < 18 GB + ~3 hours
    %     good evidence that < 12 GB + ~3 hours could work
    %   for five segment agglomerates
    %     ~42 GB + ~4:15 h
    cluster = Cluster.getCluster( ...
        '-l h_vmem=48G', ...
        '-l h_rt=24:00:00');
    
    % build and launch job
    job = Cluster.startJob( ...
        @main, taskSeparateInputs, ...
        'sharedInputs', taskSharedInputs, ...
        'cluster', cluster, ...
        'name', mfilename());
end

function main(param, classEmParam, fileName, cubeIdx)
    info = Util.runInfo();
    info.param = rmfield(info.param, 'param');
    
    param.classem = classEmParam;
    cubeParam = param.local(cubeIdx);
    rootDir = cubeParam.saveFolder;
    box = cubeParam.bboxSmall;
    
    % calculate features
    out = TypeEM.buildFeatures(param, box);
    out.gitHash = Util.getGitHash();
    out.info = info;
    
    % store result
    outFile = fullfile(rootDir, fileName);
    Util.saveStruct(outFile, out);
end