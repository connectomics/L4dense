function preds = buildPredictions(param, classifiers)
    % preds = buildPredictions(param, classifiers)
    %   Uses the given classifiers to build cube-wise predictions.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    if ~ischar(classifiers)
        error('Not implemented yet');
    end
    
    jobSharedInputs = {classifiers};
    jobInputs = arrayfun(@(c) {{c.saveFolder}}, param.local);
    
    job = Cluster.startJob( ...
        @main, jobInputs, ...
        'sharedInputs', jobSharedInputs, ...
        'name', mfilename, ...
        'numOutputs', 1);
    
    % wait until done
    wait(job);
    assert(all(cellfun(@isempty, {job.Tasks.Error})));
    
    % collect cube results
    data = fetchOutputs(job);
    dataConcat = @(data) cat(1, data{:});
    dataBuild = @(n) dataConcat(cellfun(@(d) {d.(n)}, data));
    
    % build output structure
    fieldNames = fieldnames(data{1});
    fieldVals = cellfun(dataBuild, fieldNames, 'UniformOutput', false);
    
    preds = cell2struct(fieldVals(:), fieldNames(:));
    preds.classes = preds.classes(1, :);
end

function preds = main(classifiersFile, cubeRootDir)
    % load classifiers
    classifiers = load(classifiersFile, '-mat');
    featureSetName = classifiers.featureSetName;
    
    % load features
    featFile = strcat(featureSetName, 'Features', '.mat');
    featFile = fullfile(cubeRootDir, featFile);
    feats = load(featFile);
    
    % HACK(amotta) This is a huge hack to get some first predictions going.
    % The problem is that the maskInertia features may (due to numerical
    % instability) contain complex numbers. Let's just take the real part
    % for now...
    nanMask = any(imag(feats.featMat) ~= 0, 2);
    feats.featMat = real(feats.featMat);
    
    % apply classifier
    preds = struct;
    preds.segId = feats.segId;
    preds.classes = classifiers.classes;
    preds.scores = TypeEM.Classifier.apply(classifiers, feats.featMat);
    preds.probs = TypeEM.Classifier.applyPlatt(classifiers, preds.scores);
    preds.probsMulti = bsxfun(@times, preds.probs, 1 ./ sum(preds.probs, 2));
    
    % HACK(amotta)
    preds.scores(nanMask, :) = nan;
    preds.probs(nanMask, :) = nan;
end