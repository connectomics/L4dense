function [aggloStruct, stopReason] = ...
        buildAgglomerates(adjMat, seedIds, acceptFunc, sampleFunc)
    % buildAgglomerates
    %   Build an agglomerate around a specified seed segment.
    %   This is intended to be used to grow out from a ground
    %   truth segment.
    %
    % adjMat: sparse, symmetric matrix
    %   Adjacency matrix as produced by the Graph.edges2Adj
    %   from the auxiliary methods.
    %
    % seedId: scalar
    %   Global segment id that serves as start point for the
    %   agglomeration process.
    %
    % acceptFunc: function handle
    %   Handle to a function that accepts the following three
    %   arguments: Current agglomeration step, the global id
    %   of the segment to be added and its probability.
    %     If the function returns 'true', the segment will be
    %   added to the agglomerate and the process continues.
    %     Otherwise the segment is rejected and the process
    %   of agglomeration is terminated.
    %
    % Note to self:
    %   This is a TypeEM-specific copy of jobAgglomerates.m.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    if ~exist('sampleFunc', 'var')
        sampleFunc = @sampleGreedily;
    end
    
    % neighbourhood around seed
    [neighIds, neighProbs] = ...
        findNeighours(adjMat, seedIds);
    
    % simplify list of neighbours
    [uniNeighIds, uniNeighProbs] = ...
        simplifyNeighbours(neighIds, neighProbs);
    
    % keep track
    segIds = seedIds(:);
    segProbs = inf(size(segIds));
    curStep = 1;
    
    % the main loop
    while true
        
        % exit loop when there are
        % no further neighbours left
        if isempty(uniNeighIds)
            stopReason = 'noNeighbour';
            break;
        end
        
        % sample next segment
        [candSegId, candProb] = ...
            sampleFunc(uniNeighIds, uniNeighProbs);
        
        if isempty(candSegId)
            stopReason = 'noCandidate';
            break;
        end;
        
        % should we accept this new segment?
        stopReason = acceptFunc( ...
            curStep, candSegId, candProb);
        
        % stop if there is a reason to do so
        if ~isempty(stopReason); break; end;
        
        % find its neighbors
        [candNeighIds, candNeighProbs] = ...
            findNeighours(adjMat, candSegId);
        
        % remove neighbours
        % that are already merged
        candNeighInterMask = ...
            ismember(candNeighIds, segIds);
        candNeighIds(candNeighInterMask) = [];
        candNeighProbs(candNeighInterMask) = [];
        
        % remove from neighbors
        neighCandMask = ...
            neighIds == candSegId;
        neighIds(neighCandMask) = [];
        neighProbs(neighCandMask) = [];
        
        % ... and unique neighbours
        uniNeighCandMask = ...
            uniNeighIds == candSegId;
        uniNeighIds(uniNeighCandMask) = [];
        uniNeighProbs(uniNeighCandMask) = [];
        
        % update state
        [newUniNeighIds, newUniNeighProbs] = simplifyCandidate( ...
            neighIds, neighProbs, uniNeighIds, ...
            uniNeighProbs, candNeighIds, candNeighProbs);
        
        uniNeighIds = newUniNeighIds;
        uniNeighProbs = newUniNeighProbs;
        
        segIds     = [segIds;     candSegId];
        segProbs   = [segProbs;   candProb];
        neighIds   = [neighIds;   candNeighIds];
        neighProbs = [neighProbs; candNeighProbs];
        
        % prepare next step
        curStep = curStep + 1;
    end
    
    % prepare output
    aggloStruct = struct;
    aggloStruct.seedId = seedIds;
    aggloStruct.segIds = segIds;
    aggloStruct.probs = segProbs;
    
    % convert to categorical array
    stopReason = categorical({stopReason});
end

function [uniNeighIds, uniNeighProbs] = ...
        simplifyNeighbours(neighIds, neighProbs)
    uniNeighIds = [];
    uniNeighProbs = [];
    
    % accumarray cannot handle empty inputs
    if isempty(neighIds); return; end;
    
    % find unique set of neighbors
    [uniNeighIds, ~, uniNeighInd] = unique(neighIds);
    
    % simplify probabilities
    uniNeighProbs = accumarray( ...
        uniNeighInd, neighProbs, [], @simplifyProbs);
end

function [uniNeighIds, uniNeighProbs] = ...
    simplifyCandidate( ...
        neighIds, neighProbs, ...
        uniNeighIds, uniNeighProbs, ...
        candNeighIds, candNeighProbs)
    % find elements which must be re-done
    redoIds = intersect(uniNeighIds, candNeighIds);
    
    % elements that can be re-used
    redoMask = ismember(neighIds, redoIds);
    uniRedoMask = ismember(uniNeighIds, redoIds);
    candRedoMask = ismember(candNeighIds, redoIds);
    
    % compute new entries
    [newUniNeighIds, newUniNeighProbs] = simplifyNeighbours( ...
        [neighIds(redoMask(:)); candNeighIds(candRedoMask(:))], ...
        [neighProbs(redoMask(:)); candNeighProbs(candRedoMask(:))]);
    
    % build output
    uniNeighIds = [ ...
        newUniNeighIds(:);
        uniNeighIds(~uniRedoMask(:));
        candNeighIds(~candRedoMask(:))];
    uniNeighProbs = [ ...
        newUniNeighProbs(:);
        uniNeighProbs(~uniRedoMask(:));
        candNeighProbs(~candRedoMask(:))];
end

function prob = simplifyProbs(probVec)
    % Manuel's strategy
    prob = max(probVec);
    % prob = 1 - prod(1 - probVec);
end

function [neighIds, neighProbs] = ...
        findNeighours(adjMat, segIds)
    % NOTE
    %   This function should also be able to handle seed
    %   agglomerates as opposed to single segment seeds.

    % NICE TO KNOW
    %   find(adjMat(:, segId)) is multiple orders of
    %   magnitude faster than find(adjMat(segId, :))!
    
    % look up in adjacency matrix
    [neighIds, ~, neighProbs] = find(adjMat(:, segIds));
    
    % remove neighbours that are already
    % part of the seed segments
    if ~isscalar(segIds)
        keepMask = ~ismember(neighIds, segIds);
        neighIds = neighIds(keepMask);
        neighProbs = neighProbs(keepMask);
    end
    
    % to column vectors
    neighIds = neighIds(:);
    neighProbs = neighProbs(:);
    
    neighProbs = single(neighProbs);
end

function [candSegId, candProb] = ...
        sampleGreedily(segIds, segProbs)
    % find highest probability
    [candProb, cabdIdx] = max(segProbs);
    
    % ... and corresponding segment id
    candSegId = segIds(cabdIdx);
end
