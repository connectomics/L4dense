function feat = buildFeatures(param, box)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % config
    minVoxelCount = 10;
    
    %% build segment agglomerates
    [volProps, aggloProps, box] = buildAgglomerates(param, box);
    
    %% filter out volumes which are too small
    volProps.voxelCount = cellfun(@numel, volProps.voxelIds);
    volProps(volProps.voxelCount < minVoxelCount, :) = [];
    
    [~, aggloProps.volId] = ismember(aggloProps.volId, volProps.id);
    aggloProps(not(aggloProps.volId), :) = [];
    
    %% calculate features
    featShape = buildFeaturesShape(param, box, volProps);
    featTexture = buildFeaturesTexture(param, box, volProps);
    clear('volProps');
    
    %% build output
    feat = Util.concatStructs(2, featShape, featTexture);
    clear('featShape', 'featTexture');
    
    % duplicate feature values
    feat.featMat = feat.featMat(aggloProps.volId, :);
    aggloProps(:, {'volId'}) = [];
    
    aggloProps.Properties.VariableNames{'id'} = 'segId';
    aggloProps = table2struct(aggloProps, 'ToScalar', true);
    feat = TypeEM.Util.mergeStructs(feat, aggloProps);
end

function feat = buildFeaturesShape(param, box, segProps)
    feat = TypeEM.Shape.buildFeatures(param, box, segProps);
    feat.featNames = strcat('shape_', feat.featNames);
end

function feat = buildFeaturesTexture(param, box, segProps)
    feat = TypeEM.Texture.buildFeatures(param, box, segProps);
    feat.featNames = strcat('texture_', feat.featNames);
end

function [segProps, segVol] = buildSegments(param, box)
    segProps = table;

    % load segmentation
    segVol = loadSegDataGlobal(param.seg, box);
    
    % calculate continuous, local segment IDs
    segProps.id = cast(unique(segVol(:)), 'like', segVol);
    
    % remove segment to be skipped
    skipSegIds = loadSkipSegIds(param);
    skipSegIds = cat(1, 0, skipSegIds(:));
    segProps(ismember(segProps.id, skipSegIds), :) = [];
    
    % update segmentation volume
    % NOTE(amotta): Segments to be skipped (e.g., the ones within nuclei
    % and blood vessels) were removed from segProps. Their corresponding
    % voxels will be assigned the segment ID zero here.
    [~, segVol] = ismember(segVol, segProps.id);
    
    % find voxel indices for each segment
    regProps = regionprops( ...
        segVol, segVol, 'BoundingBox', 'PixelIdxList');
    
    % make sure that uint32 is large enough
    assert(numel(segVol) <= intmax('uint32'));

    % collect voxel indices
    % NOTE that Benedikt wants these to be column vectors
    segProps.voxelIds = arrayfun( ...
        @(p) uint32(p.PixelIdxList(:)), ...
        regProps, 'UniformOutput', false);
    
    % determine bounding boxes around segments
    segProps.box = arrayfun( ...
        @(p) buildSegmentBox(p.BoundingBox), ...
        regProps, 'UniformOutput', false);
end

function box = buildSegmentBox(propBox)
    box = propBox;
    box = reshape(box, 3, 2);
    
    % dammit MATLAB!
    box = box([2, 1, 3], :);
    
    % build bounding box
    box(:, 1) = ceil(box(:, 1));
    box(:, 2) = sum(box, 2) - 1;
end

function [volProps, aggloProps, padBox] = buildAgglomerates(param, box)
    % pad bounding box
    padSize = param.classem.agglo.padSize;
    padBox = bsxfun(@times, padSize(:), [-1, +1]);
    padBox = bsxfun(@plus, box, padBox);
    
    padBox = [ ...
        max(padBox(:, 1), param.bbox(:, 1)), ...
        min(padBox(:, 2), param.bbox(:, 2))];
    padSize = box - padBox;

    % find segments and their voxels
    [segProps, segVol] = buildSegments(param, padBox);

    segVol = segVol( ...
        (1 + padSize(1, 1)):(end + padSize(1, 2)), ...
        (1 + padSize(2, 1)):(end + padSize(2, 2)), ...
        (1 + padSize(3, 1)):(end + padSize(3, 2)));
    segIds = setdiff(unique(segVol(:)), 0);
    clear segVol;
    
    % convert from the "local" segment IDs given in seg (i.e.,
    % continuous and starting at one) to the global segment IDs
    aggloProps = table;
    aggloProps.id = segProps.id(segIds);

    % build agglomerates
    aggloProps.agglo = ...
        TypeEM.buildAgglomerates(param, segProps.id, aggloProps.id);
    
    % find unique agglomerates
    [~, aggloRel] = ismember(aggloProps.agglo, segProps.id);
    [uniAgglo, ~, aggloProps.volId] = unique(sort(aggloRel, 2), 'rows');
    
    % merge lists of voxel indices
    volProps = table;
    volProps.id = reshape(1:size(uniAgglo, 1), [], 1);
    
    volProps.voxelIds = arrayfun(@(idx) cell2mat( ...
        segProps.voxelIds(setdiff(uniAgglo(idx, :), 0))), ...
        volProps.id, 'UniformOutput', false);
    
    % merge bounding boxes
    volProps.box = arrayfun(@(idx) mergeBoxes( ...
        segProps.box(setdiff(uniAgglo(idx, :), 0))), ...
        volProps.id, 'UniformOutput', false);
end

function box = mergeBoxes(boxes)
    boxes = cell2mat(reshape(boxes, 1, 1, []));
    box = [ ...
        min(boxes(:, 1, :), [], 3), ...
        max(boxes(:, 2, :), [], 3)];
end

function skipSegIds = loadSkipSegIds(param)
    heuristics = load(fullfile( ...
        param.saveFolder, 'heuristicResult.mat'));
    
    skipSegIds = heuristics.segIds( ...
        heuristics.vesselScore > 0.5 ...
      | heuristics.nucleiScore > 0.5);
end