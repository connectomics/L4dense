function convertOldGroundTruth(inNml, outNml)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    skel = skeleton(inNml);
    
    % config
    spineName = 'SpineHead';
    dendName = 'Dendrite';
    
    % find entities
    spineMask = strncmpi(skel.names, spineName, numel(spineName));
    dendId = find(strcmpi(skel.names, dendName));
    
    % coordinates of spine heads
    spineCoords = skel.nodes(spineMask);
    spineCoords = vertcat(spineCoords{:});
    spineCoords = spineCoords(:, 1:3);
    
    % find closest nodes
    neighNodeFunc = @(curIdx) ...
        skel.getClosestNode(spineCoords(curIdx, :), dendId);
    neighNodeIds = arrayfun(neighNodeFunc, 1:size(spineCoords, 1));
    
    for curIdx = 1:size(spineCoords, 1)
        curCoords = spineCoords(curIdx, :);
        curNeighIds = neighNodeIds(curIdx);
        
        % add spine head nodes to dendrites
        % and give them the comment 'SpineHead'
        skel = skel.addNode(...
            dendId, curCoords, curNeighIds, [], 'SpineHead');
    end
    
    % remove spines from skeleton
    skel = skel.deleteTrees(spineMask);
    skel.write(outNml);
end