% This script builds and evaluates a classifier where agglomerates with at
% least one spine segments are marked as positive training examples. For
% further information, see update from 02.03.2017 on
% https://mhlablog.net/2017/03/01/spine-head-classifier-2/
% 
% Written by
%   Alessandro Motta <alessandro.motta@brain.mpg.de>
rootDir = '/gaba/u/amotta/data/blog/2017-03-01-ClassEM-Spine-Heads-Part-2/five-segments-with-agglos';

train = fixLabels(fullfile(rootDir, 'train.mat'));
[classes, classifiers] = TypeEM.buildClassifiers(train);

eval = fixLabels(fullfile(rootDir, 'eval.mat'));
TypeEM.evalClassifiers(eval, classes, classifiers);

function out = fixLabels(path)
    out = load(path);
    
    % mark all agglomerates which contain at least one spine head segment
    % as positive for the spine head class
    sphdCol = out.class == 'spinehead';
    sphdMask = out.label(:, sphdCol) > 0;
    sphdSegIds = out.segIds(sphdMask);
    
    sphdAggloMask = any(ismember(out.agglo, sphdSegIds), 2);
    out.label(sphdAggloMask, sphdCol) = +1;
end
