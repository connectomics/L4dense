function feats = buildInertiaFeatures(inertiaMat)
    % get eigenvalues and -vectors
    [eigenVecs, eigenVals] = eig(inertiaMat);
    
    % large eigenvalues ➜ short axis
    [eigenVals, eigenValsAscIds] = ...
        sort(diag(eigenVals), 'ascend');
    
    % sort eigenvectors
    eigenVecs = eigenVecs(:, eigenValsAscIds);
    eigenDiams = 1 ./ sqrt(eigenVals);
    
    % find principal axes
    pcaFeats = TypeEM.Shape.buildPcaFeatures(eigenDiams);
    
    % buid output
    feats = struct;
    feats.eigenVecs  = eigenVecs;
    feats.eigenVals  = eigenVals;
    feats.eigenDiams = eigenDiams;
    
    % add principal axes
    feats = TypeEM.Util.mergeStructs(feats, pcaFeats, 'pca');
end
