function [crumpliness, packing] = ...
    buildConvHullFeatures(areaModel, volModel, areaHull, volHull)
  % Based on
  %   Corney et al. 2002. Coarse filters for shape matching.
  %   IEEE Computer Graphics and Applications.
  %
  % Author:
  %   Alessandro Motta <alessandro.motta@brain.mpg.de>
  
  % 1. Hull Crumpliness
  crumpliness = areaModel / areaHull;
  
  % 2. Hull packing
  packing = 1 - volModel / volHull;
end
