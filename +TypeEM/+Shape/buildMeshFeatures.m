function meshFeats = buildMeshFeatures(isoSurf, isoNorm)
    % FEATMESH Calculates mesh relates features
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % extract info about normal vectors
    isoNormVecs = isoNorm.vecs;
    isoNormLens = isoNorm.lens;
    
    % calculate mass properties
    massProps = TypeEM.Shape.Mesh.massProperties(isoSurf);
    
    % assuming uniform density
    volume = massProps.volume;
    area = massProps.area;
    
    % and more complex features
    volumeToArea = volume / area;
    compactness = area ^ 3 / volume ^ 2;
    
    % compress normal vectors
    normalCount = length(isoNormLens);
    [~, normalClusters] = kmeans( ...
        isoNormVecs, min(12, normalCount));
    
    % calculate angles in spherical coordinates
    % thetaVec = acos(normalVecs(:, 3));
    % phiVec = atan(normalVecs(:, 2) ./ normalVecs(:, 1));
        
    % calculate histogram of angles
    % histEdges = cell(2, 1);
    % histEdges{1} = linspace(0, pi, 6);
    % histEdges{2} = linspace(-pi, pi, 12);
    % hist3( ...
    %     [thetaVec, phiVec], ...
    %     'Edges', histEdges);
    
    % TODO
    % make use of histogram
    
    % prepare output
    meshFeats = struct;
    meshFeats.volumeToArea = volumeToArea;
    meshFeats.compactness = compactness;
    meshFeats.normalClusters = normalClusters;
    
    % merge mass properties
    meshFeats = TypeEM.Util.mergeStructs(meshFeats, massProps);
end

