function feats = buildThicknessFeatures(isoSurf, isoNorm)
    % FEATTHICKNESS Calculates shape and thickness
    % distributions as proposed by Osada and Liu,
    % respectively.
    %
    % Based on
    %   Osada et al. 2001.
    %   Matching 3D models with Shape Distributions.
    %  
    %   Liu et al. 2004.
    %   Thickness Histogram and Statistical Harmonic Representation
    %   for 3D model retrieval.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % config
    pairCount = 1000000;
    pointCount = 2 * pairCount;
    binCount = 500;
    
    % extract vertices and triangles
    isoVerts = isoSurf.vertices;
    isoFaces = isoSurf.faces;
    
    % extract normal vectors
    isoNormVecs = isoNorm.vecs;
    isoNormLens = isoNorm.lens;
    
    % calculate triangle weights
    % proportional to their surface areas
    triCount = size(isoNormLens, 1);
    triWeights = isoNormLens / sum(isoNormLens);
    
    % sample triangles
    triIndVec = randsample( ...
        triCount, pointCount, true, triWeights);
    
    % generate barycentric coordinates
    bariCoordMat = rand(pointCount, 3);
    bariCoordMat = bsxfun( ...
        @times, bariCoordMat, 1 ./ sum(bariCoordMat, 2));
    
    % generate point coordinates
    % from barycentric coordinates
    isoSurfTri = triangulation( ...
        isoFaces, isoVerts);
    pointCoords = barycentricToCartesian( ...
        isoSurfTri, triIndVec, bariCoordMat);
    
    % build pairs of points
    indOne = 1:2:pointCount;
    indTwo = 2:2:pointCount;
    
    % calculate pair-wise distance
    diffVec = pointCoords(indOne, :) - pointCoords(indTwo, :);
    distVec = sqrt(sum(diffVec .^ 2, 2));
    
    % compute weight matrix
    weightVec = ...
           dot(isoNormVecs(triIndVec(indOne), :), diffVec, 2) ...
        .* dot(isoNormVecs(triIndVec(indTwo), :), diffVec, 2);
    weightVec = weightVec ./ (distVec .^ 2);
    weightVec = abs(weightVec);
    
    % build histograms
    [distHist, weightHist] = ...
        buildHistograms(distVec, weightVec, binCount);
    
    % compute features
    % we need only half of the matrix
    % due to its symmetry
    distFeats = TypeEM.Util.buildStats(distVec);
    weightFeats = TypeEM.Util.buildStats(weightVec);
    
    % build features
    feats = struct;
    feats.distHist = distHist;
    feats.weightHist = weightHist;
    
    feats = TypeEM.Util.mergeStructs(feats, distFeats, 'dist');
    feats = TypeEM.Util.mergeStructs(feats, weightFeats, 'weight');
end

function [distHist, weightHist] = ...
        buildHistograms(distVec, weightVec, binCount)
    % compute distance histogram
    [distHist, ~, distBins] = histcounts(distVec, binCount);
    
    % build weight histogram
    uniDistBins = 1:binCount;
    uniDistBins = uniDistBins(distHist > 0);
    [~, uniDistIds] = ismember(distBins, uniDistBins);
    
    weightHist = zeros(binCount, 1);
    weightHist(uniDistBins) = accumarray(uniDistIds, weightVec);
end
