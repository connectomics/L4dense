function massProps = massProperties(meshStruct)
    % MASSPROPERTIES Calculates the surface area, mass,
    % center of mass and inertial moments for a given
    % surface triangulation.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    %
    % Based on
    %   Brian Mirtich. 1996.
    %   Fast and Accurate Computation of Polyhedral Mass Properties
    %   http://www.cs.berkeley.edu/~jfc/mirtich/massProps.html
    %
    %   David Ebely. 2009.
    %   Polyhedral Mass Properties (Revisited)
    %   http://www.geometrictools.com/Documentation/PolyhedralMassProperties.pdf
    
    verts = meshStruct.vertices;
    faces = meshStruct.faces;
    
    % triangle-spanning vectors
    aVecs = verts(faces(:, 3), :) - verts(faces(:, 1), :);
    bVecs = verts(faces(:, 2), :) - verts(faces(:, 1), :);
    
    % normal vectors
    normVecs = cross(aVecs, bVecs);
    normLens = sqrt(sum(normVecs .^ 2, 2));
    
    % build triangle matrix
    numFaces = size(faces, 1);
    triMat = verts(faces(:, :), :);
    triMat = reshape(triMat, [numFaces, 3, 3]);
    
    % calculate sub-expressions
    [fX, gX] = subExprs(triMat(:, :, 1));
    [fY, gY] = subExprs(triMat(:, :, 2));
    [fZ, gZ] = subExprs(triMat(:, :, 3));
    
    % calculate integrals
    intg0 = normVecs(:, 1)' * fX(:, 1) / 6;
    intg1 = normVecs(:, 1)' * fX(:, 2) / 24;
    intg2 = normVecs(:, 2)' * fY(:, 2) / 24;
    intg3 = normVecs(:, 3)' * fZ(:, 2) / 24;
    intg4 = normVecs(:, 1)' * fX(:, 3) / 60;
    intg5 = normVecs(:, 2)' * fY(:, 3) / 60;
    intg6 = normVecs(:, 3)' * fZ(:, 3) / 60;
    intg7 = normVecs(:, 1)' * ( ...
        triMat(:, 1, 2) .* gX(:, 1) ...
        + triMat(:, 2, 2) .* gX(:, 2) ...
        + triMat(:, 3, 2) .* gX(:, 3)) / 120;
    intg8 = normVecs(:, 2)' * ( ...
        triMat(:, 1, 3) .* gY(:, 1) ...
        + triMat(:, 2, 3) .* gY(:, 2) ...
        + triMat(:, 3, 3) .* gY(:, 3)) / 120;
    intg9 = normVecs(:, 3)' * ( ...
        triMat(:, 1, 1) .* gZ(:, 1) ...
        + triMat(:, 2, 1) .* gZ(:, 2) ...
        + triMat(:, 3, 1) .* gZ(:, 3)) / 120;
    
    mass =  intg0;
    cmX  =  intg1 / mass;
    cmY  =  intg2 / mass;
    cmZ  =  intg3 / mass;
    inXX =  intg5 + intg6 - mass * (cmY ^ 2 + cmZ ^ 2);
    inYY =  intg4 + intg6 - mass * (cmZ ^ 2 + cmX ^ 2);
    inZZ =  intg4 + intg5 - mass * (cmX ^ 2 + cmY ^ 2);
    inXY = -intg7 + mass * cmX * cmY;
    inYZ = -intg8 + mass * cmY * cmZ;
    inXZ = -intg9 + mass * cmZ * cmX;
    
    % packing up
    area = ...
        sum(normLens) / 2 ;
    centerOfMassVec = [ ...
        cmX; cmY; cmZ];
    inertiaMat = [ ...
        inXX, inXY, inXZ; ...
        inXY, inYY, inYZ; ...
        inXZ, inYZ, inZZ];
    
    % ATTENTION!
    % The triangle orientation provided by MATLAB seems
    % to be variable. Correct for that by calculating
    % the absolute vlaues
    mass = abs(mass);
    inertiaMat = abs(inertiaMat);
    
    % compute features
    % based on inertia matrix
    inertiaFeats = TypeEM.Shape.buildInertiaFeatures(inertiaMat);
    
    massProps = struct;
    massProps.area = area;
    massProps.mass = mass;
    massProps.volume = mass;
    massProps.centerOfMass = centerOfMassVec;
    massProps.inertiaMat = inertiaMat;
    
    % add inertia features
    massProps = TypeEM.Util.mergeStructs(massProps, inertiaFeats, 'inertia');
end

function [fMat, gMat] = subExprs(wVecs)
    temp0 = wVecs(:, 1)  + wVecs(:, 2);
    temp1 = wVecs(:, 1) .* wVecs(:, 1);
    temp2 = temp1 + wVecs(:, 2) .* temp0;

    f1 = temp0 + wVecs(:, 3);
    f2 = temp2 + wVecs(:, 3) .* f1;
    f3 = wVecs(:, 1) .* temp1 ...
        + wVecs(:, 2) .* temp2 ...
        + wVecs(:, 3) .* f2;

    g0 = f2 + wVecs(:, 1) .* (f1 + wVecs(:, 1));
    g1 = f2 + wVecs(:, 2) .* (f1 + wVecs(:, 2));
    g2 = f2 + wVecs(:, 3) .* (f1 + wVecs(:, 3));

    fMat = [f1, f2, f3];
    gMat = [g0, g1, g2];
end
