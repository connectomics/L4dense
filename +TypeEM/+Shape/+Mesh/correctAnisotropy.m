function outMeshStruct = correctAnisotropy(meshStruct, voxelSize)
    % CORRECTANISOTROPY Scales a mesh according to the voxel size
    % passed as argument. This enables the conversion of pixel
    % numbers to physical units (e.g. nano-metres)
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    verts = meshStruct.vertices;
    faces = meshStruct.faces;
    
    % make sure voxel size is a row vector
    voxelSize = reshape(voxelSize, 1, 3);
    
    % multiply indices with direction-dependent
    % measure of nano-metres per pixel
    verts = bsxfun(@times, verts, voxelSize);
    
    % build new mesh
    outMeshStruct = struct;
    outMeshStruct.vertices = verts;
    outMeshStruct.faces = faces;
end
