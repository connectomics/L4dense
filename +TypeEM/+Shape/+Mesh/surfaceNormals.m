function normStruct = surfaceNormals(meshStruct)
    % SURFACENORMALS Calculates the normal vectors
    % onto each face of the mesh.
    %
    % meshStruct:
    %   Structure as returned by isosurface().
    % normStruct.vecs:
    %   mx3 matrix. Each row represents a normal vector.
    % normStruct.lens:
    %   mx1 matrix. Each row contains the normal vector length.
    %
    % Example
    %   normStruct = TypeEM.Shape.Mesh.surfaceNormals(isoSurf);
    %   normVecs = normStruct.vecs;
    %   normLens = normStruct.lens;
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % extract data
    verts = meshStruct.vertices;
    faces = meshStruct.faces;
    
    % fail if mesh is empty
    if isempty(faces); error('Empty mesh!'); end
    
    % calculate edge vectors
    aVecs = verts(faces(:, 2), :) - verts(faces(:, 1), :);
    bVecs = verts(faces(:, 3), :) - verts(faces(:, 1), :);
    
    % calculate cross product along dimension two
    normals = cross(aVecs, bVecs, 2);
    
    % calculate length of normal vectors
    normalLens = sqrt(sum(normals .^ 2, 2));
    
    % reduce normal vectors to unit length
    normals = bsxfun(@times, normals, 1 ./ normalLens);
    
    % build output
    normStruct = struct;
    normStruct.vecs = normals;
    normStruct.lens = normalLens;
end
