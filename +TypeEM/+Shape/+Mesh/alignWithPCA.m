function pcaMeshStruct = alignWithPCA(meshStruct, comVec, pcaCoefs)
    % ALIGNWITHPCA Aligns the mesh in such a way that the new
    % X, Y, and Z axes will correspond to 1st, 2nd, and 3rd
    % principal components, respectively.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    verts = meshStruct.vertices;
    faces = meshStruct.faces;
    
    % make sure center-of-mass coordinates
    % are passed as a row vector
    comVec = reshape(comVec, 1, 3);
    
    % correct for center of mass
    verts = bsxfun(@minus, verts, comVec);
    
    % project onto principal axes
    verts = verts * pcaCoefs;
    
    % build new mesh
    pcaMeshStruct = struct;
    pcaMeshStruct.vertices = verts;
    pcaMeshStruct.faces = faces;
end
