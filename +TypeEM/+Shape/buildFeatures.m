function out = buildFeatures(param, box, segProps)
    % feat = buildFeatures(param, box, segProps)
    %   Calculates the shape features for all segments specified in
    %   'segProps' and returns a structure with the fields 'segIds',
    %   'featMat', and 'featNames'. For further information, see the
    %   documentation of TypeEM.buildFeaturesTexture.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    boxSize = reshape(1 + diff(box, 1, 2), 1, []);
    
    feats = arrayfun( ...
        @(idx) forSegment(param, boxSize, segProps(idx, :)), ...
        1:size(segProps, 1), 'UniformOutput', false);
    [featVecs, featNames] = cellfun( ...
        @TypeEM.Util.buildFeatVec, feats, 'UniformOutput', false);
    
    % build output
    out = struct;
    out.featMat = cat(1, featVecs{:});
    out.featNames = featNames{1};
end

function feats = forSegment(param, boxSize, segProp)
    voxelSize = param.raw.voxelSize;
    
    % build mask
    mask = buildMask(boxSize, segProp);
    maskFeats = TypeEM.Shape.buildMaskFeatures(voxelSize, mask);
    
    % build isosurface
    mask = padarray(mask, [1, 1, 1], 0);
    isoSurf = isosurface(mask, 0.5);
    
    isoSurf = TypeEM.Shape.Mesh.alignWithPCA( ...
        isoSurf, maskFeats.centerOfMass, maskFeats.inertiaEigenVecs);
    isoSurf = TypeEM.Shape.Mesh.correctAnisotropy(isoSurf, voxelSize);
    
    % calculate iso-surface features
    isoSurfNorms = TypeEM.Shape.Mesh.surfaceNormals(isoSurf);
    isoSurfFeats = TypeEM.Shape.buildMeshFeatures(isoSurf, isoSurfNorms);
    
    % compute thickness features
    thickFeats = TypeEM.Shape.buildThicknessFeatures(isoSurf, isoSurfNorms);
    
    % build convex hull features
    convHull = TypeEM.Shape.Mesh.buildConvHull(isoSurf, false);
    convHullNorm = TypeEM.Shape.Mesh.surfaceNormals(convHull);
    convHullFeats = TypeEM.Shape.buildMeshFeatures(convHull, convHullNorm);
    
    % calculate general surface features
    [crumpliness, packing] = TypeEM.Shape.buildConvHullFeatures( ...
        isoSurfFeats.area, isoSurfFeats.volume, ...
        convHullFeats.area, convHullFeats.volume);
    
    % building output
    feats = struct;
    feats.crumpliness = crumpliness;
    feats.convFeatPacking = packing;
    
    % add contributions from other modules
    feats = TypeEM.Util.mergeStructs(feats, maskFeats, 'mask');
    feats = TypeEM.Util.mergeStructs(feats, isoSurfFeats, 'isoSurf');
    feats = TypeEM.Util.mergeStructs(feats, convHullFeats, 'convHull');
    feats = TypeEM.Util.mergeStructs(feats, thickFeats, 'thick');
end

function mask = buildMask(maskSize, segProp)
    % Builds the binary mask for the segment specified in 'segProp'.
    voxelIds = cell2mat(segProp.voxelIds);
    segBox = cell2mat(segProp.box);
    
    % build mask
    mask = false(maskSize);
    mask(voxelIds) = true;
    
    % crop
    mask = mask( ...
        segBox(1, 1):segBox(1, 2), ...
        segBox(2, 1):segBox(2, 2), ...
        segBox(3, 1):segBox(3, 2));
end
