function gt = buildForSegments(param, nmlPath)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    code = buildCodeMatrix();
    
    % update label of spine head segments
    nml = TypeEM.GroundTruth.loadNml(param, nmlPath);
    nml.label(nml.comment == 'spinehead') = 'spinehead';
    
    % NOTE
    %   Dendrite segments which are also marked as being a spine head will
    %   be marked as the latter. The coding matrix will take care of the
    %   rest.
    spineHeadMask = ...
        nml.label == 'dendrite' & ismember( ...
        nml.segId, nml.segId(nml.label == 'spinehead'));
    nml.label(spineHeadMask) = 'spinehead';
    
    % NOTE
    %   The code matrix contains a list of all classes that we want to be
    %   able to predict. Segments with other labels will be mapped to the
    %   label 'other'.
    nml.label(~ismember(nml.label, code.class)) = 'other';
    
    % build binary labels
    gt = TypeEM.Classifier.buildLabelsForCodeMatrix(nml, code);
end

function code = buildCodeMatrix()
    % build class names
    classes = {'astrocyte', 'axon', 'dendrite', 'spinehead'};
    classes = categorical(classes);
    
    % labels additionally contain 'other' segments
    labels  = [classes, 'other'];
    
    % build labels
    code = struct;
    code.label = reshape(labels, [], 1);
    code.class = reshape(classes, 1, []);
    
    % code matrix
    code.mat = [ ...
        +1, -1, -1, -1;  % astrocyte
        -1, +1, -1, -1;  % axon
        -1, -1, +1, -1;  % dendrite
        -1, -1, +1, +1;  % spinehead
        -1, -1, -1, -1]; % other
end
