% Sample random mutually non-overlapping bounding boxes of specified size
% within the raw data of the 2012-09-28_ex145_07x2_ROI2017 dataset.
%
% Bounding boxes come in two flavors:
% * small boxes, which have the user-specified size. These are the boxes
%   for which ground truth annotations will be generated.
% * big boxes, which are small boxes + some user-defined margin
%
% The boxes are sampled such that
% * large boxes are mutually non-overlapping
% * large boxes are fully contained in the raw data of the dataset
%
% Written by
%   Alessandro Motta <alessandro.motta@brain.mpg.de>

clear;

%% configuration
numBoxes = 10;
smallBoxNm = 2000;
boxMarginVx = 150;

% margin of SegEM CNN
cnnMarginVx = [25, 25, 10];

% results
skelFile = '';

%% preamble
config = loadConfig('ex145_07x2_roi2017');
voxelSize = config.param.raw.voxelSize;

smallBoxVx = ceil(smallBoxNm ./ voxelSize);
bigBoxVx = smallBoxVx + 2 .* boxMarginVx;

rawBoxSmall = config.param.bbox;
rawBoxBig = rawBoxSmall + [-1, +1] .* cnnMarginVx(:);


%% sample boxes
rng(0);
boxesBig = inf(3, 2, numBoxes);

for curIdx = 1:numBoxes
    while true
        curOff = arrayfun( ...
            @(b, e, s) b + randi([0, (e - b + 1) - s]), ...
            rawBoxBig(:, 1), rawBoxBig(:, 2), bigBoxVx(:));
        curBox = horzcat(curOff, curOff + bigBoxVx(:) - 1);
        
        % sanity check
        assert(all( ...
            all(curBox >= rawBoxBig(:, 1), 2) ...
          & all(curBox <= rawBoxBig(:, 2), 2)));
      
        % avoid overlaps with other boxes
        curOtherDiffs = squeeze(boxesBig(:, 1, :));
        curOtherDiffs = abs(curOff - curOtherDiffs);
        
        curHasOverlap = all(curOtherDiffs < bigBoxVx(:), 1);
        if ~any(curHasOverlap); break; end
    end
    
    boxesBig(:, :, curIdx) = curBox;
end

% build small boxes
boxesSmall = boxesBig - [-1, +1] .* boxMarginVx;

%% show boxes in webKNOSSOS
if ~isempty(skelFile)
    skel = skeleton();

    for curIdx = 1:numBoxes
        curBox = squeeze(boxesBig(:, :, curIdx));
       [curX, curY, curZ] = ndgrid(1:2, 1:2, 1:2);

        curNodes = vertcat( ...
            curBox(1, curX(:)), ...
            curBox(2, curY(:)), ...
            curBox(3, curZ(:)));
        curNodes = transpose(curNodes);

        curEdges = [ ...
            1, 2; 1, 3; 1, 5;
            2, 4; 2, 6; 3, 4;
            3, 7; 4, 8; 5, 6;
            5, 7; 6, 8; 7, 8];
        skel = skel.addTree( ...
            sprintf('Box %d', curIdx), ...
            curNodes, curEdges);
    end
    
    skel = Skeleton.setParams4Pipeline(skel, config.param);
    skel.write(skelFile);
end

%% show boxes
fprintf('Small boxes (wK format):\n');
for curIdx = 1:numBoxes
    curName = sprintf('Box %02d', curIdx);
    curBox = WK.bbox2Str(squeeze(boxesSmall(:, :, curIdx)));
    fprintf('* %s: %s\n', curName, curBox);
end