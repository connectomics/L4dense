function gt = loadFeatures(param, featSetName, gt)
    % gt = loadFeatures(param, featFileName, gt)
    %   Loads the features for the segments in a groundtruth.
    %
    % Input arguments
    %   param
    %     Parameter structure
    %   featSetName
    %     There may exist multiple feature sets (e.g., segment- and
    %     agglomerate-based ones). The feature values returned by this
    %     function will be loaded from `strcat(featSetName, '.mat')`.
    %   gt
    %     Groundtruth data structure
    %
    % Output arguments
    %   gt
    %     Ground truth data structure with the following fields added:
    %
    %     featNames
    %       Row cell vector with M feature names.
    %     featMat
    %       N x M matrix of feature values. N is the number of segments
    %       in gt.segId. M is the number of features (see featNames)
    %
    %     Only a subset of all segments are contained in the feature set
    %     data structure. The output `gt` is the subset of the input `gt`
    %     for which feature values were calculated.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    feat = TypeEM.Pipeline.loadFeatures(param, featSetName, gt.segId);
    
   [found, featRows] = ismember(gt.segId, feat.segId);
    featRows = featRows(found);
    
    % filter ground truth
    gt.segId = gt.segId(found);
    gt.label = gt.label(found, :);
    
    % add features
    gt.featMat = feat.featMat(featRows, :);
    gt.featNames = feat.featNames;
end