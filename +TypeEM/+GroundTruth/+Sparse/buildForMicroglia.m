function buildForMicroglia(config)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
   
    param = config.param;
    repoDir = config.repoDir;
    
    setIds = [1, 1, 2, 1];
    setSizes = [2000, 270];
    setNames = {'training', 'test'};
    className = 'microglia';
    nmlDir = fullfile( ...
        repoDir, 'data', 'tracings', ...
        'ex145-07x2-roi2017', 'microglia');
    
    % build NML files
    TypeEM.GroundTruth.Sparse.buildForClass( ...
        param, setIds, setSizes, setNames, className, nmlDir);
end