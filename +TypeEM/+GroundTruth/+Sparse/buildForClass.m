function buildForClass( ...
        param, setIds, setSizes, setNames, className, nmlDir)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % sanity checks
    assert(max(setIds) == numel(setSizes));
    assert(numel(setSizes) == numel(setNames));
    
    % load NML files
    segSets = TypeEM.GroundTruth.Sparse.load(param, nmlDir);
    assert(numel(segSets) == numel(setIds));
    
    % load segment locations
    points = Seg.Global.getSegToPointMap(param);
    
    rngPrev = rng(0);
    
    for curIdx = 1:max(setIds)
        curSet = segSets(setIds == curIdx);
        curSet = cell2mat(curSet(:));
        
        % do the subsampling
        curSetSize = setSizes(curIdx);
        curRandIds = randperm(numel(curSet), curSetSize);
        
        curSet = curSet(curRandIds);
        curPoints = points(curSet, :);
        
        % build skeleton
        curSkel = skeleton();
        curSkel = curSkel.addTree(className, curPoints);
        curSkel = Skeleton.setParams4Pipeline(curSkel, param);
        
        % write skeleton
        curName = setNames{curIdx};
        curFile = fullfile(nmlDir, strcat(curName, '.nml'));
        curSkel.write(curFile);
    end
    
    rng(rngPrev);
end