function taskSegIds = load(param, nmlDir)
    % taskSegIds = load(param, nmlDir)
    %   Loads and processes sparse tracings. It is expected that each
    %   webKNOSSOS task corresponds to a cell. Also, we only use segments
    %   collected by at least two tracers.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % path to cache file
    cacheFile = fullfile(nmlDir, 'cache.mat');
    
    nmlDirEntries = dir(nmlDir);
    nmlFiles = {nmlDirEntries.name};
    nmlFiles(cat(1, nmlDirEntries.isdir)) = [];
    
    if ~exist(cacheFile, 'file')
        loadFunc = @(n) loadMicrogliaNml(nmlDir, n);
        nodes = cellfun(loadFunc, nmlFiles, 'UniformOutput', false);
        
        filterFunc = @(n) filterSoma(param, n);
        nodes = cellfun(filterFunc, nodes, 'UniformOutput', false);
        
        % look up segment IDs
        nodes = cat(1, nodes{:});
        nodes.coord = nodes.coord + 1;
        nodes.segId = Seg.Global.getSegIds(param, nodes.coord);
        
        % save to cache
        save(cacheFile, 'nodes');
    else
        load(cacheFile, 'nodes');
    end
    
    % remove invalid nodes
    nodes(not(nodes.segId), :) = [];
    
    % make unique over tasks
    nodes = nodes(:, {'taskId', 'tracingId', 'segId'});
    nodes = unique(nodes, 'rows');
    
    % only keep nodes that were collected by two annotators
    nodes = nodes(:, {'taskId', 'segId'});
   [nodes, ~, nodeCount] = unique(nodes, 'rows');
    nodeCount = accumarray(nodeCount, 1);
    nodes(nodeCount < 2, :) = [];
    
    % remove segment which occur in multiple tasks / cells
   [uniSegIds, ~, uniSegCount] = unique(nodes.segId);
    uniSegCount = accumarray(uniSegCount, 1);
    dupSegIds = uniSegIds(uniSegCount > 1);
    nodes(ismember(nodes.segId, dupSegIds), :) = [];
    
    % group segment by task / cell
   [~, ~, nodes.taskId] = unique(nodes.taskId);
    taskSegIds = accumarray(nodes.taskId, nodes.segId, [], @(s) {s(:)});
end

function nodes = loadMicrogliaNml(nmlDir, nmlName)
    % parse information from nml file name
    parts = strsplit(nmlName, '.');
    parts = strsplit(parts{1}, '__');
    
    taskId = categorical(parts(2));
    tracingId = categorical(parts(end));
    
    nmlPath = fullfile(nmlDir, nmlName);
    nml = slurpNml(nmlPath);
    
    % make sense out of the file
    nodes = NML.buildNodeTable(nml);
    
    % add meta data
    nodes.taskId = repmat(taskId, height(nodes), 1);
    nodes.tracingId = repmat(tracingId, height(nodes), 1);
end

function nodes = filterSoma(param, nodes)
    % config
    keepRadiusNm = 7.5E3;
    
    % find seed node (placed in soma)
    [~, somaIdx] = min(nodes.time);
    assert(nodes.id(somaIdx) == 1);
    
    voxelSize = param.raw.voxelSize;
    keepRadiusVx = keepRadiusNm ./ voxelSize;
    
    somaPos = nodes.coord(somaIdx, :);
    nodes.coordRel = bsxfun(@minus, nodes.coord, somaPos);
    nodes.drop = all(bsxfun(@le, abs(nodes.coordRel), keepRadiusVx), 2);
    
    % build output
    nodes(nodes.drop, :) = [];
    nodes(:, {'coordRel', 'drop'}) = [];
end