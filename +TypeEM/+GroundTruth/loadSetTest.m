function gt = loadSetTest(param, featureSetName)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    nmlDir = TypeEM.Data.getFile( ...
        fullfile('tracings', 'ex145-07x2-roi2017'));
    nmlFiles = fullfile(nmlDir, { ...
        fullfile('dense-v2', 'region-4.nml')});
    
    % load test set
    gt = TypeEM.GroundTruth.loadSet( ...
        param, featureSetName, nmlFiles);
end
