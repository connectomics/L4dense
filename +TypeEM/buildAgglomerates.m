function agglos = buildAgglomerates(param, segIds, seedIds)
    % agglos = buildAgglomerates(param, box, seedIds)
    %   Builds local agglomerates around the segments specified
    %   by 'seedIds'. The agglomeration is performed on a graph
    %   restricted to the segments in 'segIds'.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % config
    aggloParam = param.classem.agglo;
    maxSegCount = aggloParam.maxSegCount;
    minEdgeProb = aggloParam.minEdgeProb;
    
    % loading data
    adjMat = loadAdjacencyMatrix(param, segIds);
    
    % running
    acceptFunc = ...
        buildAcceptFunc(maxSegCount, minEdgeProb);
    aggloFunc = @(seedId) ...
        TypeEM.Graph.buildAgglomerates(adjMat, seedId, acceptFunc);
    agglos = arrayfun( ...
        aggloFunc, seedIds(:), 'UniformOutput', false);
    
    % build output
    agglos = buildOutputMatrix(maxSegCount, agglos);
end

function adjMat = loadAdjacencyMatrix(param, segIds)
    maxSegId = Seg.Global.getMaxSegId(param);
    graph = loadSubgraph(param, segIds);
    
    % NOTE(amotta): At some point the pipeline / auxiliary methods changed
    % the data type of the 'edges' property of the graph.mat file from
    % double to uint32. Sparse can only handle doubles, so let's cast...
    graph.edges = double(graph.edges);
    
    adjMat = sparse( ...
        graph.edges(:, 2), graph.edges(:, 1), ...
        graph.prob, maxSegId, maxSegId);
    
    % jobAgglomerate expects a symmetric matrix
    adjMat = adjMat + transpose(adjMat);
end

function graph = loadSubgraph(param, segIds)
    graph = Graph.load(param.saveFolder);
    graph = TypeEM.Graph.buildSubgraphSegments(graph, segIds);
    graph = Graph.reduce(graph);
end

function acceptClosure = buildAcceptFunc(maxSegCount, minEdgeProb)
    acceptClosure = @acceptFunc;

    function stopReason = acceptFunc(step, ~, segProb)
        stopReason = [];

        % check if we need to stop
        if step >= maxSegCount; stopReason = 'maxSegCount'; end;
        if segProb < minEdgeProb; stopReason = 'minEdgeProb'; end;
    end
end

function aggloMat = buildOutputMatrix(maxSegCount, agglos)
    aggloMatRow = @(a) [ ...
        a.segIds(:)', zeros(1, maxSegCount - numel(a.segIds))];
    aggloMat = cellfun( ...
        aggloMatRow, agglos, 'UniformOutput', false);
    aggloMat = cat(1, aggloMat{:});
end
