function [precRec, fig] = evaluate(param, classifiers, gt)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    import TypeEM.*;
    
    % load feature values
    gt = GroundTruth.loadFeatures( ...
        param, classifiers.featureSetName, gt);
    gt.scores = Classifier.apply(classifiers, gt.featMat);
    
    fig = figure();
    ax = axes(fig);
    hold(ax, 'on');
    
    % plot curve for each classifier
    precRec = cell(numel(classifiers.classes), 2);
   [precRec(:, 1), precRec(:, 2)] = arrayfun( ...
       @(curIdx) evalForClass(ax, gt, classifiers, curIdx), ...
       1:numel(classifiers.classes), 'UniformOutput', false);
    
    % build legend
    buildLegend = @(c) TypeEM.Util.toCamelCase(char(c));
    legends = arrayfun(buildLegend, classifiers.classes, 'Uni', false);
    legend(ax, legends, 'Location', 'SouthWest');
    
    precRec = cellfun( ...
        @horzcat, precRec(:, 1), precRec(:, 2), 'UniformOutput', false);
    precRec = cell2struct(precRec, categories(classifiers.classes));
end

function [precVec, recVec] = evalForClass(ax, gt, classifiers, classIdx)
    % find class in groundtruth
    mask = gt.label(:, classIdx) ~= 0;
    scores = gt.scores(mask, classIdx);
    labels = double(gt.label(mask, classIdx) > 0);
    
    % plot precision / recall curve
   [precVec, recVec] = TypeEM.Classifier.buildPrecRec(scores, labels);
    TypeEM.Classifier.plotPrecisionRecall(ax, precVec, recVec);
    
    % show characteristic numbers
    recAt95 = ...
        TypeEM.Classifier.buildRecallAtPrecision(precVec, recVec, 0.95);
    recAt90 = ...
        TypeEM.Classifier.buildRecallAtPrecision(precVec, recVec, 0.90);
    
    class = classifiers.classes(classIdx);
    disp(['For class ''', char(class), '''']);
    disp([num2str(100 * recAt95, '%.1f'), ' % recall at 95 % precision']);
    disp([num2str(100 * recAt90, '%.1f'), ' % recall at 90 % precision']);
end
