function platt = trainPlatt(param, classifiers)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    import TypeEM.*;
    
    % load feature values
    gt = GroundTruth.loadSetTest( ...
        param, classifiers.featureSetName);
    gt = GroundTruth.loadFeatures( ...
        param, classifiers.featureSetName, gt);
    gt.scores = Classifier.apply(classifiers, gt.featMat);
    
    % build platt parameters
    trainPlattFunc = @(curIdx) trainPlattForClass(gt, curIdx);
    [aVec, bVec] = arrayfun(trainPlattFunc, 1:numel(classifiers.classes));
    
    % build output
    platt = struct( ...
        'a', num2cell(aVec), ...
        'b', num2cell(bVec));
end

function [a, b] = trainPlattForClass(gt, classIdx)
    mask = gt.label(:, classIdx) ~= 0;
    scores = gt.scores(mask, classIdx);
    labels = double(gt.label(mask, classIdx) > 0);
    
    % optimize Platt parameter
    [a, b] = TypeEM.Classifier.learnPlattParams(scores, labels);
end
