function out = buildLabelsForCodeMatrix(seg, code)
    % out = buildLabelsForCodeMatrix(segData, codeMat)
    %   
    %   seg
    %     Table or structure with the fields 'segId' and 'label'. Both
    %     fields must be column vectors of equal length. The 'label' field
    %     is a categorical array.
    %
    %   code
    %     A struct with the fields 'label', 'class', and 'mat'. The 'label'
    %     column vector contains all N unique values of seg's 'label' field.
    %     'class' is a categorical row vector with M elements. The 'mat'
    %     field is a NxM matrix with entries, such that segments with label
    %     `code.label(i)` are to be considered as
    %
    %                        +1  ←→  positive
    %       code.mat(i, j) =  0  ←→  ignored
    %                        -1  ←→  negative
    %
    %     Segments, which carry positive and negative labels will be
    %     ignored automatically.
    %
    %   out
    %     A structure with the fields 'segId', 'class' and 'label'. 'segId'
    %     contains the unique values of `seg.segId`, and 'class' is the
    %     same as `code.class`. The 'label' is a LxM matrix that contains
    %     +1, 0, or -1 for each segment-class pair.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % sanity checks
    assert(size(code.mat, 1) == size(code.label, 1));
    assert(size(code.mat, 2) == size(code.class, 2));
    
    segIdsUni = unique(seg.segId);
    segLabels = unique(seg.label);
    
    % make sure that all segment labels are covered
    [code.label, labelRow] = sort(code.label);
    assert(all(ismember(segLabels, code.label)));
    code.mat = code.mat(labelRow, :);
    
    % find all segment IDs per segment label
    segIdsForLabelFunc = @(curLabel) {seg.segId(seg.label == curLabel)};
    segIdsForLabel = arrayfun(segIdsForLabelFunc, code.label);
    
    forClassFunc = @(idx) {forClass( ...
        segIdsUni, segIdsForLabel, code.mat(:, idx))};
    labels = arrayfun(forClassFunc, 1:numel(code.class));
    
    % build output
    out = struct;
    out.segId = segIdsUni;
    out.class = code.class;
    out.label = cat(2, labels{:});
end

function labels = forClass(segIdsUni, segIdsForLabel, curCode)
    labels = zeros(size(segIdsUni));
    
    posSegIds = cat(1, segIdsForLabel{curCode > 0});
    negSegIds = cat(1, segIdsForLabel{curCode < 0});
    
    % remove conflicting segment IDs
    confSegIds = intersect(posSegIds, negSegIds);
    posSegIds = setdiff(posSegIds, confSegIds);
    negSegIds = setdiff(negSegIds, confSegIds);
    
    % build label vector
    labels(ismember(segIdsUni, posSegIds)) = +1;
    labels(ismember(segIdsUni, negSegIds)) = -1;
end