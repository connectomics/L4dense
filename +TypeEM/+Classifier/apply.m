function scores = apply(classifiers, featMat)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    scores = cellfun( ...
        @(c) forClassifier(featMat, c), ...
        classifiers.classifiers, 'UniformOutput', false);
    scores = horzcat(scores{:});
end

function scores = forClassifier(featMat, classifier)
    [~, scores] = predict(classifier, featMat);
    scores = scores(:, 1);
end