function respVec = buildRecallAtPrecision(precVec, recVec, queryVec)
    % respVec = buildRecallAtPrecision(precVec, recVec, queryVec)
    %   Finds the recall values at which the precision drops below
    %   the given precision values.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    doIt = @(p) max([0, recVec(find(precVec > p, 1, 'last'))]);
    respVec = arrayfun(doIt, queryVec);
end