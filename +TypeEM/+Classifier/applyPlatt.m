function probs = applyPlatt(classifiers, scores)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % sanity check
    assert(isfield(classifiers, 'plattParams'));
    assert(size(scores, 2) == numel(classifiers.plattParams));
    
    % extract relevant parameters
    plattParams = classifiers.plattParams;
    classCount = numel(plattParams);
    
    probs = nan(size(scores));
    for curClassIdx = 1:classCount
        curPlattParams = plattParams(curClassIdx);
        curPlatt = TypeEM.Classifier.buildPlatt( ...
            curPlattParams.a, curPlattParams.b);
        
        % apply transforms column-wise
        probs(:, curClassIdx) = curPlatt(scores(:, curClassIdx));
    end
end
