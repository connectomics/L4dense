function [precVec, recVec, threshVec] = ...
        buildPrecRec(scoreVec, labelVec, weightVec)
    % BUILDPRECREC
    %   Build precision-recall curve from score and label
    %   vectors. Optionally, the weight of each test sample
    %   may be specified with `weightVec`.
    %
    % Written by
    %   Alessandro Motta
    
    % sort scores
    [threshVec, threshIndVec] = ...
        sort(scoreVec, 'descend');
    labelDescVec = labelVec(threshIndVec);
    
    % build default weights, if needed
    if ~exist('weightVec', 'var')
        weightVec = ones(numel(scoreVec), 1);
    end
    
    % mask for correct samples
    posMask = labelDescVec(:) == 1;
    
    % compute vectors
    numPosPred = cumsum(weightVec(:));
    numPosTrue = cumsum(weightVec(:) .* posMask);
    numPosTotal =   sum(weightVec(:) .* posMask);
    
    precVec = numPosTrue ./ numPosPred;
    recVec = numPosTrue / numPosTotal;
end