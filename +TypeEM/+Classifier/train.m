function out = train(param, featureSetName)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    import TypeEM.GroundTruth.*;
    
    % load feature values
    gt = loadSetTraining(param, featureSetName);
    gt = loadFeatures(param, featureSetName, gt);
    
    % train classifier
    out.classes = gt.class;
    out.classifiers = arrayfun( ...
        @(c) buildForClass(gt, c), ...
        gt.class, 'UniformOutput', false);
    out.featureSetName = featureSetName;
end

function classifier = buildForClass(gt, class)
    % classifier = buildForClass(data, className)
    %   Builds a one-versus-all (OVA) classifier for a class
    
    classIdx = find(gt.class == class);
    mask = gt.label(:, classIdx) ~= 0;
    
    % extract training data
    trainFeat = gt.featMat(mask, :);
    trainClass = gt.label(mask, classIdx) > 0;
    
    % train classifier
    classifier = fitensemble( ...
        trainFeat, trainClass, ...
        'LogitBoost', 1500, 'tree', ...
        'LearnRate', 0.1, ...
        'NPrint', 100, ...
        'Prior', 'Empirical', ...
        'Type', 'Classification', ...
        'ClassNames', [true, false]);
end
