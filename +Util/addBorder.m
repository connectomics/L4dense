function [ bbox ] = addBorder( bbox, border )
%ADDBORDER Add a border to a bounding box.
% INPUT bbox: [3x2] or [Nx6] int
%           Bounding box. If bbox is [Nx6] a vector then it is assumed that
%           each row is a linearized bbox.
%       border: [1x3] int
%           Border that is added on each side of the bounding box.
% OUTPUT bbox: [Nx3] or [6x1] int
%           The enlarged bounding box.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

sz = size(bbox);
if sz(2) == 2 % [3x2] case
    bbox = bbox(:)';
end

bbox = bsxfun(@plus, bbox, [-border(:)', border(:)']);


bbox = reshape(bbox, sz);


end

