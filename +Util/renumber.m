function [ Y, mapping, fwdMapping ] = renumber( X, mode )
%RENUMBER Renumber the unique elements in X as integers starting from 1.
% INPUT X: Numeric array
%       mode: (Optional) String specifying renumber mode
%           'stable': IDs are renumbered according to their occurrence in
%                     X(:).
%           'sorted': (Default) IDs are renumbered according to occurence
%                     in unique(X(:));
% OUTPUT Y: Renumbered array of the same size as X.
%        mapping: [Nx2] array containing the inverse mapping of renumber.
%           Use renumberBwd with Y and mapping to get X.
%        fwdMapping: [Nx1] int
%           Mapping such that Y = fwdMapping(X).
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

sX = size(X);
X = X(:);
if ~exist('mode','var') || isempty(mode)
    mode = 'sorted';
end
switch mode
    case 'stable'
        [~,ix,ic] = unique(X,'stable');
    case 'sorted'
        [~,ix,ic] = unique(X);
    otherwise
        error('Unknown mode %s.',mode);
end

Y = cast(ic,'like',X);

if nargout > 1
    mapping = [X(ix),Y(ix)];
end

if nargout > 2
	fwdMapping = zeros(max(mapping(:,1)), 1);
	fwdMapping(mapping(:,1)) = mapping(:,2);
end

Y = reshape(Y,sX);


end

