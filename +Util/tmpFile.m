function [fullPath, fclean] = tmpFile( folder, varargin )
%TMPFILE Create a temporary file with content from varargin.
% INPUT folder: File root folder. The file is saved in the subfolder tmp.
%       varargin: key-value pairs specifying the name and content of
%           variables that are saved to the file. Data is saved using
%           matfile.
% OUTPUT filename: Full path to temporary file.
%        fclean: Function handle to delete the temporary file.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

folder = [folder filesep 'tmp'];
fullPath = tempname(folder);
if ~exist(folder, 'dir')
    mkdir(folder);
end
m = matfile(fullPath,'Writable',true);
varargin = reshape(varargin,2,[]);
for i = 1:size(varargin,2)
    m.(varargin{1,i}) = varargin{2,i};
end

    function cleanData()
        delete([fullPath, '.mat']);
    end

fclean = @cleanData;

end

