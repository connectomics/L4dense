function A = setBBoxInArray( A, bbox, data )
%SETBBOXINARRAY Fill a bounding box in an array with data.
% INPUT A: nd array
%           The array into which the data is inserted.
%       bbox: [Nx2] int
%           Bounding box of the form [min_x, max_x; min_y, max_y; ...] with
%           minimal and maximal coordinates for each dimension.
%       data: nd array
%           The data that is filled into A.
% OUTPUT A: nd array
%           The input array with data inserted.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

S = arrayfun(@(x)bbox(x,1):bbox(x,2),1:size(bbox,1), ...
    'UniformOutput', false);
s.type = '()';
s.subs = S;
A = subsasgn(A, s, data);

end

