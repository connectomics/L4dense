function [ A ] = cropArray( A, targetSize )
%CROPARRAY Crop array with symmetric border.
% INPUT A: N-dimenstional array.
%       targetSize: [Nx1] int or scalar int
%           The size of the center of A that to which A is cropped.
%           If targetSize is negative than it will be interpreted as a
%           border that is cropped from each side of A.
% OUTPUT A: Cropped input.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if isscalar(targetSize)
    targetSize = repmat(targetSize,[ndims(A), 1]);
elseif length(targetSize) ~= ndims(A)
    error('Specify targetSize for each dimension of A.');
end

if all(targetSize > 0)
    % actual target size
    border = (size(A) - targetSize)./2;
    if any(floor(border) ~= border)
        error('Target cube can not be cenetered in input cube.');
    end
else
    border = - targetSize;
end

for dim = 1:ndims(A)
    idx = repmat({':'},[ndims(A), 1]);
    idx{dim} = (border(dim) + 1):(size(A,dim)-border(dim));
    S.type = '()';
    S.subs = idx;
    A = subsref(A, S);
end



end

