function A = sphereSurface( theta, phi, r )
%SPHERESURFACE Calculate the surface of a 3d sphere.
% INPUT theta: [2x1] double
%           Integration interval for polar angle in radians contained in
%           [0, pi]
%       phi: [2x1] double
%           Integration interval for azimuthal angle radians contained in
%           [0, 2*pi]
%       r: (Optional) double
%           Sphere radius.
%           (Default: 1)
% OUTPUT A: double
%           Surface area given by
%           A = r^2 \int_{\theta=\theta_1}^{\theta_2}
%           \int_{\phi=\phi_1}^{\phi_2} sin(\theta) d\theta d\phi
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('r', 'var') || isempty(r)
    r = 1;
end
if theta(1) < 0 || theta(1) > pi || theta(2) < 0 || theta(2) > pi
    error('Theta specification is wrong.');
end
if phi(1) < 0 || phi(1) > 2*pi || phi(2) < 0 || phi(2) > 2*pi
    error('Theta specification is wrong.');
end

A = r^2*(phi(2) - phi(1))*(cos(theta(1)) - cos(theta(2)));

end

