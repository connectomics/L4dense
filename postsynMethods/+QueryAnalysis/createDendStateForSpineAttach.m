% create a dendrite state combining dendrites and wholecells for automated
% spine attachment run
Util.log('Loading ...')
load('/gaba/u/mberning/results/pipeline/20170217_ROI/allParameterWithSynapses.mat');
outputFolder = fullfile(p.saveFolder, 'aggloState');

load(fullfile(outputFolder,'dendrites_16.mat'),'dendrites','indBigDends')
load(fullfile(outputFolder,'wholeCells_06.mat'),'wholeCells')

dendrites = cat(1,dendrites,wholeCells);
indBigDends = cat(1,indBigDends,true(numel(wholeCells),1));
Util.log('Saving dend+wholecells version ...')
save(fullfile(outputFolder,'dend+wholecells_01.mat'),'dendrites','indBigDends');
