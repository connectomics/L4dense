% after processing the manual spine head attachment queries, append SH and tracing to dendrites state
setConfig;
config.outDir = '/tmpscratch/sahilloo/L4/forIso/';
outDir= config.outDir;
m=load(config.param);                                                                                                                   
p = m.p;
aggloStateFolder = fullfile(p.saveFolder, 'aggloState');
config.spineFile = fullfile(aggloStateFolder,'dendrites_wholeCells_01_auto.mat')

%{
% create ff structure
skeletonFolders = {'/tmpscratch/sahilloo/L4/spines/spine_head_attachment_queries_2/'};
tic;
Util.log('Creating ff ...');
[ff.segIds, ff.neighbours, ff.filenames, ff.nodes, ff.startNode, ff.comments] ...
    = connectEM.lookupNmlMulti(p, skeletonFolders, false);
toc;
info = Util.runInfo(false);
save(fullfile(outDir,'spineHeadsToDendritesQueryState_2.mat'),'ff','info');

% combine two ff
m=load(fullfile(outDir,'spineHeadsToDendritesQueryState_1.mat'));
ff1 = m.ff;

m=load(fullfile(outDir,'spineHeadsToDendritesQueryState_2.mat'));
ff2 = m.ff;

ff = struct;
ff.segIds = vertcat(ff1.segIds,ff2.segIds);
ff.neighbours = vertcat(ff1.neighbours,ff2.neighbours);
ff.filenames = horzcat(ff1.filenames,ff2.filenames);
ff.nodes = horzcat(ff1.nodes,ff2.nodes);
ff.startNode = horzcat(ff1.startNode,ff2.startNode);
ff.comments = vertcat(ff1.comments,ff2.comments);
info = Util.runInfo(false);
save(fullfile(outDir,'spineHeadsToDendritesQueryState.mat'),'ff','info');
%}

Util.log('Loading stuff...');
m=load(fullfile(outDir,'spineHeadsToDendritesQueryState.mat'));
ff= m.ff;
maxSegId = Seg.Global.getMaxSegId(p);
% points = Seg.Global.getSegToPointMap(p);
% voxelSize = p.raw.voxelSize;
% segmentMeta = load(config.meta);

m=load(config.spineFile);
shAgglos = cellfun(@(x) double(x),m.shAgglos,'uni',0); % all shAgglos
attached = m.attached; % idx of all shAgglos attached
shAgglosLUT = Agglo.buildLUT(maxSegId,shAgglos);

dendAgglos = m.dendAgglos;  % dend with auto Spines
aggloSizes = Agglo.calcVolumes(p, dendAgglos);

candAgglosIdx = find(aggloSizes(:) > (10 ^ 5.5));
nonCandAgglosIdx = find(~(aggloSizes(:) > (10 ^ 5.5)));
candAgglos = dendAgglos(candAgglosIdx);
nonCandAgglos = dendAgglos(nonCandAgglosIdx);

candLUT = Agglo.buildLUT(maxSegId, candAgglos);
nonCandLUT = Agglo.buildLUT(maxSegId,nonCandAgglos);

% statistics
stat = struct;
stat.emptyNodes = false(numel(ff.nodes),1);
stat.firstNodeNotSh = false(numel(ff.nodes),1);
stat.commented = false(numel(ff.nodes),1);

% take only tracings that were commented solved
commented = ~cellfun(@isempty,vertcat(ff.comments));

ff.nodes(commented) = [];
ff.segIds(commented) = [];
ff.startNode(commented) = [];
ff.comments(commented) = [];
ff.filenames(commented) = [];
ff.neighbours(commented) = [];
stat.commented(commented) = true;
Util.log([num2str(sum(commented)) ' ff were removed bc commented'])

% find empty tracings
emptyNodes = cellfun(@(x) isempty(x),ff.segIds); % some maybe empty
ff.nodes(emptyNodes) = [];
ff.segIds(emptyNodes) = [];
ff.startNode(emptyNodes) = [];
ff.comments(emptyNodes) = [];
ff.filenames(emptyNodes) = [];
ff.neighbours(emptyNodes) = [];
stat.emptyNodes(emptyNodes) = true;
Util.log([num2str(sum(emptyNodes)) ' ff were removed bc nodes are empty'])

% find start nodes
startNodeSeg = cellfun(@(x) x(1),ff.segIds); % maybe 0
zeroStartNode = startNodeSeg ==0;

% check if startNode was not in spineHead and remove those in ff and indexToSh
Util.log('Finding spinehead agglo for startNode...')
indexToSh(zeroStartNode) = 0;
indexToSh(~zeroStartNode) = arrayfun(@(x) unique(shAgglosLUT(x)), startNodeSeg(~zeroStartNode));

firstNodeNotSh = indexToSh == 0;
ff.nodes(firstNodeNotSh) = [];
ff.segIds(firstNodeNotSh) = [];
ff.startNode(firstNodeNotSh) = [];
ff.comments(firstNodeNotSh) = [];
ff.filenames(firstNodeNotSh) = [];
ff.neighbours(firstNodeNotSh) = [];
indexToSh(firstNodeNotSh) = [];
stat.firstNodeNotSh(firstNodeNotSh) = true;
Util.log([num2str(sum(firstNodeNotSh)) ' ff were removed bc startNode not SH'])

% further analysis of attachment for these ff
stat.unattached = false(numel(ff.nodes),1);
stat.lastNodeGray = false(numel(ff.nodes),1);
% stat.multipleOVL = false(numel(ff.nodes),1);
% stat.mismatchOVL = false(numel(ff.nodes),1);
% stat.singleOVL = false(numel(ff.nodes),1);

% find last node per tracing using neighbourhood information
ff_lastNode = cellfun(@(x) mode(nonzeros(x(end,:))),ff.neighbours); % some still 0
ff_lastNodeGray = ff_lastNode ==0;
stat.lastNodeGray(ff_lastNodeGray) = true;

indexToCand = zeros(size(ff_lastNode));
indexToCand(~ff_lastNodeGray) = candLUT(ff_lastNode(~ff_lastNodeGray)); % can be 0 for not found cand
toEmpty = indexToCand ==0;
indexToCand = num2cell(indexToCand);
indexToCand(toEmpty) = {[]};

%[~,~,indexToCand] = cellfun(@(x) find(candLUT(nonzeros(x)),1,'last'),ff.segIds,'uni',0);
indexToCandEmpty = cellfun('isempty',indexToCand);
stat.unattached(indexToCandEmpty) = true;
Util.log([num2str(sum(stat.unattached)) ' ff did not get attached to candAgglos']);

% do the patching in
nonCandAgglosIdxToDel = [];
Util.log(' Now processing per tracing...')
indexToCandNonEmpty = find(~indexToCandEmpty);
endStep = numel(indexToCandNonEmpty); counter = 1;
tic;
for curIdx = indexToCandNonEmpty' % has to be a list/row vector
    curSegIds = shAgglos{indexToSh(curIdx)};
    curDendIdx = candAgglosIdx(indexToCand{curIdx});

    %{
    idxShToAgglosCands = unique(nonzeros(candLUT(curSegIds)));
    if any(idxShToAgglosCands)
        stat.singleOVL(curIdx) = true;
        if numel(idxShToAgglosCands)>1; stat.multipleOVL(curIdx) = true;  continue; end
        if candAgglosIdx(idxShToAgglosCands)~=curDendIdx; stat.mismatchOVL(curIdx) = true; continue; end
    end
    %}
    
    idxShToAgglosNonCands = unique(nonzeros(nonCandLUT(curSegIds)));
    curNonCandIdx = setdiff(idxShToAgglosNonCands,nonCandAgglosIdxToDel); 
    if ~isempty(curNonCandIdx)
        nonCandAgglosIdxToDel = vertcat(nonCandAgglosIdxToDel,...
                                                curNonCandIdx);
        for i=1:numel(curNonCandIdx)
            curSegIds = union(curSegIds,nonCandAgglos{curNonCandIdx(i)});
        end
    end

    % update dendAgglos
    attached(indexToSh(curIdx)) = curDendIdx;
    dendAgglos{curDendIdx} = ...
            union(dendAgglos{curDendIdx}, curSegIds);

    Util.progressBar(counter,endStep); counter = counter+1;
end

idxDel = nonCandAgglosIdx(nonCandAgglosIdxToDel);
dendAgglos(idxDel) = [];

% update attached to index new dendAgglos
attached = arrayfun(@(x) x - sum(idxDel<x),attached);

dendAggloLUT = Agglo.buildLUT(maxSegId,dendAgglos);

Util.log('Find wholecell indices ...');
somaAgglos = connectEM.getSomaAgglos(fullfile(aggloStateFolder,'somas_with_merged_somas.mat'),'all');
somaAgglos = somaAgglos(~cellfun(@isempty,somaAgglos)); % remove not found somata
somaSegIds = cell2mat(somaAgglos);

[~,ic] = unique(somaSegIds);
duplicates = somaSegIds(setdiff(1:numel(somaSegIds),ic));
% somaDuplicateIds = cellfun(@(x) any(intersect(x,duplicates)),somaAgglos);
somaAgglos = cellfun(@(x) setdiff(x,duplicates),somaAgglos,'uni',0);
somaSegIds = cell2mat(somaAgglos);
somaLUT(somaSegIds) = repelem(1:numel(somaAgglos),cellfun(@numel,somaAgglos));

dendriteSegIds = cat(1,dendAgglos{:});
[ismem,ind] = ismember(somaSegIds,dendriteSegIds);
indWholeCells = accumarray(somaLUT(somaSegIds(ismem))',dendAggloLUT(dendriteSegIds(ind(ismem)))',[],@mode);

indBigDends = Agglo.isMaxBorderToBorderDistAbove(p, 5000, dendAgglos);
indBigDends(indWholeCells) = false;
ffResult = ff;
info = Util.runInfo(false);
save(fullfile(aggloStateFolder,'dendrites_wholeCells_01_spine_attachment.mat'),...
            'shAgglos','attached','dendAgglos','indBigDends',...
            'indWholeCells','stat','info','ffResult');
%% make case distinctions
% unsolved: 10k
outDirNml = '/tmpscratch/sahilloo/L4/spines/spineHeadQueriesToLook/unsolved/';
if ~exist(outDirNml,'dir')
    mkdir(outDirNml);
end
m=load(fullfile(outDir,'spineHeadsToDendritesQueryState_2.mat'));
ff2 = m.ff;
commented = ~cellfun(@isempty,vertcat(ff2.comments));
ffC.nodes = ff2.nodes(commented);
ffC.segIds = ff2.segIds(commented);
ffC.startNode = ff2.startNode(commented);
ffC.comments = ff2.comments(commented);
ffC.filenames =ff2.filenames(commented);
ffC.neighbours = ff2.neighbours(commented);
unsolved = cellfun(@(x) any(regexp(x,'unsolved')),vertcat(ffC.comments{:}));

outfiles = vertcat(ffC.filenames(unsolved));
idx = randperm(numel(outfiles),20);
cellfun(@(x) copyfile(x,outDirNml),outfiles(idx));

% unattached to cand but attached to non-cand
outDirNml = '/tmpscratch/sahilloo/L4/spines/spineHeadQueriesToLook/attachedToNonCand/';
if ~exist(outDirNml,'dir')
    mkdir(outDirNml);
end
ind = indexToCandEmpty & ~ff_lastNodeGray;
attToNonCand = false(size(ff_lastNode));
attToNonCand(ind) = nonCandLUT(ff_lastNode(ind))~=0;

outfiles = vertcat(ffResult.filenames(attToNonCand));
idx = randperm(numel(outfiles),20);
cellfun(@(x) copyfile(x,outDirNml),outfiles(idx));

% unattached at all
outDirNml = '/tmpscratch/sahilloo/L4/spines/spineHeadQueriesToLook/unattached/';
if ~exist(outDirNml,'dir')
    mkdir(outDirNml);
end
notAttAtAll = false(size(ff_lastNode));
notAttAtAll(ind) = nonCandLUT(ff_lastNode(ind))==0;
outfiles = vertcat(ffResult.filenames(notAttAtAll));
idx = randperm(numel(outfiles),20);
cellfun(@(x) copyfile(x,outDirNml),outfiles(idx));


