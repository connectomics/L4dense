% after processing the manual spine head attachment queries, append SH and tracing to dendrites state
%{
setConfig;
config.outDir = '/tmpscratch/sahilloo/L4/forIso/';
outDir = config.outDir;
config.dendriteFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/dendrites_05.mat';                                                            
config.spineFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/spine_heads_and_attachment_03.mat';

Util.log('Loading stuff...');
config
m=load(config.param);
p = m.p;

maxId = Seg.Global.getMaxSegId(p);

points = Seg.Global.getSegToPointMap(p);
voxelSize = p.raw.voxelSize;
segmentMeta = load(config.meta);

m=load(config.spineFile);
shAgglos = m.shAgglos(~m.attached);
shEdges = m.shEdges(~m.attached);


%queryFile = '/tmpscratch/bstaffle/data/L4/spines/SpineHeadQueries.mat';   
%m = load(queryFile);
%queryPoints = m.queryPoints;
%queryPoints_center = m.queryPoints_center;

% look only in big dendrites
m = load(config.dendriteFile);
dendrites = m.dendrites(m.indBigDends);
dendriteAgglos = Superagglos.getSegIds(dendrites);


m=load(fullfile(outDir,'spineHeadsToDendritesQueryState.mat'));
ff = m.ff;

% statistics
stat = struct;
stat.unattached = false(numel(ff.nodes),1);
stat.firstNodeEmpty = false(numel(ff.nodes),1);
stat.firstNodeNotSh = false(numel(ff.nodes),1);
stat.commented = false(numel(ff.nodes,1),1);

% take only tracings that were commented solved
commented = ~cellfun(@isempty,vertcat(ff.comments));
ff.nodes(commented) = [];
ff.segIds(commented) = [];
ff.startNode(commented) = [];
ff.comments(commented) = [];
ff.filenames(commented) = [];
ff.neighbours(commented) = [];
stat.commented(commented) = true;
Util.log([num2str(sum(commented)) ' ff were removed bc commented'])

% find mapping between shAgglos and tracing startNodes
firstNodeEmpty = cellfun(@(x) isempty(x),ff.segIds); % some maybe empty
ff.nodes(firstNodeEmpty) = [];
ff.segIds(firstNodeEmpty) = [];
ff.startNode(firstNodeEmpty) = [];
ff.comments(firstNodeEmpty) = [];
ff.filenames(firstNodeEmpty) = [];
ff.neighbours(firstNodeEmpty) = [];
stat.firstNodeEmpty(firstNodeEmpty) = true;
Util.log([num2str(sum(firstNodeEmpty)) ' ff were removed bc startNode empty'])

% find start nodes
startNodeSeg = cellfun(@(x) x(1),ff.segIds);
 
% check if startNode was not in spineHead and remove those in ff and indexToSh
Util.log('Parent spinehead agglo for startNode...')
indexToSh = Agglo.findParentAggloForThisSeg(startNodeSeg,shAgglos,maxId);

firstNodeNotSh = indexToSh == 0;
ff.nodes(firstNodeNotSh) = [];
ff.segIds(firstNodeNotSh) = [];
ff.startNode(firstNodeNotSh) = [];
ff.comments(firstNodeNotSh) = [];
ff.filenames(firstNodeNotSh) = [];
ff.neighbours(firstNodeNotSh) = [];
indexToSh(firstNodeNotSh) = [];
stat.firstNodeNotSh(firstNodeNotSh) = true;
Util.log([num2str(sum(firstNodeNotSh)) ' ff were removed bc startNode not SH'])

% transform shAgglos to super agglos
Util.log('SH to superagglos...');
for i=1:size(shAgglos,1)
    %shSuperagglos(i) = Superagglos.transformAggloOldNewRepr(shAgglos(i),shEdges{i,:},segmentMeta);
    shSuperagglos(i) = Agglo.transformOldToNew(shAgglos(i),shEdges{i,:},points);
end
% ff to which dendrite
indexToDend = zeros(numel(ff.nodes),1);

% updated dendrites
dendritesWithSH = dendrites;
%}
% find part of tracings that has to be patched and where
Util.log(' Now processing per tracing...')
tic;
endStep = numel(ff.nodes);
for i=1:endStep
    trNodes = ff.nodes{i};
    trSegIds = ff.segIds{i};
    thisStartSeg = trSegIds(1);
    thisStartNode = points(thisStartSeg,:);
    thisStartNode(:,4) = thisStartSeg;

    % remove startNode
    trNodes(1,:) = []; 
    trSegIds(1) = [];

    % find parent agglo for all remaining nodes
    toDend = Agglo.findParentAggloForThisSeg(trSegIds,dendriteAgglos,maxId);
    toDend = toDend(:); % change to col    
    
    % find if any dend was reached, if not then go to next ff
    if ~any(toDend)
        stat.unattached(i) = true;
        continue;
    end
    
    % keep the first parent and cut off remaining nodes
    [trLastNodePosition,~,indexToDend(i)] = find(toDend,1);
    % check if last node position was first node in the tracing, meaning dend was one step away
    thisEndSeg = trSegIds(trLastNodePosition);
    thisEndNode = points(thisEndSeg,:);
    thisEndNode(:,4) = thisEndSeg;
    if trLastNodePosition==1
        
    else
    
    trNodes = trNodes(1:trLastNodePosition-1,:);
    trSegIds = trSegIds(1:trLastNodePosition-1);
    trNodes(:,4) = nan;

    % collect new edges
    trNodeCount = size(trNodes, 1);
    trEdges = zeros((trNodeCount - 1) + 2, 2);
    trEdges((1 + 1):end, 1) = 1:trNodeCount;
    trEdges(1:(end - 1), 2) = 1:trNodeCount;

    % combine dendrites and its sh and this tracing
    thisDendrite = dendrites(indexToDend(i));
    thisSh = shSuperagglos(indexToSh(i));
    thisTr = struct;
    thisTr.nodes = trNodes;
    thisTr.edges = trEdges;
    
    newAgglo = struct;
    newAgglo.nodes = cat(1,thisDendrite.nodes,thisSh.nodes,thisTr.nodes);

    % insert two new edges by finding start and end seg node position in new agglo
    thisStartNodePosition = find(ismember(thisSh.nodes,thisStartNode,'rows'));
    thisEndNodePosition = find(ismember(thisDendrite.nodes,thisEndNode,'rows'));  

    countDend = size(thisDendrite.nodes,1);
    dendOffset = countDend * ones(size(thisSh.edges));
    countDendSh = size(thisDendrite.nodes,1)+size(thisSh.nodes,1); 
    dendShOffset = countDendSh * ones(size(thisTr.edges));
    newAgglo.edges = cat(1,thisDendrite.edges, ...
                        dendOffset+thisSh.edges,...
                        dendShOffset+thisTr.edges );

    edgesToAdd = [thisStartNodePosition + countDend, countDend+1;...
                 thisStartNodePosition,countDendSh + size(thisTr.nodes,1)];
    newAgglo.edges = cat(1,newAgglo.edges,edgesToAdd);
    
    %{    
    thisTr.nodes([1 end],4) = [thisStartSeg;thisEndSeg];
    newAgglo = Superagglos.applyEquivalences([1,2,3],cat(1,thisDend,thisSh,thisTr));
    %}
    dendritesWithSH(indexToDend(i)).nodes = newAgglo.nodes;
    dendritesWithSH(indexToDend(i)).edges = newAgglo.edges;
    Util.progressBar(i,endStep);
end

% remove small dendrites that have been added by spines


