% process the spinehead to dendrites attachment queries
setConfig;
config.outDir = '/tmpscatch/sahilloo/L4/forIso/';
outDir = config.outDir;
config.dendriteFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/dendrites_05.mat';
config.spineFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/spine_heads_and_attachment_03.mat';
m=load(config.param);
p = m.p;

skeletonFolders = {'/tmpscratch/sahilloo/L4/spine_head_attachment_queries/'};
Util.log('Creating ff ...');
[ff.segIds, ff.neighbours, ff.filenames, ff.nodes, ff.startNode, ff.comments] ...
    = connectEM.lookupNmlMulti(p, skeletonFolders, false);

info = Util.runInfo(false);
save(fullfile(outDir,'spineHeadsToDendritesQueryState.mat'),'ff','info');
%%
% load dendrites
m = load(config.dendriteFile);
dendrites = Superagglos.getSegIds(m.dendrites);

totalQueries = numel(ff.nodes);

% find end node for each query
endNodeSeg = cellfun(@(x) x(find(x,1,'last')),ff.segIds,'uni',0); % some maybe empty
idxEmpty = cellfun(@isempty,endNodeSeg);
countEmpty = sum(idxEmpty);
Util.log([num2str(countEmpty) ' queries were empty out of ' num2str(totalQueries)]);

% assign ff to respective dendrites based on end node
attachedId = zeros(size(endNodeSeg));
attachedId(idxEmpty) = 0;
index = Agglo.findParentAggloForThisSeg(endNodeSeg(~idxEmpty),dendrites,p);
attachedId(~idxEmpty) = index;

% some analysis
commented = ~cellfun(@isempty,vertcat(ff.comments));
ff.comments(~commented) = {{''}};
unsolved = cellfun(@(x) any(regexp(x,'unsolved')),vertcat(ff.comments{:}));
soma =  cellfun(@(x) any(regexp(x,'Soma')),vertcat(ff.comments{:}));
attached = attachedId~=0; 

Util.log([num2str(sum(attached)) ' - ' num2str(100*sum(attached)/totalQueries,2) '%% queries were attached to dendrite']);
Util.log([num2str(sum(soma)) ' - ' num2str(100*sum(soma)/totalQueries,2) '%% queries were commented soma']);
Util.log([num2str(sum(~unsolved)) ' - ' num2str(100*sum(~unsolved)/totalQueries,2) '%% queries were commented solved']);
Util.log([num2str(sum(~unsolved & attached)) ' - ' num2str(100*sum(~unsolved & attached)/totalQueries,2) '%% queries were commented solved and attached']);
Util.log([num2str(sum(unsolved)) ' - ' num2str(100*sum(unsolved)/totalQueries,2) '%% queries were commented unsolved']);
Util.log([num2str(sum(unsolved & attached)) ' - ' num2str(100*sum(unsolved & attached)/totalQueries,2) '%% queries were commented unsolved but attached']);

unsolvedAndUnattached = unsolved & ~attached; 
solvedAndUnattached = ~unsolved & ~attached;
Util.log([num2str(sum(unsolvedAndUnattached)) ' - ' num2str(100*sum(unsolvedAndUnattached)/totalQueries,2) '%% queries were commented unsolved and remained unattached']);
Util.log([num2str(sum(solvedAndUnattached)) ' - ' num2str(100*sum(solvedAndUnattached)/totalQueries,2) '%% queries were commented solved but unattached']);
%{
% view in wK
Util.log('Creating nmls to view in wK')
outDir1 = '/tmpscratch/sahilloo/L4/spineHeadQueriesSolvedAndUnattached/';
outDir2 = '/tmpscratch/sahilloo/L4/spineHeadQueriesUnsolvedAndUnattached/';
outfiles1 = vertcat(ff.filenames(solvedAndUnattached));     
outfiles2 = vertcat(ff.filenames(unsolvedAndUnattached));     
cellfun(@(x) copyfile(x,outDir1),outfiles1);
cellfun(@(x) copyfile(x,outDir2),outfiles2);
%}
% Calculate overlap of all queries with segments
%[uniqueSegments, neighboursStartNode, nodesExcludedIdx, startNodeIdx] = cellfun(@connectEM.queryAnalysis, ...
%            ff.segIds, ff.neighbours, ff.nodes', ff.startNode', 'uni', 0);


