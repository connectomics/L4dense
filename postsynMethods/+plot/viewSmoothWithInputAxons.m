%viewSmoothWithInputAxons
% view in wK smooth dendrites with input axons

outDir = '/tmpscratch/sahilloo/L4/data/';
m = load('/gaba/u/mberning/results/pipeline/20170217_ROI/allParameter.mat');
p = m.p;
points = Seg.Global.getSegToPointMap(p);                                                                                                
voxelSize = p.raw.voxelSize;

% load dendrites all info
m=load(fullfile(outDir,'spineDistribution.mat'));                                                                                       
spineTable = m.spineTable;
m=load(fullfile(outDir,'dendritesPostQueryWithShafts.mat'));
dendritesTable = [spineTable table(m.dendritesShaftSegs)]; % add shaft segs to dendrites

% load all axons, axonBoutons and their labels
m=load(fullfile(outDir,'boutonTable.mat'));
boutonTable = m.boutonTable;

% load smooth dendrites with high prob
m=load(fullfile(outDir,'smoothPreAxon.mat'));
smoothPreAxon = m.smoothPreAxon;
smoothTable = spineTable(m.idxSmooth,:);
disp('Finished loading dendrites and their smooth indices and input axons \n');

%% view in wK smooth dendrites with input axons
idxToPlot = randperm(size(smoothTable,1),10);

for i=1:length(idxToPlot)
    nodes = smoothTable{idxToPlot(i),1};
    skel = Skeleton.fromMST({points(nodes{:},:)},voxelSize);
    skel.names(1) = {'smooth'};
    curAxonsCount = sum(smoothPreAxon{idxToPlot(i)});
    if curAxonsCount > 0
        curAxons = boutonTable{(smoothPreAxon{idxToPlot(i)}),1};
        curAxonsSF = boutonTable{(smoothPreAxon{idxToPlot(i)}),4};
        for j=1:curAxonsCount
            skelAxon =  Skeleton.fromMST({points(curAxons{j},:)},voxelSize);
            skelAxon.names(1) = {['sf: ' num2str(curAxonsSF(j))]};
            skel = skel.addTreeFromSkel(skelAxon);
            clear skelAxon
        end
        fprintf('Smooth dendrite %d has %d input axons \n',idxToPlot(i),curAxonsCount);
    else
        fprintf('Smooth dendrite %d has no input axons \n',idxToPlot(i));
    end
    skel = Skeleton.setParams4Pipeline(skel, p);
    skel.write(fullfile(outDir, ['nmls/smoothWInAxons_' num2str(i) '_' ...
        datestr(now,'yyyymmdd') '.nml']));
end

