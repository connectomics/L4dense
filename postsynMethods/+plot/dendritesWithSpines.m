% view dendrites with spines
outDir = '/tmpscratch/sahilloo/L4/data/';
m = load('/gaba/u/mberning/results/pipeline/20170217_ROI/allParameter.mat');
p = m.p;
points = Seg.Global.getSegToPointMap(p);
voxelSize = p.raw.voxelSize;

% test spine head attachment
m=load('/tmpscratch/amotta/l4/spine-attachment/20170725T200449/spine-heads-and-attachment.mat');
n=load('/tmpscratch/sahilloo/L4/data/dendritesPostQueryWithShafts.mat');
dendritesWithSpines = m.dendAgglos;
dendrites = n.dendritesPostQuery;
rng(0);
% view in wk
idxToPlot = randi(length(dendrites),1,10);
for i=1:length(idxToPlot)
    nodes = dendritesWithSpines(idxToPlot(i));
    if numel(dendritesWithSpines{idxToPlot(i)})==1
        fprintf('DendritesAgglo %d is single segId \n',idxToPlot(i));
        continue;
    end
    skel = Skeleton.fromMST({points(nodes{:},:)},voxelSize);
    skel.names(1) = {'agglo'};
    skel = Skeleton.setParams4Pipeline(skel, p);
    if numel(dendritesWithSpines{idxToPlot(i)}) == numel(dendrites{idxToPlot(i)})
        fprintf('DendritesAgglo %d has no spines attached \n',idxToPlot(i));
        %continue;
         skel.names(1) = {'agglo (no spines)'};
    end  
    skel.write(fullfile(outDir, ['nmls/dendritesWithSpines_' num2str(idxToPlot(i)) '.nml']));
end            

