function attached = findParentAggloForThisSeg(nodes,agglos,maxId)
% attached: idx in agglos where nodes segments id was found
%maxId = Seg.Global.getMaxSegId(p);
%nodes: array
% attached = 0 if seg was zero;

nodes = nodes(:);

lut = Agglo.buildLUT(maxId,agglos);

attached = zeros(size(nodes));

% find non zero nodes only
[r,c,nodesValid] = find(nodes);

attached(r) = arrayfun(@(x) lut(x),nodesValid); 

end
