% Based on
%   File: L4/+QueryAnalysis/findAISffOVL.m
%   Repository: https://gitlab.mpcdf.mpg.de/connectomics/loombas
%   Commit: 35aa0fffd038b00a23a0a1ad7bcb876d67024f0b
%
% Written by
%   Sahil Loomba <sahil.loomba@brain.mpg.de>
%   Alessandro Motta <alessandro.motta@brain.mpg.de>


clear;

%% Configuration
config = loadConfig('ex145_07x2_roi2017');

sahilDir = '/tmpscratch/sahilloo/L4/forIso';
aggloDir = fullfile(config.param.saveFolder, 'aggloState');

overlapFile = fullfile(aggloDir, 'axonDendriteQueryOverlaps_1.0.mat');
oldDendFile = fullfile(aggloDir, 'dendrites_flight_01.mat');

newDendFile = fullfile(aggloDir, 'dendrites_16.mat');
outFile     = fullfile(aggloDir, 'dendrites_17_a.mat');

info = Util.runInfo();

%% Loading data
voxelSize = config.param.raw.voxelSize;

Util.log('Loading segment data');
maxSegId = Seg.Global.getMaxSegId(config.param);
points = Seg.Global.getSegToPointMap(config.param);

Util.log('Loading old dendrites from %s', oldDendFile);
oldDend = load(oldDendFile);

Util.log('Loading new dendrites from %s', newDendFile);
newDend = load(newDendFile);

Util.log('Cleaning dendrites');
newDend.dendrites = SuperAgglo.clean(newDend.dendrites);

%% Translate agglomerate IDs
oldAgglos = Agglo.fromSuperAgglo( ...
    oldDend.dendrites(oldDend.indBigDends));
newAgglos = Agglo.fromSuperAgglo( ...
    newDend.dendrites(newDend.indBigDends));
newAggloLUT = Agglo.buildLUT( ...
    maxSegId, newAgglos, find(newDend.indBigDends)); %#ok

newAggloIdLUT = cellfun( ...
    @(ids) setdiff(newAggloLUT(ids), 0), ...
    oldAgglos, 'UniformOutput', false);

%% Preprocess dendrite → flight → axon overlapas
m = load(overlapFile);

overlapT = table;
overlapT.dendId = m.results.startDendrite;
overlapT.axonId = m.results.endAxon;

overlapT.nodes = m.results.ff.nodes(:);
overlapT.segIds = m.results.ff.segIds(:);
clear m;

% Require attachment to axon
overlapT(cellfun(@isempty, overlapT.axonId), :) = [];

% Clean-up
assert(all(cellfun(@isscalar, overlapT.dendId)));
overlapT.dendId = cell2mat(overlapT.dendId);

overlapT.dendIdOld = overlapT.dendId;
overlapT.dendId = newAggloIdLUT(overlapT.dendIdOld);

%% Limit to manually identified AIS
% Find "straight things"
m = load(fullfile(sahilDir, 'STfromDendAxOL.mat'));
straightIds = m.rInd_1;

% Find AIS from among the "straight things"
m = load(fullfile(sahilDir, 'idxAISinST.mat'));
aisIds = straightIds(m.idxAISinST);

aisT = overlapT;
aisT = aisT(aisIds, :);

% Postprocessing
% No. 1 has large merger in dendrite
% No. 14 has a tracing with much more than just AIS
aisT([1, 14], :) = [];

% Sanity check
assert(all(cellfun(@isscalar, aisT.dendId)));
aisT.dendId = cell2mat(aisT.dendId);

%% Prepare dendrite super-agglomerates
pickupIds = find(~newDend.indBigDends);
pickupAgglos = Agglo.fromSuperAgglo(newDend.dendrites(pickupIds));
pickupLUT = Agglo.buildLUT(maxSegId, pickupAgglos, pickupIds);

%% Build super-agglomerates
% Prepare output
out = struct;
out.dendrites = newDend.dendrites;

% Initialize state
usedPickupIds = zeros(0, 1);

for curIdx = 1:size(aisT, 1)
    curDendId = aisT.dendId(curIdx);
    
    curAisSegIds = nonzeros(aisT.segIds{curIdx});
    curAisSegIds = unique(curAisSegIds, 'stable');
    
    % Build flight path-based super-agglomerate
    % Please note that this assumes linear flight paths!
    curAis = struct;
    curAis.nodes = horzcat( ...
        points(curAisSegIds, :), curAisSegIds);
    curAis.edges = horzcat( ...
        (2:numel(curAisSegIds))' - 1, ...
        (2:numel(curAisSegIds))');
    
    % Merge in picked-up agglomerates
    curPickupIds = setdiff(pickupLUT(curAisSegIds), 0);
    curPickupIds = setdiff(curPickupIds, usedPickupIds);
    usedPickupIds = union(usedPickupIds, curPickupIds);
    
    for curPickupId = reshape(curPickupIds, 1, [])
        curAis = SuperAgglo.merge( ...
            curAis, out.dendrites(curPickupId));
    end
    
    % Merge into large dendrite agglomerate
    out.dendrites(curDendId) = SuperAgglo.merge( ...
        out.dendrites(curDendId), curAis);
end

%% Complete output
Util.log('Checking dendrites');
SuperAgglo.check(out.dendrites)

% Build masks
out.indBigDends = newDend.indBigDends;
out.indAIS = false(size(out.dendrites));
out.indAIS(aisT.dendId) = true;

% Sanity check
assert(all(out.indBigDends(out.indAIS)));

% Remove picked-up agglomerates
mask = true(size(out.dendrites));
mask(usedPickupIds) = false;

out = structfun( ...
    @(vals) vals(mask, :), out, ...
    'UniformOutput', false);

% Save result
out.info = info;

%% Save result
Util.log('Writing results to %s', outFile);
Util.saveStruct(outFile, out);

