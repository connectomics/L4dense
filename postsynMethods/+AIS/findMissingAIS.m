% GT wholecells have 60 ais but I found 58 only from wholeCells superagglos
% find which GT axon/ais is missing from current ais state

setConfig;
load(config.param)
aggloFolder = fullfile(p.saveFolder,'aggloState/');

m = load(fullfile(aggloFolder,'dendrites_18_b.mat'))
ais = m.dendrites(m.indAIS);
aisSeg = Superagglos.getSegIds(ais);


folder = '/tmpscratch/sahilloo/L4/L4_borderCells_AIS_GT/';
files = dir(fullfile(folder,'*.nml'));

skelBorder = cellfun(@(x) skeleton(fullfile(folder,x)),{files.name});

centerCellAIS = '/tmpscratch/sahilloo/L4/all_axons_only_centerWC.nml'
skelCenter = skeleton(centerCellAIS);

Util.log('Getting segIds for GT AIS')
segBorder = arrayfun(@(x) Seg.Global.getSegIds(p,x.getNodes),skelBorder,'uni',0);
segBorder = segBorder(:);

segCenter = cell(skelCenter.numTrees,1);
for i =1:skelCenter.numTrees
    treeInd = false(skelCenter.numTrees,1);
    treeInd(i) = true;
    thisNodes = getNodes(skelCenter,treeInd);
    segCenter{i} = Seg.Global.getSegIds(p,thisNodes);
end
save(fullfile(config.outDir,'missingAISstate.mat'),'segBorder','segCenter')

maxSegId = Seg.Global.getMaxSegId(p);
aisLUT = Agglo.buildLUT(maxSegId, aisSeg);

segAll = vertcat(segBorder,segCenter);
for i=1:numel(segAll)
    this(i) = any(aisLUT(nonzeros(segAll{i})));
end

Util.log(['AIS not found are: ' num2str(find(~this))]);
