setConfig;
config.outDir = '/tmpscratch/sahilloo/L4/forIso/';
outDir = config.outDir;
config.dendriteFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/dendrites_03.mat';                                                            
config.spineFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/spine_heads_and_attachment_03.mat';

Util.log('Loading stuff...');
config
m=load(config.param);
p = m.p;

outDirForIso = '/tmpscratch/sahilloo/L4/forIso/straightThingsISO/';

% load info on 423 straight things
m=load('/tmpscratch/sahilloo/L4/forIso/straightThingsState.mat');
agglos = m.straightThingsSA; % these are superagglos

m = load(fullfile(outDir,'straightThingsFromMH.mat'));
indexAD = m.agglosSortValues == 1;
agglosToPrint = m.agglosSorted(indexAD);
agglosToPrint = Superagglos.getSegIds(agglosToPrint);

% make ply files for straight things
Util.log('Now running main viz')
p.seg.root = ['/gabaghi/wKcubes_archive/2012-09-28_ex145_07x2_ROI2017/segmentation/1/'];
p.seg.backend = 'wkwrap';
%Visualization.exportAggloToAmira(p,agglos,outDir);
Visualization.exportAggloToAmira(p,agglosToPrint,outDirForIso,'reduce',0.5,'smoothSizeHalf',4,'smoothWidth',8);

