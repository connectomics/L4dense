% find dendrites with flight paths that touch up and down (based on all node coords, not seg CoMs)
setConfig;
config.outDir = '/tmpscratch/sahilloo/L4/dataPostSyn/';
outDir = config.outDir;
config.dendriteFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/dendrites_17_a.mat';

Util.log('Loading stuff...');
config
m=load(config.param);
p = m.p;

m=load(config.dendriteFile);
indBigDends = m.indBigDends;
dendrites = m.dendrites(indBigDends);

% extract node coords from superagglos
nodes = {dendrites(:).nodes};
points = cellfun(@(x) x(:,1:3), nodes,'uni',0);

tol = 500; % ~ 6um

bounds = p.bbox;
xbounds = bounds(1,:); ybounds = bounds(2,:); zbounds = bounds(3,:);
xboundsDown = xbounds(1) + tol; 
xboundsTop = xbounds(2) - tol;

Util.log('filtering touch to up and down based on nodes coordinates...')
idxTD = cellfun(@(p) any(p(:,1)<=xboundsDown) & any(p(:,1)>=xboundsTop), points);
topDownThings = dendrites(idxTD);
save(fullfile(config.outDir,'topDownThingsState.mat'),'topDownThings','idxTD');
% not much different from the previous approach of using the segment COMs
