
% plot AD/AIS
%choose offdir or uncommented as agglos to print

setConfig;
config.outDir = '/tmpscratch/sahilloo/L4/forIso/';
outDir = config.outDir;
config.dendriteFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/dendrites_03.mat';                                                            
config.spineFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/spine_heads_and_attachment_03.mat';
plotFlag = false;

Util.log('Loading stuff...');
config
m=load(config.param);
p = m.p;
points = Seg.Global.getSegToPointMap(p); 
voxelSize = p.raw.voxelSize;

%sourceFile = '/tmpscratch/sahilloo/L4/forIso/ADdiscovery.xlsx';
sourceFile = '/tmpscratch/sahilloo/L4/forIso/ADdiscovery_v2.xlsx';
outDirForPdf = '/tmpscratch/sahilloo/L4/forIso/straightThingsPDF/';
outDirForIso = '/tmpscratch/sahilloo/L4/forIso/straightThingsISO/';
outDirForNml = '/tmpscratch/sahilloo/L4/forIso/straightThingsNML/';

% load info on 423 straight things
m=load('/tmpscratch/sahilloo/L4/forIso/straightThingsState.mat');
agglosST = m.straightThingsSA; % these are superagglos

%[~,~,xl] = xlsread(sourceFile,'E8:J235'); v1
[~,~,xl] = xlsread(sourceFile,'E8:M235'); % v2

xlTable = cell2table(xl);
xlTable.Properties.VariableNames = {'ID','AD','wc','Admerger','othermerger','comment','empty1','empty2','sorting'};

% find nan comments and make the empty
% find uncommented
uncommented = logical(cellfun(@(x) sum(isnan(x)),xlTable.comment));
xlTable.comment(uncommented) = {''};
uncommentedIds = xlTable.ID(uncommented);
% one is nan
uncommentedIds(isnan(uncommentedIds)) = [];
agglosUncommented = agglosST(uncommentedIds);

% find 'off-dir'tokens = {'off dir','direction off'};
tokens = {'off'};
offDir = cellfun(@(x) any(regexp(x,tokens{1})),xlTable.comment);
offDirIds = xlTable.ID(offDir);
agglosOffDir = agglosST(offDirIds);

% sort them by last column
xlTableSorted = xlTable;
valueSort = xlTableSorted.sorting;
xlTableSorted(valueSort ==0 ,:)=[];
valueSort = xlTableSorted.sorting;
[~,idxSort] = sort(valueSort,'ascend');
xlTableSorted = xlTableSorted(idxSort,:);
sortedCellIds = xlTableSorted.ID;
agglosSorted = agglosST(sortedCellIds);
agglosSortValues = xlTableSorted.sorting;

save(fullfile(outDir,'straightThingsFromMH.mat'),'agglosUncommented','agglosOffDir','uncommentedIds','offDirIds','agglosSorted','agglosSortValues','sortedCellIds');

% plot
offset = [ 128, 128, 128 ].* [ 11.239999771118164, 11.239999771118164, 28 ];
bbox = [5504,8448,3328] .* [ 11.239999771118164, 11.239999771118164, 28 ];
bbox = bsxfun(@plus,[zeros(3,1), bbox'],offset');

Util.log('Now plotting...')

agglosToPrint = agglosSorted;
classNames = {'AD','fromBelow','fromAbove','oblique'};
%{
for i=1:numel(agglosToPrint)
    fileName(i) = ['ID_' num2str(sortedCellIds(i)) '_' classNames{agglosSortValues(i)}];
end
%}
tic;
endStep = numel(agglosToPrint);
for i =1:endStep
    skel = Superagglos.toSkel(agglosToPrint(i)); 
%    nodes = agglosToPrint(i);
%    skel = Skeleton.fromMST({points(nodes{:},:)},voxelSize);
    fileName = [classNames{agglosSortValues(i)} '_ID_' num2str(sortedCellIds(i))];
    skel = Skeleton.setParams4Pipeline(skel, p);
    skel.write(fullfile(outDirForNml, ['straightThing_' fileName '.nml']));
    if plotFlag
    fig = figure('Units','centimeters','Position',[0 -2 23 30]);
    hold on;
    Visualization.plotBbox(bbox);
    skel.plot
    axis equal
    view(2)
    xlim([0 70000])
    ylim([0 100000])
    zlim([0 100000])
    
    set(findall(gcf,'-property','FontSize'),'FontSize',20);
    set(gcf, 'PaperSize', [29.7 21]);
    set(gcf, 'PaperPosition', [0 0 29.7 21]);

    pdfFile = fullfile(outDirForPdf,['straightThing_' fileName '.nml']);
    print(gcf,pdfFile,'-dpdf')
    close(fig);
    end
    Util.progressBar(i,endStep)
end


