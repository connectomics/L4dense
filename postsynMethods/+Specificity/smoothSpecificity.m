% script to determine smooth dendrite specificity (using synapses)
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>
% modified by: SL
% load smooth, axons and synapses
if ~exist('plotFlag','var') || isempty('plotFlag')
    plotFlag = true;
end
%m = load(config.param);
%p = m.p;
p = Gaba.getSegParameters('ex145_ROI2017');
outDir = config.outDir;

% smooth
m = load(fullfile(outDir,'dendritesSmoothInputState.mat'));
smoothDenAgglos = m.smoothTable{:,1};

% axons
m = load(fullfile(outDir,'axonTable.mat'));
axonAgglos = m.axonTable{:,1};

% synapses
m = load(config.synapseFile);
synapses = m.synapses;

%% get synapse agglo mappings
[syn2Agglo, pre2syn, post2syn, connectome] = ...
    L4.Synapses.synapsesAggloMapping(synapses, axonAgglos, smoothDenAgglos);


%% examples to nml (connections with > 1 synapses)
if plotFlag
    [graph, segmentMeta, borderMeta] = Seg.IO.loadGraph(p, false);
    
    idx = find(cellfun(@length, connectome.synIdx) > 1);
    i = 4;
    skel = L4.Agglo.agglo2Nml( ...
        [axonAgglos(connectome.edges(idx(i), 1)); ...
        smoothDenAgglos(connectome.edges(idx(i), 2))], segmentMeta.point);
    skel = L4.Agglo.agglo2Nml( ...
        synapses.edgeIdx(connectome.synIdx{idx(i)}, 1), ...
        borderMeta.borderCoM, skel);
    skel.write(fullfile(outDir, 'nmls/ConnectomeEntrySample.nml'));
end

%% specificity

% axons onto smooth
axonIdx = unique(connectome.edges(:,1));

% get synapses onto smooth
isSynOntoSmooth = false(size(synapses, 1), 1);
isSynOntoSmooth(cell2mat(connectome.synIdx)) = true;

% get synapse counts for axons
totalNoSyn = cellfun(@length, pre2syn(axonIdx));
synOntoSmooth = cellfun(@(x)sum(isSynOntoSmooth(x)), pre2syn(axonIdx));

% specificities
sp_minus_one = (synOntoSmooth - 1)./(totalNoSyn - 1);

% all information to table
axonsOntoSmooth = table(axonIdx, totalNoSyn, synOntoSmooth, ...
    sp_minus_one, 'VariableNames', {'axonIdx', 'totalNoSyn', ...
    'synOntoSmooth', 'specificity_minus_one'});

save(fullfile(outDir,'axonsOntoSmooth.mat'),'axonsOntoSmooth');

%% fractional specificity

if plotFlag
    fig = figure;
    histogram(axonsOntoSmooth.specificity_minus_one)
    xlabel('Fraction of synapses onto smooth (minus one)')
    ylabel('Count')
    a = gca;
    Util.setPlotDefault(a,true);
    saveas(fig,fullfile(outDir,'figures/smoothSpecificityHist.png'))
    close all
    
    fig = figure;
    scatter(axonsOntoSmooth.specificity_minus_one, ...
        axonsOntoSmooth.totalNoSyn);
    xlabel('Fraction of synapses onto smooth (minus one)')
    ylabel('Total number of synapses (minus one)')
    a = gca;
    a.YScale = 'log';
    Util.setPlotDefault(a,true);
    saveas(fig,fullfile(outDir,'figures/smoothSpecificityScatter.png'))
    close all
end

