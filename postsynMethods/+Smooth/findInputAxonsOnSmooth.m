function [smoothPreAxon, idxSmooth] = findInputAxonsOnSmooth()
% find axons input onto smooth dendrites

outDir = '/tmpscratch/sahilloo/L4/data/';
m = load('/gaba/u/mberning/results/pipeline/20170217_ROI/allParameter.mat');
p = m.p;
points = Seg.Global.getSegToPointMap(p);                                                                                                
voxelSize = p.raw.voxelSize;

% load dendrites all info
m=load(fullfile(outDir,'spineDistribution.mat'));                                                                                       
spineTable = m.spineTable;
m=load(fullfile(outDir,'dendritesPostQueryWithShafts.mat'));
dendritesTable = [spineTable table(m.dendritesShaftSegs)]; % add shaft segs to dendrites

% load all axons, axonBoutons
m=load(fullfile(outDir,'boutonTable.mat'));
axonBoutons = m.boutonTable{:,2};

% load pre-post synaptic interfaces 
m=load(fullfile(outDir, 'interfacesPrePost.mat'));
preInterfaces = m.preInterfaces;

% collect post-syn segments for each dendrite: shaft and spine
allSh = dendritesTable{:,3};
allShf = dendritesTable{:,6};
dendritesPostSeg = cellfun(@(x,y) vertcat(x{:},y), allSh,allShf,'uni',0);

% load smooth dendrites with high prob
idxSmooth = Smooth.viewSmoothDendritesAllIn({[],5},{100,300});
disp('Finished loading dendrites and their smooth indices');
smoothPostSeg = dendritesPostSeg(idxSmooth);

% find axons input onto each dendrite
tic;
smoothPreAxon = cellfun(@(x) findAxonsInputOnThisDendrite(x,preInterfaces,axonBoutons),smoothPostSeg,'uni',0);
save(fullfile(outDir,'smoothPreAxon.mat'),'smoothPreAxon','idxSmooth');
end

function allPreSynAxons = findAxonsInputOnThisDendrite(allPostSeg, preInterfaces,axonBoutons)
    % find all pre-syn segments
    idxSyn = ismember(preInterfaces(:,2),allPostSeg); % pre syn segments in first column, post in second
    allPreSyn = sort(preInterfaces(idxSyn,1));
    % find boutons/axons part of each pre-syn
    allPreSynAxons = cell2mat(cellfun(@(x) any(ismembc(vertcat(x{:}),allPreSyn)),axonBoutons,'uni',0));
    toc;
end
