function mainScriptForInputSynapsesOnSmooth(config,sfThr,plotFlag)

if ~exist('plotFlag','var') || isempty(plotFlag)
    plotFlag = true;
end

outDir = config.outDir;
outfile  = fullfile(outDir,'dendritesSmoothInputState.mat');

% load axon table and extend it with relabels
Util.log('Loading and extending axon table with chosen sfThr spine relabels')
m = load(fullfile(outDir,'axonTable.mat'));
axonTable = Axon.extendAxonTable(config,m.axonTable,sfThr);

% load spine table and extend it
Util.log('Loading and extending spine table with calculated spine relabels')
m = load(fullfile(outDir,'spineTable.mat'));
spineTable = Spines.extendSpineTable(config,m.spineTable);

% load smooth index
Util.log('Loading smooth index');
m=load(fullfile(outDir,'dendritesSmoothState.mat'));
idxSmooth = m.idxSmooth;

smoothTable = spineTable(idxSmooth,:);
if plotFlag
    % scatter plot
    Util.log('Plotting "e"/"i" on smooth dendrites')
    inputSynapseLabels = smoothTable{:,7};
    fig = figure;
    excCount = cell2mat(cellfun(@(x) sum(x==true), inputSynapseLabels,'uni',0));
    inhCount = cell2mat(cellfun(@(x) sum(x==false), inputSynapseLabels,'uni',0));
    scatter(inhCount,excCount,'mo')
    axis('equal')
    ylabel('e')
    xlabel('i');
    hold on;
    coef_fit = polyfit(inhCount,excCount,1);
    yfit = coef_fit(1)*inhCount + coef_fit(2);
    plot(inhCount,yfit,'r-.')
    legend({'',['E/I = ' num2str(coef_fit(1))]})
    title('E and I synapses input per smooth')
    hold off
    saveas(fig,fullfile(outDir,'figures/synapsesEIPerSmoothScatter.png'));
    close all
    fig = figure;
    subplot(3,1,1)
    % histogram of e/i
    out = excCount./inhCount;
    out(out==Inf)=[];
    histogram(out,'DisplayStyle','stairs')
    xlabel('e/i')
    ylabel('# of smooth dendrites')
    title('Histogram of smooth e/i')
    comment = ['μ±σ: ' num2str(nanmean(out)) ' +/- ' num2str(nanstd(out))];
    legend({comment})
    % histogram of e/e+i
    subplot(3,1,2)
    out = excCount./(excCount+inhCount);
    histogram(out,'DisplayStyle','stairs')
    xlabel('e/e+i')
    ylabel('# of smooth dendrites')
    title('Histogram of smooth e/e+i')
    comment = ['μ±σ: ' num2str(nanmean(out)) ' +/- ' num2str(nanstd(out))];
    legend({comment})
    % histogram of e-i/e+i
    subplot(3,1,3)
    out = (excCount-inhCount)./(excCount+inhCount);
    histogram(out,'DisplayStyle','stairs')
    xlabel('e-i/e+i')
    ylabel('# of smooth dendrites')
    title('Histogram of smooth e-i/e+i')
    comment = ['μ±σ: ' num2str(nanmean(out)) ' +/- ' num2str(nanstd(out))];
    legend({comment});  
    saveas(fig,fullfile(outDir,'figures/synapsesEIPerSmoothHist.png'));
    close all  
end
                                                                                                                                        
Util.log('Saving results for smooth table extended')
info = Util.runInfo(false);
save(outfile,'smoothTable','info');

end
