function idxSmooth = mainScriptForSmooth(config,preScoreRange,postScoreRange,wkFlag)
% Description:
%    main script to find smooth dendrites. Call dependent functions and analyze here
if ~exist('wkFlag','var') || isempty(wkFlag)
    wkFlag = true;
end

outDir = config.outDir;
outfile = fullfile(outDir,'dendritesSmoothState.mat');
thrLowSD = 0.25;
thrLongPL = 10;
%preScoreRange = {[],5};
%postScoreRange = {100,300};

m = load(config.param);
p = m.p;
points = Seg.Global.getSegToPointMap(p);                                                                                                
voxelSize = p.raw.voxelSize;

% load spine table
Util.log('Now loading spine table');
m=load(fullfile(outDir,'spineTable.mat'));
spineTable = m.spineTable;

idxLowSD = spineTable.shDensity < thrLowSD;
idxLongPL = spineTable.dendriteLengths > thrLongPL;

% use typeEM scores
Util.log('Now loading typeEM agglo scores');
inputfile = fullfile(outDir,'dendritesTypeEMScores.mat');
if exist(inputfile,'file')
    m=load(inputfile);
    typeEMscores =m.scores;
else
    typeEMscores = Util.typeEMScoresForAgglos(config,spineTable.dendritesExt,'dendritesTypeEMScores');
end
g=[typeEMscores(:).astrocyte]; a=[typeEMscores(:).axon]; d=[typeEMscores(:).dendrite];
idxTypeEM = (d>a & d>g)';% relative thr  
clear a g d

% load post vs pre synapse scores
Util.log('Now loading pre-post agglo scores');
m=load(fullfile(outDir,'dendritesPrePostSynapseScores.mat'));
post = [m.scores(:).post];
pre  = [m.scores(:).pre];
idxPrePost = findIdxForPrePostRange(preScoreRange,postScoreRange,pre,post);
clear m

% combine all in
idxSmooth = idxLowSD & idxLongPL & idxTypeEM & idxPrePost;
smoothTable = spineTable(idxSmooth,:);
smoothTypeEMscores = typeEMscores(idxSmooth);
smoothPre = pre(idxSmooth);
smoothPost = post(idxSmooth);

Util.log('Found %d smooth dendrites',size(smoothTable,1));

if any(idxSmooth) & wkFlag
    % view smooth dendrites in wK
    Util.log('Now writing nmls to view in wK...')
    if ~exist([outDir 'nmls'],'dir')
        mkdir([outDir 'nmls']);
    end
    % name output nmls to score ranges
    outrange = [num2str(preScoreRange{1}) '-' num2str(preScoreRange{2}) '_'...
                 num2str(postScoreRange{1}) '-' num2str(postScoreRange{2})]; 
    count = ceil(0.80*size(smoothTable,1));
    rng(0);
    idxToPlot = randperm(size(smoothTable,1),count);
    for i=1:length(idxToPlot)
        nodes = smoothTable{idxToPlot(i),1};
        skel = Skeleton.fromMST({points(nodes{:},:)},voxelSize);
        skel.names(1) = {['- sd:' num2str(smoothTable{idxToPlot(i),5}) ' - '...
            num2str(smoothTable{idxToPlot(i),2}) 'um - ' ...
            num2str(smoothTypeEMscores(idxToPlot(i)).astrocyte,'%.2f') '/' ...
            num2str(smoothTypeEMscores(idxToPlot(i)).axon,'%.2f') '/' ...
            num2str(smoothTypeEMscores(idxToPlot(i)).dendrite,'%.2f') ' - pre/post: ' ...
            num2str(smoothPre(idxToPlot(i))) '/' num2str(smoothPost(idxToPlot(i)))]};
        skel = Skeleton.setParams4Pipeline(skel, p);
        skel.write(fullfile(outDir, ['nmls/dendritesSmooth ' outrange '_' num2str(i) '_' datestr(now,'yyyymmdd') '.nml']));
    end
    Util.log('Finished creating nmls to view');
else
    Util.log('Agglos have nothing to meet your conditions! or wkFlag was false \n');
end

% save results                                                                                                                          
info = Util.runInfo(false);
Util.log('Saving results for smooth');
save(fullfile(outDir,'dendritesSmoothState.mat'),'idxSmooth','smoothTable','info');

end

function idx = findIdxForPrePostRange(preScoreRange,postScoreRange,pre,post)
    idx = true(size(pre));
    idx =  pre >= preScoreRange{1} &  pre <= preScoreRange{2} & post >= postScoreRange{1} & post <= postScoreRange{2}; 
    if isrow(idx)
        idx = idx';
    end
end
