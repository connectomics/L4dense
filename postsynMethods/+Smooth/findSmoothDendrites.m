% findSmoothDendrites
outDir = '/tmpscratch/sahilloo/L4/data/';
m = load('/gaba/u/mberning/results/pipeline/20170217_ROI/allParameter.mat');
p = m.p;
points = Seg.Global.getSegToPointMap(p);                                                                                                
voxelSize = p.raw.voxelSize;

% test spine head attachment
m=load('/tmpscratch/sahilloo/L4/data/dendritesPostQueryStateLengths.mat');
dendriteLengths = m.aggloLens;
m=load('/tmpscratch/amotta/l4/spine-attachment/20170725T200449/spine-heads-and-attachment.mat');
dendritesExt = m.dendAgglos;
n=load('/tmpscratch/sahilloo/L4/data/dendritesPostQueryWithShafts.mat');
dendrites = n.dendritesPostQuery; 
% smooth dendrites did not get assigned any spine head
% some spine heads were already in dendAgglo so len_den agglo remained same after attachment
idx_smooth= ~ismember([1:length(dendritesExt)],m.attached);
fprintf('Number of smooth dendrites: %d includes single segment dendrites \n',sum(idx_smooth));
dendritesSmooth = dendritesExt(idx_smooth); % still includes dendAgglo with single segment

% save results
save(fullfile(outDir,'dendritesSmoothState.mat'),'dendritesSmooth','idx_smooth','dendrites','dendritesExt');

% smooth dendrites
dendritesSmoothLengths = dendriteLengths(idx_smooth);

% view smooth dendrites in wK
rng(0);
idxToPlot = randi(length(dendritesSmooth),1,100);
%[~,sortIdx] = sort(dendritesSmoothLengths,'descend');
%idxToPlot = sortIdx(1:100); % view 100 largest smooth dendrites

for i=1:length(idxToPlot)
    nodes = dendritesSmooth(idxToPlot(i));
    if numel(dendritesSmooth{idxToPlot(i)})==1
        fprintf('DendritesAgglo %d is single segId \n',idxToPlot(i));
        continue;
    end
    skel = Skeleton.fromMST({points(nodes{:},:)},voxelSize);
    skel.names(1) = {'aggloSmooth'};
    skel = Skeleton.setParams4Pipeline(skel, p);
    skel.write(fullfile(outDir, ['nmls/dendritesSmooth_' num2str(idxToPlot(i)) '.nml']));
end
%% agglos smooth with path lengths > 10um
rng(0);
m=load(fullfile(outDir,'spineDistribution.mat'));
spineTable = m.spineTable;
idx_smooth = spineTable{:,5}==0;
idx_longPathLength = spineTable{:,2}>10;
idx_out = idx_smooth & idx_longPathLength;
table_out  = spineTable(idx_out,:);

idxToPlot = randperm(size(table_out,1),100);

for i=1:length(idxToPlot)
    nodes = table_out{idxToPlot(i),1};
    if numel(table_out{idxToPlot(i),1}{1})==1
        fprintf('DendritesAgglo %d is single segId \n',idxToPlot(i));
        continue;
    end
    skel = Skeleton.fromMST({points(nodes{:},:)},voxelSize);
    skel.names(1) = {['agglo - ' num2str(table_out{idxToPlot(i),5}) ' - '...
        num2str(table_out{idxToPlot(i),2}) 'um']};
    skel = Skeleton.setParams4Pipeline(skel, p);
    skel.write(fullfile(outDir, ['nmls/dendritesSmoothLarge_' num2str(i) '.nml']));
end

%% agglos with low spine density and long path lengths > 10um
rng(0);
m=load(fullfile(outDir,'spineDistribution.mat'));
spineTable = m.spineTable;
idx_lowSpineDensity = spineTable{:,5}<0.25;
idx_longPathLength = spineTable{:,2}>10;
idx_out = idx_lowSpineDensity & idx_longPathLength;
table_out  = spineTable(idx_out,:);

idxToPlot = randperm(size(table_out,1),100);

for i=1:length(idxToPlot)
    nodes = table_out{idxToPlot(i),1};
    if numel(table_out{idxToPlot(i),1}{1})==1
        fprintf('DendritesAgglo %d is single segId \n',idxToPlot(i));
        continue;
    end
    skel = Skeleton.fromMST({points(nodes{:},:)},voxelSize);
    skel.names(1) = {['agglo - ' num2str(table_out{idxToPlot(i),5}) ' - '...
        num2str(table_out{idxToPlot(i),2}) 'um']};
    skel = Skeleton.setParams4Pipeline(skel, p);
    skel.write(fullfile(outDir, ['nmls/dendritesLowSpineDensity_' num2str(i) '.nml']));
end

