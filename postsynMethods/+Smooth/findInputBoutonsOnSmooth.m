function findInputBoutonsOnSmooth(sfThr,plotFlag)

if ~exist('plotFlag','var') || isempty(plotFlag)
    plotFlag = true;
end

if ~exist('sfThr','var') || isempty(sfThr)
    sfThr = 0.5;
end

outDir = '/tmpscratch/sahilloo/L4/data/';

% load dendrites all info
m=load(fullfile(outDir,'spineDistribution.mat'));                                                                                       
spineTable = m.spineTable;
m=load(fullfile(outDir,'dendritesPostQueryWithShafts.mat'));
dendritesTable = [spineTable table(m.dendritesShaftSegs)];
allSh = dendritesTable{:,3};
allShf = dendritesTable{:,6};
dendritesPostSeg = cellfun(@(x,y) vertcat(x{:},y), allSh,allShf,'uni',0);

% load smooth dendrites with high prob
idxSmooth = Smooth.viewSmoothDendritesAllInRange({[],5},{100,300});
disp('Finished loading dendrites and their smooth indices in given range');
smoothPostSeg = dendritesPostSeg(idxSmooth);
smoothLengths = spineTable{idxSmooth,2};
smoothAgglo = dendritesTable{idxSmooth,1};

% load all axons, axonBoutons and their labels
m=load(fullfile(outDir,'boutonTable.mat'));
boutonTable = m.boutonTable;
oldLabels = boutonTable{:,3};
aboveThr = num2cell(boutonTable{:,4}>sfThr);

newLabels = cellfun(@(x,y) relabelBoutons(x,y),oldLabels,aboveThr,'uni',0);
boutonTable = [boutonTable table(newLabels)];

% load pre-post synaptic interfaces 
m=load('/gaba/u/bstaffle/data/L4/Synapses/SynapseAgglos_v1.mat');
synapses = m.synapses;

% pick up boutons input on smooth dendrites
tic;
allBoutons = vertcat(boutonTable{:,2}{:});
allBoutonLabels = vertcat(boutonTable{:,5}{:});
allBSeg = sort(vertcat(allBoutons{:})); 

[inputBoutonsIdx, inputBoutonLabels,stats.notPicked,stats.notPickedEdgeIdx] = cellfun(@(x,y) ... 
findBoutonsInputOnThisDendrite(x,y,synapses, allBoutons,allBoutonLabels,allBSeg), smoothPostSeg,smoothAgglo,'uni',0);

save(fullfile(outDir,'smoothPreBoutons.mat'), 'inputBoutonsIdx','inputBoutonLabels','stats');

if plotFlag
    % scatter plot
    fig = figure;
    excCount = cell2mat(cellfun(@(x) sum(x==true), inputBoutonLabels,'uni',0));
    inhCount = cell2mat(cellfun(@(x) sum(x==false), inputBoutonLabels,'uni',0));
    scatter(inhCount,excCount,'mo')
    axis('equal')
    ylabel('e')
    xlabel('i');
    hold on;
    coef_fit = polyfit(inhCount,excCount,1);
    yfit = coef_fit(1)*inhCount + coef_fit(2);
    plot(inhCount,yfit,'r-.')
    legend({'',['E/I = ' num2str(coef_fit(1))]})
    title('E and I boutons input per smooth')
    hold off
    saveas(fig,fullfile(outDir,'figures/boutonsEIPerSmoothScatter.png'));
    close all
    fig = figure;
    subplot(3,1,1)
    % histogram of e/i
    out = excCount./inhCount;
    out(out==Inf)=[];
    histogram(out,'DisplayStyle','stairs')
    xlabel('e/i')
    ylabel('# of smooth dendrites')
    title('Histogram of smooth e/i')
    comment = ['μ±σ: ' num2str(nanmean(out)) ' +/- ' num2str(nanstd(out))];
    legend({comment})
    % histogram of e/e+i
    subplot(3,1,2)
    out = excCount./(excCount+inhCount);
    histogram(out,'DisplayStyle','stairs')
    xlabel('e/e+i')
    ylabel('# of smooth dendrites')
    title('Histogram of smooth e/e+i')
    comment = ['μ±σ: ' num2str(nanmean(out)) ' +/- ' num2str(nanstd(out))];
    legend({comment})
    % histogram of e-i/e+i
    subplot(3,1,3)
    out = (excCount-inhCount)./(excCount+inhCount);
    histogram(out,'DisplayStyle','stairs')
    xlabel('e-i/e+i')
    ylabel('# of smooth dendrites')
    title('Histogram of smooth e-i/e+i')
    comment = ['μ±σ: ' num2str(nanmean(out)) ' +/- ' num2str(nanstd(out))];
    legend({comment})
    saveas(fig,fullfile(outDir,'figures/boutonsEIPerSmoothHist.png'));
    close all  
    
    % histogram of unpicked pre-syn fraction
    notPickedFr = cellfun(@(x) sum(x)/length(x),stats.notPicked);
    fig = figure;
    histogram(notPickedFr,'NumBins',10,'DisplayStyle','stairs')
    xlabel('pre-syn fraction not picked by boutons')
    saveas(fig,fullfile(outDir,'figures/smoothPreSynNotPickedHist.png'));
    close all

end
end

function newLabels = relabelBoutons(labels,aboveThr)
    if aboveThr
        newLabels = true(size(labels,1),1);
    else
         newLabels = false(size(labels,1),1);
    end
end

function [inputBoutonsIdx, inputBoutonLabels,notPicked,notPickedEdgeIdx] = findBoutonsInputOnThisDendrite(allPostSeg,dendAgglo,synapses,allBoutons,allBoutonLabels,allBSeg)

    preInterfaces = [vertcat(synapses{:,2}{:}),vertcat(synapses{:,3}{:})];
    preInterfacesEdgeIdx = vertcat(synapses{:,1}{:});

    % find all pre-syn segments
    idxSyn = ismember(preInterfaces(:,2),allPostSeg); % pre syn segments in first column, post in second
    allPreSyn = double(sort(preInterfaces(idxSyn,1)));
    allPreSynEdgeIdx = preInterfacesEdgeIdx(idxSyn);

    % remove internal false positives pre-syn segments if any
    toDel = cell2mat(arrayfun(@(x) ismember(x,dendAgglo),allPreSyn,'uni',0));
    allPreSyn(toDel)=[];
    allPreSynEdgeIdx(toDel)=[];

    % find boutons part of each pre-syn
    inputBoutonsIdx = cell2mat(cellfun(@(x) any(ismembc(x ,allPreSyn)), allBoutons,'uni',0));
    inputBoutonLabels = allBoutonLabels(inputBoutonsIdx);

    % how many pre-syn did not get bouton assigned
    notPicked = ismember(allPreSyn,allBSeg);
    notPickedEdgeIdx = allPreSynEdgeIdx(notPicked);
    toc;
end
