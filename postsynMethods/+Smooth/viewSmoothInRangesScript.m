% check the counts of smooth across different pre-post score thr
setConfig;
%preScoreRange = {0,5};
%bounds = 10:10:60;
preScoreRange = {0,0.1};
bounds = [0.2,1.5];

out = table();
for i = 1:numel(bounds)-1
    postScoreRange = {bounds(i),bounds(i+1)};
    idxSmooth = Smooth.mainScriptForSmooth(config,preScoreRange,postScoreRange);
    count = sum(idxSmooth);
    out = [out; table(preScoreRange,postScoreRange,count)];
end

