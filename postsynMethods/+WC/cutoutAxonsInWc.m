% Use wholecellsv07 and cut out axons from the hiwi tracings

setConfig;
load(config.param);

outputFolder = fullfile(p.saveFolder, 'aggloState');
info = Util.runInfo();

load(fullfile(outputFolder,'wholeCells_07.mat'))

if ~exist('graph','var') || ~all(isfield(graph,{'edges','prob','borderIdx'}))
    graph = load(fullfile(p.saveFolder, 'graphNew.mat'),'edges','prob','borderIdx');
end
if  ~all(isfield(graph,{'neighbours','neighBorderIdx'}))
    [graph.neighbours, neighboursIdx] = Graph.edges2Neighbors(graph.edges);
    graph.neighBorderIdx = cellfun(@(x)graph.borderIdx(x), neighboursIdx, 'uni', 0);
    clear neighboursIdx
end

if ~exist('segmentMeta','var')
    segmentMeta = load(fullfile(p.saveFolder, 'segmentMeta.mat'));
end

if ~exist('borderMeta','var')
    borderMeta = load(fullfile(p.saveFolder, 'globalBorder.mat'), 'borderSize', 'borderCoM');
end

Util.log('Reordering wholecells using soma info...')
%% load all soma whole cell agglos
[somaAgglos,somaCoords,KAMINcoords] = connectEM.getSomaAgglos(fullfile(outputFolder,'somas_with_merged_somas.mat'),'all');
somaCoords = somaCoords(~cellfun(@isempty,somaAgglos)); % remove not found somata
somaAgglos = somaAgglos(~cellfun(@isempty,somaAgglos)); % remove not found somata
somaSegIds = cell2mat(somaAgglos);
% remove duplicate segIds
[~,ic] = unique(somaSegIds);
duplicates = somaSegIds(setdiff(1:numel(somaSegIds),ic));
% somaDuplicateIds = cellfun(@(x) any(intersect(x,duplicates)),somaAgglos);
[somaAgglos,ia] = cellfun(@(x) setdiff(x,duplicates),somaAgglos,'uni',0);
somaCoords = arrayfun(@(x) somaCoords{x}(ia{x},:),1:numel(ia),'uni',0);
somaSegIds = cell2mat(somaAgglos);
somaLUT(somaSegIds) = repelem(1:numel(somaAgglos),cellfun(@numel,somaAgglos));

newInd = arrayfun(@(x) mode(nonzeros(somaLUT(x.nodes(~isnan(x.nodes(:,4)),4)))),wholeCells);
assert(numel(unique(newInd)) == numel(newInd))
[~,newInd] = sort(newInd);
wholeCells = wholeCells(newInd);

Util.log('Reading wholecell nml files....')
dirWCGT = {'/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/centerWholeCellGT/axonInfo','/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/borderWholeCellGT_axonId'};
filenames = cell(0);
for a = 1:numel(dirWCGT)
    files = dir(fullfile(dirWCGT{a},'*.nml'));
    filenames = cat(2,filenames,cellfun(@(x) fullfile(dirWCGT{a},x),{files.name},'uni',0));
end
for n = 1:numel(wholeCells)  % make the axon field NaN for all wholeCells, to have those wCs labeled which do not have an axon marked in the GT
    wholeCells(n).axon = NaN(size(wholeCells(n).nodes,1),1);
end
wcLUT = Superagglos.buildLUT(wholeCells,segmentMeta.maxSegId);

skeletonsAll = cellfun(@(x) skeleton(x),filenames);
%skelSegIdsAll = connectEM.lookupNmlMulti(p,dirWCGT,false);

usedCells = zeros(numel(wholeCells),1);
skelsNotFound = [];
endStep = numel(filenames);
for f = 1:endStep
    Util.log(['Now processing file ' num2str(f) ' out of ' num2str(endStep)]);
    skel = skeletonsAll(f);
    numSkelNodes = cellfun(@(x) size(x,1),skel.nodes);
    skelLUT = repelem(1:numel(skel.nodes),numSkelNodes);
    warning('off')
    skelSegIds = Seg.Global.getSegIds(p,cell2mat(cellfun(@(x) x(:,1:3),skel.nodes,'uni',0)));  % extract the seg Ids of all skel nodes
%    skelSegIds = skelSegIdsAll{f};
    warning on
    
    ind = mode(nonzeros(wcLUT(nonzeros(skelSegIds)))); % get the whole cell overlapping the most with the skeleton in terms of segIds
    if isnan(ind) || sum(wcLUT(nonzeros(skelSegIds))==ind)/numel(nonzeros(skelSegIds)) < 0.33 % if no overlap found or overlap is less than one third of the skeleton
        warning('Found no corresponding whole Cell to skeleton from file %s. Trying to use somaAgglo as index...',filenames{f})
        ind = mode(nonzeros(somaLUT(nonzeros(skelSegIds))));
        if isnan(ind)
            % last try to retrieve correct whole cell by checking
            % neighbours of the skel, but only if skel is very small
            % (as it would be for a border soma)
            if sum(numSkelNodes) > 10 || isnan(mode(nonzeros(somaLUT(nonzeros(cat(1,graph.neighbours{nonzeros(skelSegIds)}))))))
                warning('Still no corresponding whole Cell to skeleton from file %s found! Skipping this one...',filenames{f})
                skelsNotFound = cat(1,skelsNotFound,f);
                continue
            else
                ind = mode(nonzeros(somaLUT(nonzeros(cat(1,graph.neighbours{nonzeros(skelSegIds)})))));
            end
        end
    end
    if usedCells(ind)
        error('Error skeleton %s: Cell %d already used from skeleton %s!',filenames{f},ind,filenames{usedCells(ind)})
    else
        usedCells(ind) = f;
    end
    
    if numel(numSkelNodes) > 1  % if there is only one skeleton, there was no axon found
        indAxonSkel = find(~cellfun(@isempty,regexp(skel.names,'axon')));
        if isempty(indAxonSkel)  % if skeleton was not marked with an axon label, check comments for axon label
            [comments,treeIndComment] = skel.getAllComments;
            indComment = ~cellfun(@isempty,regexp(comments,'axon','ONCE'));
            indAxonSkel = unique(treeIndComment(indComment));
            if isempty(indAxonSkel)
                % if also nothing found in comments, take the smaller one of the two skeletons as axon
                [~,indAxonSkel] = min(numSkelNodes);
            end
        end
        % get node indices in whole cell which overlap with GT
        t = skelLUT ==indAxonSkel;
        t = t(1:numel(skelSegIds));
        [~,mask] = ismember(unique(nonzeros(skelSegIds(t))),wholeCells(ind).nodes(:,4));
        mask = sort(unique(nonzeros(mask)));
        centerSoma = mean(somaCoords{ind},1); % get center coordinate of soma
        % get distance of all nodes to center
        distToCenter = sqrt(sum(bsxfun(@minus,bsxfun(@times,wholeCells(ind).nodes(:,1:3),[11.24,11.24,28]),centerSoma).^2,2));
        % get the minimal distance of the axon branch to the soma plus
        % some threshold
        minDist = min(distToCenter(mask))+2500;
        if minDist < 2000
            warning('Distance of axon to center of soma is less than 2 micron. Seems strange.. (%s)',filenames{f})
        end
        % grow out soma branch to get all nodes of the branch but do
        % not go below the minDist threshold
        while true
            maskNew = unique(wholeCells(ind).edges(any(ismember(wholeCells(ind).edges,mask),2),:));
            if isequal(mask,maskNew(distToCenter(maskNew) > minDist))
                break
            else
                mask = maskNew(abs(distToCenter(maskNew)) > minDist);
            end
        end
        wholeCells(ind).axon = false(size(wholeCells(ind).nodes,1),1);
        wholeCells(ind).axon(mask) = true;
    end
end
if ~isempty(skelsNotFound)
    warning('Skeletons of files \n%s\n did not have a whole cell partner!',filenames{skelsNotFound})
end
undefAxon = arrayfun(@(x) any(isnan(x.axon)),wholeCells);
nodesToDelete = arrayfun(@(x) find(x.axon),wholeCells,'uni',0);
nodesToDelete(undefAxon) = cell(sum(undefAxon),1);
% stupid splits can occur during axon deletion when a branch node was
% the only connection between a segID being in somaAgglo and one (or more)
% being not, so check each deletion and choose the biggest agglo as the
% one to take as wholeCell...
clear wholeCellsNoAxon
for n = 1:numel(wholeCells)
    tmp = Superagglos.removeNodesFromAgglo(wholeCells(n),nodesToDelete(n));
    if numel(tmp)>1
        [~,ind] = max(arrayfun(@(x) size(x.nodes,1),tmp));
    else
        ind = 1;
    end
    wholeCellsNoAxon(n) = rmfield(tmp(ind),'axon');
end
Util.log('Saving state...')
save(fullfile(config.outDir,'wholeCellsAxonCutState_SL_v5.mat'),'usedCells','filenames','skeletonsAll','wholeCells','wholeCellsNoAxon');

Util.log('Saving wholecells state...')
save(fullfile(outputFolder,'wholeCells_GTAxon_08_v5.mat'),'wholeCells','wholeCellsNoAxon');  

