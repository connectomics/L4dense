function wcTable = buildWCTable(config,plotFlag)
% Description:
% build a table with all info on wholecells: segmentIds, length, spine count, spine rate    
if ~exist('plotFlag','var') || isempty(plotFlag)
    plotFlag = true;
end

outDir = config.outDir;
outfile = fullfile(outDir,'wcTable.mat');

m = load(config.param);
p = m.p;

% load dendrite agglos
Util.log('Loading dendrite_wholeCells superagglos');
m=load(config.spineFileOriginal);
dendAgglosAll = m.dendrites;
wc = dendAgglosAll(m.indWholeCells);

% spine attachment
Util.log('Collecting spine attachments for all dendrites_wholeCells')
m=load(config.spineFile);
dendAgglos = cellfun(@(x) double(x),m.dendAgglos,'uni',0);
shAgglos = cellfun(@(x) double(x),m.shAgglos,'uni',0);
attached = m.attached;
indBigDends = m.indBigDends;
indWholeCells = m.indWholeCells;

sh = arrayfun(@(x) shAgglos(attached==x),(1:length(dendAgglos))','uni',0);
shCount = cellfun(@length,sh);

sh = sh(indWholeCells);
shCount = shCount(indWholeCells);
wcExt = dendAgglos(indWholeCells);

% calculate wc lengths after cutting out soma
Util.log('Calculating path lengths w/o spines with cut out somas');
inputfile = fullfile(outDir,'wcLengths.mat');
if exist(inputfile,'file')
    m = load(inputfile);
    wcLengths = m.wcLengths;
else
    [somas,~] = connectEM.getSomaAgglos(config.somaFile,'all');
    wcLengths = zeros(length(wc),1);
    tic;
    endStep = length(wc);
    for curIdx = 1:endStep
        wcWithoutSoma = Superagglos.removeSegIdsFromAgglos(wc(curIdx),somas{curIdx});
        wcLengths(curIdx) = sum(Superagglos.calculateTotalPathLength(wcWithoutSoma,p.raw.voxelSize));
        Util.progressBar(curIdx,endStep);
    end
    wcLengths = wcLengths./1E3; %um
    save(inputfile,'wcLengths');
end

% find indices of wcExt corresponding to wc
idxForPl = Agglo.findAggloAcrossVersions(p,wcExt,Superagglos.getSegIds(wc));
shDensity = shCount./wcLengths(idxForPl);
shDensity(isnan(shDensity))=0; %zero spines
wcTable = table(wcExt,sh,shCount,shDensity,wcLengths);

Util.log('Plotting...');
if plotFlag
    %% plot per wc sh numbers
    if ~exist([outDir 'figures'],'dir')
        mkdir([outDir 'figures']);
    end 
    fig = figure;
    histogram(shDensity);
    xlabel('Spine density');
    ylabel('# wc agglos');
    saveas(fig,fullfile(outDir,'figures/spineDensityForWC.png'))
    close all
end

Util.log('Saving results to wcTable');
info = Util.runInfo(false);
save(outfile,'wcTable','info');

end


