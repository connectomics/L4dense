function writeWCToNml(config)
% Description:

outDir = config.outDir;

m = load(config.param);
p = m.p;
points = Seg.Global.getSegToPointMap(p);
voxelSize = p.raw.voxelSize;

m=load(fullfile(outDir,'wcTable.mat'));
wcTable = m.wcTable;
wcExt = wcTable.wcExt;
shDensity = wcTable.shDensity;

%view wc in wK
Util.log('Now writing nmls to view in wK...')
if ~exist([outDir 'nmls'],'dir')
    mkdir([outDir 'nmls']);
end

% name output nmls to shCount and wc index
tic;
endStep = length(wcExt);
for i=1:endStep
    nodes = wcExt(i);
    skel = Skeleton.fromMST({points(nodes{:},:)},voxelSize);
    skel.names(1) = {['- sd:' num2str(shDensity(i))]};
    skel = Skeleton.setParams4Pipeline(skel, p);
    skel.write(fullfile(outDir, ['nmls/wc_' num2str(i) '.nml']));
    Util.progressBar(i,endStep);
end
Util.log('Finished creating nmls to view');
end


