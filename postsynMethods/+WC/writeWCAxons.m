% Description: write wholecells with highlighted axon parts
setConfig;
outDirNml = fullfile(config.outDir,'wcWithAxons_v5');
outDirPdf = fullfile(config.outDir,'wcWithAxonsPdf_v5');

Util.log('Loading data...')
m = load(config.param);
p = m.p;
wkFlag = false;
pdfFlag = true;


aggloFolder = fullfile(p.saveFolder, 'aggloState');
% wholeCells_07 still has axons
% wholeCells_08 has axons removed automated fashion only
% wholeCells_GTAxon_08_v3 has axons removed using hiwi tracings but bugs
% wholeCells_GtAxon_08_v4 
m=load(fullfile(config.outDir,'wholeCellsAxonCutState_SL_v5.mat'));
%m=load(fullfile(aggloFolder,'wholeCells_GTAxon_08_v4.mat'));
wc = m.wholeCells;

info = Util.runInfo();
if wkFlag
    
    Util.log('Now writing nmls to view in wK...')
    
    if ~exist(outDirNml,'dir')
        mkdir(outDirNml);
    end
    
    tic;
    endStep = length(wc);
    %idxToPlot = 1:randperm(endStep,5);
    idxToPlot = 1:endStep;
    %idxToPlot = [27,35,19,14,13,9,8,7,4,1,3];
    for i = idxToPlot
        thisWC = wc(i);
        skel = Superagglos.toSkel(thisWC);
        thisWC.axon(isnan(thisWC.axon)) = 0;% axon mask can be NaN
        nodesToComment = find(thisWC.axon);
    %    skel = skel.replaceComments('','axonNode','','','',nodesToComment); 
        skel = skel.addBranchpoint(nodesToComment);
        skel.write(fullfile(outDirNml, ['wcWAxon_' num2str(i) '.nml']));
        Util.progressBar(i,endStep);
    end
    
    Util.log('Finished creating nmls to view');
end

if pdfFlag
    
    if ~exist(outDirPdf,'dir')
        mkdir(outDirPdf);
    end
    
    bbox = [129 5574; 129 8509; 129 3414];
    bbox = bsxfun(@times, bbox, [11.24; 11.24; 28]./1000);
    Util.log('Skeletonizing...')
    ovSkels = {};
    endStep = length(wc);
    idxToPlot = 1:endStep;
    for i = idxToPlot
        thisWC = wc(i);
        skel = Superagglos.toSkel(thisWC);
        thisWC.axon(isnan(thisWC.axon)) = 0;% axon mask can be NaN
        nodesToComment = logical(thisWC.axon);
        nodesToAdd = skel.nodes{1}(nodesToComment,:);
        edgesToAdd = skel.extractEdgeList(1,nodesToComment);
        skel = skel.addTree('',nodesToAdd,edgesToAdd);
        ovSkels{i} = skel;
    end
    Util.log('Writing out pdf gallery...')   
    tic;
    for i = idxToPlot
        %set f to din A4 size
        f = figure;
        f.Units = 'centimeters';
        f.Position = [1 1 21 29.7];
        Visualization.Figure.adaptPaperPosition();
        for j = 1:4
            a = subplot(2, 2, j);
            ovSkels{i}.plot(1, [0, 0, 0], true, 2);
            hold on
            ovSkels{i}.plot(2, [0, 0, 1], true, 4);
            xlabel('x (um)');
            ylabel('y (um)');
            zlabel('z (um)');
            grid on
            switch j
                case 1
                    %xy
                    view([0, 90]);
                case 2
                    %xz
                    view([0, 0]);
                case 3
                    %yz
                    view([90, 0]);
                case 4
                    % default 3d view
            end
            axis equal
            a.XLim = bbox(1,:);
            a.YLim = bbox(2,:);
            a.ZLim = bbox(3,:);
        end
        ovSkels{i}.filename = sprintf('wc%03d', i);
        outfile = fullfile(outDirPdf, ...
            sprintf('wc%02d_%s_axonOverlap.pdf', i, ovSkels{i}.filename));
        print(outfile, '-dpdf')
        Util.log('Saving file %s.', outfile);
        close(f);
    end
    Util.log('Finished creating pdfs to view');
    
end


