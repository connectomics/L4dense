% proof read four ss from Benedikt et al by writing out nmls

setConfig;
outDir = config.outDir;


m=load(fullfile(outDir,'wcTable.mat'));
wcTable = m.wcTable;
wcExt = wcTable.wcExt;
load(config.param)
maxSegId = Seg.Global.getMaxSegId(p);
aggloLUT = Agglo.buildLUT(maxSegId,wcExt); 

% segment ids peresent in each nml
ids = [6607910, 11912622, 8682144, 7163341];
thisID = zeros(4,1);
tic;
for i=1:4
%    skel = skeleton(fullfile(outDir,['ssForPaper' num2str(i) '.nml']));

%    skelCoords = cell2mat(cellfun(@(x) x(:,1:3),skel.nodes,'uni',0));  % putting all skel nodes together
%    skelSegIds = Seg.Global.getSegIds(p,skelCoords);  % extract the seg Ids of all skel nodes
    skelSegIds = ids(i);
    thisID(i) = mode(aggloLUT(skelSegIds));
    Util.progressBar(i,4);
end
