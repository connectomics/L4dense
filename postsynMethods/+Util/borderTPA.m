function tpa = borderTPA( edgeIdx, edges, borderCoM, ...
    segCoM, maxDist )
%BORDERTPA Create border TPAs.
% INPUT edgeIdx: [Nx1] logical or int
%           Linear or logical indices of the edges of interest.
%       edges: [Nx2] int
%           Global edges list
%       borderCoM: [Nx3] float
%           Global border CoMs.
%       segCoM: [Nx3] float
%           Global seg CoMs.
%       maxDist: (Optional) float
%           Maximal distance of border CoMs from border CoMs in voxel space
%           of the TPAs in voxel space.
% OUTPUT tpa: [Nx1] cell
%           Cell array of [3x3] int containing the TPAs for each edgeIdx.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

tpa = [segCoM(edges(edgeIdx,1), :), borderCoM(edgeIdx,:), ...
       segCoM(edges(edgeIdx,2), :)];
   
if exist('maxDist', 'var') && ~isempty(maxDist)
    tpa = single(tpa);
    dir = tpa(:,1:3) - tpa(:, 4:6);
    d = sqrt(sum(dir.^2, 2)); %length of direction vector
    idx = d > maxDist;
    tpa(idx, 1:3) = tpa(idx, 4:6) + ...
        round(bsxfun(@rdivide, dir(idx, :), d(idx)).*maxDist);
    
    dir = tpa(:,7:9) - tpa(:, 4:6);
    d = sqrt(sum(dir.^2, 2)); %length of direction vector
    idx = d > maxDist;
    tpa(idx, 7:9) = tpa(idx, 4:6) + ...
        round(bsxfun(@rdivide, dir(idx, :), d(idx)).*maxDist);
end

tpa = num2cell(tpa, 2);
tpa = cellfun(@(x)permute(reshape(x, 3, 3), [2 1]), tpa, 'uni', 0);

end

