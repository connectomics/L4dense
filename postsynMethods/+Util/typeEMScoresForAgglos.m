function scores = typeEMScoresForAgglos(config,agglos,outName,plotFlag)
% plot distributions of typeEM scores for agglos
if ~exist('plotFlag','var') || isempty(plotFlag)
    plotFlag = true;
end
outDir = config.outDir;
outfile = fullfile(outDir,[outName '.mat']);
m=load('/gaba/u/mberning/results/pipeline/20170217_ROI/segmentAggloPredictions.mat');
probs = m.probs;
probsMulti = m.probsMulti;
segIdList = m.segId;
scores = cell2mat(cellfun(@(x) findMedianScores(x,probs,segIdList),agglos,'uni',0));
save(outfile,'scores');

if plotFlag
    % plot histogram
    fig = figure; hold on;
    histogram([scores(:).astrocyte],'DisplayStyle','stairs');
    histogram([scores(:).axon],'DisplayStyle','stairs');
    histogram([scores(:).dendrite],'DisplayStyle','stairs');
    legend({'astrocyte','axon','dendrite'});
    xlabel('typeEM scores')
    ylabel('# of agglos')
    a = gca;
    Util.setPlotDefault(a,true);
    saveas(fig,fullfile(outDir,'figures',[outName, 'Hist.png']));
    close all
end
end

function scores = findMedianScores(agglo,probs,segIdList)
% Description: For a given agglomerate, find the median of
% glia/axon/dendrite scores
scores = struct('astrocyte',[],'axon',[],'dendrite',[]);
idx = ismember(segIdList,agglo);
probs = probs(idx,:);
scores.astrocyte = median(probs(:,1));
scores.axon = median(probs(:,2));
scores.dendrite = median(probs(:,3));
end
