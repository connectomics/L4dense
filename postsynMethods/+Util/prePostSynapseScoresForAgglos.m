function scores = prePostSynapseScoresForAgglos(config,agglos,outName,aggloLens)
% For an input agglo, find number of synapses for which agglo is pre,
% number of synapses for which agglos is post
if nargin>3
    perUMFlag = true;
else
    perUMFlag = false;
end

outDir = config.outDir;
outfile  = fullfile(outDir,[outName 'PrePostSynapseScores.mat']);

% make sure all agglos are of same class eg. double
agglos = cellfun(@(x) double(x),agglos,'uni',0);

% use it for dendrites
presynAgglos = agglos;
postsynAgglos = agglos;

% load synapse table
m=load(config.synapseFile);
synapses = m.synapses;

Util.log('Now finding pre and post syn for agglos')
[ syn2Agglo, pre2syn, post2syn, ~ ] = ...
            L4.Synapses.synapsesAggloMapping( synapses, presynAgglos, postsynAgglos );

scores = struct('pre',[],'post',[]);

scores.pre = cellfun(@numel,pre2syn);
scores.post = cellfun(@numel,post2syn);
if perUMFlag
    scores.pre = scores.pre./aggloLens;
    scores.post = scores.post./aggloLens;
end

% scatter plot
Util.log('Plotting scatter for pre-post scores')
if ~exist([outDir 'figures'],'dir')
    mkdir([outDir 'figures']);
end
fig = figure;
plot([scores(:).pre],[scores(:).post],'.','color','b')
axis('equal')
xlabel('pre')
ylabel('post')
Util.setPlotDefault();
saveas(fig,[outDir 'figures' filesep outName 'PrePostSynapseScoresScatter.fig']);
saveas(fig,[outDir 'figures' filesep outName 'PrePostSynapseScoresScatter.png']);
close all

Util.log('Saving results')
save(outfile,'scores');
end

