function idx = isInBBox( nodes, bbox )
%ISINBBOX Returns whether nodes are in a given bounding box.
% INPUT nodes: [Nx3] int
%           3d coordinates of nodes.
%       bbox: [3x2] int
%           Bounnding box.
% OUTPUT idx: [Nx1] logical
%           True if the corresponding node is within the bounding box.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

idx = all(bsxfun(@ge, nodes, bbox(:,1)') & ...
          bsxfun(@le, nodes, bbox(:,2)'), 2);

end


