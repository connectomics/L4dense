function [preInterfaces,postInterfaces] = findPrePostInterfaces()

% find which synapses are shaft
outDir = '/tmpscratch/sahilloo/L4/data/';

m = load('/gaba/u/mberning/results/pipeline/20170217_ROI/allParameter.mat');
p = m.p;
m = load([p.saveFolder 'globalEdges.mat']);
edges = m.edges;

m = load([p.saveFolder 'globalSynScores.mat']);
synScores = nan(size(edges));
synScores(m.edgeIdx, :) = m.synScores;

% synapses
synThr = -2.06;
idx = synScores > synThr;

% ignore where both edges predicted pre
preIdx = idx; preIdx(~xor(idx(:,1),idx(:,2)),:)=0; 
postIdx = ~idx; postIdx(~xor(idx(:,1),idx(:,2)),:)=0;

preEdgeIdx = preIdx(:,1)|preIdx(:,2);
preIdx = preIdx(preEdgeIdx,:);
preInterfaces = edges(preEdgeIdx,:);
preInterfaces(preIdx(:,2),:) = fliplr(preInterfaces(preIdx(:,2),:)); % all pre in first column

postInterfaces = fliplr(preInterfaces);

save(fullfile(outDir,'interfacesPrePost.mat'),'preInterfaces','postInterfaces');
end