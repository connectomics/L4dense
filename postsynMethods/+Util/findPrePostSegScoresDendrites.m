function [scores] = findPrePostScoresDendrites()

% find how many segments are pre/post in dendrite agglo
outDir = '/tmpscratch/sahilloo/L4/data/';

m = load('/gaba/u/mberning/results/pipeline/20170217_ROI/allParameter.mat');
p = m.p;

%load all pre and post segments by synEM
[pre,post]=findPrePostSeg();

% load dendrite agglos
k = load('/tmpscratch/sahilloo/L4/dendriteQueryResults2/dendritesPostQueryState.mat'); % no mask of 5um distance, only 200k vx size
dendrites = k.dendritesPostQuery;

scores = cell2mat(cellfun(@(x) findPrePostScoreAgglo(x,pre,post),dendrites,'uni',0));

save(fullfile(outDir,'dendritesPrePostSegScores.mat'),'scores');

end

function scores = findPrePostScoreAgglo(agglo,pre,post)

scores = struct('pre',[],'post',[]);

scores.pre = sum(ismember(agglo,pre));
scores.post = sum(ismember(agglo,post));

end




