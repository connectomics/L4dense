setConfig;

outDir = '/gaba/u/mberning/results/pipeline/20170217_ROI/postSynIso/soma/';

Util.log('Loading stuff...')
m=load(config.param);
p = m.p;

m=load(config.somaFile);

agglos = m.somas(:,3);

Util.log('Now starting soma viz...')
Visualization.exportAggloToAmira(p,agglos,outDir);

