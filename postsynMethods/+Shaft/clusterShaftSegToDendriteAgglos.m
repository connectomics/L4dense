function clusterShaftSegToDendriteAgglos()
% cluster the found shaft synapses into dendrite agglos
m = load('/gaba/u/mberning/results/pipeline/20170217_ROI/allParameter.mat');
load('/gaba/u/mberning/results/pipeline/20170217_ROI/globalBorder.mat')
p = m.p;
m = load([p.saveFolder 'globalEdges.mat']);                                                                                             
edges = m.edges;
m=load([p.saveFolder 'globalBorder.mat']);
borderCoM = m.borderCoM;
clear m                                                                                                                                 
rng(0);
points = Seg.Global.getSegToPointMap(p);
voxelSize = p.raw.voxelSize;

outDir = '/tmpscratch/sahilloo/L4/data/';
[post,edgeIdx,postIdxPseudo] = Shaft.findShaftSeg();
save(fullfile(outDir,'shaftSegments.mat'),'post','edgeIdx','postIdxPseudo');

% load dend agglos
k = load('/tmpscratch/sahilloo/L4/dendriteQueryResults2/dendritesPostQueryState.mat') % no 5um distance, only 200k vx size;
dendritesPostQuery = k.dendritesPostQuery;

% find dendAgglo eqclass for each shaft seg
tic;
idxEqClass = arrayfun(@(x) clusterShaftSeg_findIdx(dendritesPostQuery,x),post.segId,'uni',0);
toc;
post.segEqClass = idxEqClass;
save(fullfile(outDir,'shaftSegmentsOrdered.mat'),'post','edgeIdx');

% find per dendAgglo associated shaft segs
listEqClasses = vertcat(post.segEqClass{:});
dendritesShaftSegs = arrayfun(@(x) post.segId(listEqClasses==x),[1:length(dendritesPostQuery)]','uni',0);

% find per dendAggloShafts - edgeIdx
%dendritesShaftsEdgeIdx = arrayfun(@(x) clusterShaftSeg_findEdgeIdx(listEqClasses,x),[1:length(dendritesPostQuery)]','uni',0);
save(fullfile(outDir,'dendritesPostQueryWithShafts.mat'),'dendritesPostQuery','dendritesShaftSegs');

% plot 
idxToPlot = randperm(length(dendritesPostQuery),10);
% find tpa edgeIdx per shaft segment
edgeIdx = arrayfun(@(x) clusterShaftSeg_findEdgeIdx(post.segId,postIdxPseudo,dendritesShaftSegs(x)),idxToPlot,'uni',0);

for i=1:length(idxToPlot)
    nodes = dendritesPostQuery(idxToPlot(i));
    if numel(dendritesPostQuery{idxToPlot(i)})==1
        fprintf('DendritesPostQuery %s is single segId \n',num2str(idxToPlot(i)));
        continue;
    end
    if ~any(edgeIdx{i})
        fprintf('DendritesPostQuery %s has no shaft seg \n',num2str(idxToPlot(i)));
        continue;                                                                                                                       
    end

    skel = Skeleton.fromMST({points(nodes{:},:)},voxelSize);
    skel.names(1) = {'agglo'};
    skel = Skeleton.setParams4Pipeline(skel, p);
%    node = points(dendritesShaftSegs{idxToPlot(i)},:);
%    names = repmat({'shaft'},length(node),1);
%    skel = skel.addNodesAsTrees(node,names);
    tpa = Util.borderTPA(edgeIdx{i},edges,borderCoM,points);
    for j = 1:length(tpa)
        %add tpa such that border is first node
        skel = skel.addTree(['inh_' num2str(j)], tpa{j}([2 1 3], :), ...
            [1 2; 1 3]);
    end
    skel.write(fullfile(outDir, ['nmls/dendrites_' num2str(idxToPlot(i)) '.nml']));
%    clear skel
end

end

function idx = clusterShaftSeg_findIdx(denSeg,segment)
    idx = false(length(denSeg),1);
    for i=1:length(denSeg)
        idx(i)=any(denSeg{i}==segment);
    end
    idx = find(idx);
end

function edgeIdx = clusterShaftSeg_findEdgeIdx(segId,postIdxPseudo,x)
    sizeEdges = [105450767,2];                                                                                                          
    idxKeep = ismember(segId,x{:});
    postIdxPseudo = postIdxPseudo(idxKeep);
    [r,c] = ind2sub(sizeEdges,postIdxPseudo);
    edgeIdx = ismember([1:sizeEdges(1)],r);
end
