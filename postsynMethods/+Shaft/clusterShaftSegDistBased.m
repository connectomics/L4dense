% cluster interfaces based on 320nm distances as in synEM paper
outDir = '/tmpscratch/sahilloo/L4/data/';
m = load('/gaba/u/mberning/results/pipeline/20170217_ROI/allParameter.mat');
load('/gaba/u/mberning/results/pipeline/20170217_ROI/globalBorder.mat')
p = m.p;
voxelSize = p.raw.voxelSize;
m=load([p.saveFolder 'globalBorder.mat']);
borderCoM = m.borderCoM;

% load shaftSegments, edgeIdx
%load('/tmpscratch/sahilloo/L4/data/dendritesPostQueryWithShafts.mat')
m=load('/tmpscratch/sahilloo/L4/data/shaftSegmentsOrdered.mat');
edgeIdx = m.edgeIdx;

% remove soma
edgeIdx = Util.removeNCInterfaces(edgeIdx,0.5);

% find clusterId for synCoMs
tic;
synCom = borderCoM(edgeIdx,:);
synComNM = bsxfun(@times, single(synCom), p.raw.voxelSize);
cutoff = 320.12; % in nm
T = Util.clusterSynapticInterfaces( synComNM, cutoff );
outfile = fullfile(outDir,['clusteredComs_' num2str(cutoff) '.mat']);
save(outfile,'T','synCom');
toc;
disp('Finished finding cluster Ids of synaptic interfaces.');

% cluster CoMs
tic;
synClustered = arrayfun(@(x) synCom(T==x,:),[1:max(T)]','uni',0);
toc;
disp('Finished clustering synaptic interfaces');

% some heuristics
sprintf('Total shaft interfaces: %d \n',size(synCom,1))
sprintf('Total number of clusters: %d \n',max(T(:)))
save(fullfile(outDir,['clusteredComs_' num2str(cutoff) '.mat']),'T','synCom','synClustered');

% plot
count = cellfun(@(x) size(x,1),synClustered);
h=histogram(count,max(count));

% view in wK
rng(0);
out = find(count>1);
idxToPlot = randperm(length(out),50);

for i=idxToPlot
    nodes = synClustered(out(i));
    skel = Skeleton.fromMST(nodes,voxelSize);
    skel = Skeleton.setParams4Pipeline(skel, p);
    skel.write(fullfile(outDir, ['nmls/clusteredShafts/clusteredShafts_' num2str(i) '.nml']));
end

