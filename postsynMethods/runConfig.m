% order in which the functions are to be run

setConfig;

axonTable = Axon.buildAxonTable(config);

spineTable = Spines.buildSpineTable(config);

Util.prePostSynapseScoresForAgglos(config,spineTable.dendritesExt,'dendrites');

preScoreRange = {0,5};
postScoreRange = {50,260};
idxSmooth = Smooth.mainScriptForSmooth(config,preScoreRange,postScoreRange);

sfThr = 0.5;
Smooth.mainScriptForSmoothInput(config,sfThr);

Specificity.smoothSpecificity;
