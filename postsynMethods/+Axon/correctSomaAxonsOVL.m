% correct the axons_19 to axons_19_a overlaps of soma axons with axons_19

setConfig;
load(config.param);
    
outputFolder = fullfile(p.saveFolder, 'aggloState');
outfile = fullfile(outputFolder,'axons_19_a.mat');
info = Util.runInfo();
    
% use axon state
Util.log('loading list of overlapping agglos...');
m=load(fullfile(config.outDir,'debugSomaAxonsOVLState.mat'));
idxSomaAxons = m.idxSomaAxons;
idxOvlAxons = m.idxOvlAxons;

m=load(fullfile(outputFolder,'axons_19.mat'));
axons = m.axons;    indBigAxons = m.indBigAxons; indWcAxon = m.indWcAxon; idxWcAxon = m.idxWcAxon;

idxError = [8,10];
Util.log('Adding NaNs where merger error occured...')
for i =1:numel(idxError)
    idxToReplace = idxOvlAxons(idxError(i));
    oldNodes = axons(idxToReplace).nodes;
    seg = oldNodes(:,4);
    segSoma = axons(idxSomaAxons(idxError(i))).nodes(:,4);
    toRem = ismember(seg,segSoma); % add Nans where seg was also found in soma seg
    newSeg = seg;
    newSeg(toRem) = NaN;
    axons(idxToReplace).nodes = horzcat(oldNodes(:,1:3),newSeg);
end

idxMerge = setdiff([1:numel(idxSomaAxons)]',idxError);

Util.log('Adding overlapping superagglos into one...')
idxSomaAxonsMerge = idxSomaAxons(idxMerge);
idxOvlAxonsMerge = idxOvlAxons(idxMerge);

leftAll = unique(idxSomaAxonsMerge); % unique soma axons in which to merge others
for i = 1:numel(leftAll)
    left = leftAll(i);
    right = idxOvlAxonsMerge(find(idxSomaAxonsMerge==left)); % find small axons ovl with left soma axon
    leftAxon = axons(left);
    rightAxon = axons(right);
    out= leftAxon;
    for j = 1:numel(rightAxon)
        out = SuperAgglo.merge(out,rightAxon(j));
    end
    leftAllAxons(i) = out; % big axon with all small axons added
end
%{
% to wk for sanity
for i=1:numel(leftAllAxons)
    skel = Superagglos.toSkel(leftAllAxons(i));
    skel.write(fullfile(config.outDir,['somaAxonsOvlAxons/combined_' num2str(i) '.nml']));
end
%}
Util.log('restructure to match axons')
for i=1:numel(leftAllAxons)
    leftAllAxons(i).endings = [];
    leftAllAxons(i).solvedChiasma = false;
end
Util.log('update axons...')
axons(leftAll) = leftAllAxons; % replace with bigger ones
axons(idxOvlAxons) = []; indBigAxons(idxOvlAxons) = []; indWcAxon(idxOvlAxons) = []; idxWcAxon(idxOvlAxons) = [];
Util.log('cleaning axons...')
axons = SuperAgglo.clean(axons);

out = struct;
out.axons = axons;
out.indBigAxons = indBigAxons;
out.indWcAxon = indWcAxon;
out.idxWcAxon = idxWcAxon;
out.info = info;                                                                                                                                                                            
Util.log('Saving axons...');
Util.saveStruct(outfile,out);
