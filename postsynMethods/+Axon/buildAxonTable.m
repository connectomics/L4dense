function axonTable = buildAxonTable(config,plotFlag)
% build a boutonTable with axons, axonSynapses,labels,spineFraction
if ~exist('plotFlag','var') || isempty(plotFlag)
    plotFlag = true;
end
% find for each axon agglo, the fraction of exc vs inh interfaces
outDir = config.outDir;
outfile = fullfile(outDir,'axonTable.mat');

% load all axon agglos
Util.log('Loading axons...')
m=load(config.axonFile);
%axons = Superagglos.getSegIds(m.axons(m.indBigAxons));
axons = m.axonAgglos;
axons(cellfun(@isempty,axons))=[]; % remove empty axons

% load pre-post synaptic interfaces 
Util.log('Loading synapses...')
m=load(config.synapseFile);
synapses = m.synapses;

% load spine-heads
Util.log('Loading spine-head agglos...')
m=load(config.spineFile);
shAgglos = m.shAgglos;

% classify synapses as spine targeting or not
Util.log('Loading synapse spine-targeting labels...')
if exist(fullfile(outDir,'synapseLabels.mat'),'file')
    m=load(fullfile(outDir,'synapseLabels.mat'));
    synapseLabels = m.synapseLabels;
else
    synapseLabels = Axon.classifySynapses(config,synapses,shAgglos);
end

Util.log('Finding synapses per axon...')
presynAgglos = axons;
postsynAgglos = {};
[ ~, pre2syn, ~, ~ ] = ...
    L4.Synapses.synapsesAggloMapping( synapses, presynAgglos, postsynAgglos );
axonSynapses = pre2syn;
labels = cellfun(@(x) synapseLabels(x),axonSynapses,'uni',0);

% spine-targetting fraction
spineTargetingFraction = cellfun(@(x) sum(x)/length(x),labels);

% build axonTable
Util.log('Building axonTable')
axonTable = table(axons,axonSynapses,labels,spineTargetingFraction);

if plotFlag
    Util.log('Plotting scatter plot for axons and spine fraction hist')
    if ~exist([outDir 'figures'],'dir')
        mkdir([outDir 'figures']);
    end
    fig = figure;
    subplot(2,1,1)
    % scatter plot
    excCount = cellfun(@(x) sum(x),labels);
    inhCount = cellfun(@(x) sum(~x),labels);
    plot(inhCount,excCount,'.'); axis('equal')
    xlabel('non-spine targeting')
    ylabel('spine targeting')
    title('# "spine-targeting" and "non-spine-targeting" synapses per axon')
    % histogram of spine fraction of axons
    subplot(2,1,2)
    histogram(spineTargetingFraction,'DisplayStyle','stairs');
    xlabel('Spine-targeting synapse fraction');
    ylabel('# of axons');
    title('Histogram of spine-targeting synapses/total synapses in axons')
    Util.setPlotDefault();
    saveas(fig,fullfile(outDir,'figures/spine-non-spineTargetingSynapses.png'));
    close all
end

Util.log('Saving results of axonTable')
info = Util.runInfo(false);
save(outfile,'axonTable','info');
    
end


