% exploring the possibility that the ais are present as flight paths that
% got attached to axons
% https://mhlablog.net/2018/04/05/ais-in-dendrites-axon-state/

setConfig;
config.outDir = '/tmpscratch/sahilloo/L4/dataPostSyn/';
outDir = config.outDir;
config.axonFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/axons_17_a.mat';
config
info = Util.runInfo(false);

Util.log('Loading stuff...');
m=load(config.param);
p = m.p;

m=load(config.axonFile);
indBigAxons = m.indBigAxons;
axons = m.axons(indBigAxons);

% extract node coords from superagglos
nodes = {axons(:).nodes};
points = cellfun(@(x) x(:,1:3), nodes,'uni',0);

tol = 500; % ~ 6um

bounds = p.bbox;
xbounds = bounds(1,:); ybounds = bounds(2,:); zbounds = bounds(3,:);
xboundsDown = xbounds(1) + tol; 
xboundsTop = xbounds(2) - tol;

Util.log('filtering touch to up and down based on nodes coordinates...')
idxTD = cellfun(@(p) any(p(:,1)<=xboundsDown) & any(p(:,1)>=xboundsTop), points);
topDownThings = axons(idxTD);
save(fullfile(config.outDir,'axonsTopDownThingsState.mat'),'topDownThings','idxTD','info');

Util.log('Writing random nmls...')
outDirNmls = fullfile(outDir,'nmls','topDownInAxons');
if ~exist('outDirNmls','dir')
    mkdir(outDirNmls)
end
count = 30;
idxToPlot = randperm(length(topDownThings),count);
out = topDownThings(idxToPlot);
tic;
for i =1:count
    skel = Superagglos.toSkel(out(i));
    skel.write(fullfile(outDirNmls,['topDownAx_' num2str(i) '.nml']));
    Util.progressBar(i,count);
end
