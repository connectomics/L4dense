function [boutons,boutonLabels]= classifyBoutons(wkFlag)
% find whether a bouton is spine-targeting or not
if ~exist('wkFlag','var') || isempty(wkFlag)
    wkFlag = false;
end

% find for each axon agglo, the fraction of exc vs inh interfaces
outDir = '/tmpscratch/sahilloo/L4/data/';
m = load('/gaba/u/mberning/results/pipeline/20170217_ROI/allParameter.mat');
p = m.p;
points = Seg.Global.getSegToPointMap(p);                                                                                                
voxelSize = p.raw.voxelSize;

m = load(fullfile(outDir,'spine-heads-and-attachment.mat'));
shIds = vertcat(m.shAgglos{:});

% load pre-post synaptic interfaces 
m=load('/gaba/u/bstaffle/data/L4/Synapses/SynapseAgglos_v1.mat');
synapses = m.synapses;

% load all bouton agglos
m = load('/tmpscratch/sahilloo/L4/data/boutonAgglos.mat');
boutons = m.boutons_nOv{:,1};
boutonsPost = m.boutons_nOv{:,2};

% labels boutons targeting-spines
boutonLabels = {};
m1 = max(cellfun(@max,boutonsPost));
m2 = max(shIds);
m = max(m1,m2);

shLUT = false(m,1);
shLUT(shIds)=true;

tic;
boutonLabels = cell2mat(cellfun(@(x) any(shLUT(x)),boutonsPost,'uni',0));
toc;
save(fullfile(outDir,'boutonLabels.mat'),'boutonLabels','boutons');

if wkFlag
    % view in wK
    excBoutonsAll = boutons(boutonLabels);
    inhBoutonsAll = boutons(~boutonLabels);

    rng(0);
    idxToPlot = randperm(size(excBoutonsAll,1),50);
    for i=1:length(idxToPlot)
        nodes = excBoutonsAll(idxToPlot(i));
        skel = Skeleton.fromMST({points(nodes{:},:)},voxelSize);
        skel = Skeleton.setParams4Pipeline(skel, p);
        skel.write(fullfile(outDir, ['nmls/excBoutons_' num2str(i) '_' datestr(now,'yyyymmdd') '.nml']));
    end

    rng(0);
    idxToPlot = randperm(size(inhBoutonsAll,1),50);
    for i=1:length(idxToPlot)
        nodes = inhBoutonsAll(idxToPlot(i));
        skel = Skeleton.fromMST({points(nodes{:},:)},voxelSize);
        skel = Skeleton.setParams4Pipeline(skel, p);
        skel.write(fullfile(outDir, ['nmls/inhBoutons_' num2str(i) '_' datestr(now,'yyyymmdd') '.nml']));
    end
end
end