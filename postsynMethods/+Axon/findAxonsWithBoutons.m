function [axons, axonsExt, axonBoutons] = findAxonsWithBoutons(excludeSoma,wkFlag)
if ~exist('wkFlag','var') || isempty(wkFlag)
    wkFlag = false;
end
if ~exist('excludeSoma','var') || isempty(excludeSoma)
    excludeSoma = false;
end

% find for each axon agglo, the fraction of exc vs inh interfaces
outDir = '/tmpscratch/sahilloo/L4/data/';
outfile = fullfile(outDir,'axonsWithBoutons.mat');
m = load('/gaba/u/mberning/results/pipeline/20170217_ROI/allParameter.mat');
p = m.p;
points = Seg.Global.getSegToPointMap(p);                                                                                                
voxelSize = p.raw.voxelSize;

% load all axon agglos
m = load('/tmpscratch/mberning/axonQueryResults/axonQueryAnalysisBS.mat');
axons = m.axonsNew;

% load all bouton agglos
n = load('/tmpscratch/sahilloo/L4/data/boutonAgglos.mat');
boutons = n.boutons_nOv{:,1};
b2a = n.b2agglo_ov;

if excludeSoma
    m=load('/gaba/u/rhesse/forBenedikt/somasNoClosedHoles.mat');
    somaAgglos = m.somas(:,3);
    somaSeg = sort(vertcat(somaAgglos{:})); % sort for ismembc
    isSoma = cell2mat(cellfun(@(x) any(ismembc(x,somaSeg)),boutons,'uni',0)); % ismembc because we need just one seg to intersect
    boutons = boutons(~isSoma);
    b2a = b2a(~isSoma);
end

% extend/attach boutons to axons

axonsExt = cellfun(@(x,y) unique([x(:) ; vertcat(y{:})]),axons,axonBoutons,'uni',0);
save(outfile,'axons','axonsExt','axonBoutons');

if wkFlag
% view in wK
rng(0);
idxToPlot = randperm(size(axonsExt,1),30);
for i=1:length(idxToPlot)
   nodes = axonsExt(idxToPlot(i));
   skel = Skeleton.fromMST({points(nodes{:},:)},voxelSize);
   skel = Skeleton.setParams4Pipeline(skel, p);
   skel.write(fullfile(outDir, ['nmls/axonsWithBoutons_' num2str(i) '_' datestr(now,'yyyymmdd') '.nml']));
end
end
end
