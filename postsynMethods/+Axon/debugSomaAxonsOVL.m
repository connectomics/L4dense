% overlaps of soma axons with axons_19

setConfig;
load(config.param);
    
outputFolder = fullfile(p.saveFolder, 'aggloState');
info = Util.runInfo();
    
% use axon state
m=load(fullfile(outputFolder,'axons_19.mat'));
axons = m.axons;

Util.log('running Agglo.check...');
segEq = Agglo.fromSuperAgglo(axons, true);
posMask = cellfun( ...
    @(ids) all(ids > 0), segEq);
assert( ...
    all(posMask), ['Non-positive segment ', ...
    'ID in agglomerate %d'], find(~posMask, 1));

maxSegId = getMaxSegId(segEq);
segLUT = Agglo.buildLUT(maxSegId, segEq);

idxSomaAxons = [];
idxOvlAxons = [];

for curIdx = 1:numel(segEq)
    curAggloIds = segLUT(segEq{curIdx});
    if all(curAggloIds == curIdx); continue; end
    
    warning( ...
        'Agglomerate %d overlaps with %s', ...
        curIdx, num2str(setdiff(curAggloIds, curIdx)));
    idxOvlAxons = [idxOvlAxons;curIdx];
    idxSomaAxons = [idxSomaAxons; setdiff(curAggloIds, curIdx)];
end

somaAxons = axons(idxSomaAxons);
ovlAxons = axons(idxOvlAxons);

tic;
endStep =  numel(somaAxons);
for i=1:endStep
    skelSoAx = Superagglos.toSkel(somaAxons(i));
    skelOvAx = Superagglos.toSkel(ovlAxons(i));

    out = skelSoAx;
    out.names(1) = {'somaAxon'};

    out = out.addTreeFromSkel(skelOvAx);
    out.names(2) = {'ovlAxon'};
    outname = fullfile(config.outDir,['somaAxonsOvlAxons/skelSomaAxonOvl_' num2str(i) '.nml']);
    out.write(outname);
    Util.progressBar(i,endStep);
end

save(fullfile(config.outDir,'debugSomaAxonsOVLState.mat'),'idxSomaAxons','idxOvlAxons','somaAxons','ovlAxons');
function maxSegId = getMaxSegId(agglos)
    maxSegId = ~cellfun(@isempty, agglos);
    maxSegId = max(cellfun(@max, agglos(maxSegId)));
    if isempty(maxSegId); maxSegId = 0; end
end

