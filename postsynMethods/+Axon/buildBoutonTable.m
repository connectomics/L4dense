function boutonTable = buildBoutonTable(excludeSoma,wkFlag,plotFlag)
% build a boutonTable with axons, axonBoutons,labels,spineFraction

if ~exist('wkFlag','var') || isempty(wkFlag)
    wkFlag = false;
end
if ~exist('excludeSoma','var') || isempty(excludeSoma)
    excludeSoma = true;
end
if ~exist('plotFlag','var') || isempty(plotFlag)
    plotFlag = false;
end

% find for each axon agglo, the fraction of exc vs inh interfaces
outDir = '/tmpscratch/sahilloo/L4/data/';
outfile = fullfile(outDir,'boutonTable.mat');
m = load('/gaba/u/mberning/results/pipeline/20170217_ROI/allParameter.mat');
p = m.p;
points = Seg.Global.getSegToPointMap(p);                                                                                                
voxelSize = p.raw.voxelSize;

% load all axon agglos
m = load('/tmpscratch/mberning/axonQueryResults/axonQueryAnalysisBS.mat');
axons = m.axonsNew;

% load pre-post synaptic interfaces 
m=load('/gaba/u/bstaffle/data/L4/Synapses/SynapseAgglos_v1.mat');
synapses = m.synapses;

% load all bouton agglos
m = load('/tmpscratch/sahilloo/L4/data/boutonAgglos.mat');
boutons = m.boutons_nOv{:,1};
b2a = m.b2agglo_ov;

if excludeSoma
    m=load('/gaba/u/rhesse/forBenedikt/somasNoClosedHoles.mat');
    somaAgglos = m.somas(:,3);
    somaSeg = vertcat(somaAgglos{:});
    m1 = max(cellfun(@max,boutons));
    m2 = max(somaSeg);
    m = max(m1,m2);
    somaIdx = false(m,1);
    somaIdx(somaSeg)=true;
    isSoma = cell2mat(cellfun(@(x) any(somaIdx(x)),boutons,'uni',0));
    boutons = boutons(~isSoma);
    b2a = b2a(~isSoma);
end

% get bouton labels and assign to axons
if exist(fullfile(outDir,'boutonLabels.mat'),'file')
    m=load(fullfile(outDir,'boutonLabels.mat'));
    boutonLabels = m.boutonLabels;
else
    [~,boutonLabels]= Axon.classifyBoutons(false);
end

[axonBoutons , labels ] = arrayfun(@(x) pickBoutonsAndLabelsPerAxon(b2a==x,boutons,boutonLabels),...
    [1:size(axons,1)]','uni',0);

% spine-targetting fraction
spineFraction = cell2mat(cellfun(@(x) sum(x)/length(x),labels,'uni',0));

% build boutonTable
boutonTable = table(axons,axonBoutons,labels,spineFraction);
save(outfile,'boutonTable');

if plotFlag
    fig = figure;
    subplot(2,1,1)
    % scatter plot
    excCount = cellfun(@(x) sum(x),labels);
    inhCount = cellfun(@(x) sum(~x),labels);
    plot(inhCount,excCount,'.'); axis('equal')
    xlabel('non-spine targeting')
    ylabel('spine targeting')
    title('# "spine-targeting" and "non-spine-targeting" boutons per axon')
    % histogram of spine fraction of axons
    subplot(2,1,2)
    histogram(spineFraction,'DisplayStyle','stairs');
    xlabel('Spine-targeting bouton fraction');
    ylabel('# of axons');
    title('Histogram of spine-targetingBoutons/totalBoutons in axons')
    saveas(fig,fullfile(outDir,'figures/spine-non-spineTargetingBoutons.png'));
    close all
end

end

function [b,l]= pickBoutonsAndLabelsPerAxon(idx,boutons,boutonLabels)
    b = boutons(idx);
    l = boutonLabels(idx);
end



