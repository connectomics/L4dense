function bbox = intersectBbox(bbox1, bbox2, throwError)
    % bbox = intersectBbox(bbox1, bbox2, throwError)
    %   Returns the intersection of two bounding boxes. If the bounding
    %   boxes do not intersect, the entries of `bbox` are all set to `nan`
    %   and an error is thrown (unless defused via `throwError`).
    %
    % Written by
    %   Ali Karimi <ali.karimi@brain.mpg.de>
    %   Marcel Beining <marcel.beining@brain.mpg.de>
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    if ~exist('throwError', 'var') || isempty(throwError)
        % NOTE(amotta): For backward compatibility only! If possible we
        % should try to get rid of this.
        throwError = true;
    end
    
    assert(all(bbox1(:, 1) <= bbox1(:, 2)));
    assert(all(bbox2(:, 1) <= bbox2(:, 2)));
    
    bbox = [ ...
        max(bbox1(:, 1), bbox2(:, 1)), ...
        min(bbox1(:, 2), bbox2(:, 2))];
    
    if any(bbox(:, 1) >= bbox(:, 2))
        % No intersection
        bbox(:) = nan;
        
        if throwError
            error('No intersection possible');
        end
    end
end
