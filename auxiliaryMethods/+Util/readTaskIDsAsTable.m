function [idsTab] = readTaskIDsAsTable(IDs_path)
seedfile = fullfile(IDs_path);
fileID = fopen(seedfile,'r');
colNames = {'taskId', 'filename', 'position'};
% skip first line if it is there
if strcmp(char(fread(fileID,6)'),'taskId')
    fseek(fileID,25,'bof');
end
data = textscan(fileID, '%s %s (%f %f %f)','Delimiter',{','});
idsTab = table( ...
    data{1}, ...
    data{2}, ...
    horzcat(data{3:5}), ...
    'VariableNames', colNames);
end


 