function agglos = removeSegmentOverlap(agglos)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % NOTE(amotta): This assumes non-empty agglomerates. Are empty
    % agglomerates even legal? Let's just throw an error for now...
    maxSegId = max(cellfun(@max, agglos));
    
    keepLUT = true(maxSegId, 1);
    keepLUT(Agglo.segmentOverlap(agglos)) = false;
    
    agglos = cellfun( ...
        @(segIds) segIds(keepLUT(segIds)), ...
        agglos, 'UniformOutput', false);
end
