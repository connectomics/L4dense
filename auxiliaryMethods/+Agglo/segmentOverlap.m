function segIds = segmentOverlap(agglos)
    % segIds = segmentOverlap(agglos)
    %   Calculates the segments that occur in at least two agglomerates.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    segIds = cell2mat(cellfun( ...
        @(segIds) unique(segIds(:)), ...
        agglos(:), 'UniformOutput', false));
   [segIds, ~, segCount] = unique(segIds);
    segCount = accumarray(segCount, 1);
    segIds = segIds(segCount > 1);
end
