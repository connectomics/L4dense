function [segIds, surfAreas, varargout] = ...
        physicalSurfaceArea(maskOrLabelVol, scale, varargin)
    % [segIds, areas, capAreas] = physicalSurfaceArea(maskOrLabelVol, scale)
    %   Measures the physical surface area of a volume using the algorithm
    %   of TrakEM2¹. The input may be either a binary or label volume.
    %
    % References
    % ¹ ini.trakem2.display.AreaList.measure method
    %   written by Albert Cardona and Rodney Douglas
    %   https://github.com/trakem2/TrakEM2/blob/e2c46b0851a090310542e58ee2d7afc895b57a1d/src/main/java/ini/trakem2/display/AreaList.java#L1308
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    opts = struct;
    opts.method = 'trakem';
    opts = Util.modifyStruct(opts, varargin{:});
    
    % Sanitization
    opts.method = lower(opts.method);
    assert(isequal(scale(1), scale(2)));
    
    if isempty(opts.method)
        opts.method = 'trakem2';
    end
    
    switch opts.method
        case 'trakem2'
            doIt = @Seg.Local.Internal.physicalSurfaceAreaTrakEM2;
        otherwise
            error('Unknown method "%s"', opts.method);
    end
    
    segIds = setdiff(maskOrLabelVol, 0);
    segIds = reshape(segIds, [], 1);
    
    varargout = cell(1, nargout - 2);
   [surfAreas, varargout{:}] = arrayfun( ...
       @(id) doIt(opts, scale, maskOrLabelVol == id), segIds);
end


