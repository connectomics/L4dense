function am=createAdjacencyMatrix(obj,treeIdx,distWeighted,asym)
%Create the adjacency matrix of a tree of the Skeleton.
% INPUT treeIdx: Integer index of the tree in obj to create
%           the adjacency matrix for.
%       distWeighted: Boolean (DEFAULT 0) to specify if matrix is logical
%                     or contains the physical distance information between
%                     the edges
%       asym: Boolean if true returns asymmetric matrix
% OUTPUT am: Sparse, rectangular and symmatric matrix,
%           where am(i,j) = am(j,i) =  1 (or Lij), if node i and node j
%           are connected.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('distWeighted','var') || isempty(distWeighted)
    distWeighted = false;
end
if ~exist('asym','var') || isempty(asym)
    asym = false;
end

numNodes = size(obj.nodes{treeIdx}, 1);

if numNodes > 1
    edgesInTree = obj.edges{treeIdx};
    edgesInTree = unique(edgesInTree,'rows'); % remove duplicant edges
    edgeCount = size(edgesInTree, 1);
    if distWeighted
        [~,edgeVec] = obj.physicalPathLength(obj.nodes{treeIdx}, obj.edges{treeIdx}, obj.scale);
    else
        edgeVec = true(1, edgeCount);
    end
    % build adjacency matrix
    am = sparse( ...
        edgesInTree(:, 1)', edgesInTree(:, 2)', ...
        edgeVec, numNodes, numNodes);
    if ~asym
        % make symmetric
        am = am + am';
    end
else
    % single node case
    am = zeros(1);
end
end