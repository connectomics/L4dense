function skel = addVolume(skel, volDir, volId)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    if ~exist('volId', 'var') || isempty(volId)
        % Let's just add a new volume to the tracing
        volId = height(skel.volumes) + 1;
    end
    
    locName = sprintf('data%d.zip', volId);
    otherVolIds = setdiff(1:height(skel.volumes), volId);
    
    % Sanity checks
    assert(1 <= volId && volId <= height(skel.volumes) + 1);
    assert(not(ismember(volId, skel.volumes.id(otherVolIds))));
    assert(not(ismember(locName, skel.volumes.location(otherVolIds))));
    
    header = wkwLoadHeader(fullfile(volDir, '1'));
    assert(isequal(header.version, 1));
    assert(isequal(header.voxelsPerBlockDim, 32));
    assert(isequal(header.blocksPerFileDim, 1));
    assert(isequal(header.blockType, 'RAW'));
    assert(isequal(header.voxelType, 'uint32'));
    assert(isequal(header.numChannels, 1));
    
    % Create directories
   [tmpDir, tmpCleanup] = Util.makeTempDir(); %#ok
    
    if isempty(skel.volumeDir)
        skel.volumeDir = Util.makeTempDir();
    end
    
    tmpZipPath = fullfile(tmpDir, locName);
    zipPath = fullfile(skel.volumeDir, locName);
    
    % Create ZIP file and move it to volume directory
    zip(tmpZipPath, fullfile(volDir, '1'), volDir);
    assert(movefile(tmpZipPath, zipPath));
    
    % Update volume table
    skel.volumes(volId, :) = {volId, locName};
end
