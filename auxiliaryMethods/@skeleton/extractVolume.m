function outDir = extractVolume(skel, volId, outDir)
    % outDir = extractVolume(skel, volId, outDir)
    %   Extracts the volume tracing `volId` and returns the path to the
    %   uncompressed data. If no output directory `outDir` is specified, a
    %   temporary directory is created automatically.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    if ~exist('outDir', 'var') || isempty(outDir)        
        while true
            outDir = tempname(Util.getTempDir());
            if ~exist(outDir, 'file'); break; end
        end
    end
    
    zipName = skel.volumes.location{volId};
    zipPath = fullfile(skel.volumeDir, zipName);
    
    assert(mkdir(outDir));
    unzip(zipPath, outDir);
end
