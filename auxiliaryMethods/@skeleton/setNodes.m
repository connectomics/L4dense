function  skel  = setNodes(skel, tree_idcs, nodes)
%SETNodes Summary of this function goes here
%   from Kevin's skeleton class
%
% added functionality for simultaneaously set nodes for multiple trees
% > author: Martin Schmidt <martin.schmidt@brain.mpg.de>

if ~exist('tree_idcs', 'var') || isempty(tree_idcs)
    tree_idcs = 1:skel.numTrees;
    assert(numel(nodes) == skel.numTrees);
end

if ~iscell(nodes)
    nodes = {nodes};
    assert(numel(tree_idcs) == 1);
end

for k = 1:numel(tree_idcs)
    tree_index = tree_idcs(k);

    assert(size(nodes{k}, 1) == size(skel.nodes{tree_index}, 1));
    skel.nodes{tree_index}(:, 1 : 3) = nodes{k};
    skel.nodesNumDataAll{tree_index}(:, 3 : 5) = nodes{k};
    dimNames = 'xyz';
    for dimIt = 1 : 3
        temp = convertNumbersForNodesAsStruct(nodes{k}(:, 1));
        [skel.nodesAsStruct{tree_index}(:).(dimNames(dimIt))] = temp{:};
    end
end
end
function y = convertNumbersForNodesAsStruct(numbers)
            y = strtrim(cellstr(num2str(numbers)))';
end
