function trainingSetFwdPassForSegTest()
% Make a fwdPass through the network of region aligned with denseL4 tracing from Heiko

% Load CNN
dateStrings = '20131012T234219';
iter = 24; 
net = 26;
load(['/zdata/manuel/results/parameterSearch/' dateStrings '/iter' num2str(iter, '%.2i') '/gpu' num2str(net, '%.2i') '/' 'saveNet0000000001.mat'], 'cnet');
cnet = cnet.loadLastCNN;

% Load raw data
raw.root = '/zdata/manuel/data/cortex/2012-09-28_ex145_07x2_corrected/color/1/';
raw.prefix = '2012-09-28_ex145_07x2_corrected_mag1';
raw.bbox = [4097 4736; 4481 5248; 2250 2450];
raw.bbox = raw.bbox + [

% Normalization
if cnet.normalize
        raw = normalizeStack(single(raw));
else
        raw = single(raw);
end

% Run fwdPass
classification = cnet.fwdPass3DonCPU(raw);

save('/zdata/manuel/sync/parameterSearch/segTest.mat');

end
