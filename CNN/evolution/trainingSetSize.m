function trainingSetSize()

% Modify stacks for new structure
load('/nfs/bmo/mberning/zdata/NOBACKUP/data/e_k0563/tracing/KLEE/stackLocalCoordsXYcorrected/e_k0563_ribbon_0001_fused.mat');
target = kl_stack;
load('/nfs/bmo/mberning/zdata/NOBACKUP/data/e_k0563/KLEEcubes/e_k0563_ribbon_0001_raw.mat');
raw = kl_roi;
savePath = '/zdata/manuel/data/e_k0563/testStackNewStruct.mat';
save(savePath, 'raw', 'target');
stacks.targetFile = savePath;
settings = struct;

nets = loadNetsRecursive('/nfs/bmo/mberning/zdata/NOBACKUP/results/CNNtrain/15-Nov-2012/');
netLabels = fieldnames(nets);
for net=1:length(netLabels)
	fileLabels = fieldnames(nets.(netLabels{net}));
	for file=1:length(fileLabels)
		cnet = nets.(netLabels{net}).(fileLabels{file});
		cnet = doubleWeights(cnet);
		cnet.run.savingPath = strrep(cnet.run.savingPath, '/zdata/manuel/results/CNNtrain', '/nfs/bmo/mberning/zdata/NOBACKUP/results/CNNtrain/');
		results(net) = loadResults(cnet);
		%cnet.run.savingPath = strrep(cnet.run.savingPath, '/nfs/bmo/mberning/zdata/NOBACKUP/results/CNNtrain/15-Nov-2012/', '/zdata/manuel/sync/parameterSearch/trainingSetSize/');
		%startCPU(@plotNetActivities, {cnet, stacks, settings});
	end
end

startCPU(@plotError, {results, '/zdata/manuel/sync/parameterSearch/trainingSetSize', 1});

end
