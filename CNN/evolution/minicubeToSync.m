function minicubeToSync(param, name)

aff = readKnossosRoi(param.pathResult.root, param.pathResult.prefix, param.pathRaw.bbox, 'single', '', 'raw');
raw = readKnossosRoi(param.pathRaw.root, param.pathRaw.prefix, param.pathRaw.bbox, 'uint8', '', 'raw');
save(['/zdata/manuel/sync/parameterSearch/' name '.mat'], 'aff', 'raw');

