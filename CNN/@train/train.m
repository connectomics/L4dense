%TRAIN class representing a trainging run for a cnn 
%   inputSize - input cube size
%   eta - Learning rate
%   savingPath - Folder for saving data generated during trainGradient      
%   maxIter - max number of iterations over samples
%   maxIterMini - max number of iterations within a sample

classdef train
    %TRAIN Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        % Parameter
        outputSize
        savingPath
        maxIter
        wStart
	bStart
        % Parameter with default value
        iterations = 0;
        saveClass = @single;
        actvtClass = @gsingle;
        debug = false;
	linearRate = true;
    end
    methods
        function obj = train(varargin)
            if(nargin~=0)
                obj.outputSize = varargin{1};            
                obj.savingPath = varargin{2};
                obj.maxIter = varargin{3};
 		obj.wStart = varargin{4};
		obj.bStart = varargin{5};
            end
        end
    end
end

