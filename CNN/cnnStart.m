function cnnStart()
	% Example of starting a CNN (in debug mode for better comprehesion) using the CNN class 
	% Load raw and trace data
	stackFolder = '/zdata/manuel/data/cortex/20131222T012557/';
	load([stackFolder 'parameter.mat']);
	
	% Set up class objects
	runSetting = train([100 100 100], '/zdata/manuel/results/CNNtrain/debug/', 1e4, [1e-9 1e-9 1e-9 1e-9 1e-9], [1e-9 1e-9 1e-9 1e-9 1e-9]);
	runSetting.debug = true;
	runSetting.actvtClass = @gsingle;
	cnet = cnn(4,[10 10 10 10],[21 21 11], runSetting);
	cnet = cnet.init;
	
	% Train CNN (remove Timeit from function name to remove display of running times)
	cnet.learnTimeit(stacks, settings);
end
