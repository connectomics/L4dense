function [activity, activityWithoutNL] = fwdPass3D(cnet, input)

activity = cell(cnet.numLayer, max(cnet.numFeature));
activityWithoutNL = cell(cnet.numLayer, max(cnet.numFeature));
activity{1,1}= input;
for layer=2:cnet.numLayer
    for fm=1:cnet.layer{layer}.numFeature
        activityWithoutNL{layer,fm} = zeros(size(input) - (layer - 1) * (cnet.filterSize - 1), class(input));
        for prevFm=1:cnet.layer{layer-1}.numFeature
            activityWithoutNL{layer, fm} = activityWithoutNL{layer, fm} + convn(activity{layer-1, prevFm}, cnet.layer{layer}.W{prevFm,fm}, 'valid');
        end
	activityWithoutNL{layer,fm} = activityWithoutNL{layer,fm} + cnet.layer{layer}.B(fm);
	activity{layer, fm} = cnet.nonLinearity(activityWithoutNL{layer, fm});
    end
end

end
