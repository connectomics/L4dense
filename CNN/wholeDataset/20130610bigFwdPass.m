function bigFwdPass20130610()

% Which raw dataset
parameter.raw.root = '/zdata/manuel/data/cortex/2012-09-28_ex145_07x2/mag1/';
parameter.raw.prefix = '2012-09-28_ex145_07x2_mag1';
% Define input region
parameter.bbox = {129:128:8705 385:128:6017 1:128:3457}; % CAREFUL, bbox should be 'aligned' to KNOSSOS cubes, otherwise routines will probably overwrite part of each other(s) results
% Where to save classifier
parameter.class.root = '/zdata/manuel/results/20130610cortexFwdPass/class/';
parameter.class.prefix = parameter.raw.prefix;
% Where to save segmentation
parameter.seg.root = '/zdata/manuel/results/20130610cortexFwdPass/seg/';
parameter.seg.prefix = parameter.raw.prefix;
% Which classsifier to use
parameter.cnn.dateStrings = '20130516T204040';
parameter.cnn.iter = 8; 
parameter.cnn.gpu = 3;
parameter.cnn.root = ['/zdata/manuel/results/parameterSearch/' parameter.cnn.dateStrings '/iter' num2str(parameter.cnn.iter, '%.2i') '/gpu' num2str(parameter.cnn.gpu, '%.2i') '/'];
% Function to use for classification
parameter.class.func = @fwdPass3DonKhierachy;
% Function to use for segmentation
parameter.seg.func = @seg20130604;

jm = findResource('scheduler', 'type', 'jobmanager', 'configuration', 'fermat-cpu');
%% CLASSIFICATION
tic;
% Load last CNN from training batch given in parameter
load([parameter.cnn.root 'saveNet0000000001.mat'], 'cnet');
cnet = cnet.loadLastCNN;
cnet.run.actvtClass = @single;
% Start a task for each cube within bbox, on CPU
for x = 1:length(parameter.bbox{1})-1
	job{x} = createJob(jm, 'configuration', 'fermat-cpu');
	for y = 1:length(parameter.bbox{2})-1
		for z = 1:length(parameter.bbox{3})-1
			bbox(:,1) = [parameter.bbox{1}(x) parameter.bbox{2}(y) parameter.bbox{3}(z)];
			bbox(:,2) = [parameter.bbox{1}(x+1)-1 parameter.bbox{2}(y+1)-1 parameter.bbox{3}(z+1)-1];
			inputCell = {cnet, parameter.raw, parameter.class, bbox};
			createTask(job{x}, parameter.class.func, 0, inputCell, 'configuration', 'fermat-cpu');
		end
	end
	submit(job{x});
end
multipleJobMonitor(job);
t1 = toc;

%% SEGMENTATION
tic;
for x = 1:length(parameter.bbox{1})-1
	job{x} = createJob(jm, 'configuration', 'fermat-cpu');
	for y = 1:length(parameter.bbox{2})-1
		for z = 1:length(parameter.bbox{3})-1
			bbox(:,1) = [parameter.bbox{1}(x) parameter.bbox{2}(y) parameter.bbox{3}(z)]
			bbox(:,2) = [parameter.bbox{1}(x+1)-1  parameter.bbox{2}(y+1)-1 parameter.bbox{3}(z+1)-1]
			inputCell = {parameter, bbox};
			createTask(job{x}, parameter.seg.func, 0, inputCell, 'configuration', 'fermat-cpu');
		end
	end
	submit(job{x});
end
multipleJobMonitor(job);
t2 = toc;

%% SHOW COMPUTATION TIME
display(['Classification: ' num2str(t1/3600, '%.2f') ' hours');
display(['Segmentation: ' num2str(t2/3600, '%.2f') ' hours');

end

