% raw dataset
parameter.raw.root = '/nfs/bmo/boergens/cubing_data/boergens/cortex/results/2014-03-24_ex571_st19x1c/cubes/color/1/';
parameter.raw.prefix = '2014-03-24_ex571_st19x1c_mag1';
% Which classifier to use
parameter.cnn.dateStrings = '20130516T204040';
parameter.cnn.iter = 8;
parameter.cnn.gpu = 3;
parameter.cnn.first = ['/zdata/manuel/results/parameterSearch/' parameter.cnn.dateStrings '/iter' num2str(parameter.cnn.iter, '%.2i') '/gpu' num2str(parameter.cnn.gpu, '%.2i') '/saveNet0000000001.mat'];
parameter.cnn.GPU = true;
parameter.class.root = '/zdata/manuel/results/firstTryNewDataset/';
parameter.class.prefix = parameter.raw.prefix;

bigFwdPass(parameter, [1025 2304; 513 2304; 129 1152]);

%% WAIT HERE until job is finished
% copy results to matlab file afterwards
raw = readKnossosRoi(parameter.raw.root, parameter.raw.prefix, [1061 2060; 601 2120; 201 1030]);
save('/zdata/manuel/firstTryOn20140324ex571_raw.mat', 'raw', '-v7.3');
class = readKnossosRoi(parameter.class.root, parameter.class.prefix, [1061 2060; 601 2120; 201 1030], 'single', '', 'raw');
save('/zdata/manuel/firstTryOn20140324ex571.mat', 'class', '-v7.3');

