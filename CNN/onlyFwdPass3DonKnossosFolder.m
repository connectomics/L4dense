function onlyFwdPass3DonKnossosFolder(cnetLocation, gpuSwitch, input, result, bbox, normFunc)

% Load the CNN used for classification
load(cnetLocation, 'cnet');
% Fix needed fur to fermat -> gaba transition
cnet.run.savingPath = [fileparts(cnetLocation) '/'];
cnet = cnet.loadLastCNN;
if gpuSwitch
    cnet.run.actvtClass = @gpuArray;
else
    cnet.run.actvtClass = @single;
end

% Load data with right border for cnet
bboxWithBorder(:,1) = bbox(:,1) - ceil(cnet.randOfConvn'/2);
bboxWithBorder(:,2) = bbox(:,2) + ceil(cnet.randOfConvn'/2);
raw = loadRawData(input, bboxWithBorder);

% Normalize data
if cnet.normalize
	raw = normFunc(single(raw));
else
	raw = single(raw);
end

% Memory efficent fwd pass
classification = onlyFwdPass3D(cnet, raw);

% Save result to KNOSSOS folder
saveClassData(result, bbox(:, 1)', classification);

end
