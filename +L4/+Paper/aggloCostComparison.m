% total cost (computational/$) of the L4 agglomeration comparison
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

%% runtimes for our agglomeration on ex145_07x2_ROI2017

vol_ex145 = 61*94*92; %um^3
t_typeEM = 24*3; % h (number from amotta - estimate)
t_synEM = 5; % h (number from bstaffler - extrapolated from ex145_segNewBig)
t_rest = 1*24; % h (number from mberning - estimate)

cost_per_cpu_h = 6.7/64; %64 cpu, ~1000G ram; $/h (ec2 amazon price)
cost_gaba_cpu_h = cost_per_cpu_h * 16*24; % 16*24: (~num cores on gaba available using same ram as ec2)* num nodes on gaba

cost_pipeline = cost_gaba_cpu_h * (t_typeEM + t_synEM + t_rest);

%% januszewski et al., 2017, biorxiv

cost_per_gpu_h = 0.9; % $/h (ec2 amazon)
num_gpus = 1000; % supp table 3
t = 16.02; % supp table 3
vol_songbird = 96*98*114; %um^3; results 4 paragraph

cost_mj = cost_per_gpu_h * num_gpus * t * (vol_ex145/vol_songbird);