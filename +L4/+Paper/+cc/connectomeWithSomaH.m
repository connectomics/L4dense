% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>
%
% Creates synapse agglos and connectome using the soma FP exclusion
% heuristic
% (just as a reference of the steps; was not calculated via this script)


%% synapse agglos

L4.Synapses.Scripts.synapseDetection_v2
clear


%% connectome

L4.Connectome.Scripts.generateConnectome_v6
clear


%% intersyn distance

connectEM.Axon.Script.calculateSynToSynDists_v2
clear


%% synapse type

connectEM.Synapse.Script.classifyType_v2
clear


%% spine synapse fraction

connectEM.Connectome.plotSpineSynapseFraction_v2
clear


%% specificities

connectEM.Connectome.plotSpecificitiesVsNullHypothesis_v2
clear;


%% synapse density

connectEM.Axon.Script.detectThalamocorticals_v2
clear;


%% fraction of axon specificity

connectEM.Figure.fractionOfAxonsSpecific_v2
clear


%% coinnervation

connectEM.Figure.coinnervation_v2
clear;

