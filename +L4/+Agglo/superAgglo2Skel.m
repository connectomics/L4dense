function skel = superAgglo2Skel( agglo, skel )
%SUPERAGGLO2SKEL Convert a super agglo to a skeleton object.
% INPUT agglo: [Nx1] struct
%           Struct array containing the fields 'nodes' and 'edges'.
%       skel: (Optional) skeleton object
%           Skeleton object to which the agglos are added as new trees.
% OUTPUT skel: skeleton object
%           Skeleton where each agglo is saved in a separate tree.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('skel', 'var') || isempty(skel)
    skel = Skeleton.setParams4Dataset([], 'ex145_ROI2017');
end

for i = 1:length(agglo)
    skel = skel.addTree(sprintf('Agglo%3d', i), agglo(i).nodes(:,1:3), ...
        agglo(i).edges);
end

end

