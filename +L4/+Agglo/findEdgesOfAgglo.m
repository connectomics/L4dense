function edgeIdx = findEdgesOfAgglo( agglos, edges )
%FINDEDGESOFAGGLO Get the indices of all edges for an agglo.
% INPUT agglos: [Nx1] cell
%           Agglos as list of segment ids.
%       edges: [Nx2] int
%           Global edge list.
% OUTPUT edgeIdx: [Nx1] cell
%           Linear indices w.r.t edges containing the edges for the
%           corresponding agglo.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

% edge list wrt agglos
lut = L4.Agglo.buildLUT(agglos, max(edges(:)));
edgesA = lut(edges);

% edge indices between different agglos
idx = any(edgesA, 2);

edgesA = edgesA(idx, :);
edgesA = sort(edgesA, 2);
[c, ~, ic] = unique(edgesA(:));
if c(1) == 0
    tmpIdx = ic == 1; % sort out zeros
    ic(tmpIdx) = [];
    ic = ic - 1;
end

tmp = repmat(find(idx), 2, 1);
tmp(tmpIdx) = [];
edgeIdx = accumarray(ic, tmp, [], @(x){x});
edgeIdx = cellfun(@sort, edgeIdx, 'uni', 0);

end

