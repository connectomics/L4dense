% check agglo overlaps within and between axon and dendrite agglos again
% Date: 06.03.2018
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

info = Util.runInfo(false);
p = Gaba.getSegParameters('ex145_ROI2017');


%% load axons

[axons, indAxons] = L4.Axons.getLargeAxons(p, true, true);
idx = cellfun(@isempty, axons);
axons = axons(~idx);
indAxons = indAxons(~idx);


%% load dendrites 

Util.log('Loading dendrites and target classes.');
[dendFile, targetClass, targetLabels] = L4.Connectome.buildTargetClasses();
m = load(dendFile);
dendrites = m.dendAgglos(targetClass ~= 'Ignore');
targetClass(targetClass == 'Ignore') = [];


%% overlap calculation

% within axons
[ovAgglos_ax, segId_ax, cc_ax] = L4.Agglo.aggloOverlaps(axons);

% within dendrites
[ovAgglos_de, segId_de, cc_de] = L4.Agglo.aggloOverlaps(dendrites);

% between both
l_ax = length(axons);
agglos = vertcat(axons, dendrites);
[ovAgglos, segId, cc] = L4.Agglo.aggloOverlaps(agglos);
ovAgglos(cellfun(@(x)all(x <= l_ax) | all(x > l_ax), ovAgglos)) = [];
cc(cellfun(@(x)all(x <= l_ax) | all(x > l_ax), ovAgglos)) = [];


%% some statistics

stats.overlapping_axons = sum(cellfun(@length, cc_ax));
stats.overlapping_dends = sum(cellfun(@length, cc_de));
stats.overlapping_inter_ax_de = sum(cellfun(@length, cc));