function contactome = findEdgesBetweenAgglos2( agglos1, agglos2, edges )
%FINDEDGESBETWEENAGGLOS2 Find pairwise edges between two sets of agglos.
%
% INPUT agglos1, agglos2: [Nx1] struct or cell
%           Agglos in the agglo or superagglo format.
%       edges: [Nx2] int
%           Global edge list.
%
% OUTPUT contactome: [Nx2] table
%           Table containing the rows
%           'aggloIdx': [1x2] int. The indices of the agglos that do touch
%               (first index wrt. to agglos1)
%           'edgeIdx': [Nx1] int
%               The linear indices of all edges between the corresonding
%               agglos.
%
% NOTE To find edges between all agglos use
%      L4.Agglo.findEdgesBetweenAgglos
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if isstruct(agglos1)
    agglos1 = Superagglos.getSegIds(agglos1);
end
if isstruct(agglos2)
    agglos2 = Superagglos.getSegIds(agglos2);
end

l = length(agglos1);
[c, edgeIdx] = L4.Agglo.findEdgesBetweenAgglos(cat(1, agglos1, agglos2), ...
    edges);

% delete touches withing agglos1 or agglos2
toDel = all(c <= l, 2) | all(c >= l, 2);
c = sort(c(~toDel, :), 2); % sort just to make sure
assert(all(c(:,1) <= l) & all(c(:,2) > l));
edgeIdx = edgeIdx(~toDel);

% correct indices in c
c(c > l) = c(c > l) - l;

% output
contactome.aggloIdx = c;
contactome.edgeIdx = edgeIdx;
contactome = struct2table(contactome);

end

