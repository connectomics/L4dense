function lut = buildLUT( agglos, maxSegId, ovMode )
%BUILDLUT Build a lookup table for agglos.
% INPUT agglos: [Nx1] cell
%           Cell array of integer ids. Cells should be non-overlapping.
%       maxSegId: (Optional) int
%           The maximal segment id. If the maximal segment id in agglos is
%           smaller than this then the output will be enlarged up to this
%           id.
%       ovMode: (Optional) logical
%           Flag to indicate that agglos can be overlapping. In this case
%           the output will be a cell array containing all agglos that
%           contain a segId (and [] if an id is in no agglo). Note that
%           each agglos should be unique in this case otherwise non-unique
%           agglo ids are listed several times in the lookup table.
%           (Default: false)
% OUTPUT lut: [Nx1] int
%           Mapping containing the linear index of the agglo at the
%           locations of the segment id, i.e. lut(segId) = aggloIdx.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('ovMode', 'var') || isempty(ovMode)
    ovMode = false;
end

if exist('maxSegId', 'var') && ~isempty(maxSegId)
    if ~ovMode
        lut = zeros(maxSegId, 1);
    end
end

if ovMode
    % overlapping agglos
    lut = accumarray(cell2mat(agglos), ...
        repelem((1:length(agglos))', cellfun(@length, agglos)), ...
        [], @(x){x});
    lut(end+1:maxSegId) = {[]};
else
    % default case with non-overlapping agglos
    lut(cell2mat(agglos)) = repelem((1:length(agglos))', ...
        cellfun(@length, agglos));
end
lut = lut(:);

end

