function [ skel ] = agglo2Nml( agglos, segmentCom, skel, noMST )
%AGGLO2NML Convert agglomerations to a nml using MST of centroids.
% INPUT agglos: [Nx1] int or [Nx1] cell
%           Segment ids of the agglomeration segments or cell array of
%           segment ids. Each cell will be saved to a separate tree.
%       segmentPoints: [Nx3] int
%           Segment center of mass/points that will be used as the node
%           location for an agglomeration id.
%       skel: (Optional) skeleton object
%           The agglomerates are added as additional trees to skel.
%           (Default: new skeleton is created)
%       noMST: (Optional) logical
%           Flag to simply add the nodes connected as they appear in
%           agglos.
%           (Default: false)
% OUTPUT skel: skeleton object
%           Skeleton object containing each agglomeration as a single tree.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('noMST', 'var') || isempty(noMST)
    noMST = false;
end

if ~iscell(agglos)
    agglos = {agglos};
end

if ~exist('skel', 'var') || isempty(skel)
    skel = Skeleton.setParams4Dataset([], 'ex145_ROI2017');
end

if noMST
    for i = 1:length(agglos)
        skel = skel.addTree([], segmentCom(agglos{i}, :));
    end
else
    skel = Skeleton.fromMST( ...
        cellfun(@(ids) {segmentCom(ids, :)}, agglos), ...
        [11.24, 11.24, 28], skel);
end


end

