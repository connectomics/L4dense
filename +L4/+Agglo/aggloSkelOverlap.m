function [skelToAgglos, segIds] = aggloSkelOverlap( gtSkels, p, agglos )
%AGGLOSKELOVERLAP Get the overlap of skeletons with agglos.
% INPUT gtSkels: [Nx1] cell of skeleton objects or [Mx1] cell
%           The ground truth skeletons or the corrresponding segment ids
%           for the nodes, i.e. the output of
%           Skeleton.getSegmentIdsOfSkelCellArray(p, skels).
%       p: struct or [Nx1] cell
%           Segmentation parameter struct to load the skeleton node segment
%           ids. Only required if the first input are skeleton objects.
%       agglos: [Nx1] struct or cell
%           The agglos used for overlap calculation in the agglo or
%           superagglo format.
% OUTPUT skelToAgglos: [Nx1] cell
%           For each gtSkel it contains the overlapping agglos in the
%           first row and the number of skeleton nodes in the corresponding
%           agglo in the second row.
%        segIds: [Nx1] cell
%           The segment ids for the gtSkels.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if isa(gtSkels{1}, 'skeleton')
    segIds = Skeleton.getSegmentIdsOfSkelCellArray(p, gtSkels);
else
    segIds = gtSkels;
end

if isstruct(agglos)
    agglos = Superagglos.getSegIds(agglos);
end

if iscell(segIds{1})
    % combine ids of all trees of the gt skels
    segIdsC = cellfun(@(x)vertcat(x{:}), segIds, 'uni', 0);
end

m = max(cellfun(@max, segIdsC));
aggloLUT = Agglo.buildLUT(m, agglos);

skelToAgglos = cellfun(@(x)aggloLUT(x(x > 0)), segIdsC, 'uni', 0);
% add zero to have only the counts for the numbers in x
skelToAgglos = cellfun(@(x)tabulate([0; x]), skelToAgglos, 'uni', 0);
skelToAgglos = cellfun(@(x)x(x(:,1) > 0, 1:2), ...
    skelToAgglos, 'uni', 0);

end

