function p = updateParamsToNewestFiles( p )
%UPDATEPARAMSTONEWESTFILES Update the paths in p to the newest pipeline
%result files.
% INPUT p: struct
%           Segmentation parameter struct.
% OUTPUT p: struct
%           Update segmentation parameter struct.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~isfield(p, 'svg')
    p = Seg.Util.addSvgFiles(p);
end

% add newest svg files
graphFile = 'graphNew.mat';
corrFile = 'correspondencesNew2.mat';
contProbFile = 'globalGPProbList.mat';
heuristicFile = 'heuristicResult.mat';

p.svg.graphFile = fullfile(p.saveFolder, graphFile);
p.svg.correspondenceFile = fullfile(p.saveFolder, corrFile);
p.svg.contProbFile = fullfile(p.saveFolder, contProbFile);
p.svg.heuristicFile = fullfile(p.saveFolder, heuristicFile);

% add agglo state fiels
p.agglo.saveFolder = fullfile(p.saveFolder, ['aggloState' filesep]);
p.agglo.axonAggloFile = fullfile(p.agglo.saveFolder, 'axons_19_a.mat');
p.agglo.dendriteAggloFile = fullfile(p.agglo.saveFolder, ...
    'dendrites_08.mat');
p.agglo.spineHeadFile = fullfile(p.agglo.saveFolder, ...
    'spine_heads_and_attachment_03.mat');
p.agglo.somaFile = fullfile(p.saveFolder, ...
    'soma_BS', 'somaAgglo_07.mat');

% connectome state fields
p.connectome.saveFolder = fullfile(p.saveFolder, ...
    ['connectomeState' filesep]);
p.connectome.synFile = fullfile(p.connectome.saveFolder, ...
    'SynapseAgglos_v8.mat');

% change local synapse files
p = Seg.Util.changeFilename(p, 'synapseFile', 'synapseScores.mat');

end
