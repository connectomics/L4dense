function p = getParams()
%GETPARAMS Get the segmentation parameter struct.
%
% OUTPUT p: struct
%           Segmentation parameter struct for the dataset
%           2012-09-28_ex145_07x2_ROI2017.
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>


p = Gaba.getSegParameters('ex145_ROI2017');

end

