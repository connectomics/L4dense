% prepare unattached spine heads for ortho tracing in wk
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

info = Util.runInfo(false);
param.marginNM = 3000;
info.param = param;

%% load data

p = Gaba.getSegParameters('ex145_ROI2017');
m = load(fullfile(p.agglo.saveFolder, 'spine_heads_and_attachment_03.mat'));
attached = m.attached;
shAgglos = m.shAgglos;
segmentMeta = load(p.svg.segmentMetaFile, 'point', 'voxelCount');
segmentMeta.point = segmentMeta.point';
vxC = segmentMeta.voxelCount;

%% get points for unattached spines

isUnattached = (attached == 0);

% use larger agglo to query (hack with find to make it in cellfun)
shAgglos = cellfun(@(x)x(find(vxC(x) == max(vxC(x)), 1)), shAgglos);
queryPoints = segmentMeta.point(shAgglos(isUnattached), :);

%% exclude points at border of dataset (3um)

% discard borders at boundary
margin = round(param.marginNM./p.raw.voxelSize(:));
bboxM = bsxfun(@plus, p.bbox, [margin, -margin]);
isInCenter = Util.isInBBox(queryPoints, bboxM);
queryPoints_center = queryPoints(isInCenter, :);

%% save result

outputFile = '/tmpscratch/bstaffle/data/L4/spines/SpineHeadQueries.mat';
if exist(outputFile, 'file')
    warning('File %s already exists.', outputFile);
else
    Util.log('Saving output to %s.', outputFile);
    save(outputFile, 'queryPoints', 'queryPoints_center', 'info');
end

%% examples for annotation

rng('shuffle')
idx = randi(size(queryPoints_center, 1), 50, 1);
samplePoints = queryPoints_center(idx, :);
t = table(idx, samplePoints, 'VariableNames', {'SpineIdx', 'Coordinates'});
% writetable(t, 'SpineHeadTaskSamples_center.xlsx');