function [ syn2Agglo, pre2syn, post2syn, connectome ] = ...
    synapsesAggloMapping( synapses, presynAgglos, postsynAgglos )
%SYNAPSESAGGLOMAPPING Get the pre- and postsynaptic agglos for synapses.
% INPUT synapses: table
%           Table containing the 'presynId' and 'postsynId' colums with
%           pre- and postsynaptic segment ids for each synapse.
%           (see also L4.Synapses.clusterSynapticInterfaces)
%       presynAgglos: [Nx1] cell or [Nx1] struct
%           Presynaptic agglomeration as a cell array of segment id for
%           each agglo or as a struct array with nodes and edges for each
%           agglo (superagglo). The overlap with this agglos and the
%           presynId of the synapses is calculated. The presynAgglos are
%           assumed to be non-overlapping. It is possible to insert [].
%       postsynAgglos: [Nx1] cell or [Nx1] struct
%           Postsynaptic agglomeration as a cell array of segment id for
%           each agglo or as a struct array with nodes and edges for each
%           agglo (superagglo). The overlap with this agglos and the
%           postsynId of the synapses is calculated. The postsynAgglos are
%           assumed to be non-overlapping. It is possible to insert [].
% OUTPUT syn2Agglo: [Nx2] table
%           The indices of the presynaptic agglomerates overlapping with
%           the presynaptic ids of the corresponding synapse and
%           the indices of the postsynaptic agglomerates overlapping with
%           the postsynaptic ids of the corresponding synapse.
%           [] is used if there is no agglo found.
%        pre2syn: [Nx1] cell
%           The indices of the synapses whose presynaptic ids overlap
%           with the corresponding presynaptic agglomerate.
%           [] is used if there is no synapse found.
%        post2syn: [Nx1] cell
%           The indices of the synapses whose postsynaptic ids overlap
%           with the corresponding postsynaptic agglomerate.
%           [] is used if there is no synapse found.
%        connectome: [Nx2] table
%           Table containing which pre- and postsynaptic agglos connect
%           ('edges') and the corresponding synapse indices ('synIdx').
%           (see also L4.Synapses.connectomeFromSyn2Agglo)
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

% convert superagglos to cell arrays of ids
hasPresyn = ~isempty(presynAgglos);
hasPostsyn = ~isempty(postsynAgglos);
if hasPresyn && isstruct(presynAgglos)
    presynAgglos = Superagglos.getSegIds(presynAgglos);
end
if hasPostsyn && isstruct(postsynAgglos)
    postsynAgglos = Superagglos.getSegIds(postsynAgglos);
end

% get lookup tables for agglos
Util.log('Calculating synapse to agglo mapping.');
m1 = 0;
m2 = 0;
if hasPresyn
    m1 = max(cellfun(@max, presynAgglos));
end
if hasPostsyn
    m2 = max(cellfun(@max, postsynAgglos));
end
m3 = max(cellfun(@max, synapses.presynId));
m4 = max(cellfun(@max, synapses.postsynId));
m = max([m1, m2, m3 , m4]);

if hasPresyn
    presynAggloLUT = Agglo.buildLUT(m, presynAgglos);
    syn2pre = cellfun(@(x)setdiff(presynAggloLUT(x), 0), ...
    synapses.presynId, 'uni', 0);
else
    syn2pre = cell(size(synapses, 1), 1);
end

if hasPostsyn
    postsynAggloLUT = Agglo.buildLUT(m, postsynAgglos);
    syn2post = cellfun(@(x)setdiff(postsynAggloLUT(x), 0), ...
        synapses.postsynId, 'uni', 0);
else
    syn2post = cell(size(synapses, 1), 1);
end
syn2Agglo = table(syn2pre, syn2post, 'VariableNames', ...
    {'syn2pre', 'syn2post'});

if nargout == 1
    return;
end

% getting lookup tables for synapses
Util.log('Calculating agglo to synapse mapping.');

% prepare for overlapping lookup table
upresynId = synapses.presynId;
upresynId(cellfun(@length, upresynId) > 1) = cellfun(@unique, ...
    upresynId(cellfun(@length, upresynId) > 1), 'uni', 0);
upostsynId = synapses.postsynId;
upostsynId(cellfun(@length, upostsynId) > 1) = cellfun(@unique, ...
    upostsynId(cellfun(@length, upostsynId) > 1), 'uni', 0);

presynLUT = L4.Agglo.buildLUT(upresynId, m, true);
postsynLUT = L4.Agglo.buildLUT(upostsynId, m, true);

if hasPresyn
    pre2syn = cellfun(@(x)unique(cell2mat(presynLUT(x))), ...
        presynAgglos, 'uni', 0);
else
    pre2syn = [];
end
if hasPostsyn
    post2syn = cellfun(@(x)unique(cell2mat(postsynLUT(x))), ...
        postsynAgglos, 'uni', 0);
else
    post2syn = [];
end

% connectome
if hasPresyn && hasPostsyn && (nargout > 3)
    Util.log('Calculating the connectome.');
    connectome = L4.Connectome.connectomeFromSyn2AggloMapping(syn2Agglo);
elseif nargout > 3
    connectome = [];
end

end

