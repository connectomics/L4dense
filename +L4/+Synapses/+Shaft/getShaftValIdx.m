function idx = getShaftValIdx( cubeFolder, discardUndecided, areaT )
%GETSHAFTVALIDX Hack to get the indices of the shaft synapses validation
% set within the whole validation set.
% INPUT cubeFolder: string
%           Path to training cube folders.
%       discardUndecided: (Optional) logical
%           Flag to discard undecided interfaces as well.
%       areaT: (Optional) int
%           Only interfaces larger than area threshold (>) are considered.
%           (Default: 150)
% OUTPUT idx: [8x1] cell
%           Cell array with the indices for validation cube.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('discardUndecided', 'var') || isempty(discardUndecided)
    discardUndecided = false;
end

if ~exist('areaT', 'var') || isempty(areaT)
    areaT = 150;
end

s = what(cubeFolder);
valIdx = getValCubes(s.mat);

idx = cell(length(valIdx), 1);
for i = 1:length(valIdx)
    m = load(fullfile(cubeFolder, s.mat{valIdx(i)}));
    area = cellfun(@length, m.data.interfaceSurfaceList);
    toDel = false(length(m.interfaceLabels), 1);
    if discardUndecided
        undec = m.undecidedList;
    else
        undec = false(length(m.interfaceLabels), 1);
    end

    tL = m.typeLabels;
    toDelNonShaftSyns = tL.idx(~ismember(tL.type, {'shaft', 'soma'}));
    toDel(toDelNonShaftSyns) = true;
    toDel(undec) = [];
    area(undec) = [];
    toDel(area <= areaT) = [];
    idx{i} =  ~[toDel; toDel];
end


end

function idx = getValCubes(files)
prefix = '2012-09-28_ex145_07x2_mag1_';
validationCubes = { ...
    [prefix 'x1501-2000_y1501-2000_z1501-1700.mat'], ...
    [prefix 'x1501-2000_y1501-2000_z2001-2200.mat'], ...
    [prefix 'x2001-2500_y1001-1500_z1001-1200.mat'], ...
    [prefix 'x2001-2500_y2001-2500_z1901-2100.mat'], ...
    [prefix 'x2001-2500_y3001-3500_z801-1000.mat'], ...
    [prefix 'x3001-3500_y3001-3500_z1401-1600.mat'], ...
    [prefix 'x4001-4500_y2501-3000_z1001-1200.mat'], ...
    [prefix 'x7001-7500_y5001-5500_z1501-1700.mat']};
idx = find(ismember(files, validationCubes));
end