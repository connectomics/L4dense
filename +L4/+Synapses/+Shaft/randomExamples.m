% export random examples from shaft classifiers to WK
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

p = Gaba.getSegParameters('ex145_ROI2017');
if ~ispc
    inhTestFolder = ['/u/bstaffle/data/L4/Synapses/PerformanceAssessment/' ...
        'InhTestSetROI2017'];
else
    inhTestFolder = ['E:\workspace\data\backed\L4\Synapses\' ...
        'PerformanceAssessment\InhTestSetROI2017\'];
    p.saveFolder = 'E:\workspace\data\backed\20170217_ROI\';
    p.svg.synScoreFile = ['E:\workspace\data\backed\20170217_ROI\' ...
        'globalSynScores.mat'];
    p.svg.borderFile = ['E:\workspace\data\backed\20170217_ROI\' ...
        'globalBorder.mat'];
end

m = load(p.svg.borderFile, 'borderCoM');
borderCom = m.borderCoM;

m = load(fullfile(p.saveFolder, 'globalSynScores.mat'));
synScores = max(m.synScores, [], 2);

files = {'globalSynScores_cnn17_2_synem_preOnly_trShaft.mat', ...
    'globalSynScores_cnn17_2_synem_trShaft.mat', ...
    'globalSynScores_synem_trShaft.mat'}';


%% cnn17_2 pre only trShaft

m = load(fullfile(p.saveFolder, files{1}));
borderCom = borderCom(m.edgeIdx, :);
scores = max(m.synScores, [], 2);

[~,sI] = sort(scores, 'descend');

N = 100;
skel = L4.Util.getSkel();
skel = skel.addNodesAsTrees(borderCom(sI(1:N), :));
skel.names = arrayfun(@(x,y,z)sprintf('%s - Score: %.2f - SynEM score: %.2f', x{1}, y, z), ...
    skel.names, scores(sI(1:N)), synScores(sI(1:N)), 'uni', 0);
skel.write('cnn17_2_synem_preOnly_trShaft_top100.nml');


%% cnn17_2 trShaft

m = load(fullfile(p.saveFolder, files{2}));
scores = max(m.synScores, [], 2);

[~,sI] = sort(scores, 'descend');

N = 100;
skel = L4.Util.getSkel();
skel = skel.addNodesAsTrees(borderCom(sI(1:N), :));
skel.names = arrayfun(@(x,y,z)sprintf('%s - Score: %.2f - SynEM score: %.2f', x{1}, y, z), ...
    skel.names, scores(sI(1:N)), synScores(sI(1:N)), 'uni', 0);
skel.write('cnn17_2_synem_trShaft_top100.nml');


%% synem trShaft

m = load(fullfile(p.saveFolder, files{3}));
scores = max(m.synScores, [], 2);

[~,sI] = sort(scores, 'descend');

N = 100;
skel = L4.Util.getSkel();
skel = skel.addNodesAsTrees(borderCom(sI(1:N), :));
skel.names = arrayfun(@(x,y,z)sprintf('%s - Score: %.2f - SynEM score: %.2f', x{1}, y, z), ...
    skel.names, scores(sI(1:N)), synScores(sI(1:N)), 'uni', 0);
skel.write('synem_trShaft_top100.nml');
