% compare classifiers on validation set with shaft synapses only
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>


%% shaft syn performance for classifiers trained directly on shaft syns

runFolder = '/tmpscratch/bstaffle/data/SynEM/Classifier/runShaft/';
list = dir(runFolder);
subfolder = {list([list.isdir]).name}';
subfolder(ismember(subfolder, {'.', '..'})) = [];
subfolder(cellfun(@(x)isempty(strfind(x, 'trShaft')), subfolder)) = [];
N = length(subfolder);
rpVal = cell(N, 1);
rpTest = cell(N, 1);
names = cell(N, 1);
for i = 1:N
    m = load(fullfile(runFolder, subfolder{i}, 'classifier', ...
        'result_LogitBoost.mat'));
    rpVal{i} = m.result.rpVal;
    rpTest{i} = m.result.rpTest;
    names{i} = subfolder{i};
end


%% evaluate full classifiers again

% full validation set to shaft syn validation set indices
idx = L4.Synapses.Shaft.getShaftValIdx(['/gaba/u/bstaffle/data/SynEM/' ...
    'SynapseDetectionTrainingData_2_typeLabels']);
idx = cell2mat(idx);

runDissFolder = '/tmpscratch/bstaffle/data/SynEM/Classifier/runDiss/';
runShaftFolder = '/tmpscratch/bstaffle/data/SynEM/Classifier/runShaft/';
runs = {fullfile(runDissFolder, 'synem_tr2/'), ...
    fullfile(runDissFolder, 'cnn17_4_large_synem_tr2/'), ...
    fullfile(runDissFolder, 'cnn17_synem_tr2/'), ...
    fullfile(runShaftFolder, 'cnn17_2_synem_preOnly_tr2/'), ...
    fullfile(runShaftFolder, 'synem_preOnly_tr2/')};
for i = 1:length(runs)
    runFolder = runs{i};
    % validation performance
    [X_val, y_val] = SynEM.Util.getTrainingDataFrom( ...
        fullfile(runFolder, 'val'), 'direction', false, false);
    X_val = X_val(idx, :);
    y_val = y_val(idx);
    m = load(fullfile(runFolder, 'classifier', 'LogitBoost.mat'));
    [~, scoresVal] = m.classifier.predict(X_val);
    rpVal{end+1} = SynEM.Eval.interfaceRP(y_val, scoresVal);
    m = load(fullfile(runFolder, 'classifier', 'result_LogitBoost.mat'));
    rpTest{end+1} = m.result.rpTest;
    [~, names{end+1}] = fileparts(fileparts(runFolder));
    clear m X_val y_val
end


%% result to table

names = cellfun(@(x)strrep(x, '_all_tr2', ''), names, 'uni', 0);
names(1:N) = cellfun(@(x)strrep(x, 'tr2', 'trShaft'), names(1:N), 'uni', 0);
names = cellfun(@(x)strrep(x, '_tr2', ''), names, 'uni', 0);
t = table(names(:), rpVal(:), rpTest(:), 'VariableNames', ...
    {'name', 'rpVal', 'rpTest'});


%% plot

N = 8; % trained on shaft syns
Visualization.plotPRCurve(t.rpVal(1:N))
Visualization.plotPRCurve(t.rpVal(N+1:end), '--', [], 0, 0, gcf)
legend(strrep(t.name, '_', '\_'), 'Location', 'SouthWest');
title('Validation set (shaft syn only)')

Visualization.plotPRCurve(t.rpTest(1:N))
Visualization.plotPRCurve(t.rpTest(N+1:end), '--', [], 0, 0, gcf)
legend(strrep(t.name, '_', '\_'), 'Location', 'SouthWest');
title('Test set performance (all synapses)')
