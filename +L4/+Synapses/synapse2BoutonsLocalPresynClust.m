function boutons = synapse2BoutonsLocalPresynClust( synapses, ...
    edges, prob, probT, dist, borderCom, borderDistT, scale )
%SYNAPSE2BOUTONSLOCALPRESYNCLUST Cluster synapses into boutons by
%associating synapses by doing a local presynaptic agglomeration and
%collecting synapses whose presyn agglomeration overlap.
% INPUT synapses: [Nx1] table
%           Table with synapses. For the presynaptic agglomeration the
%           presynaptiId column is needed and for the borderCom clustering
%           the edgeIdx column.
%           (see also output of L4.Synapses.clusterSynapticInterfaces)
%       edges: [Nx2] int
%           The global edge list. Correspondences need to be contained here
%           if they should be included.
%       prob: [Nx1] double
%           The continuity probabilities for the corresponding edges.
%       probT: double
%           Lower threshold on the continuity probabilities to consider to
%           edges as connected.
%       dist: int
%           Number of agglomeration steps starting from the presynaptic
%           synapse segments.
%       borderCom: (Optional) [Nx3] double
%           The global com list for borders. If supplied boutons can
%           further be clustered based on maximal distance between
%           synapses. This is only used if borderDistT is also supplied.
%           (Default: no clustering based on com distance).
%       borderDistT: (Optional) double
%           Distance threshold in units of that is used to further cluster
%           synapses associated with one bouton from the presynaptic
%           agglomeration. This is only used if borderCom was supplied.
%           (Default: no clustering based on com distance).
%       scale: (Optional) [1x3] double
%           Voxel size for border com.
%           (Default: [1, 1, 1])
% OUTPUT boutons: [Nx1] cell
%           Cell array containing the linear indices of all synapses that
%           have overlapping local presynaptic agglomerations and are thus
%           assumed to belong to the same bouton.
%
% NOTE The local presynaptic agglomeration is done for each synapse
%      separately and overlapping presynaptic agglos are combined. Hence
%      with dist = N, (N-1) segments between two presynaptic segments can
%      potentially be bridged (depending on their prob).
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

% boutons based on presynaptic agglomeration only
[~, boutons] = L4.Agglo.distRestrictedAgglo(synapses.presynId, ...
    edges, prob, probT, dist, true);
boutons = Util.group2Cell(boutons);

% further cluster all synapses of the same bouton based on distance
if exist('borderCom', 'var') && exist('borderDistT', 'var')
    % synapse coms
    if exist('scale', 'var')
        borderCom = bsxfun(@times, borderCom, scale(:)');
    end
    synCom = cellfun(@(x)mean(borderCom(x, :), 1), synapses.edgeIdx, ...
        'uni', 0);
    T = cellfun(@(x)distClustering(vertcat(synCom{x}), borderDistT), ...
        boutons, 'uni', 0);
    boutons = cellfun(@(t, idx)accumarray(t, idx, [], @(x){x}), ...
        T, boutons, 'uni', 0);
    boutons = vertcat(boutons{:});
end

end

function T = distClustering(coms, distCutoff)
if size(coms, 1) > 1
    T = clusterdata(coms, 'linkage', 'single', ...
                'criterion', 'distance', 'cutoff', distCutoff);
else
    T = 1;
end
end

