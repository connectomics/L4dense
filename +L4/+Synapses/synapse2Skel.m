function skel = synapse2Skel( synapses, borderCom, segmentCom, skel, ...
    mode, idx )
%SYNAPSE2SKEL Write synapses to a skeleton for inspection.
% INPUT synapses: [Nx2] table
%           Table of synapses containing the columns 'edgeIdx' and if
%           segmentCom is supplied also 'presynId' and 'postsynId'.
%       borderCom: [Nx3] double
%           The border com list. The first edgeIdx of a synapse is used to
%           plot the border position.
%       segmentCom: (Optional) [Nx3] double
%           Segment com list. If this input is supplied then the pre- and
%           postsynaptic segments of a synapse are added to the tree as
%           well.
%       skel: (Optional) skeleton object
%           The synapses are added as trees to the skeleton object.
%           (Default: new skeleton object is created).
%       mode: (Optional) string
%           Mode for synapse position
%           'first': (Default) First edge index in a synapse is used for
%               plotting.
%           'all': Coms of all edges in a synapse are plotted.
%       idx: (Optional) [Nx1] int
%           Linear indices of the synapses that is added to the tree name.
%           (Default: index of the position in synapses)
% OUTPUT skel: skeleton object
%           The skeleton object with each synapse in a separate tree.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('skel', 'var') || isempty(skel)
    skel = L4.Util.getSkel();
end

if ~exist('mode', 'var') || isempty(mode)
    mode = 'first';
end

if ~exist('idx', 'var') || isempty(idx)
    idx = 1:size(synapses, 1);
end

switch mode
    case 'first'
        synCoords = cellfun(@(x)borderCom(x(1), :), synapses.edgeIdx, ...
            'uni', 0);
    case 'all'
        synCoords = cellfun(@(x)borderCom(x(end:-1:1), :), ...
            synapses.edgeIdx, 'uni', 0);
    otherwise
        errror('Unknown mode %s.', mode);
end

if exist('segmentCom', 'var') && ~isempty(segmentCom)
    preCoords = cellfun(@(x)segmentCom(x(1),:), synapses.presynId, 'uni', 0);
    postCoords = cellfun(@(x)segmentCom(x(1),:), synapses.postsynId, 'uni', 0);
    % sort nodes such that synCoord is last = active node
    nodes = cellfun(@(x, y, z)cat(1, x, y, z), preCoords, postCoords, ...
        synCoords, 'uni', 0);
    % edges between nodes (connect pre coord & post coord to last syn
    % coord and synCoords from first to last node)
    edges = cellfun(@(x)reshape([1, 2+ size(x, 1), 2, 2 + size(x, 1), ...
        2 + (1:(size(x, 1) - 1)), 2 + (2:size(x, 1))], 2, [])', ...
        synCoords, 'uni', 0);
else
    nodes = synCoords;
    edges = cell(length(nodes), 1);
end

for i = 1:size(synapses, 1)
    skel = skel.addTree(sprintf('Synapse%03d', idx(i)), nodes{i}, edges{i});
end


end

