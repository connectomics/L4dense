function idx = findSynapseById( synapses, idPre, idPost )
%FINDSYNAPSEBYID Find a synapse based on pre- and postsynaptic id.
% INPUT synapses: [Nx3] table or [Nx2] cell
%           Table with synapses.
%           (see also L4.Synapses.clusterSynapticInterfaces).
%           Or cell array corresponding to pre- and postsynaptic seg id
%           lookup tables
%           (see L4.Synapses.L4.Synapses.getIdLookup);
%       idPre: int
%           Id of the presynaptic segment.
%       idPost: int
%           Id of the postsynaptic segment.
% OUTPUT idx: [Nx1]
%           Linear indices of the detected synapses in the synapse table.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if istable(synapses)
    if ~isempty(idPre)
        idx1 = cellfun(@(x)ismember(idPre, x), synapses.presynId);
    else
        idx1 = true(size(synapses, 1), 1);
    end
    if ~isempty(idPre)
        idx2 = cellfun(@(x)ismember(idPost, x), synapses.postsynId);
    else
        idx2 = true(size(synapses, 1), 1);
    end
    idx = find(idx1 & idx2);
elseif iscell(synapses)
    lutPre = synapses{1};
    lutPost = synapses{2};
    idx = intersect(lutPre{idPre}, lutPost{idPost});
end

end

