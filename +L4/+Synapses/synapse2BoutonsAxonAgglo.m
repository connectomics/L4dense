function boutons = synapse2BoutonsAxonAgglo( synapses, axons, distT, ...
    borderCom, scale, pre2syn )
%SYNAPSE2BOUTONSAXONAGGLO Calculate boutons from synapses using an axon
% agglomeration to calculate synapses from the same process which are
% subsequently clustered using a distance threshold.
% INPUT synapses: [Nx1] table
%           Table with synapses. For the presynaptic agglomeration the
%           presynaptiId column is needed and for the borderCom clustering
%           the edgeIdx column.
%           (see also output of L4.Synapses.clusterSynapticInterfaces)
%       axons: [Nx1] cell
%           Cell array of segment ids corresponding to axonal processes.
%           This is used to calculate pre2syn via 
%           L4.Synapses.synapsesAggloMapping. To skip this calculation set
%           axons = [] and supply pre2syn.
%       distT: double
%           Distance threshold in units of scale. Synapses at the same
%           axons are considered to belong to the same bouton if their
%           distance is less than distT.
%       borderCom: [Nx3] double
%           Interface border center-of-mass points.
%       scale: (Optional) [1x3] double
%           Voxel size.
%       pre2syn: (Optional) [Nx1] cell
%           see second output of L4.Synapses.synapsesAggloMapping.
%           (Default: will be calculated using the axons).
% OUTPUT boutons: [Nx1] cell
%           Cell array containing the linear indices of all synapses that
%           have overlapping local presynaptic agglomerations and are thus
%           assumed to belong to the same bouton.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~isempty(axons)
    [ ~, pre2syn ] = ...
        L4.Synapses.synapsesAggloMapping( synapses, axons, [] );
end

if exist('scale', 'var')
    borderCom = bsxfun(@times, borderCom, scale(:)');
end

synCom = cellfun(@(x)mean(borderCom(x, :), 1), synapses.edgeIdx, ...
    'uni', 0);
T = cellfun(@(x)distClustering(vertcat(synCom{x}), distT), ...
    pre2syn, 'uni', 0);
idx = cellfun(@isempty, pre2syn);
boutons = cellfun(@(t, idx)accumarray(t, idx, [], @(x){x}), ...
    T(~idx), pre2syn(~idx), 'uni', 0);
boutons = vertcat(boutons{:});
    
end

function T = distClustering(coms, distCutoff)
if size(coms, 1) > 1
    T = clusterdata(coms, 'linkage', 'single', ...
                'criterion', 'distance', 'cutoff', distCutoff);
else
    T = 1;
end
end