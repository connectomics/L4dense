function [synapses, toMergeSynIdx, toDel] = clusterSameSpineSynapses( ...
    axons, spineHeads, synapses )
%CLUSTERSAMESPINESYNAPSES Cluster synapses that are formed between one axon
%and one spine.
% INPUT axons: [Nx1] struct or cell
%           Axon agglos in the agglo or superagglo format.
%       spineHeads: [Nx1] struct or cell
%           Spine head agglos in the agglo or superagglo format.
%       synapses: [Nx3] table
%           The synapse agglos.
%           (see also L4.Synapses.clusterSynapticInterfaces and
%            L4.Synapses.Scripts.synapseDetection)
% OUTPUT synapses: [Nx3] table
%           The updated synapse agglo with the merged synapses.
%        toMergeSynIdx: [Nx1] cell
%           Cell of the linear indices w.r.t. input synapses that are
%           merged in the output synapses.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

[ ~, ~, ~, connectome ] = ...
    L4.Synapses.synapsesAggloMapping( synapses, axons, spineHeads );

% get connections with multiple synapses
idx = cellfun(@(x)length(x) > 1, connectome.synIdx);
toMergeSynIdx = connectome.synIdx(idx);

% merge synapses
[synapses, toDel] = L4.Synapses.mergeSynapses(synapses, toMergeSynIdx);

end

