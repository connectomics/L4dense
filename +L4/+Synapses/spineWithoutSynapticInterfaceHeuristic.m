function [synIdx, edgeIdx] = spineWithoutSynapticInterfaceHeuristic(shAgglos, ...
    synIdx, edges, synScores, synT, dirMode)
%SPINEWITHOUTSYNAPTICINTERFACEHEURISTIC
% Heuristic that adds interfaces at spine head without innervation to the
% synaptic interfaces.
%
% INPUT shAgglos: [Nx1] cell
%           Spine heads in the agglo format.
%       synIdx: [Nx1] logical
%           Synaptic interfaces as logical list among all interfaces/edges.
%       edges: [Nx2] int
%           The global edge list.
%       synScores: [Nx2] double
%           Synapse prediction for an edge.
%       synT: double
%           Minimal required interface score to consider it as synaptic.
%           If a spine head only has interfaces below synT, then no
%           interfaces is added to this spine head.
%       dirMode: (Optional) logical
%           Flag to use the direction of interfaces. Otherwise, the maximum
%           over both directions is used.
%           (Default: true)
% OUTPUT synIdx: [Nx1] logical
%           The udpated synaptic edges.
%        edgeIdx: [Nx1] int
%            The linear indices of the edges that are added to the
%            synaptic edges.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('dirMode', 'var') || isempty(dirMode)
    dirMode = true;
end

% get syn scores such that sh are always postsyn
if dirMode
    tmp = ismember(edges, cell2mat(shAgglos));
    synScores(tmp(:,1),:) = synScores(tmp(:,1), [2, 1]);
    synScores = synScores(:, 1);
else
    synScores = max(synScores, [], 2); % without direction
end

% get all edges for each spine head
edgeIdx = L4.Agglo.findEdgesOfAgglo(shAgglos, edges);

% delete spine heads with synaptic edges
toDel = cellfun(@(x)any(synIdx(x)), edgeIdx);
edgeIdx(toDel) = [];

% get edge with highest score onto each agglo
[maxScores, idx] = cellfun(@(x)max(synScores(x)), edgeIdx);
toKeep = maxScores > synT;
edgeIdx = arrayfun(@(eIdx, i)eIdx{1}(i), edgeIdx(toKeep), idx(toKeep));
synIdx(edgeIdx) = true;

end

