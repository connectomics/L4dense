function [ synCom ] = synapseCom( synapses, borderCom, mode )
%SYNAPSECOM Get the com for the input synapses.
% INPUT synapses: [Nx2] table
%           Table of synapses containing the columns 'edgeIdx' and if
%           segmentCom is supplied also 'presynId' and 'postsynId'.
%       borderCom: [Nx3] double
%           The border com list. The first edgeIdx of a synapse is used to
%           plot the border position.
%       mode: (Optional) string
%           Calculation mode:
%           'first': CoM of the first interface of a synapse (Default)
%           'mean': CoM as the mean of all interface coms.
% OUTPUT synCom: [Nx3] int
%           The coms for the input synapses.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if nargin < 3
    mode = 'first';
end

switch mode
    case 'first'
        synCom = borderCom(cellfun(@(x)x(1), synapses.edgeIdx), :);
    case 'mean'
        synCom = cellfun(@(x)mean(borderCom(x, :), 1), synapses.edgeIdx, ...
            'uni', 0);
        synCom = cell2mat(synCom);
    otherwise
        error('Unknown mode %s.', mode);
end

end

