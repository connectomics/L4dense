% synapses for the border free interfaces from ax18a_deWC01wSp
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

info = Util.runInfo();

p = Gaba.getSegParameters('ex145_ROI2017');
aggloFolder = ['/gaba/u/mberning/results/pipeline/20170217_ROI/SVGDB/' ...
    'agglos/ax18a_deWC01wSp/'];

params.synT = -1.67;
params.marginNM = 3000;

ver = 'v5_ax18a_deWC01wSp';
outputFile = fullfile(p.connectome.saveFolder, ['SynapseAgglos_' ver]);


%% propagate spine heads

m = load(p.svg.edgeFile, 'edges');
edges = m.edges;
m = load(fullfile(p.agglo.saveFolder, ...
    'dendrites_wholeCells_01_spine_attachment.mat'));
shAgglos = m.shAgglos;
shId = L4.Spine.shEdges(shAgglos, edges);
shIdx = find(shId > 0);

m = load(fullfile(aggloFolder, 'edges.mat'), 'edges', 'borderMapping');
edges = m.edges;
mapsto = m.borderMapping(shIdx);
hasTarget = mapsto > 0;
shIdx(~hasTarget) = [];
mapsto(~hasTarget) = [];
shIdAgglo = zeros(size(edges, 1), 1);
shIdAgglo(mapsto) = shId(shIdx);


%% get synaptic interfaces & spine interfaces

% bounding box margin
m = load(fullfile(aggloFolder, 'globalBorders.mat'), 'borderCoM');
borderCom = m.borderCoM;
margin = round(params.marginNM./p.raw.voxelSize(:));
bboxM = bsxfun(@plus, p.bbox, [margin, -margin]);
isAtBorder = ~Util.isInBBox(borderCom, bboxM);

% get synaptic interfaces
synScores = SynEM.Util.loadGlobalSynScores( ...
    fullfile(aggloFolder, 'synapseScores.mat'), size(edges, 1));
synIdx = max(synScores, [], 2) > params.synT;

% exclude synapses at border
synIdx(isAtBorder) = false;

% heuristic exclusions
m = load(fullfile(aggloFolder, 'heuristicSynExclIdx.mat'));
toExclIdx = m.toExcludeSynIdx;
synIdx(toExclIdx) = false;

synEdges = edges(synIdx, :);
isSpineSyn = synIdx & (shIdAgglo > 0);
isSpineSyn(~synIdx) = [];


%% create synapse table

idx1 = synScores(synIdx, 1) >= synScores(synIdx, 2);
synapses.presynId = synEdges([idx1, ~idx1]);
synapses.postsynId = synEdges([find(idx1) + size(synEdges, 1); find(~idx1)]);
edgeIdx = find(synIdx);
synapses.edgeIdx = edgeIdx([find(idx1); find(~idx1)]);
isSpineSyn = isSpineSyn([find(idx1); find(~idx1)]);
synapses = structfun(@num2cell, synapses, 'uni', 0);
synapses = struct2table(synapses);


%% delete synapses with presyn soma segment

m = load(fullfile(aggloFolder, 'eClass.mat'), 'segMapping');
mSom = load(p.agglo.somaFile);
somata = cell2mat(mSom.somaAgglos(:,1));
somata = L4.AggloSeg.mapSegIds(m.segMapping, somata);
isSomatic = false(max(edges(:)), 1);
isSomatic(somata) = true;
toDel = cellfun(@(x)any(isSomatic(x)), synapses.presynId);
synapses(toDel, :) = [];
isSpineSyn(toDel) = [];


%% save result

if ~exist(outputFile, 'file')
    Util.log('Saving output to %s', outputFile);
    info.params = params;
    save(outputFile, 'synapses', 'info', 'isSpineSyn');
else
    warning(['Output file %s already exists and will not be ' ...
        'overwritten. Save the data manually.']);
end


%% look at examples in wk

plotting = false;
if plotting
    synCom = borderCom(cell2mat(synapses.edgeIdx), :);
    skel = L4.Util.getSkel();

    % shaft/spine examples
    numSamples = 50;
    spineIdx = find(isSpineSyn);
    shaftIdx = setdiff(1:size(synapses, 1), spineIdx);
    spineIdx = spineIdx(randperm(length(spineIdx), numSamples));
    shaftIdx = shaftIdx(randperm(length(shaftIdx), numSamples));
    skel = skel.addNodesAsTrees(synCom(spineIdx, :), ...
        arrayfun(@(x)sprintf('SpineSynapse_%d', x), spineIdx, 'uni', 0));
    skel = skel.addNodesAsTrees(synCom(shaftIdx, :), ...
        arrayfun(@(x)sprintf('ShaftSynapse_%d', x), shaftIdx, 'uni', 0));
    skel.write(['Syn_' ver]);
end
