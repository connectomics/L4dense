% allow only a single synapse onto spine head agglomerates consisting of
% single segments
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

info = Util.runInfo();
plotting = false;

%% load data

p = Gaba.getSegParameters('ex145_ROI2017');

% synapses (use those that are clustered for same axon - same spine)
synFile = fullfile(p.connectome.saveFolder, ...
    'SynapseAgglos_v3_ax_spine_clustered.mat');
m = load(synFile);
synapses = m.synapses(m.isSpineSyn,:);

% spine heads
shFile = fullfile(p.agglo.saveFolder, 'spine_heads_and_attachment_03.mat');
m = load(shFile);
shAgglos = m.shAgglos;

% only spine head consisting of single segments
shAgglos(cellfun(@length, shAgglos) > 1) = [];

% graph
[graph, segmentMeta, borderMeta] = Seg.IO.loadGraph(p, false);
synScores = max(graph.synScores, [], 2);


%% synapse spine head mapping

[ ~, ~, post2syn ] = ...
    L4.Synapses.synapsesAggloMapping( synapses, [], shAgglos );


%% get synapse with maximal score for spine heads with multiple synapses

hasMultipleSyns = cellfun(@length, post2syn) > 1;
post2syn = post2syn(hasMultipleSyns);
maxScoreSyn = length(post2syn);
% deleted synapse with highest score from post2syn
for i = 1:length(post2syn)
    thisScores = cellfun(@(x)max(synScores(x)), ...
        synapses.edgeIdx(post2syn{i}));
    [~, idx] = max(thisScores);
    maxScoreSyn(i) = post2syn{i}(idx);
    post2syn{i}(idx) = []; % i.e. only the ones to delete are kept
end
toDel = cell2mat(post2syn);


%% examples to wk

% deleted synapses to wk
if plotting
    N = 50;
    N = min(N, length(toDel));
    idx = toDel(randperm(length(toDel), N));
    skel = L4.Synapses.synapse2Skel(synapses(idx,:), borderMeta.borderCoM);
    skel = skel.setDescription(['Synapse exclusion heuristic that only ' ...
        'keeps the synapse with highest score onto spine head ' ...
        'agglomerates consisting of single segments.']);
    skel.write('SingleSegSHExclHeuristic_delSynapses.nml');
end

% deleted synapses & max spine head synapse to wk
if plotting
    N = 50;
    N = min(N, length(post2syn));
    idx = randperm(length(post2syn), N);
    coms = borderMeta.borderCoM;
    % add max syn and del syns
    skel = L4.Util.getSkel();
    for i = 1:length(idx)
        skel = L4.Synapses.synapse2Skel(synapses(maxScoreSyn(idx(i)), :), ...
            coms, [], skel, [], maxScoreSyn(idx(i)));
        skel.names{end} = sprintf('MaxScoreSyn_%d', idx(i));
        skel.colors{end} = [0 0 1 1];
        skel = L4.Synapses.synapse2Skel(synapses(post2syn{idx(i)}, :), ...
            coms, [], skel, [], post2syn{idx(i)});
    end
    skel = skel.setDescription(['Synapse exclusion heuristic that only ' ...
        'keeps the synapse with highest score onto spine head ' ...
        'agglomerates consisting of single segments.']);
    skel.write('SingleSegSHExclHeuristic.nml');
end


%% save result

outFile = fullfile(p.connectome.saveFolder, ...
    'SynapseAgglos_v3_ax_spine_clustered_spineSynOnly.mat');
singleSynPerSingleSegmentSpine.info = info;
singleSynPerSingleSegmentSpine.toDel = toDel;
synapses(toDel,:) = [];

info_singleSynPerSpineConstraint = info;
if ~exist(outFile, 'file')
    save(outFile, 'synapses', 'singleSynPerSingleSegmentSpine');
else
    warning('File %s already exists and will not be overwritten.', ...
        outFile);
end
