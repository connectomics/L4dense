%% synapse detection pipeline from SynEM predictions
%
% NOTE Indices in synapses are w.r.t. to the edges that were used during
%      synaptic interface clustering. This is done with the edges from the
%      graph, i.e. including correspondences.
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

plotting = false; %#ok<*UNRCH>


%% set parameters

info = Util.runInfo(false);
p = Gaba.getSegParameters('ex145_ROI2017');
outputFile = fullfile(p.connectome.saveFolder, 'SynapseAgglos');
ver = '_v3';
outputFile = [outputFile ver '.mat'];
params.synT = -1.67;
params.probT = 0.98;
params.dist = 2;
params.marginNM = 3000;


%% load data

[graph, segmentMeta, borderMeta] = Seg.IO.loadGraph(p, false);

% get soma
p.agglo.somaFile = ['/gaba/u/mberning/results/pipeline/20170217_ROI/' ...
    'soma_BS/somaAgglo_06.mat'];
m = load(p.agglo.somaFile); % somaAgglo_06
somaAgglos = m.somaAgglos(:,1);

% synapse predictions
synIdx = L4.Synapses.getSynapsePredictions(graph.synScores, ...
    params.synT, true, graph.prob, [], graph.edges, ...
    segmentMeta.myelinScore, [], somaAgglos);

% discard borders at boundary
margin = round(params.marginNM./p.raw.voxelSize(:));
bboxM = bsxfun(@plus, p.bbox, [margin, -margin]);
isAtBorder = ~Util.isInBBox(borderMeta.borderCoM, bboxM);
synIdx(isAtBorder) = false;


%% get synapses

synapses = L4.Synapses.clusterSynapticInterfaces(graph.edges, ...
    graph.synScores, synIdx, graph.prob, params.probT, params.dist);


%% spine synapses

m = load(p.agglo.spineHeadFile);
isSH = false(length(segmentMeta.voxelCount), 1);
isSH(cell2mat(m.shAgglos)) = true;
isSpineSyn = cellfun(@(x)any(isSH(x)), synapses.postsynId);


%% boutons via presynaptic agglomeration

probT = 0.98;
aggloDistT = 1;
comDistT = 2e3;
boutons = L4.Synapses.synapse2BoutonsLocalPresynClust(synapses, ...
    graph.edges, graph.prob, probT, aggloDistT, borderMeta.borderCoM, ...
    comDistT, p.raw.voxelSize);

params.boutons.probT = probT;
params.boutons.aggloDistT = aggloDistT;
params.boutons.comDistT = comDistT;


%% save result

if ~exist(outputFile, 'file')
    Util.log('Saving output to %s', outputFile);
    info.params = params;
    save(outputFile, 'synapses', 'info', 'isSpineSyn', 'boutons');
else
    warning(['Output file %s already exists and will not be ' ...
        'overwritten. Save the data manually.']);
end


%% examples to wk

if plotting
    
    borderCom = borderMeta.borderCoM;
    
    % random synapse examples
    n = 100;
    rng('shuffle')
    idx = randperm(length(synapses.edgeIdx), n);
    skel = L4.Agglo.agglo2Nml(synapses.edgeIdx(idx), borderCom);
    skel.names = arrayfun(@(x)sprintf('Synapse%d', x), idx, 'uni', 0)';
    skel.write(['SynapseAgglo' ver '_examples.nml'])
    
    % shaft/spine examples
    numSamples = 50;
    spineIdx = find(isSpineSyn);
    shaftIdx = setdiff(1:size(synapses, 1), spineIdx);
    skel = L4.Agglo.agglo2Nml(synapses.edgeIdx( spineIdx( ...
        randperm(length(spineIdx), numSamples))), borderCom);
    skel = L4.Agglo.agglo2Nml(synapses.edgeIdx( shaftIdx( ...
        randperm(length(shaftIdx), numSamples))), borderCom, skel);
    skel.names(1:numSamples) = arrayfun( ...
        @(x)sprintf('SpineSynapse%03d', x), ...
        1:numSamples, 'uni', 0)';
    skel.names(numSamples+1:end) = arrayfun( ...
        @(x)sprintf('ShaftSynapse%03d', x), ...
        1:numSamples, 'uni', 0)';
    skel.write(['SynapseAgglo' ver '_ShaftSpineSynExamples.nml']);
end