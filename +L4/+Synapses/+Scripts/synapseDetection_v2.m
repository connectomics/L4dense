% synapse detection pipeline starting from SynEM predictions
%
% NOTE Indices in synapses are w.r.t. to the edges that were used during
%      synaptic interface clustering. This is done with the edges from the
%      graph, i.e. including correspondences.
%
% This file should produce the same result as
% L4.Synapses.Scripts.synapseDetection, e.g. when run with axons_19_a and
% somaAgglos_06 it outputs the same synapses as in
% SynapseAgglos_v3_ax_spine_clustered.mat.
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>


info = Util.runInfo();
p = Gaba.getSegParameters('ex145_ROI2017');
ver = 'v6';

outputFile = fullfile(p.connectome.saveFolder, ...
    sprintf('SynapseAgglos_%s.mat', ver));

% output file with soma heuristic executed
outputFileSH = fullfile(p.connectome.saveFolder, ...
    sprintf('SynapseAgglos_%s_somaH.mat', ver));


%% load/set agglo files for heuristics

p.agglo.axonAggloFile = fullfile(p.saveFolder, ...
    '/aggloState/axons_19_a.mat');
axons = L4.Axons.getLargeAxons(p, true, true);

p.agglo.somaFile = fullfile(p.saveFolder, 'soma_BS', 'somaAgglo_07.mat');


%% interface agglomeration

params.synT = -1.67; % optimal threshold from dense test set
% params.synT = -2.42; % 80% rec, 70.8% prec in SynEM inh test set on ROI2017
%                      % (see also lablog)

[synapses, isSpineSyn, heuristics, debug] = ...
    L4.Synapses.createSynapseAgglomerates(p, params, axons);


%% save result

if ~exist(outputFile, 'file')
    Util.log('Saving output to %s.', outputFile);
    save(outputFile, 'synapses', 'isSpineSyn', 'heuristics', 'debug', ...
        'info');
else
    Util.log(['Output file %s already exists and will not be ' ...
        'overwritten.'], outputFile);
end


%% synapses with soma fp exclusion executed

synapses(heuristics.toDelSomaFPs, :) = [];
isSpineSyn(heuristics.toDelSomaFPs) = [];
heuristics.toDelSingleSH = setdiff(heuristics.toDelSingleSH, ...
    heuristics.toDelSomaFPs);
heuristics.toDelSingleSH = arrayfun( ...
    @(x)x - sum(heuristics.toDelSingleSH < x), ...
    heuristics.toDelSingleSH);
heuristics = rmfield(heuristics, 'toDelSomaFPs');

if ~exist(outputFileSH, 'file')
    Util.log('Saving output to %s.', outputFileSH);
    save(outputFileSH, 'synapses', 'isSpineSyn', 'heuristics', 'info');
else
    Util.log(['Output file %s already exists and will not be ' ...
        'overwritten.'], outputFileSH);
end

