% compare synapses from different agglo versions
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>


%% load synapse agglos

p = L4.getSegParameter();

% old agglo
oldAgglos = fullfile(p.connectome.saveFolder, ...
    'SynapseAgglos_v3_ax_spine_clustered_classified.mat');
[~, nameOld] = fileparts(oldAgglos);
stats.oldAgglos = oldAgglos;
m1 = load(oldAgglos);
synapses_1 = m1.synapses;

% new agglo
newAgglos = fullfile(p.connectome.saveFolder, ...
    'SynapseAgglos_v8_classified.mat');
[~, nameNew] = fileparts(newAgglos);
stats.newAgglos = newAgglos;
m2 = load(newAgglos);
synapses_2 = m2.synapses;


%% lookup for edge indices

edgesIdx_1 = cellfun(@uint32, synapses_1.edgeIdx, 'uni', 0);
edgesIdx_2 = cellfun(@uint32, synapses_2.edgeIdx, 'uni', 0);
m = max(cellfun(@max, cat(1, edgesIdx_1, edgesIdx_2)));

lut1 = L4.Agglo.buildLUT(edgesIdx_1, m);
lut2 = L4.Agglo.buildLUT(edgesIdx_2, m);


%% compare synapses
% Note: single interfaces in synapses that do not appear in the other
%       synapse agglo are neglected (i.e. if two there are two synapses in
%       the different agglos only differing by an additional interfaces
%       they are considered equal)

tmp = cellfun(@(x)setdiff(lut2(x), 0), edgesIdx_1, 'uni', 0);
notEqual_1 = ~cellfun(@(x)length(x) == 1 & all(x > 0), tmp);
wasSplit = cellfun(@length, tmp) > 1;
splits = table(find(wasSplit), tmp(wasSplit), 'VariableNames', ...
    {'oldIdx', 'newIdx'});
removed = cellfun(@isempty, tmp);
removed = table(find(removed), 'VariableNames', {'oldIdx'});
tmp(notEqual_1) = repmat({nan}, sum(notEqual_1), 1);
tmp = cell2mat(tmp);

tmp2 = cellfun(@(x)setdiff(lut1(x), 0), edgesIdx_2, 'uni', 0);
notEqual_2 = ~cellfun(@(x)length(x) == 1 & all(x > 0), tmp2);
wasMerged = cellfun(@length, tmp2) > 1;
merger = table(tmp2(wasMerged), find(wasMerged), 'VariableNames', ...
    {'oldIdx', 'newIdx'});
additional = cellfun(@isempty, tmp2);
additional = table(find(additional), 'VariableNames', {'newIdx'});
tmp2(notEqual_2) = repmat({nan}, sum(notEqual_2), 1);
tmp2 = cell2mat(tmp2);

tmp = [tmp; nan];
tmp2(isnan(tmp2)) = length(tmp);
isSame = tmp(tmp2) == (1:length(tmp2))';
isSame2_to_1(isSame) = tmp2(isSame);
isSame2_to_1 = isSame2_to_1(:);
% for each synapse_2 contains the index of the equal synapse in synapses_1

% verify if same synapses are really same (except for new interfaces)
assert(all(cellfun(@(x, y) isequal(sort(x(lut2(x) > 0)), sort(y(lut1(y) > 0))), ...
    edgesIdx_1(isSame2_to_1(isSame2_to_1 > 0)), ...
    edgesIdx_2(isSame2_to_1 > 0))))

stats.numEqualSynapses = sum(isSame2_to_1 > 0);
stats.splits = splits;
stats.merger = merger;
stats.removedSynapses = removed;
stats.additionalSynapses = additional;

% check if all synapses are accounted for
assert((length(edgesIdx_1) - size(removed, 1) + size(additional, 1) - ...
    sum(cellfun(@length, merger.oldIdx) - 1) + ...
    sum(cellfun(@length, splits.newIdx) - 1)) == length(edgesIdx_2))


%% write samples to WK

N = 50;
N_splits = N;
N_merger = N;
N_removed = N;
N_additional = N;

% [~, segMeta, borderMeta] = Seg.IO.loadGraph(p, false); % too slow if not in FFM
% borderCom = borderMeta.borderCoM;
% load border com from local files (in general use the two lines directly above)
m = load('/media/benedikt/DATA2/workspace/data/backed/20170217_ROI/graph.mat', 'borderIdx');
m2 = load('/media/benedikt/DATA2/workspace/data/backed/20170217_ROI/globalBorder.mat', 'borderCoM');
borderCom = nan(size(m.borderIdx, 1), 3);
borderCom(~isnan(m.borderIdx), :) = m2.borderCoM;
clear m m2

skel = L4.Util.getSkel();

% removed synapses
idx = randperm(size(stats.removedSynapses, 1), N_removed);
skel = L4.Synapses.synapse2Skel( ...
    synapses_1(stats.removedSynapses.oldIdx(idx), :), borderCom, [], ...
    skel, 'all', stats.removedSynapses.oldIdx(idx));
skel = skel.addGroup('removed_synapses');
idx = (skel.numTrees() - N_removed + 1):skel.numTrees();
skel = skel.addTreesToGroup(idx, 'removed_synapses');

% additional synapses
idx = randperm(size(stats.additionalSynapses, 1), N_additional);
skel = L4.Synapses.synapse2Skel( ...
    synapses_2(stats.additionalSynapses.newIdx(idx), :), borderCom, [], ...
    skel, 'all', stats.additionalSynapses.newIdx(idx));
skel = skel.addGroup('additional_synapses');
idx = (skel.numTrees() - N_additional + 1):skel.numTrees();
skel = skel.addTreesToGroup(idx, 'additional_synapses');
skel.colors(idx) = repmat({[0 1 0 1]}, N_additional, 1);

% merged synapses
idx = randperm(size(stats.merger, 1), min(N_merger, size(stats.merger, 1)));
skel = L4.Synapses.synapse2Skel( ...
    synapses_2(stats.merger.newIdx(idx), :), borderCom, [], ...
    skel, 'all', stats.merger.newIdx(idx));
skel = skel.addGroup('merged_synapses');
idx = (skel.numTrees() - N_merger + 1):skel.numTrees();
skel = skel.addTreesToGroup(idx, 'merged_synapses');
skel.colors(idx) = repmat({[1 1 0 1]}, N_merger, 1);

% split synapses
skel = skel.addGroup('split_synapses');
idx = randperm(size(stats.splits, 1), min(N_splits, size(stats.splits, 1)));
trCount = 0;
for i = 1:length(idx)
    this_syn_idx = stats.splits.newIdx{idx(i)};
    skel = L4.Synapses.synapse2Skel( synapses_2(this_syn_idx, :), ...
        borderCom, [], skel, 'all', this_syn_idx);
    [skel, this_id] = skel.addGroup(sprintf('split_%d', i), 'split_synapses');
    this_trees = (skel.numTrees() - length(this_syn_idx) + 1) : ...
        skel.numTrees();
    skel = skel.addTreesToGroup(this_trees, this_id);
    trCount = trCount + length(this_syn_idx);
end
skel.colors((end-trCount+1):end) = repmat({[0 1 1 1]}, trCount, 1);

skel.write(sprintf('Comparison_%s_%s', nameNew, nameOld));
