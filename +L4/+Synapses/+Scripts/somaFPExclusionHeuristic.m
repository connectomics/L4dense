% heuristic to exclude soma synapses:
% discard soma synapses for which the corresponding bouton is also the only
% innervation onto a spine head
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

info = Util.runInfo(false);
d_bouton = 1e3; % um
plotting = false;
suffix = '_SomaSynExcl';


%% load agglos

p = Gaba.getSegParameters('ex145_ROI2017');

% axons
Util.log('Loading axons.');
p.agglo.axonAggloFile = ['/gaba/u/mberning/results/pipeline/' ...
    '20170217_ROI/aggloState/axons_18_a.mat'];
axons = L4.Axons.getLargeAxons(p, true, true);

% dendrites
Util.log('Loading dendrites and target classes.');
[dendFile, targetClass, targetLabels] = L4.Connectome.buildTargetClasses();
m = load(dendFile);
dendrites = m.dendAgglos(targetClass ~= 'Ignore');
targetClass(targetClass == 'Ignore') = [];

% add somata to dendrites
mSom = load(p.agglo.somaFile);
somata = mSom.somaAgglos(:,1);
[dendrites, toDel] = L4.Specificity.addSomasToDendrites(dendrites, ...
    somata, 'separate');
targetClass(toDel) = [];
targetClass(end+1:end+length(somata)) = 'Somata';

% target classes
idxSoma = targetClass == 'Somata';
idxWC = targetClass == 'WholeCell';
idxAD = targetClass == 'ApicalDendrite';
idxSD = targetClass == 'SmoothDendrite';
idxAIS = targetClass == 'AxonInitialSegment';
idxOther = targetClass == 'OtherDendrite';

% synapses
Util.log('Loading synapses.');
% mSyn = load(p.connectome.synFile);
synFile = ['/gaba/u/mberning/results/pipeline/20170217_ROI/' ...
    'connectomeState/SynapseAgglos_v3_ax_spine_clustered.mat'];
mSyn = load(synFile);
synapses = mSyn.synapses;
[graph, segmentMeta, borderMeta] = Seg.IO.loadGraph(p, false);
% not sure why there are uint32 in there
synapses.edgeIdx = cellfun(@double, synapses.edgeIdx, 'uni', 0);
synComs = L4.Synapses.synapseCom(synapses, borderMeta.borderCoM);

% spine heads
shFile = fullfile(p.agglo.saveFolder, 'spine_heads_and_attachment_03.mat');
m = load(shFile);
shAgglos = m.shAgglos;


%% synapse agglo mappings

[ syn2Agglo, pre2syn, post2syn, connectome ] = ...
    L4.Synapses.synapsesAggloMapping( synapses, axons, shAgglos );


%% get spine and soma synapses

maxId = Seg.Global.getMaxSegId(p);
shLUT = Agglo.buildLUT(maxId, shAgglos);
isSpine = shLUT > 0;
isSoma = Agglo.buildLUT(maxId, somata) > 0;
isSpineSyn = cellfun(@(x)any(isSpine(x)), synapses.postsynId);
isSomaSyn = cellfun(@(x)any(isSoma(x)), synapses.postsynId);

% % consistency check
% isequal(mSyn.isSpineSyn, isSpineSyn)


%% boutons

boutons = L4.Synapses.synapse2BoutonsAxonAgglo(synapses, [], d_bouton, ...
    borderMeta.borderCoM, p.raw.voxelSize, pre2syn);

% keep only boutons with soma synapses & spine synapses
boutons = boutons(cellfun(@(x)any(isSomaSyn(x)) & any(isSpineSyn(x)), ...
    boutons));

% strange syns
tmp = cellfun(@(x)any(isSpineSyn(x) & isSomaSyn(x)), boutons);
boutonsHybridSyns = boutons(tmp);
hybridSyns = cellfun(@(x)x(isSpineSyn(x) & isSomaSyn(x)), ...
        boutonsHybridSyns, 'uni', 0);
boutons = boutons(~tmp);


%% get synapses at same bouton

% add soma and spine synapses to separate arrays
boutonsSomaSyns = cellfun(@(x)x(isSomaSyn(x)), boutons, 'uni', 0);
boutonsSpineSyns = cellfun(@(x)x(isSpineSyn(x)), boutons, 'uni', 0);

isSingleSpineSyn = false(length(boutonsSpineSyns), 1);
for i = 1:length(boutonsSpineSyns)
    curSingleSpineSyns = false(length(boutonsSpineSyns{i}), 1);
    for j = 1:length(boutonsSpineSyns{i})
        thisShAggloIdx = shLUT(synapses.postsynId{boutonsSpineSyns{i}(j)});
        thisShAggloIdx = setdiff(thisShAggloIdx, 0);
        curSingleSpineSyns(j) = all(arrayfun(@(x)length(post2syn{x}) == 1, ...
            thisShAggloIdx));
    end
    isSingleSpineSyn(i) = any(curSingleSpineSyns);
end
idx = find(isSingleSpineSyn);
toDel = cell2mat(boutonsSomaSyns(idx));

numToDel = length(cell2mat(boutonsSomaSyns(idx)));


%% examples to wk

if plotting
    N = 50;
    skel = L4.Util.getSkel();
    sampleIdx = idx(randperm(length(idx), N));
    for i = 1:N
        skel = skel.addTree(sprintf('Bouton_%d_SomaSyns', ...
            sampleIdx(i)), synComs(boutonsSomaSyns{sampleIdx(i)}, :));
        skel = skel.addTree(sprintf('Bouton_%d_SpineSyns', ...
            sampleIdx(i)), synComs(boutonsSpineSyns{sampleIdx(i)}, :));
    end
    skel = skel.setDescription( ['Samples of boutons with soma ' ...
        'synapses that also have a single spine innervation. The ' ...
        'heuristic would exclude all soma synapses.']);
    skel.write('SomaSynapseFPExclusionHeuristic.nml');
end


%% hybrid synapses to wk

if plotting
    skel = L4.Util.getSkel();
    synIdx = cell2mat(hybridSyns);
    tmp = synapses.postsynId(synIdx);
    tmp2 = cellfun(@(x)find(shLUT(x) > 0, 1, 'first'), tmp, 'uni', 0);
    intIdx = cellfun(@(x, y)x(y), synapses.edgeIdx(synIdx), tmp2);
    coms = borderMeta.borderCoM(intIdx, :);
    names = arrayfun(@(x)sprintf('HybridSyn_%d', x), synIdx, 'uni', 0);
    skel = skel.addNodesAsTrees(coms, names);
    skel = skel.setDescription( ...
        'Synapses that have both spine and soma postsyn ids.');
    skel.write('HybridSynapses.nml');
end


%% all deleted to wk

if plotting
    % all excluded to WK
    tmp = synapses(toDel);
    coms = cellfun(@(x)mean(borderMeta.borderCoM(x,:), 1), tmp.edgeIdx, 'uni', 0);
    coms = cell2mat(coms);
    coms = round(coms);
    skel = L4.Util.getSkel();
    skel = skel.addNodesAsTrees(coms);
end


%% delete soma synapses and save output

synapses(toDel, :) = [];
isSpineSyn(toDel) = [];
isSomaSyn(toDel) = [];

[outPath, outName] = fileparts(synFile);
outFile = fullfile(outPath, [outName suffix '.mat']);
if ~exist(outFile, 'file')
    save(outFile, 'synapses', 'isSpineSyn', 'isSomaSyn', 'info');
else
    Util.log(['Output file %s already exists and will not be ' ...
        'overwritten.'], outFile)
end
