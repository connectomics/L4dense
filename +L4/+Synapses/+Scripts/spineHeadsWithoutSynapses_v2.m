%
% look into spine heads without synapses
%
% see also https://mhlablog.net/2018/03/27/l4-spine-heads-without-synapse/
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>


%%

p = Gaba.getSegParameters('ex145_ROI2017');
marginNM = 3000;

if ispc
    p.svg.edgeFile = 'E:\workspace\data\backed\20170217_ROI\globalEdges.mat';
    p.svg.borderMetaFile = 'E:\workspace\data\backed\20170217_ROI\globalBorder.mat';
end


%% synapses

synFile = fullfile(p.connectome.saveFolder, ...
    'SynapseAgglos_v9.mat');
p.svg.synScoreFile = ['/media/benedikt/DATA2/workspace/data/backed/' ...
    '20170217_ROI/globalSynScores_cnn17_synem_tr2.mat'];
m = load(synFile);
synapses = m.synapses;


%% spine heads

shFile = fullfile(p.agglo.saveFolder, 'spine_heads_and_attachment_03.mat');
m = load(shFile);
shAgglos = m.shAgglos;
isAttached = m.attached > 0;
shAgglos = shAgglos(isAttached);


%% get spine heads without synapses

[ ~, ~, post2syn ] = ...
    L4.Synapses.synapsesAggloMapping( synapses, [], shAgglos );
hasSyn = ~cellfun(@isempty, post2syn);
Util.log('Spine heads with synapses: %.1f%%.', ...
    100 * sum(hasSyn) / length(hasSyn));


%% spine heads without synapse to WK

m = load(p.svg.segmentMetaFile, 'point');
point = m.point';
sh = shAgglos(~hasSyn);
% shPoint = point(cellfun(@(x)x(1), shAgglos(~hasSyn)), :);

% discard borders at boundary
margin = round(marginNM./p.raw.voxelSize(:));
bboxM = bsxfun(@plus, p.bbox, [margin, -margin]);
% isAtBorder = ~Util.isInBBox(shPoint, bboxM);
% shPoint(isAtBorder, :) = [];

% discard agglos with all points at margin
toDel = ~cellfun(@(x)all(Util.isInBBox(point(x, :), bboxM)), sh);
sh(toDel) = [];
Util.log('Of the spines without synapses, %.1f%% are at the boundary margin.', ...
    100 * sum(toDel) / length(toDel));


%% find synapse with highest prob

m = load(p.svg.edgeFile);
edges = m.edges;
synScores = SynEM.Util.loadGlobalSynScores(p.svg.synScoreFile, ...
    size(edges, 1), true);

edgeIdx = L4.Agglo.findEdgesOfAgglo(sh, edges);
[mlScore, mSynIdx] = cellfun(@(x)max(synScores(x)), edgeIdx);
mlSyn = arrayfun(@(x, y)x{1}(y), edgeIdx, mSynIdx);

m = load(p.svg.borderMetaFile, 'borderCoM', 'borderSize');
borderCom = m.borderCoM;


%% to wk

score_t = -0.75; % additional score threshold on ml synapses
aboveT = find(mlScore > score_t);

% to wk
N = 50;
idx = randperm(length(aboveT), N);
idx = aboveT(idx);

skel = L4.Agglo.agglo2Nml(sh(idx), point);
skel.names = arrayfun(@(x)sprintf('sh_%02d', x), 1:N, 'uni', 0)';
names = arrayfun(@(x, y)sprintf('sh_%02d_mlSyn_%.2f', x, y), 1:N, ...
    mlScore(idx)', 'uni', 0);
skel = skel.addNodesAsTrees(borderCom(mlSyn(idx), :), names);

for i = 1:N
    [skel, gid] = skel.addGroup(sprintf('Spine_%02d', i));
    skel = skel.addTreesToGroup([i; i+N], gid);
end

[~, synAggloName] = fileparts(synFile);

skel = skel.setBbox(bboxM, true);
skel = skel.setDescription(sprintf(['Spine heads with most likely ' ...
    'synapse (interface threshold %.2f).'],score_t));

skel.write(sprintf('SH_att_wo_Syn_%s.nml', synAggloName));


%% check cube  boundary missed syns in agglomerated seg

synScores = SynEM.Util.loadGlobalSynScores( ...
    fullfile(p.aggloSeg.saveFolder, 'synapseScores.mat'));
m = load(fullfile(p.aggloSeg.saveFolder, 'edges.mat'), 'borderMapping');
bmap = m.borderMapping;
idx = 5; % add idx of border here
idxM = bmap(idx);
synScores(idxM, :)
