% debug dendrites with presynaptic synapse segments
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

%% get synapse mappings

p = Gaba.getSegParameters('ex145_ROI2017');
p.agglo.dendriteAggloFile
m = load(fullfile(p.agglo.saveFolder,'dendrites_03.mat'));
dendrites = m.dendrites(m.indBigDends);
m = load(p.connectome.synFile);
synapses = m.synapses;
[syn2Agglo, pre2syn, post2syn] = L4.Synapses.synapsesAggloMapping( ...
    synapses, dendrites, dendrites);

%% examples to nml

dendrites = struct2table(dendrites);

% indices of dendrites that have presynaptic segments
idx = find(~cellfun(@isempty,pre2syn));

% synapses are wrt to whole graph
[graph, segmentMeta, borderMeta] = Seg.IO.loadGraph(p, false);

% 10 examples to nml
rng('shuffle')
idxR = idx(randi(length(idx), 10, 1));
idxR = [2313,8756,3775,5651,2335,6499,3315,6988,7456,8214];
skel = L4.Util.getSkel();
for i = 1:10
    skel = L4.Agglo.agglo2Nml(dendrites.nodes{idxR(i)}(:,4), ...
        segmentMeta.point, skel);
    skel.names{end} = sprintf('Dendrite_%d', idxR(i));
    synEdgeIdx = synapses.edgeIdx(pre2syn{idxR(i)});
    for j = 1:length(synEdgeIdx)
        skel = skel.addTree( ...
            sprintf('Dendrite_%d_Synapse_%02d', idxR(i), j), ...
            borderMeta.borderCoM(synEdgeIdx{j}, :));
    end
end
skel.write('DendritePrePostDebugExamples.nml')