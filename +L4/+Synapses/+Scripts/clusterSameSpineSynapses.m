% cluster synapses from the same axon onto the same spine
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>


info = Util.runInfo();


%% load data

p = Gaba.getSegParameters('ex145_ROI2017');

% axons
% axonsFile = fullfile(p.agglo.saveFolder, 'axons_18_a.mat');
% m = load(axonsFile);
% axons = m.axons(m.indBigAxons);
[axons, indAxons] = L4.Axons.getLargeAxons(p, true, true); % axons_18_a.mat by time of coding
idx = cellfun(@isempty, axons);
axons = axons(~idx);
indAxons = indAxons(~idx);

% spine heads
shFile = fullfile(p.agglo.saveFolder, 'spine_heads_and_attachment_03.mat');
m = load(shFile);
shAgglos = m.shAgglos;

% synapses
synFile = fullfile(p.connectome.saveFolder, 'SynapseAgglos_v3.mat');
m = load(synFile);
synapses = m.synapses;


%% merge synapses

[synapses, ~, toDel] = L4.Synapses.clusterSameSpineSynapses(axons, ...
    shAgglos, synapses);


%% save results

isSpineSyn = m.isSpineSyn;
isSpineSyn(toDel) = [];

outFile = fullfile(p.connectome.saveFolder, ...
    'SynapseAgglos_v3_ax_spine_clustered.mat');
if ~exist(outFile, 'file')
    save(outFile, 'synapses', 'isSpineSyn', 'info');
end
