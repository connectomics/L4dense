% look at synapses whose interfaces are far apart
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>


%% load data

p = Gaba.getSegParameters('ex145_ROI2017');

% synapses
Util.log('Loading synapses.');
% mSyn = load(p.connectome.synFile);
synFile = ['/gaba/u/mberning/results/pipeline/20170217_ROI/' ...
    'connectomeState/SynapseAgglos_v3_ax_spine_clustered.mat'];
mSyn = load(synFile);
synapses = mSyn.synapses;
[graph, segmentMeta, borderMeta] = Seg.IO.loadGraph(p, false);
% not sure why there are uint32 in there
synapses.edgeIdx = cellfun(@double, synapses.edgeIdx, 'uni', 0);
synComs = L4.Synapses.synapseCom(synapses, borderMeta.borderCoM);


%% get synapses with interfaces that are far apart

d = 2e3; % interface distance (euclidean) in nm

syn_int_coms = cellfun(@(x)borderMeta.borderCoM(x,:), synapses.edgeIdx, ...
    'uni', 0);
syn_int_coms = cellfun( ...
    @(x)bsxfun(@times, single(x), p.raw.voxelSize(:)'), ...
    syn_int_coms, 'uni', 0);
idx = find(cellfun(@(x)any(pdist(x) > d), syn_int_coms));


%% examples to wk

N = 50;
N = min(N, length(idx));
sidx = idx(randperm(length(idx), N));
skel = L4.Synapses.synapse2Skel(synapses(sidx,:), borderMeta.borderCoM, ...
    [], [], 'all');
skel.write('PotentialMergedSynapses.nml');


%% post-hoc simple distance clustering

dClust = 1.5e3; % nm
fClust = @(x)clusterdata(x, 'linkage', 'single', 'criterion', ...
    'distance', 'cutoff', dClust);
T = cell(length(syn_int_coms), 1);
tmp = cellfun(@(x)size(x, 1), syn_int_coms) > 1;
T(tmp) = cellfun(fClust, syn_int_coms(tmp), 'uni', 0);
T(~tmp) = {1};

%% examples of split synapses to wk

idx = cellfun(@(x)length(unique(x)) > 1, T);
idx = find(idx);
m = load(['+Visualization' filesep 'autoKLEE_colormap.mat']);
cmap = m.autoKLEE_colormap;
cmap(:,4) = 1;

N = 50;
sidx = idx(randperm(length(idx), N));
coms = cellfun(@(x)borderMeta.borderCoM(x,:), synapses.edgeIdx(sidx), ...
    'uni', 0);
skel = L4.Util.getSkel();
for i = 1:length(coms)
    for j = 1:max(T{sidx(i)})
        t = T{sidx(i)};
        skel = skel.addTree(sprintf('Syn_%d_Part_%d', sidx(i), j), ...
            coms{i}(t == j,:), [], cmap(j, :));
    end
end
skel.write('SynapseSplittingTest.nml');
