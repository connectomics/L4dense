% look into spine heads without synapses
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

p = Gaba.getSegParameters('ex145_ROI2017');
marginNM = 3000;


%% synapses

synFile = fullfile(p.connectome.saveFolder, ...
    'SynapseAgglos_v3_ax_spine_clustered.mat');
m = load(synFile);
synapses = m.synapses;


%% spine heads

shFile = fullfile(p.agglo.saveFolder, 'spine_heads_and_attachment_03.mat');
m = load(shFile);
shAgglos = m.shAgglos;
isAttached = m.attached > 0;
shAgglos = shAgglos(isAttached);


%% get spine heads without synapses

[ ~, ~, post2syn ] = ...
    L4.Synapses.synapsesAggloMapping( synapses, [], shAgglos );
hasSyn = ~cellfun(@isempty, post2syn);
Util.log('Spine heads with synapses: %.2f.', sum(hasSyn) / length(hasSyn));


%% spine heads without synapse to WK

m = load(p.svg.segmentMetaFile, 'point');
point = m.point';
sh = shAgglos(~hasSyn);
% shPoint = point(cellfun(@(x)x(1), shAgglos(~hasSyn)), :);

% discard borders at boundary
margin = round(marginNM./p.raw.voxelSize(:));
bboxM = bsxfun(@plus, p.bbox, [margin, -margin]);
% isAtBorder = ~Util.isInBBox(shPoint, bboxM);
% shPoint(isAtBorder, :) = [];

% discard agglos with all points at margin
toDel = ~cellfun(@(x)all(Util.isInBBox(point(x, :), bboxM)), sh);
sh(toDel) = [];
Util.log('Discarding %.3f of spines oob.', sum(toDel) / length(toDel));


%% find synapse with highest prob

% p.svg.edgeFile = 'E:\workspace\data\backed\20170217_ROI\globalEdges.mat';
m = load(p.svg.edgeFile);
edges = m.edges;
% p.svg.synScoreFile = ['E:\workspace\data\backed\20170217_ROI\' ...
%     'globalSynScores.mat'];
synScores = SynEM.Util.loadGlobalSynScores(p.svg.synScoreFile, ...
    size(edges, 1), true);

edgeIdx = L4.Agglo.findEdgesOfAgglo(sh, edges);
[mlScore, mSynIdx] = cellfun(@(x)max(synScores(x)), edgeIdx);
mlSyn = arrayfun(@(x, y)x{1}(y), edgeIdx, mSynIdx);

% m = load('E:\workspace\data\backed\20170217_ROI\globalBorder.mat', ...
%     'borderCoM');
m = load(p.svg.borderMetaFile, 'borderCoM');
borderCom = m.borderCoM;


%% to wk

% to wk
N = 50;
idx = randperm(size(sh, 1), N);
skel = L4.Agglo.agglo2Nml(sh(idx), point);
skel.names = arrayfun(@(x)sprintf('sh_%02d', x), 1:N, 'uni', 0)';
names = arrayfun(@(x, y)sprintf('sh_%02d_mlSyn_%.2f', x, y), 1:N, ...
    mlScore(idx)', 'uni', 0);
skel = skel.addNodesAsTrees(borderCom(mlSyn(idx), :), names);
skel.write('SH_att_wo_Syn.nml');


%% check cube  boundary missed syns in agglomerated seg

synScores = SynEM.Util.loadGlobalSynScores( ...
    fullfile(p.aggloSeg.saveFolder, 'synapseScores.mat'));
m = load(fullfile(p.aggloSeg.saveFolder, 'edges.mat'), 'borderMapping');
bmap = m.borderMapping;
idx = 5; % add idx of border here
idxM = bmap(idx);
synScores(idxM, :)