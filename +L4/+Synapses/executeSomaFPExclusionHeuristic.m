function [synapses, isSpineSyn, heuristics] = ...
    executeSomaFPExclusionHeuristic( synapses, isSpineSyn, heuristics)
%EXECUTESOMAFPEXCLUSIONHEURISTIC Execute the soma FP exclusion heuristic
% see L4.Synapses.somaFPExclusionHeuristic
%
% INPUT synapses: [Nx3] table
%           Synapse agglomerate table
%           (see also L4.Synapses.clusterSynapticInterfaces)
%       isSpineSyn: [Nx1] logical
%           Logical array specifying whether the correspoding synapse in
%           the synapses output is onto a spine head (meaning that at least
%           one postsynaptic segment is in a spine head agglomerate).
%           (see output of L4.Synapses.createSynapseAgglomerates)
%       heuristics: struct
%           Struct with indices of synapses that should be deleted
%           according to several heuristics.
%           (see output of L4.Synapses.createSynapseAgglomerates)
%
% OUTPUT The updated inputs according to the heuristic.
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

synapses(heuristics.toDelSomaFPs, :) = [];
isSpineSyn(heuristics.toDelSomaFPs) = [];
heuristics.toDelSingleSH = setdiff(heuristics.toDelSingleSH, ...
    heuristics.toDelSomaFPs);
heuristics.toDelSingleSH = arrayfun( ...
    @(x)x - sum(heuristics.toDelSingleSH < x), ...
    heuristics.toDelSingleSH);
heuristics = rmfield(heuristics, 'toDelSomaFPs');

end

