% script to convert the SynEM inhibitory test set tracings to ROI2017
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>


outFolder = ['E:\workspace\data\backed\L4\Synapses\' ...
    'PerformanceAssessment\InhTestSetROI2017\Tracings'];

folder = ['E:\workspace\data\backed\Synapse detection\data\' ...
    'InhibitoryTestSet\Tracings\MergeModeAxons\'];
idx = [1, 5, 8];
skels = cell(6, 1);
for i = 1:3
    skels{i} = skeleton(fullfile(folder, ...
        sprintf('Axon%dForInhTestSet_mergeMode_cropped', idx(i))));
end

folder = ['E:\workspace\data\backed\Synapse detection\data\' ...
    'InhibitoryTestSet\Tracings\Concensus\'];
skels{4} = skeleton(fullfile(folder, 'Axon1ForInhTestSet_cropped_concensus_postsyn_AG_BS_forPSIdent.nml'));
skels{5} = skeleton(fullfile(folder, 'Axon5ForInhTestSet_cropped_concensus_postsyn_AG_BS.nml'));
skels{6} = skeleton(fullfile(folder, 'Axon8ForInhTestSet_concensus_postsyn_AG_BS.nml'));

axons{1} = skeleton(fullfile(folder, 'Axon1ForInhTestSet_cropped_concensus_AG_BS.nml'));
axons{2} = skeleton(fullfile(folder, 'Axon5ForInhTestSet_cropped_concensus_AG_BS.nml'));
axons{3} = skeleton(fullfile(folder, 'Axon8ForInhTestSet_concensus_AG_BS.nml'));

for i = 1:length(skels)
    skels{i} = convertSkeleton(skels{i});
end
for i = 1:length(axons)
    axons{i} = convertSkeleton(axons{i});
end

for i = 1:3
    axons{i}.write(fullfile(outFolder, sprintf('Axon%d_concensus_converted', idx(i))));
    skels{i}.write(fullfile(outFolder, sprintf('Axon%d_mergeMode_converted', idx(i))));
    skels{3+i}.write(fullfile(outFolder, sprintf('Axon%d_postsyn_converted', idx(i))));
end