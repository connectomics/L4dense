% extract SynEM inhibitory test set labels on ROI2017
% see also Paper.SynEM.InhTestSet.extractLabelsScript
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

info = Util.runInfo();


%% load tracings

trFolder = ['/u/bstaffle/data/L4/Synapses/PerformanceAssessment/' ...
    'InhTestSetROI2017/Tracings'];

axonMM = cell(3, 1);
axon = cell(3, 1);
postsyn = cell(3, 1);
idx = [1, 5, 8];
for i = 1:3
    try
        axonMM{i} = skeleton(fullfile(trFolder, ...
            sprintf('Axon%d_mergeMode_converted_corrected', idx(i))));
    catch
        axonMM{i} = skeleton(fullfile(trFolder, ...
            sprintf('Axon%d_mergeMode_converted', idx(i))));
    end
    
    try
        axon{i} = skeleton(fullfile(trFolder, ...
            sprintf('Axon%d_concensus_converted_corrected', idx(i))));
    catch
        axon{i} = skeleton(fullfile(trFolder, ...
        sprintf('Axon%d_concensus_converted', idx(i))));
    end
    try
        postsyn{i} = skeleton(fullfile(trFolder, ...
            sprintf('Axon%d_postsyn_converted_corrected', idx(i))));
    catch
        postsyn{i} = skeleton(fullfile(trFolder, ...
            sprintf('Axon%d_postsyn_converted', idx(i))));
    end
end

%remove axon tracings from postsyn
for i = 1:length(postsyn)
    idx = postsyn{i}.getTreeWithName('Axon', 'regexp');
    postsyn{i} = postsyn{i}.deleteTrees(idx);
end


%% extract labels

edgeIdx = cell(length(postsyn), 1);
label = cell(length(postsyn), 1);
edgeDir = cell(length(postsyn), 1);
axonSegIds = cell(length(postsyn), 1);

p = Gaba.getSegParameters('ex145_ROI2017');
m = load(p.svg.edgeFile, 'edges');
edges = m.edges;
m = load(p.svg.borderMetaFile, 'borderCoM');
borderCoM = m.borderCoM;

for i = 1:length(postsyn)
    [ edgeIdx{i}, label{i}, edgeDir{i}, axonSegIds{i} ] = ...
        Paper.SynEM.InhTestSet.extractLabelsFromTracings(...
        axonMM{i}, postsyn{i}, p, edges, borderCoM);
end

%combine labels for different axons (requires renumbering of 2nd and 3rd
%labels)
labelAll = label;
for i = 2:3
    labelAll{i}(labelAll{i} > 0) = labelAll{i}(labelAll{i} > 0) ...
        + max(labelAll{i-1});
end
labelAll = cell2mat(labelAll);


%% save results

outFolder = ['/u/bstaffle/data/L4/Synapses/PerformanceAssessment/' ...
    'InhTestSetROI2017/'];

% save(fullfile(outFolder, 'GroundTruth.mat'), ...
%     'axon', 'postsyn', 'edgeIdx', 'label', 'labelAll', 'edgeDir', ...
%     'axonSegIds', 'info');


%% get the labels for the PS nodes using distance of PS nodes to postsyn

psNodes = cell(length(postsyn), 1);
for i = 1:length(postsyn)
    idx = axon{i}.getNodesWithComment('PS', 1, 'regexp');
    psNodes{i} = axon{i}.getNodes(1, idx);
end
psNodes = cell2mat(psNodes);
psNodesNM = bsxfun(@times, double(psNodes), [11.24, 11.24, 28]);

postsynAll = skeleton.fromCellArray(postsyn);
postsynNodes = cellfun(@(x)x(:,1:3), postsynAll.nodes, 'uni', 0);
postsynNodesNM = cellfun(@(x)bsxfun(@times, double(x), ...
    [11.24, 11.24, 28]), ...
    postsynNodes, 'uni', 0);

%find minimal distance postsyn
psLabel = zeros(size(psNodes, 1), 1);
for i = 1:size(psNodes, 1)
    d = cellfun(@(x)min(pdist2(psNodesNM(i,:), x)), postsynNodesNM);
    [~, psLabel(i)] = min(d);
end

if length(unique(psLabel)) < 171
    warning('Not all PS nodes were assigned to unique synapses.');
end

% check which psNode corresponds to which postsyn tree
l = cellfun(@(x)x.numTrees(), postsyn);
pre = repelem(arrayfun(@(x)sprintf('Axon%d_', x), [1; 5; 8], 'uni', 0), l);
postsynAll.names = cellfun(@(x, y) strcat(x, y), pre, postsynAll.names, ...
    'uni', 0);
psTrees = postsynAll.names(psLabel);

assert(isequal(psTrees, sort(psTrees)));

% save(fullfile(outFolder, 'PostsynNodesAndLabels.mat'), ...
%     'psNodes', 'psLabel', 'psTrees', 'info');

