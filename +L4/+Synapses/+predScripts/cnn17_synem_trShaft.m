% prediction on L4 dataset using the cnn17_synem classifier trained on
% shaft synapses only
% see also SynEM.Training.runConfigShaft.cnn17_2_synem_trShaft
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>


%% prediction configuration


p = Gaba.getSegParameters('ex145_ROI2017');

out_cnn17_ROI2017 = '/tmpscratch/bstaffle/data/SynEM/SVM/cnn17_ROI2017';
fm = SynEM.getFeatureMap('PaperAndWkLoad', ...
    Datasets.WkDataset(out_cnn17_ROI2017));
m = load(['/tmpscratch/bstaffle/data/SynEM/Classifier/' ...
    'runShaft/cnn17_synem_trShaft/classifier/LogitBoost.mat']);
classifier = m.classifier;
outputFile = 'synapseScores_cnn17_synem_trShaft';
saveClassAndFM = 'SynapseClassifier_cnn17_synem_trShaft';


%% run the prediction

cluster = getCluster('cpu', struct('h_vmem', '20G'));
SynEM.Seg.predictDataset(p, fm, classifier, outputFile, cluster, [], ...
    [], [], [], saveClassAndFM, false);


%% create global prediction matfile (just for doc)

p = L4.getSegParameter();
p = Seg.Util.changeFilename(p, 'synapseFile', outputFile);
[pred, edgeIdx] = Seg.Global.getGlobalSynapsePrediction(p);
Util.ssave(fullfile(p.saveFolder, 'globalSynScores_cnn17_synem_trShaft'), ...
    pred, edgeIdx);