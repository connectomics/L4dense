% prediction on L4 dataset using the cnn17_2_synem classifier trained on
% shaft synapses only
% see also SynEM.Training.runConfigShaft.cnn17_2_synem_preOnly_trShaft
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>


%% prediction configuration


p = Gaba.getSegParameters('ex145_ROI2017');

out_cnn17_2_ROI2017 = ['/tmpscratch/bstaffle/data/' ...
    '2012-09-28_ex145_07x2_ROI2017/SVM/cnn17_2/'];
fm = SynEM.getFeatureMap('PaperAndWkLoad', ...
    Datasets.WkDataset(out_cnn17_2_ROI2017));
fm.setPreOnly(true);
m = load(['/tmpscratch/bstaffle/data/SynEM/Classifier/' ...
    'runShaft/cnn17_2_synem_preOnly_trShaft/classifier/LogitBoost.mat']);
classifier = m.classifier;
outputFile = 'synapseScores_cnn17_2_synem_preOnly_trShaft';
saveClassAndFM = 'SynapseClassifier_cnn17_2_synem_preOnly_trShaft';


%% run the prediction

cluster = getCluster('cpu');
SynEM.Seg.predictDataset(p, fm, classifier, outputFile, cluster, [], ...
    [], [], [], saveClassAndFM, false);
