% script to collect the results of whole dataset runs
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

p = Gaba.getSegParameters('ex145_ROI2017');


%% synem_shaft_tr2
% see also L4.Synapses.predScripts.synem_shaft

p = Seg.Util.changeFilename(p, 'synapseFile', ...
    'synapseScores_synem_shaft_tr2.mat');
[synScores, edgeIdx] = Seg.Global.getGlobalSynapsePrediction(p);
outFile = fullfile(p.saveFolder, 'globalSynScores_synem_trShaft.mat');
if ~exist(outFile, 'file')
    save(outFile, 'synScores', 'edgeIdx');
end


%% synem_preOnly_shaft_tr2
% see also L4.Synapses.predScripts.synem_shaft_preOnly

p = Seg.Util.changeFilename(p, 'synapseFile', ...
    'synapseScores_synem_preOnly_shaft_tr2.mat');
[synScores, edgeIdx] = Seg.Global.getGlobalSynapsePrediction(p);
outFile =fullfile(p.saveFolder, ...
    'globalSynScores_synem_preOnly_trShaft.mat');
if ~exist(outFile, 'file')
    save(outFile, 'synScores', 'edgeIdx');
end


%% synem_preOnly_tr2
% see also L4.Synapses.predScripts.synem_preOnly

p = Seg.Util.changeFilename(p, 'synapseFile', ...
    'synapseScores_synem_preOnly_tr2.mat');
[synScores, edgeIdx] = Seg.Global.getGlobalSynapsePrediction(p);
outFile =fullfile(p.saveFolder, ...
    'globalSynScores_synem_preOnly_tr2.mat');
if ~exist(outFile, 'file')
    save(outFile, 'synScores', 'edgeIdx');
end


%% cnn17_2_synem_preOnly_trShaft
% see also L4.Synapses.predScripts.cnn17_2_synem_preOnly_trShaft

p = Seg.Util.changeFilename(p, 'synapseFile', ...
    'synapseScores_cnn17_2_synem_preOnly_trShaft.mat');
[synScores, edgeIdx] = Seg.Global.getGlobalSynapsePrediction(p);
outFile =fullfile(p.saveFolder, ...
    'globalSynScores_cnn17_2_synem_preOnly_trShaft.mat');
if ~exist(outFile, 'file')
    save(outFile, 'synScores', 'edgeIdx');
end


%% cnn17_2_synem_trShaft
% see also L4.Synapses.predScripts.cnn17_2_synem_trShaft

p = Seg.Util.changeFilename(p, 'synapseFile', ...
    'synapseScores_cnn17_2_synem_trShaft.mat');
[synScores, edgeIdx] = Seg.Global.getGlobalSynapsePrediction(p);
outFile =fullfile(p.saveFolder, ...
    'globalSynScores_cnn17_2_synem_trShaft.mat');
if ~exist(outFile, 'file')
    save(outFile, 'synScores', 'edgeIdx');
end
