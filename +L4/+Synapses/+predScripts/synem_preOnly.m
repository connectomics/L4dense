% prediction on L4 dataset using the synem classifier trained on all
% synapses using only the presynaptic segment
% see also SynEM.Training.runConfigShaft.synem_preOnly_tr2
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>


%% prediction configuration

p = Gaba.getSegParameters('ex145_ROI2017');
fm = SynEM.getFeatureMap('paper_opt');
fm.setPreOnly(true);
m = load(['/tmpscratch/bstaffle/data/SynEM/Classifier/runShaft/' ...
    'synem_preOnly_tr2/classifier/LogitBoost.mat'], 'classifier');
classifier = m.classifier;
outputFile = 'synapseScores_synem_preOnly_tr2';
saveClassAndFM = 'SynapseClassifier_synem_preOnly_tr2';


%% run the prediction

cluster = getCluster('cpu');
SynEM.Seg.predictDataset(p, fm, classifier, outputFile, cluster, [], ...
    [], [], [], saveClassAndFM, false);
