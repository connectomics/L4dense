function [axons,  indBigAxons, axonsSegIds] = getAxons( p )
%GETAXONS Get the current axon agglomerate output.
% INPUT p: struct
%           Segmentation parameter struct containing the field
%           p.agglo.axonAggloFile.
% OUTPUT axons: struct
%           The axon superagglomerates.
%        indBigAxons: [Nx1] logical
%           The logical indices of the axons >5um wrt axons.
%        axonsSegIds: [Nx1] cell
%           Cell array with the seg ids for the axons. Flight paths are not
%           considered and cells corresponding to axons consisting of
%           flight paths only will be empty.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

axFile = p.agglo.axonAggloFile;
Util.log('Using axons from %s.', axFile);
mAx = load(axFile);
axons = mAx.axons;
indBigAxons = mAx.indBigAxons;

if nargout > 2
    axonsSegIds = Superagglos.getSegIds(axonsAll);
end

end

