function [axons, indBigAxons, parentIds, axFile] = ...
        getLargeAxons( p, inclFlightPath, toAgglo, forceRecalcOv )
%GETLARGEAXONS Get the large axons for a dataset.
%
% INPUT p: struct
%           Segmentation parameter struct. Axons will be loaded from
%           p.agglo.axonAggloFile.
%       inclFlightPath: (Optional) logical
%           Flag to specify that small agglomerates that are picked up by
%           flight paths of large agglomerates are merged into the large
%           agglomerates by simply concatenating the overlapping small axon
%           segment ids to the large one (assuming that the axon segments
%           ids are mutually exclusive).
%           Note that inclFlightPath is intended to be used in the agglo
%           format and not converting to the agglo format is not really
%           tested.
%           see also Superagglos.mergeFlightPathAgglos
%           (Default: false)
%       toAgglo: (Optional) logical
%           Flag indicating that the output should be in the agglo format,
%           i.e. a list of segmentation ids for each agglo. This is
%           typically much faster.
%           (Default: false)
%       forceRecalcOv: (Optional) logical
%           Force to redo the overlap calculation.
%           (Default: false)
%
% OUTPUT axons: [Nx1] struct
%           Axon agglos in the superagglo format.
%        indBigAxons: [Nx1] logical
%           Logical indices for the big axons within all axons.
%           Note that if the flight paths are merged then empty large axons
%           are thus making the output wrong and it is set to empty.
%        parentIds: [Nx1] double
%           Linear indices for `axons` within all axons of `axonAggloFile`.
%        axFile: string
%           Path to the axon file containing the axons. This can be
%           different from p.agglo.axonFile in case inclFlightPath is set
%           to true.
%
% see also L4.Axons.getAxons
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('toAgglo', 'var') || isempty(toAgglo)
    toAgglo = true;
end

if toAgglo
    mode = 'toAgglo';
else
    mode = 'simple';
end

if ~exist('forceRecalcOv', 'var') || isempty(forceRecalcOv)
    forceRecalcOv = false;
end

if exist('inclFlightPath', 'var') && inclFlightPath
    
    % load axons that include the flight path, otherwise create it
    [aggloDir, axFile] = fileparts(p.agglo.axonAggloFile);
    fpFile = fullfile(aggloDir, [axFile, '_large_mergedFlightPaths.mat']);
    
    if exist(fpFile, 'file') && ~forceRecalcOv
        % cached flight path overlaps
        Util.log('Loading flight path merged axons from file %s.', fpFile);
        m = load(fpFile);
        
        if (toAgglo && iscell(m.axons) ) || (~toAgglo && isstruct(m.axons))
            % flight path overlaps were initially only thought to be used
            % in the agglo format
            axons = m.axons;
            try
                parentIds = m.parentIds;
            catch
                Util.log('Axon parent ids not found in cached file.');
                parentIds = [];
            end
        else
            % use the overlaps saved in m to redo the merging
            Util.log(['Cached axons are in the wrong format and ' ...
                'merging of flight path pickups will be redone.']);
            Util.log('Note that this result is not cached.');
            [axons, indBigAxons] = L4.Axons.getAxons(p);
            axonsBigIds = find(indBigAxons);
            axonsBig = axons(axonsBigIds);
            axonsSmall = axons(~indBigAxons);
            ov = m.ov;
            Util.log('Merging agglos picked up by flight paths.');
            [axons, parentIds] = Superagglos.mergeFlightPathAgglos(ov, ...
                axonsBig, axonsSmall, mode);
            parentIds = axonsBigIds(parentIds);
        end
        axFile = fpFile;
    else
        % calculate flight path overlaps
        Util.log('Calculating flight path overlaps.');
        [axons, indBigAxons] = L4.Axons.getAxons(p);
        axonsBigIds = find(indBigAxons);
        axonsBig = axons(axonsBigIds);
        axonsSmall = axons(~indBigAxons);
        fpAxonsBig = Superagglos.getFlightPath(axonsBig, 'nodes');
        info = Util.runInfo(false);
        ov = Superagglos.flightPathAggloOverlap(p, fpAxonsBig, ...
            axonsSmall, [], 'max');
        Util.log('Merging agglos picked up by flight paths.');
        [axons, parentIds, ov] = Superagglos.mergeFlightPathAgglos(ov, ...
            axonsBig, axonsSmall, mode);
        parentIds = axonsBigIds(parentIds);
        Util.log('Saving large axons with flight paths to %s.', fpFile);
        Util.ssave(fpFile, axons, parentIds, ov, info);
        axFile = fpFile;
    end
    
    indBigAxons = [];
else
    [axons, indBigAxons] = L4.Axons.getAxons(p);
    parentIds = find(indBigAxons);
    axons = axons(parentIds);
    if toAgglo
        axons = Superagglos.getSegIds(axons);
    end
    axFile = p.agglo.axonAggloFile;
end


end

