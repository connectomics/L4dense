function dir = getDirection( pOrig, pDest )
%GETDIRECTION Get a direction vector from two points. The direction is from
%first to second point.
% INPUT pOrig: [NxM] double
%           Set of start points. Each row is considered a start point with
%           an end point given in the same row of pDest.
%       pDest: [NxM] double or [1xM] double
%           End point for each start point or for all start points.
% OUTPUT dir: [NxM] double
%           Direction for each pair of input points which is a unit vector.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

dir = bsxfun(@minus, pDest, pOrig);
dir = bsxfun(@times, dir, 1./sqrt(sum(dir.^2, 2)));

end

