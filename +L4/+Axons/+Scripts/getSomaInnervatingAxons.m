% get all axons with synapses onto somata
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>


%% load data

p = Gaba.getSegParameters('ex145_ROI2017');

m = load(p.agglo.axonAggloFile);
axons = m.axons(~m.indBigAxons);
toDel = Superagglos.getPureFlightPathAgglos(axons);
axons(toDel) = [];

m = load(p.agglo.somaFile);
somaAgglos = m.somaAgglos(:,1);

m = load(p.connectome.synFile);
synapses = m.synapses;


%% get axons onto somata

[syn2Agglo, pre2syn, post2syn, connectome] = ...
    L4.Synapses.synapsesAggloMapping(synapses, ...
    Superagglos.getSegIds(axons), somaAgglos);


%% get the indices of the axons

somaInnervatingAxonIdx = unique(connectome{:,1}(:,1));