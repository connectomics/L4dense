% Written by
%   Alessandro Motta <alessandro.motta@brain.mpg.de>
clear;

%% Configuration
p = Gaba.getSegParameters('ex145_ROI2017');
axonFile = fullfile(p.saveFolder, 'aggloState', 'axons_18_b.mat');

%% Build large axons
p.agglo.axonAggloFile = axonFile;
L4.Axons.getLargeAxons(p, true, true);
