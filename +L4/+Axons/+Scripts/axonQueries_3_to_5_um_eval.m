% script to evaluate/patch in the queries for the 3-5 um axons
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

info = Util.runInfo(false);
debug = false;

%% query folder (round 1)

p = Gaba.getSegParameters('ex145_ROI2017');
% use wkwrap segmentation
p.seg.backend = 'wkwrap';
p.seg.root = ['/gabaghi/wKcubes_archive/2012-09-28_ex145_07x2_ROI2017/' ...
    'segmentation/1/'];

queryRound = 'round_1';
outputFolder = fullfile(p.agglo.saveFolder, 'axons_3_to_5_um', queryRound);
axonAggloVerOut = '17_a';


%% load axons

axFile = fullfile(p.agglo.saveFolder, 'axons_16_b.mat');
Util.log('Using axons from %s for overlap calculation.', axFile);
mAx = load(axFile);
axonsAll = mAx.axons;
axonsAllSegIds = Superagglos.getSegIds(axonsAll);
idxAxonsBig = mAx.indBigAxons;
lidxAxonsBig = find(mAx.indBigAxons);


%% load query data

% query start positions
m = load(fullfile(outputFolder, 'queries.mat'), 'pos');
pos = m.pos;
taskGenFile = fullfile(outputFolder, 'axons_3_to_5_um_ending_tasks.txt');
taskIdFile = fullfile(outputFolder, 'result', ...
    'IDs_L4_axonEndingQueries_22_12_2017.txt');

% recover query information about start axon and start node id
% (should have been done in query generation but I was not aware of this)
m = load(fullfile(outputFolder, 'data.mat'));
maxSegIds = Seg.Global.getMaxSegId(p);
lut = Agglo.buildLUT(maxSegIds, axonsAllSegIds);
startAggloIdx = lut(cellfun(@(x)x(1), m.agglos)); % wrt to all axons
m2 = load(fullfile(outputFolder, 'startSegIds.mat'));
seedNodeId = m2.startSegId;

% actually the seed node idx in the agglo is required and not the id
seedNodeIdxInAgglo = cellfun( ...
    @(aggloNodes, id)find(ismember(aggloNodes(:,4), id)), ...
    {axonsAll(startAggloIdx).nodes}', num2cell(seedNodeId));

% check of seed nodes are in query agglos
for i = 1:length(m.agglos)
    assert(ismember(seedNodeId(i), m.agglos{i}));
end

% check if startAggloIdx agglo is equal to query agglos
% (this should be satisfied by the definition of startAggloIdx but who
% knows ...)
for i = 1:length(m.agglos)
    assert(isempty(setdiff(axonsAllSegIds{startAggloIdx(i)}, m.agglos{i})));
end


%% get flight paths

Util.log('Loading flight queries.');
[flights, info_flight] = connectEM.QueryUtil.loadProject( ...
    fullfile(outputFolder, 'result', 'nml'), taskIdFile, p);
[flights, toDel] = connectEM.QueryUtil.rmEmptyAndCommentedFlights(flights);
startAggloIdx = startAggloIdx(~toDel);
seedNodeId = seedNodeId(~toDel);
seedNodeIdxInAgglo = seedNodeIdxInAgglo(~toDel);

% fill in data from query generation
flights.seedNodeIds = num2cell(seedNodeIdxInAgglo);


%% debugging

if debug
    skel = L4.Util.getSkel(); %#ok<*UNRCH>
    for i = 1:10
        skel = skel.addTree(sprintf('Query_%d_Flight', i), ...
            flights.nodes{i});
        skel = skel.addTree(sprintf('Query_%d_Start', i), ...
            axonsAll(startAggloIdx(i)).nodes(:, 1:3));
    end
    skel.write('FlightQueryDebug_1.nml')
end


%% agglo overlap

Util.log('Calculating flight query overlaps.');
% get overlap wrt to axons big (i.e. end attachment is only possible to big
% axons)
flights.overlaps = ...
    connectEM.Flight.overlapWithAgglos( ...
        p, flights, axonsAllSegIds(idxAxonsBig), ...
        'minStartEvidence', 13, 'minEndEvidence', 2 * 27);

% fill in the start agglo idx of the small axons determined above
flights.overlaps(:,1) = num2cell(startAggloIdx);

flights = connectEM.QueryUtil.filterFlights(flights);

% assure that indices of end attachment are in axons big (this should
% automatically be the case if the big axons are at the beginning of the
% axons array in which case lidxAxonsBig = 1:N)
flights.overlaps(flights.overlaps(:,2) > 0, 2) = ...
    lidxAxonsBig(flights.overlaps(flights.overlaps(:,2) > 0, 2));

flights.seedNodeIds = cell2mat(flights.seedNodeIds);


%% debugging

if debug
    skel = L4.Util.getSkel();
    for i = 1:10
        skel = skel.addTree(sprintf('Query_%d_Flight', i), ...
            flights.nodes{i});
        skel = skel.addTree(sprintf('Query_%d_Start', i), ...
            axonsAll(flights.overlaps(i, 1)).nodes(:, 1:3));
        if flights.overlaps(i, 2) > 0
            skel = skel.addTree(sprintf('Query_%d_End', i), ...
                axonsAll(flights.overlaps(i, 2)).nodes(:, 1:3));
        end
    end
    skel.write('FlightQueryDebug_2.nml')
end


%% deal with overlapping flights paths
% unfortunately it can happen that a large agglo has a dangling flight path
% that overlaps with the small agglo (which were not picked up back then).
% thus it can happen that the small agglo now has a flight path parallel to
% the dandling of the flight path of the large agglo till it reaches a
% segment of the large agglo. these cases need to be sorted out or
% otherwise we would overestimate our reconstruction length

% for agglos that are target overlaps of queries get the flight paths
% segments
target_idx = setdiff(flights.overlaps(:,2), 0);
toTargetIdx = sparse(target_idx, 1, 1:length(target_idx));
target_axons = axonsAll(target_idx);
% flights_target = Superagglos.getFlightPathSegIds(p, ...
%     Superagglos.origFields(axonsAll(target_idx))); % has a strange error
fp = Superagglos.getFlightPath(target_axons, 'nodes');
fp = Superagglos.getFlightPathSegIds2(p, fp);

% check if they overlap with the start agglo
isAlreadyOverlapping = false(length(flights.seedNodeIds), 1);
for i = 1:size(flights.overlaps, 1)
    if flights.overlaps(i,2) > 0
        isAlreadyOverlapping(i) = any(ismember( ...
            axonsAll(flights.overlaps(i,1)).nodes(:,4), ...
            fp(toTargetIdx(flights.overlaps(i,2))).segIds));
    end
end

if debug % check if they are really overlapping
    skel = L4.Util.getSkel();
    tmp = find(isAlreadyOverlapping);
    tmp = tmp(randperm(length(tmp)));
    for i = 1:10
        idx = tmp(i);
        skel = skel.addTree(sprintf('Query_%d_Flight', i), ...
            flights.nodes{idx});
        skel = skel.addTree(sprintf('Query_%d_Start', i), ...
            axonsAll(flights.overlaps(idx, 1)).nodes(:, 1:3),...
            axonsAll(flights.overlaps(idx, 1)).edges);
        skel = skel.addTree(sprintf('Query_%d_End', i), ...
            axonsAll(flights.overlaps(idx, 2)).nodes(:, 1:3), ...
            axonsAll(flights.overlaps(idx, 2)).edges);
    end
    skel.write('FlightQueryDebug_3.nml')
end

% merge the already overlapping ones based on the closest node
% (currently causes overlaps of flight paths and the merged agglos. flight
% path nodes in the merged agglos should be removed)
Util.log('Merging small agglos that already overlap with large ones.')
% axonsAllOld = axonsAll; % for debugging
for i = 1:length(axonsAll)
    axonsAll(i).edges = sort(axonsAll(i).edges, 2);
end
ov = flights.overlaps(isAlreadyOverlapping, :);
toDelAgglo = ov(:,1);
for i = 1:size(ov, 1)
    % merge smaller into larger one by discarding the flight path of the
    % larger one inside the small agglo based on distance
    axonsAll(ov(i, 1)) = Superagglos.mergeOnOverlaps( ...
        axonsAll(ov(i, 1)), axonsAll(ov(i, 2)), ...
        'scale', p.raw.voxelSize, ...
        'overlapDistNm', 500, 'minLenNm', 500, 'use2015b', true);
end

if debug % look at merged ones
    skel = L4.Util.getSkel();
    N = 20;
    ov = ov(randperm(size(ov, 1)), :);
    for i = 1:N
        skel = skel.addTree(sprintf('%d_Small', i), ...
            axonsAllOld(ov(i, 1)).nodes(:, 1:3),...
            axonsAllOld(ov(i, 1)).edges, [0, 0, 1, 1]);
        skel = skel.addTree(sprintf('%d_Large', i), ...
            axonsAllOld(ov(i, 2)).nodes(:, 1:3),...
            axonsAllOld(ov(i, 2)).edges, [0, 1, 0, 1]);
        skel = skel.addTree(sprintf('%d_Combined', i), ...
            axonsAll(ov(i, 1)).nodes(:, 1:3),...
            axonsAll(ov(i, 1)).edges, [1, 0, 0, 1]);
    end
    skel.write('FlightQueryDebug_4.nml')
end

% delete flights that were already merged
flights = structfun(@(x)x(~isAlreadyOverlapping,:), flights, 'uni', 0);

% delete small agglos that were merged
toDel = ov(:,1);
assert(max(lidxAxonsBig) < min(toDel)); % otherwise ov(:,2) needs to be
                                        % changes as well
tmp = unique(toDel);

% correct overlaps for deleted agglos
flights.overlaps(:,1) = flights.overlaps(:,1) - ...
    arrayfun(@(x)sum(tmp < x), flights.overlaps(:,1));
axonsAll(toDel) = [];
idxAxonsBig(toDel) = [];


%% create axon agglo version

Util.log('Creating new axon agglo version.');
out = connectEM.Flight.patchIntoAgglos(p, axonsAll, flights);
out.axons = out.agglos;
out = rmfield(out, 'agglos');

out.indBigAxons = accumarray( ...
    out.childIds, idxAxonsBig, [], @any);
out.info = info;


%% save new axon version

outFile = fullfile(p.agglo.saveFolder, ...
    sprintf('axons_%s.mat', axonAggloVerOut));
if exist(outFile, 'file')
    Util.log(['Output file %s already exists and will not be ' ...
        'overwritten.'], outFile);
else
    Util.log('Saving new axon agglo version at %s.', outFile);
    save(outFile, '-struct', 'out');
end
