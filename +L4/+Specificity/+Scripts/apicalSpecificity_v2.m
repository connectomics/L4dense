% script to determine apical dendrite specificity (using synapses)
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

plotting = false; %#ok<*UNRCH>


%% load apicals, axons and synapses

p = Gaba.getSegParameters('ex145_ROI2017');

% apicals
m = load('/tmpscratch/zecevicm/L4/apicalDendrites/apicalDendritesL4.mat');
apicalDenAgglos = m.filteredAgglos;

% axons
m = load('/tmpscratch/mberning/axonQueryResults/postQueryAgglomerates.mat');
axonAgglos.segIds = m.axonsPostQuery(m.isAbove5um);
axonAgglos.segIds  = axonAgglos.segIds(2:end); % ignore super-merger

% synapses
m = load('/gaba/u/bstaffle/data/L4/Synapses/SynapseAgglos_v1.mat');
synapses = m.synapses;


%% get synapse agglo mappings

[syn2Agglo, pre2syn, post2syn, connectome] = ...
    L4.Synapses.synapsesAggloMapping(synapses, axonAgglos.segIds, ...
    apicalDenAgglos);


%% examples to nml (connections with > 1 synapses)

if plotting
    p = Gaba.getSegParameters('ex145_ROI2017');
    [graph, segmentMeta, borderMeta] = Seg.IO.loadGraph(p, false);
    
    idx = find(cellfun(@length, connectome.synIdx) > 1);
    i = 4;
    skel = L4.Agglo.agglo2Nml( ...
        [axonAgglos.segIds(connectome.edges(idx(i), 1)); ...
        apicalDenAgglos(connectome.edges(idx(i), 2))], segmentMeta.point);
    skel = L4.Agglo.agglo2Nml( ...
        synapses.edgeIdx(connectome.synIdx{idx(i)}, 1), ...
        borderMeta.borderCoM, skel);
    skel.write('ConnectoneEntrySample.nml')
end

%% specificity

% axons onto apicals
axonIdx = unique(connectome.edges(:,1));

% get synapses onto apicals
isSynOntoApical = false(size(synapses, 1), 1);
isSynOntoApical(cell2mat(connectome.synIdx)) = true;

% get synapse counts for axons
totalNoSyn = cellfun(@length, pre2syn(axonIdx));
synOntoApicals = cellfun(@(x)sum(isSynOntoApical(x)), pre2syn(axonIdx));

% specificities
sp_minus_one = (synOntoApicals - 1)./(totalNoSyn - 1);

% all information to table
axonsOntoApicals = table(axonIdx, totalNoSyn, synOntoApicals, ...
    sp_minus_one, 'VariableNames', {'axonIdx', 'totalNoSyn', ...
    'synOntoApicals', 'specificity_minus_one'});

%% fractional specificity

if plotting 
    figure
    histogram(axonsOntoApicals.specificity_minus_one)
    xlabel('Fraction of synapses onto apicals (minus one)')
    ylabel('Count')
    Visualization.Figure.plotDefaultSettings()
    a = gca;
    a.XLim = [0, 1];

    figure
    scatter(axonsOntoApicals.specificity_minus_one, ...
        axonsOntoApicals.totalNoSyn);
    xlabel('Fraction of synapses onto apicals (minus one)')
    ylabel('Total number of synapses (minus one)')
    Visualization.Figure.plotDefaultSettings()
    a = gca;
    a.XLim = [0, 1];
    a.YScale = 'log';
end