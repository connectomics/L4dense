% test lda model for specificity analysis
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>


%% load class connectome

% run connectEM.Connectome.plotInputRatiosVsNullHypothesis


%% prepare lda

bow = classConnectome(curAxonClasses(2).axonIds,:);
classProbs = bow ./ sum(bow, 2);

save('tmp.mat', 'bow', 'targetClasses')
% save to matfile and to in python
load('tmp2.mat') % results from python lda
topic = topic';


%% look at synapses

group = Util.group2Cell(topic');
groupProb = cellfun(@(x)classProbs(x, :), group, 'uni', 0);
group = cellfun(@(x)bow(x, :), group, 'uni', 0);
