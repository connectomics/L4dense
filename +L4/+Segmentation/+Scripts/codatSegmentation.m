%% script to produce the codat segmentation based
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

info = Util.runInfo();

p = Gaba.getSegParameters('ex145_ROI2017');
bbox = p.bbox;
cnetFile = '/u/bstaffle/results/cnn2/smallTarget/cnn2l_11.mat';
m = load(cnetFile);
cnet = m.cnet;
info.param.cnet = cnet;
info.param.bbox = bbox;
info.param.p = p;

% create input and output dataset
outputFolder = ['/tmpscratch/bstaffle/data/' ...
    '2012-09-28_ex145_07x2_ROI2017/cnn2l_11/'];
wkwInit('new', outputFolder, 32, 32, 'single', 1);
datIn = Datasets.WkDataset(p.raw);
datOut = Datasets.WkDataset(outputFolder);

%% cnn prediction

job = Codat.CNN.Cluster.predictDataset(cnet, bbox, datIn, datOut);
save(fullfile(outputFolder, 'runInfo.mat'), 'info')