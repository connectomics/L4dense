% calculate the L4 agglo error free path length (ERL)
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

info = Util.runInfo();
plotFlag = false; %#ok<*UNRCH>


%% load the data

p = Gaba.getSegParameters('ex145_ROI2017');
m = load([p.saveFolder 'globalEdges.mat']);
edges = m.edges;
m = load([p.saveFolder 'globalBorder.mat'], 'borderCoM');
borderCom = m.borderCoM;
skels = skeleton.loadSkelCollection(['/gaba/u/bstaffle/' ...
    'data/L4/Segmentation/ERL_Tracings/'], 1, true);
m = load([p.agglo.saveFolder 'axons_06_c.mat']);
axons = m.axons;
m = load([p.agglo.saveFolder 'dendrites_03_v2.mat']);
dendrites = m.dendrites;

agglos = cat(1, Superagglos.getSegIds(axons), ...
    Superagglos.getSegIds(dendrites));
agglos = cellfun(@sort, agglos, 'uni', 0);


%% precalculate the segIds

segIds = Skeleton.getSegmentIdsOfSkelCellArray(p, skels);

%% get overlapping dendrite agglos

denAgglos = Superagglos.getSegIds(dendrites);
denLUT = Agglo.buildLUT(max(edges(:)), denAgglos);

skelToDenAgglos = cellfun(@(x)denLUT(x(x > 0)), segIds, 'uni', 0);
skelToDenAgglos = cellfun(@(x)tabulate(x), skelToDenAgglos, 'uni', 0);
skelToDenAgglos = cellfun(@(x)x(x(:,1) > 0, 1:2), skelToDenAgglos, 'uni', 0);

ovT = 10;
if plotFlag
    m = load(p.svg.segmentMetaFile, 'point');
    point = m.point';
    for i = 1:length(skels)
        skel = skels{i};
        skel.verbose = false;
        ov = skelToDenAgglos{i}(:,2);
        idx = skelToDenAgglos{i}(ov >= ovT, 1);
        skel = L4.Agglo.agglo2Nml(denAgglos(idx), point, skel);
        skel.names{1} = 'GT_Tracing';
        skel.names(2:end) = arrayfun(@(x, y) ...
            sprintf('Agglo_%d_ov%d', x, y), idx, ov(ov >= ovT), ...
            'uni', 0);
        skel.write([skel.filename '_dendriteAggloOverlap']);
    end
end


%% run erl calculation

CRC = L4.Segmentation.ERL.calculateCRC(skels, segIds, edges, borderCom, ...
    agglos);
Util.log('Calculating ERL.');
[erl, erl_s] = L4.Segmentation.ERL.calculateERL(CRC);


%% write out CRC skeletons

if plotFlag
    names = cellfun(@(x)x.filename, skels, 'uni', 0);
    for i = 1:length(names)
        skel = CRC{i}.skel;
        for j = 1:skel.numTrees()
            skel.names{j} = sprintf('%s_%fum', skel.names{j}, ...
                CRC{i}.skelL(j)/1000);
        end
        skel = skel.addTreeFromSkel(skels{i}, 1);
        skel.names{end} = 'GT';
        skel.write([names{i} '_CRC']);
    end
end