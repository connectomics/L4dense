function [erl, erl_s] = calculateERL( CRC )
%CALCULATEERL Error free path length calculation.
% INPUT CRC: [Nx1] cell
%           see output of L4.Segmentation.ERL.calculateCRC
% OUTPUT erl: double
%           The error free path length.
%           (In units as documented in L4.Segmentation.ERL.calculateCRC)
%        erl_s: [Nx1] double
%           Single skeleton erl.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

L = cellfun(@(x)x.L, CRC);
erl_s = zeros(length(L), 1);
for i = 1:length(CRC)
    erl_s(i) = sum(CRC{i}.skelL.*(CRC{i}.skelL./CRC{i}.L));
end

erl = sum(L./sum(L).*erl_s);

end

