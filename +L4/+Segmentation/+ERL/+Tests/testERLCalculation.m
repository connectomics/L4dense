% Some erl consistency tests
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

function tests = testERLCalculation()
tests = functiontests(localfunctions);
end

function test_erl1(testCase)
% Supp Fig 2A from Januszewski et al., 2017
skel = skeleton();
nodes = repmat([1, 0, 0], 9, 1);
nodes = bsxfun(@times, nodes, (1:9)');
skel = skel.addTree([], nodes);
segIds{1} = [1, 1, 2, 2, 3, 3, 4, 4, 5]';
crc = L4.Segmentation.ERL.calculateCRC({skel}, segIds, [], [], [], true);
erl = L4.Segmentation.ERL.calculateERL(crc);
verifyEqual(testCase, erl, 0.5); % 1 is correct according to paper
end

function test_erl2(testCase)
% Supp Fig 2B from Januszewski et al., 2017
skel = skeleton();
nodes = repmat([1, 0, 0], 9, 1);
nodes = bsxfun(@times, nodes, (1:9)');
eps = 1;
nodes = [nodes; eps.*repmat([1, 1, 0], 4, 1)];
edges = [[(1:8)', (2:9)']; [1, 10]; [1 11]; [1 12]; [1 13]];
skel = skel.addTree([], nodes, edges);
segIds{1} = [ones(1, 9), 2, 3, 4, 5]';
crc = L4.Segmentation.ERL.calculateCRC({skel}, segIds, [], [], [], true);
erl = L4.Segmentation.ERL.calculateERL(crc);
verifyEqual(testCase, erl, 64/12);
end

function test_erl3(testCase)
% Supp Fig 2C from Januszewski et al., 2017
skel = skeleton();
nodes = repmat([1, 0, 0], 9, 1);
nodes = bsxfun(@times, nodes, (1:9)');
skel = skel.addTree([], nodes);
segIds{1} = [1, 1, 1, 2, 2, 2, 2, 2, 2]';
crc = L4.Segmentation.ERL.calculateCRC({skel}, segIds, [], [], [], true);
erl = L4.Segmentation.ERL.calculateERL(crc);
verifyEqual(testCase, erl, 4/8 + 25/8);
end