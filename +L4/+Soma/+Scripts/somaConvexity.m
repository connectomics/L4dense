% script to determine soma convexity
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

%% load data

p = Gaba.getSegParameters('ex145_ROI2017');

% get somas from whole cell gt for now
somaAgglos = load(fullfile(p.agglo.saveFolder,'center_somas.mat'));
somaAgglos = somaAgglos.result(:,2);

m = load(p.svg.segmentMetaFile, 'point');
point = m.point';

%% fit ellipsoid to soma via pca

somaIdx = 1;

% get center and axis length of ellipsoid
coords = point(somaAgglos{somaIdx}, :);
[coeff, score, latent] = pca(coords);
center = mean(coords);
coords_mean = bsxfun(@minus, coords, center);
% score_2 = coords_mean * coeff; % should be equal to score
% coords_mean_2 = score * inv(coeff); % should be coords_mean

% get distance
d = sum(bsxfun(@times, score.^2, 1./latent(:)'), 2);
[f, x] = ecdf(d);
scale = x(find(f > 0.95, 1, 'first'));
% scale = 1;

idx = d <= scale;

% visualize
tmp = sqrt(scale).*sqrt(latent);
[x, y, z] = ellipsoid(0, 0, 0, tmp(1), tmp(2), tmp(3));
% [x, y, z] = ellipsoid(0, 0, 0, 1, 1, 1);
X = [x(:), y(:), z(:)];
sz = size(x);
% X = bsxfun(@plus, X * (eye(3).*sqrt(latent)) * inv(coeff), center);
X = bsxfun(@plus, X * inv(coeff), center);
x = reshape(X(:,1), sz);
y = reshape(X(:,2), sz);
z = reshape(X(:,3), sz);
figure;
Visualization.scatter3(coords);
hold on
h = surf(x, y, z);


[x, y, z] = ellipsoid(0, 0, 0, tmp(1), tmp(2), tmp(3));
figure
Visualization.scatter3(score);
hold on
h = surf(x, y, z);

%% Visualize in wk

skel = L4.Util.getSkel();
skel = skel.addTree([], coords(idx, :));
skel = skel.addTree([], coords(~idx, :));