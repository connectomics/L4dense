% script to check the completeness of the soma agglo
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

%% load kamin cells

filePath = 'E:\workspace\data\backed\L4\Somata\KAMIN_cells.xlsx';
somata = L4.Soma.getSomaList( filePath );
% somata([107, 109],:) = []; % discard duplicates
somaCen = somata.Centroid;
somaCen(81,:) = 0; % set to zero otherwise it is skipped from nml below

%% get nuclei coordinates from CS

m = load('E:\workspace\data\backed\L4\Somata\NucleiCoordinates.mat');
nuclei = m.rp;
mag1bbox = [128, 128, 128, 5573, 8508, 3413];
mag1bbox = Util.convertWebknossosToMatlabBbox(mag1bbox);
mag1bbox = mag1bbox + [-25 25; -25 25; -10 10];
nucleiCen = [nuclei.Centroid];
nucleiCen = reshape(nucleiCen, 3, []);
nucleiCen = round(nucleiCen');
nucleiCen = bsxfun(@plus, 4.*nucleiCen, mag1bbox(:,1)');
[~, tmp] = xlsread('E:\workspace\pipeline\+Soma\somaDoc.xlsx', 'B1:B126');
% somehow xlsread only trims empty fields at beginning and end
tmp = tmp(2:end);
tmp(end+1:125) = {[]};
isGliaNuc = ~cellfun(@isempty, tmp);

%% to skeleton to check which

skel = L4.Util.getSkel();
skel = skel.addNodesAsTrees(somaCen, 'Soma');
skel.names(somata.isGlia) = cellfun(@(x)[x ' - glia'], ...
    skel.names(somata.isGlia), 'uni', 0);
skel.names{81} = [skel.names{81} ' - Nan soma'];
names = arrayfun(@(x)sprintf('Nucleus%03d', x), 1:size(nucleiCen, 1), ...
    'uni', 0);
skel = skel.addNodesAsTrees(nucleiCen, names);
skel.colors(size(somata, 1) + 1:end) = {[0 1 0 1]};
skel.write('SomataAndNuclei.nml');

% get nuclei->soma map from tracing
skel = skeleton('E:\workspace\data\backed\L4\Somata\SomataAndNuclei.nml');
skel = skel.sortTreesById();
nucNames = skel.names(size(somaCen, 1) + 1:end);
nucNames = cellfun(@(x)x(14:end), nucNames, 'uni', 0);
hasSoma = strfind(nucNames, 'Soma');
hasSoma = cellfun(@(x)any(x==1), hasSoma); % any due to nucleus 23 and duplicates
nuc2soma = zeros(size(nucleiCen, 1), 1);
nuc2soma(hasSoma) = cellfun(@(x)str2double(x(5:7)), nucNames(hasSoma));
soma2nuc = zeros(size(somata, 1), 1);
idx = find(nuc2soma);
soma2nuc(nuc2soma(idx)) = idx;
soma2nuc(127) = 23; % nucleus 23 is unclear which soma
soma2nuc(106) = 114;
soma2nuc(107) = 12;
soma2nuc(81) = nan;

neuronWithoutNucleus = find(soma2nuc == 0 & ~somata.isGlia);
gliaWithoutNucleus = find(soma2nuc == 0 & somata.isGlia);