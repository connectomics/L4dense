function [somaSynIdx, toDiscard] = somaSynapsePostProcessing( synapses, ...
    boutons, somaSynIdx, isSpineSyn, synScores, synScoresT )
%SOMASYNAPSEPOSTPROCESSING Discard soma synapses that have a spine synapse
%or a very likely other synapse at the same boutons.
% INPUT synapses: [Nx1] table
%           Table with synapses.
%           (see also output of L4.Synapses.clusterSynapticInterfaces)
%       boutons: [Nx1] cell
%           Cell array with linear indices w.r.t. 'synapses' that contain
%           all synapses of a bouton.
%           (see also L4.Synapses.synapse2BoutonsLocalPresynClust)
%       somaSynIdx: [Nx1] int
%           Linear indices of the synapses onto somas.
%       isSpineSyn: [Nx1] logical
%           Logical array whether the corresponding synapse is onto a spine
%           head.
%       synScores: [Nx2] or [Nx1] double
%           Synapse scores w.r.t. the edgeIdx in 'synapses' for each
%           interface (either for each direction separately or both
%           directions).
%       synScoresT: (Optional) struct
%           Struct with the thresholds for synapse exclusion based on the
%           other synapses of the same bouton. The fields are
%           'synT': double
%               Threshold for the soma synapse in question below which
%               exclusion based on other soma synapses is done. I.e. the
%               soma synapse is only considered for removal if the synapse
%               score is below synT.
%           'otherSynT': double
%               If any of the synapses of the same bouton is above
%               otherSynT then the corresponding soma synapse is removed.
% OUTPUT somaSynIdx: [Nx1] int
%           Linear indices of the synapses onto somas after discarding
%        toDiscard: [Nx1] logical
%           Logical vector w.r.t. somaSynIdx input telling which somaSynIdx
%           were discarded.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if size(synScores, 2) == 2
        synScores = max(synScores, [], 2);
end
    
boutonLookup = L4.Agglo.buildLUT(boutons, size(synapses, 1));
somaSynBoutonIdx = boutonLookup(somaSynIdx);
% hasSpineSyn = cellfun(@(x)any(isSpineSyn(x)), boutons(somaSynBoutonIdx));
spineSynIdx = cellfun(@(x)x(isSpineSyn(x)), ...
    boutons(somaSynBoutonIdx), 'uni', 0);

% discard spine syn only if it is the highest prob synapse onto the spine
postsynLUT = L4.Agglo.buildLUT(synapses.postsynId, [], true);
for i = 1:length(spineSynIdx)
    toDel = false(length(spineSynIdx{i}), 1);
    for j = 1:length(spineSynIdx{i})
        
        % get all other synapses onto the same spine
        otherSynIdx = setdiff( ...
            vertcat(postsynLUT{synapses.postsynId{spineSynIdx{i}(j)}}), ...
            spineSynIdx{i}(j));
        if ~isempty(otherSynIdx)
            scoreOthers = max(synScores(vertcat( ...
                synapses.edgeIdx{otherSynIdx})));
            thisScore = max(synScores(synapses.edgeIdx{spineSynIdx{i}(j)}));
            if scoreOthers > thisScore
                toDel(j) = true;
            end
        end
    end
    spineSynIdx{i}(toDel) = [];
end

toDiscard = ~cellfun(@isempty, spineSynIdx);

if exist('synScores', 'var') && exist('synScoresT', 'var')
    otherSynIdx = arrayfun(@(bIdx, sIdx)setdiff(boutons{bIdx}, sIdx), ...
        somaSynBoutonIdx, somaSynIdx, 'uni', 0);
    somaSynScores = cellfun(@(x)max(synScores(x)), ...
        synapses.edgeIdx(somaSynIdx));
    otherSynScores = cell(length(somaSynIdx), 1);
    idx = ~cellfun(@isempty, otherSynIdx);
    otherSynScores(idx) = cellfun(@(x)synScores(vertcat(synapses.edgeIdx{x})), ...
        otherSynIdx(idx), 'uni', 0);
    otherSynScores(~idx) = {nan};
    otherSynScoresMax = cellfun(@max, otherSynScores);
    toDiscard = toDiscard | ...
        (somaSynScores < synScoresT.synT & ...
         otherSynScoresMax >= synScoresT.otherSynT);
    
end

somaSynIdx(toDiscard) = [];

end

