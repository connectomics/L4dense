function exits = exitDetection( agglos, segComs, edges, prob, probT, ...
    borderSize, bSizT, dT, scale )
%EXITDETECTION Detect exits of soma agglos by doing a local agglomeration
% and and consider locations that extend further than a given threshold
% from the original agglomeration.
% INPUT agglos: [Nx1] cell
%           List of segment ids for each soma.
%       segComs: [Nx3] int
%           Center of mass coordinates of each segment.
%       edges: [Nx2] int
%           The segment adjacency edge list.
%       prob: [Nx1] double
%           Edge continuity probability for each edge in edges.
%       probT: double
%           Continuity score threshold for agglomeration. Edges with
%           prob >= probT will be agglomerated.
%       borderSize: [Nx1] int
%           The border size for the corresponding edges in voxel.
%       bSizT: (Optional)
%           Border size threshold.
%           (Default: 2000)
%       dT: double
%           The minimal length to consider the parts that are added to a
%           soma via agglomeration as exits in units of scale.
%       scale: (Optional) [1x3] double
%           The voxel size scaling.
%           (Default: [1, 1, 1])
% OUTPUT exits: [Nx1] cell
%           Contains a struct with each agglo with the fields
%           'points': 3d coms of the segments that have been identified as
%               exits.
%           'segIds': The segment ids for segments that have been
%               identified as exits.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('scale', 'var') || isempty(scale)
    scale = [1, 1, 1];
end
scale = scale(:)';
margin = 5.*dT./scale;

if ~exist('bSizT', 'var') || isempty(bSizT)
    bSizT = 2000;
end

% border size threshold & keep nans because they are correspondences
toKeepIdx = borderSize >= bSizT | isnan(borderSize);
edges = edges(toKeepIdx, :);
prob = prob(toKeepIdx);
edges = edges(prob >= probT, :);

exits = cell(length(agglos), 1);
for i = 1:length(agglos)
    
    % restrict edge list to bbox around soma
    somaComs = segComs(agglos{i}, :);
    if isempty(somaComs)
        continue;
    end
    bbox = getBbox(somaComs, margin);
    idx = find(Util.isInBBox(segComs, bbox));
    edgeIdx = all(ismember(edges, idx), 2);
    edgesR = edges(edgeIdx, :);
    
    % add artifical edges between soma ids
    edgesR = cat(1, edgesR, [agglos{i}(1:end-1), agglos{i}(2:end)]);
    
    % agglomerate soma agglo
    comps = Graph.findConnectedComponents(edgesR);
    somaCompIdx = find(cellfun(@(x)any(ismember(agglos{i}, x)), comps));
    assert(length(somaCompIdx) == 1);
    newIds = setdiff(comps{somaCompIdx}, agglos{i});
    d = min(pdist2(bsxfun(@times, segComs(newIds, :), scale), ...
                   bsxfun(@times, somaComs, scale)), [], 2);
    exits{i}.points = segComs(newIds(d < dT), :);
    exits{i}.segIds = newIds(d < dT);
end

% TODO: cluster exit points

end

function bbox = getBbox(nodes, margin)
bbox = [min(nodes, [], 1)', max(nodes, [], 1)'];
if isscalar(margin)
    margin = repmat(margin, 3, 1);
end
bbox = bsxfun(@plus, bbox, [-margin(:), margin(:)]);
end