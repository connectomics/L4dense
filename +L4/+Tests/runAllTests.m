function result = runAllTests()
%RUNALLTESTS Run all tests in this package.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

tic;
try
    result = runtests('L4.Tests');
    result = table(result);
    disp(result);
    if all([result.Passed])
        Util.log('Total testing time %.2f secons. All tests passed.', toc);
    else
        warning('Some tests failed or are incomplete. Check rt for details.');
    end
catch e
    disp(getReport(e, 'extended'));
end

end

