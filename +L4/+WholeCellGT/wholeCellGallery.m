% script for the whole cell gallery
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

%% whole cells after merger fixing to amira

p = Gaba.getSegParameters('ex145_ROI2017');
p.seg.backend = 'wkwrap';
p.seg.root = ['/gabaghi/wKcubes_archive/2012-09-28_ex145_07x2_ROI2017/' ...
    'segmentation/1/'];
p.agglo.dendriteAggloFile = fullfile(p.agglo.saveFolder, ...
    'dendrites_03_v2_splitmerged.mat');
m = load(p.agglo.dendriteAggloFile);
wholeCellAgglos = m.dendrites(m.WholeCellId);
wholeCellAgglos = Superagglos.getSegIds(wholeCellAgglos);
outputFolder = '/tmpscratch/bstaffle/data/L4/wholeCells/issfs_amira/';
Visualization.exportAggloToAmira(p, wholeCellAgglos, outputFolder);