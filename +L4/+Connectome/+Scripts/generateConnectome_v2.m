%% script to generate the connectome_v2
% based on synapses from L4.Synapses
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

info = Util.runInfo(false);


%% load agglos & synapses

p = Gaba.getSegParameters('ex145_ROI2017');

% axons
p.agglo.axonAggloFile = ['/gaba/u/mberning/results/pipeline/' ...
    '20170217_ROI/connectomeState/axons.mat'];
mPre = load(p.agglo.axonAggloFile);
axons = mPre.axonAgglos;
axons(cellfun(@isempty, axons)) = [];
% axons = mPre.axons(mPre.indBigAxons);

% dendrite agglos
p.agglo.dendriteAggloFile = ['/gaba/u/mberning/results/pipeline/' ...
    '20170217_ROI/aggloState/dendrites_05.mat'];
mPost = load(p.agglo.dendriteAggloFile, 'indBigDends', 'idxApicals', ...
    'idxSmooth');
indBigDends = mPost.indBigDends;
idxAD = mPost.idxApicals;
idxSD = mPost.idxSmooth;
p.agglo.dendriteAggloFile = ['/gaba/u/mberning/results/pipeline/' ...
    '20170217_ROI/aggloState/spine_heads_and_attachment_dendrites_05.mat'];
mPost = load(p.agglo.dendriteAggloFile);
dendrites = mPost.dendAgglos(indBigDends);
dendrites = cellfun(@double, dendrites, 'uni', 0);
% dendrites = mPost.dendrites(mPost.indBigDends);
p.connectome.synFile = fullfile(p.connectome.saveFolder, ...
    'SynapseAgglos_v2.mat');
mSyn = load(p.connectome.synFile);
synapses = mSyn.synapses;

clear mPre mPost


%% add whole cells as separate class

mSom = load(p.agglo.somaFile);
somata = mSom.somas(:,3);
[dendrites, toDel] = L4.Specificity.addSomasToDendrites(dendrites, ...
    somata, 'separate');

% update target classes
idxSoma = (length(dendrites) - length(somata) + 1):length(dendrites);
idxAD(toDel) = false;
idxSD(toDel) = false;
idxAD = find(idxAD);
idxSD = find(idxSD);

%% calculate connectome

[ syn2Agglo, pre2syn, post2syn, connectome ] = ...
    L4.Synapses.synapsesAggloMapping( synapses, axons, dendrites );
connectomeFull = L4.Connectome.pairwise2Full(connectome, ...
    [length(axons), length(dendrites)]);


%% connectome synapse information

Util.log('Adding synapse meta information.');

% load border meta (must be wrt to edges in graph)
[graph, segmentMeta, borderMeta] = Seg.IO.loadGraph(p, false);
m = load(p.svg.borderMetaFile, 'borderArea');
borderMeta.borderArea = nan(size(borderMeta.borderSize));
borderMeta.borderArea(~isnan(borderMeta.borderSize)) = m.borderArea;

connectomeMeta = L4.Connectome.connectomeBorderMeta(connectome, ...
    synapses, borderMeta);


%% generate contactome

contactome = L4.Agglo.findEdgesBetweenAgglos2(axons, dendrites, ...
    graph.edges);
contactomeMeta = L4.Connectome.contactomeBorderMeta(contactome, ...
    borderMeta);


%% target class connectome

Util.log('Target class mapping.');
denClassMapping = zeros(length(dendrites), 1);
denClasses = {'soma', 'apical dendrites', 'smooth dendrite', 'other'};
otherId = length(denClasses);
denClassMapping(idxSoma) = 1;
denClassMapping(idxAD) = 2;
denClassMapping(idxSD) = 3;
denClassMapping(denClassMapping == 0) = otherId;

classConnectome = zeros(length(axons), 4);
classConnectome(:,1) = sum(connectomeFull(:,idxSoma), 2);
classConnectome(:,2) = sum(connectomeFull(:, idxAD), 2);
classConnectome(:,3) = sum(connectomeFull(:, idxSD), 2);
classConnectome(:,otherId) = ...
    sum(connectomeFull(:,denClassMapping == otherId), 2);


%% axon statistics
% taken from connectEM.Connectome.augmentForMoritz

Util.log('Adding axon meta information.');
axonMeta = table;
axonMeta.id = reshape(1:numel(axons), [], 1);
axonMeta.synCount = accumarray( ...
    connectome.edges(:, 1), ...
    cellfun(@numel, connectome.synIdx), ...
   [numel(axons), 1], @sum, 0);
axonMeta.spineSynCount = accumarray( ...
    connectome.edges(:, 1), cellfun(@(ids) ...
    sum(mSyn.isSpineSyn(ids)), connectome.synIdx), ...
   [numel(axons), 1], @sum, 0);


%% save results

outFile = fullfile(p.connectome.saveFolder, 'connectome.mat');
if ~exist(outFile, 'file')
    save(outFile, 'connectome', 'contactome', 'connectomeMeta', ...
        'contactomeMeta', 'axons', 'dendrites', 'info', ...
        'classConnectome', 'denClasses', 'denClassMapping', 'axonMeta');
end
