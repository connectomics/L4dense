function connectomeNew = pairwise2Full( connectome, shp, outputFormat )
%PAIRWISE2FULL Convert a pairwise connectome to a full one.
% INPUT connectome: table
%           Table containing which pre- and postsynaptic agglos connect
%           ('edges') and the corresponding synapse indices ('synIdx').
%           (see also L4.connectome.connectomeFromSyn2AggloMapping)
%       shp: (Optional) [1x2] int
%           The shape of the connectome.
%           (Default: max indices in connectome.edges)
%       outputFormat: (Optional) string
%           The kind of output format:
%           'numerical': (default) Number of connections between pre- and
%               postsynaptic processes.
% OUTPUT connectomeNew: [NxM] numerical or cell
%           The full output connectome.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('shp', 'var') || isempty(shp)
    shp = max(connectome.edges, [], 1);
end

if ~exist('outputFormat', 'var') || isempty(outputFormat)
    outputFormat = 'numerical';
end

switch outputFormat
    case 'numerical'
        connectomeNew = zeros(shp);
        idx = sub2ind(shp, connectome.edges(:, 1), connectome.edges(:,2));
        noSyn = cellfun(@length, connectome.synIdx);
        connectomeNew(idx) = noSyn;
    otherwise
        error('Mode %s not implemented.');
end

end

