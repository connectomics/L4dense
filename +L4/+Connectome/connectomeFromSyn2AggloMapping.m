function connectome = connectomeFromSyn2AggloMapping( syn2Agglo )
%CONNECTOMEFROMSYN2AGGLOMAPPING Calculate the connectome from a synapse to
% agglo mapping.
% INPUT syn2pre: [Nx2] table
%           The indices of the presynaptic agglomerates overlapping with
%           the presynaptic ids of the corresponding synapse and
%           the indices of the postsynaptic agglomerates overlapping with
%           the postsynaptic ids of the corresponding synapse.
%           (see also L4.Synapses.synapseAggloMapping)
% OUTPUT connectome: [Nx2] table
%           Table containing which pre- and postsynaptic agglos connect
%           ('edges') and the corresponding synapse indices ('synIdx').
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

% find which synapses actually connect two agglos
connectsAgglos = ~any(cellfun(@isempty, syn2Agglo{:,:}), 2);

% get all combinations of agglos that a synapse connects
prepost = cellfun(@(x, y)combvec(x', y')', ...
    syn2Agglo.syn2pre(connectsAgglos), ...
    syn2Agglo.syn2post(connectsAgglos), 'uni', 0);

% get the index of the synapse that connects the agglos in prepost
synIdx = repelem(find(connectsAgglos), ...
    cellfun(@(x)size(x, 1), prepost));

% sort by pre-post partners
prepost = cell2mat(prepost);

[edges, ~, idx] = unique(prepost, 'rows');
synIdxForEdges = accumarray(idx, synIdx, [], @(x){x});
connectome = table(edges, synIdxForEdges, ...
    'VariableNames', {'edges', 'synIdx'});

end

