function stats = dirichletmom(alpha)
%DIRICHLETMOM Moments of a dirichlet distribution.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

assert(all(alpha > 0));
k = length(alpha);
assert(k > 1);

a_0 = sum(alpha);

stats.mean = alpha / a_0;
stats.mode = (alpha - 1) / (a_0 - k);
stats.var =  (alpha .* (a_0 - alpha)) / (a_0^2 * (a_0 + 1));

end

