function y = dirichletpdf(x, alpha)
%DIRICHLETPDF Probability density function of the dirichlet distribution.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

assert(all(alpha > 0));
k = length(alpha);
assert(k > 1);

b_alpha = prod(arrayfun(@gamma, alpha)) ./ gamma(sum(alpha));
y = x .* (alpha - 1) ./ b_alpha;

end

