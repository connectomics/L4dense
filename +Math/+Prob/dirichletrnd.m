function x = dirichletrnd(alpha, varargin)
%DIRICHLETRND Generate samples from a dirichlet distribution by sampling
% from a gamma distribution and normalizing the result.
%
%   x = dirichletrnd(alpha) generates a random number from the dirichlet
%       distribution with parameters alpha
%
%   x = dirichletrnd(alpha, m, n, ...) and
%   x = dirichletrnd(alpha, [m, n, ...]) generates and [m, n, ...] array of
%       samples with size [m, n, ..., length(alpha)]
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

assert(all(alpha > 0));
k = length(alpha);
assert(k > 1);

if isempty(varargin)
    varargin{1} = 1;
elseif length(varargin) > 1
    varargin = {cell2mat(varargin(:))'};
else
    varargin{1} = varargin{1}(:)';
end
sz = length(varargin{1});

x = cell(k, 1);

for i = 1:k
    x{i} = gamrnd(alpha(i), 1, varargin{:});
end
x = permute(x, [2:(sz+1), 1]);
x = cell2mat(x);
x = x ./ sum(x, sz+1);

end

