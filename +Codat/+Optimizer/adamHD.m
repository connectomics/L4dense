classdef adamHD < Codat.Optimizer.optimizer
    %ADAM Implementation of Adam (Kingma, D., & Ba, J. (2014). Adam: A
    %method for stochastic optimization. arXiv preprint arXiv:1412.6980.)
    % with hypgergradient descent(see arXiv:1703.04782v1).
    % Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

    properties
        t = 1;
        decay_mom1 = 0.9;
        decay_mom2 = 0.999;
        offset = 1e-8;
        lambda = 1 - 1e-8;
        est_mom1_b
        est_mom2_b
        learningRate = 1
        hyp_lr
        lr = 1
    end

    methods
        function ad = adamHD(hyp_lr, init_lr, lambda, decay_mom1, decay_mom2, offset)
            ad = ad@Codat.Optimizer.optimizer;
            ad.hyp_lr = hyp_lr;
            if exist('init_lr', 'var') && ~isempty(init_lr)
                lr = init_lr;
            end
            if exist('lambda','var') && ~isempty(lambda)
                ad.lambda = lambda;
            end
            if exist('decay_mom1','var') && ~isempty(decay_mom1)
                ad.decay_mom1 = decay_mom1;
            end
            if exist('decay_mom2','var') && ~isempty(decay_mom2)
                ad.decay_mom2 = decay_mom2;
            end
            if exist('offset','var') && ~isempty(offset)
                ad.offset = offset;
            end
        end

        function ad = init(ad, numParams)
            ad.est_mom1_b = zeros(numParams,1,'single');
            ad.est_mom2_b = zeros(numParams,1,'single');
            ad.lr = ad.lr.*ones(numParams, 1, 'single');
        end

        function [paramsNew,ad] = optimize(ad,paramsOld,gradient)
            ad.lr = ad.lr + ad.hyp_lr.*gradient.*(ad.est_mom1_b/(sqrt(ad.est_mom2_b) + add.offset));
            ad.est_mom1_b = ad.decay_mom1.*ad.est_mom1_b + (1 - ad.decay_mom1).*gradient;
            ad.est_mom2_b = ad.decay_mom2.*ad.est_mom2_b + (1 - ad.decay_mom2).*gradient.^2;
            ad.est_mom1_b = ad.est_mom1_b./(1 - ad.decay_mom1.^ad.t);
            ad.est_mom1_b = ad.est_mom2_b./(1 - ad.decay_mom2.^ad.t);
            paramsNew = paramsOld - ad.learningRate.*ad.lr.*...
                (ad.est_mom1_b/(sqrt(ad.est_mom2_b) + ad.offset));
            ad.t = ad.t + 1;
        end

        function optimizer = setParamsToActvtClass(optimizer,actvtClass)
            optimizer.est_mom1_b = actvtClass(gather(optimizer.est_mom1_b));
            optimizer.est_mom2_b = actvtClass(gather(optimizer.est_mom2_b));
        end
    end

end
