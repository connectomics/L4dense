function tests = testOptimizer
%Optimize f(x) = x^2 as simple test case.
tests = functiontests(localfunctions);
end

function testFunctionGD(testCase)
    %Test gd
    disp('Testing gradient descent.')
    grad = testCase.TestData.fD;
    gd = Codat.Optimizer.gradientDescent(1,0.9);
    gd = gd.init(1);
    param = 10;
    for i = 1:1000
        currGrad = grad(param);
        [param,gd] = gd.optimize(param,currGrad);
    end
    errorBound = 1e-20;
    verifyEqual(testCase,param < errorBound,true);
end

function testFunctionGD_NAG(testCase)
    %Test gd with nesterov momentum
    disp('Testing gradient descent with nesterov momentum.')
    grad = testCase.TestData.fD;
    gd = Codat.Optimizer.gradientDescent(1, 0.9);
    gd.nesterov_mom = true;
    gd = gd.init(1);
    param = 10;
    for i = 1:1000
        currGrad = grad(param);
        [param,gd] = gd.optimize(param,currGrad);
    end
    errorBound = 1e-20;
    verifyEqual(testCase,param < errorBound,true);
end

function testFunctionRMSProp(testCase)
    %Test rmsProp
    disp('Testing rmsprop.')
    grad = testCase.TestData.fD;
    rmsOpt = Codat.Optimizer.rmsProp(1,0.95,0);
    rmsOpt = rmsOpt.init(1);
    param = 10;
    errorBound = 1e-20;
    for i = 1:1000
        currGrad = grad(param);
        [param,rmsOpt] = rmsOpt.optimize(param,currGrad);
        if abs(param) < errorBound
            break;
        end
    end
    verifyEqual(testCase,param < errorBound,true);
end

function testFunctionAdam(testCase)
    %Test rmsProp
    disp('Test adam.')
    grad = testCase.TestData.fD;
    ad = Codat.Optimizer.adam(0.1);
    ad = ad.init(1);
    param = 10;
    for i = 1:1000
        currGrad = grad(param);
        [param,ad] = ad.optimize(param,currGrad);
    end
    errorBound = 1e-20;
    verifyEqual(testCase,param < errorBound,true);
end

function setupOnce(testCase)
% optimizing f(x) = 1/2 x^2 - maybe too simple?
testCase.TestData.fD = @(x)x;
end
