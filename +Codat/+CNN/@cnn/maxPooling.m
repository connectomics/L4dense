function [ M, ind ] = maxPooling( cnet, X, lyr )
%MAXPOOLING Perform max pooling given d-regularity and stride.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

d = cnet.d(lyr - 1,1:3).*cnet.stride{lyr};
mp = cnet.maxPool(lyr,:);

if all(mp == [2, 2, 2])
	[M, ind] = max([subsref(X(1:end-d(1),1:end-d(2),1:end-d(3),:),struct('type','()','subs',{{':'}})), ...
                    subsref(X(1 + d(1):end,1:end-d(2),1:end-d(3),:),struct('type','()','subs',{{':'}})), ...
                    subsref(X(1:end-d(1),1 + d(2):end,1:end-d(3),:),struct('type','()','subs',{{':'}})), ...
                    subsref(X(1 + d(1):end,1 + d(2):end,1:end-d(3),:),struct('type','()','subs',{{':'}})), ...
                    subsref(X(1:end-d(1),1:end-d(2),1 + d(3):end,:),struct('type','()','subs',{{':'}})), ...
                    subsref(X(1 + d(1):end,1:end-d(2),1 + d(3):end,:),struct('type','()','subs',{{':'}})), ...
                    subsref(X(1:end-d(1),1 + d(2):end,1 + d(3):end,:),struct('type','()','subs',{{':'}})), ...
                    subsref(X(1 + d(1):end,1 + d(2):end,1 + d(3):end,:),struct('type','()','subs',{{':'}}))],[],2);
elseif all(mp == [2, 2, 1])
	[M, ind] = max([subsref(X(1:end-d(1),1:end-d(2),:,:),struct('type','()','subs',{{':'}})), ...
                    subsref(X(1 + d(1):end,1:end-d(2),:,:),struct('type','()','subs',{{':'}})), ...
                    subsref(X(1:end-d(1),1 + d(2):end,:,:),struct('type','()','subs',{{':'}})), ...
                    subsref(X(1 + d(1):end,1 + d(2):end,:,:),struct('type','()','subs',{{':'}}))],[],2);
else
    error('Max-pooling size of %s is currently not supported.', ...
        mat2str(mp));
end
sX = size(X);
sX(end + 1:4) = 1;
ts = targetSize(sX, mp, d);
M = reshape(M,ts);
ind = reshape(ind,ts);
end

function ts = targetSize(sX, mp, d)
ts = [sX(1:3) - d.*(mp - 1), sX(4)];
end