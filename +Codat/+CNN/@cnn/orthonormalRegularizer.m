function [ l_reg, dldW ] = orthonormalRegularizer( W, lambda )
%ORTHONORMALREGULARIZER Orthonogmal weight regularization.
% The regularizer for a single layer is given by
% \labmda/2 \| W^t W - 1\|^2_F,
% where \|.\|_F is the Frobenius norm and W is a matrix containing as many
% columns as the number of output feature maps of the corresponding layer
% and as many rows as all remaining parameters, i.e. the i-th row of W has
% consists of all parameters of W(:,:,:,:,i).
% The derivative of the loss function is given by
% dldW = 4 W W^T W - 4 W
% INPUT W: [Nx1] cell
%           The cnet weight matrix.
%       lambda: float
%           Loss weighting factor.
% OUTPUT l_reg: float
%           Total loss.
%        dldW: [Nx1] cell
%           Matrix of derivatives for the corresponding entry in the
%           input matrix.
% Author Benedikt Staffler <benedikt.staffler@brain.mpg.de>

siz = cellfun(@(x)Codat.CNN.cnn.mSize(x, 1:5), W(2:end), ...
    'UniformOutput', false);
W(2:end) = cellfun(@(x)reshape(x, [], size(x, 5)), W(2:end), ...
    'UniformOutput', false);
l_reg = sum(cellfun(@(x)frobNorm(x, lambda), W(2:end)));

if nargout > 1
    dldW = W;
    dldW(2:end) = cellfun(@(x)frobNormD(x, lambda), W(2:end), ...
        'UniformOutput', false);
    dldW(2:end) = cellfun(@(x, s)reshape(x, s), dldW(2:end), siz, ...
        'UniformOutput', false);
end
end

function n = frobNorm(W, lambda)
d = size(W, 2);
n = lambda/2*norm(transpose(W)*W - eye(d, d), 'fro').^2;
end

function dndW = frobNormD(W, lambda)
dndW = lambda.*2.*(W*transpose(W)*W - W);
end

