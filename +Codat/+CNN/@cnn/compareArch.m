function [ isEqual, diffProps ] = compareArch( cnet, otherCnet )
%COMPAREARCH Compares the architectures of two `cnn` objects.
% INPUT cnet: A cnn object
%       otherCnet: Another cnn object
% OUTPUT isEqual: `true` if and only if the architectures of `cnet` and
%           `otherCnet` are equal. `false` otherwise.
%        diffProps: A cell array with the name of all fields in which
%           `cnet` and `otherCnet` differ.
%
% Author: Alessandro Motta <alessandro.motta@brain.mpg.de>

% The following properties are modified during trainig. As such, they do
% not represent the architecture and will be ignored in this comparison.
propsIgnore = {'W', 'b', 'bn_muInf', 'bn_sig2Inf' 'isTraining'};

% The optimizer is compared manually below
propsIgnore(end + 1) = {'optimizer'};

% Compare properties
props = setdiff(properties(cnet), propsIgnore);
propsEqual = cellfun(@(n) isequal(cnet.(n), otherCnet.(n)), props);

% Compare optimizers
props(end + 1) = {'optimizer'};
propsEqual(end + 1) = isa(otherCnet.optimizer, class(cnet.optimizer));

% Build output
isEqual = all(propsEqual);
diffProps = props(~propsEqual);

end
