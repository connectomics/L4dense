function DEDX = maxPoolingBwd( cnet, DEDY, ind, lyr )
%MAXPOOLINGBWD Backpropagation through 2x2x2 max pooling layer.
% This function readily allows for backpropagation through arbitrary
% pooling windows.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

d = cnet.d(lyr - 1,:).*cnet.stride{lyr};
mpSize = cnet.maxPool(lyr,:);
%size difference between input and output
sD = d.*(mpSize - 1);
sDEDY = size(DEDY);
sDEDY(end+1:4) = 1;
DEDX = zeros([sDEDY(1:3) + sD,sDEDY(4)],'like',DEDY);

%get global indices in DEDX for values in DEDY
indConv = indConversion(d, mpSize, size(DEDX));
zeroMat = reshape(1:numel(DEDX),size(DEDX));
gl_ind = indConv(ind) + zeroMat(1:end-sD(1),1:end-sD(2),1:end-sD(3),:);

%determine which values need to be propagated back to the
%same index in DEDX
[u_gl_ind,~,ic] = unique(gl_ind);

%sum values in DEDY which came from same position in DEDX
DEDY_summed = accumarray(ic,DEDY(:));

%assign summed DEDY to corresponding positions in DEDX
DEDX(u_gl_ind) = DEDY_summed;
end

function indConv = indConversion(d, mpSize, szY)
    indConv = zeros(mpSize);
    v = permute(0:mpSize(1) - 1,[2, 1]);
    indConv = bsxfun(@plus,indConv,v.*d(1));
    v = 0:mpSize(2) - 1;
    indConv = bsxfun(@plus,indConv,v.*d(2)*szY(1));
    v = permute(0:mpSize(3) - 1, [1 3 2]);
    indConv = bsxfun(@plus,indConv,v.*d(3)*szY(1)*szY(2));
    indConv = indConv(:);
end

