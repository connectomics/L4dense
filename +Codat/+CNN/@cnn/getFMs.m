function fms = getFMs( cnet, input, fmL, sizOut, fmL_idx )
%GETFMS Similar to predict but can also output intermediate FMs.
% INPUT input: 3d or 4d single
%           Input to cnet.
%       fmL: [Nx1] int
%           Layers for which the featureMaps will be concatenated and
%           returned.
%       sizOut: (Optional) [3x1] int
%           Size of the output fms.
%           (Default: all fms are cropped to the smallest requested fm
%           size).
%       fmL_idx: (Optional) [Nx1] cell
%           The indices of the feature maps for the corresponding layer
%           in fmL that are kept.
%           (Default: all feature maps of a layer)
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

fmL = sort(fmL, 'ascend');

if ~exist('fmL_idx', 'var')
    fmL_idx = [];
end

%initialize cells
input = cnet.actvtClass(input);
activity = input;
activityShortcut = cell(cnet.layer,1);
if any(cnet.shortcut == 1)
    activityShortcut{1} = input;
end

fms = cell(length(fmL), 1);

for lyr = 2:max(fmL)

    %conv layer
    activity = cnet.convLayerFwd(activity, lyr);

    %mp layer
    if any(cnet.maxPool(lyr, :) > 1)
        activity = cnet.maxPooling(activity,lyr);
    end

    %shortcut in
    if cnet.shortcut(lyr) > 0
        activity = activity + ...
            cnet.cropActivation(activityShortcut{cnet.shortcut(lyr)}, ...
            size(activity));
    end

    %batch normalization
    if cnet.batchNorm(lyr)
        activity = Codat.NN.batchNormalization(activity, ...
            cnet.bn_beta{lyr}, cnet.bn_gamma{lyr}, cnet.bn_muInf{lyr}, ...
            cnet.bn_sig2Inf{lyr}, false);
    end

    %non_linearity
    activity = cnet.nonLinearity{lyr}(activity);

    %save activity shortcut
    if any(cnet.shortcut == lyr)
        activityShortcut{lyr} = activity;
    end

    tmp = fmL == lyr;
    if any(tmp)
        fms{tmp} = activity;
        if ~isempty(fmL_idx)
            fms{tmp} = fms{tmp}(:,:,:,fmL_idx{tmp});
        end
    end
end

if ~exist('sizOut', 'var') || isempty(sizOut)
    sizOut = cnet.mSize(fms{end}, 1:3);
end

%crop fms to common size
for i = 1:length(fms)
    fms{i} = cnet.cropActivation(fms{i}, [sizOut, size(fms{i}, 4)]);
end

fms = cat(4, fms{:});

end
