tic;
result = run(Codat.CNN.Tests.testCNNGradients);
rt = table(result);
disp(rt);
if all([rt.Passed])
    Util.log('Total testing time %.2f secons. All tests passed.', toc);
else
    warning('Some tests failed or are incomplete. Check rt for details.');
end
