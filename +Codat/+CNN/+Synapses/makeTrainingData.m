function makeTrainingData( dataOverview, targetDir, targetSize, border, ...
    ilastikExport )
%MAKETRAININGDATA Create training data and output stack files.
% INPUT dataOverview: table
%           Synapse training data parameter struct.
%           (see output of generateDataOverview)
%       targetDir: (Optional) string
%           Directory for output files.
%       	(Default: pwd)
%       targetSize: (Optional) [1x3] int
%           Size of target cubes.
%           (Default: [100 100 60])
%       border: (Optional) [1x3] int
%           Size of border around target cubes in raw.
%           (Default: [100, 100, 40])
%       ilastikExport: (Optional) logical
%           Flag to save data in hdf5 format for ilastik.
%           (Default: false)
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('targetSize','var') || isempty(targetSize)
    targetSize = [100, 100, 60];
end

if ~exist('border','var') || isempty(border)
    border = [100, 100, 40];
end

if ~exist('targetDir','var') || isempty(targetDir)
    targetDir = pwd;
end
targetDir = Util.addFilesep(targetDir);

if ~exist('ilastikExport','var') || isempty(ilastikExport)
    ilastikExport = false;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%cube writing function
    function stacks = writeToCubes(files, targetDir, bbox)
        count = 1;
        for i = 1:length(files)
            m = matfile(files{i});
            data = m.data;
            target = m.voxelLabels;
            target = target(101:400,101:400,41:160);
            thisBbox = bbox{i}(:,1);
            thisBbox = bsxfun(@plus, thisBbox, ...
                [[100; 100; 40], [399; 399; 159]]);
            [x_train,y_train, ~, tiles] = Codat.CNN.Misc.tileTrainingCubes( ...
                data.raw(1:500,1:500,1:200), target, targetSize, border);
            tiles = tiles - 1;
            for j = 1:length(y_train)
                raw = x_train{j};
                target = y_train{j};
                tileBbox = bsxfun(@plus, thisBbox(:,1), ...
                    [tiles(j,:)', [tiles(j,:) + targetSize - 1]']);
                if sum(target(:) == 1) > 100
                    if ilastikExport
                        labels = zeros(size(target),'uint8');
                        labels(target) = 2;
                        labels(~imdilate(target, ...
                            Util.ballMask(6,[1 1 2.5]))) = 1;
                        idx = labels(:) == 1;
                        idx(idx) = rand(sum(idx),1) > 0.1;
                        labels(idx) = 0;
                        labels = padarray(labels, border./2,0);
                        Codat.Ilastik.convertToH5(raw, labels, ...
                            [targetDir, 'Cube_', num2str(count)]);
                    else
                        save([targetDir num2str(count), '.mat'], ...
                            'raw','target');
                        stacks(count).stackFile = [targetDir ...
                            num2str(count), '.mat']; %#ok<AGROW>
                        stacks(count).bbox = tileBbox; %#ok<AGROW>
                        stacks(count).synapticVoxels = ...
                            sum(target(:) == 1); %#ok<AGROW>
                    end
                    count = count + 1;
                end
            end
        end
        fprintf('[%s] Saved %d files to %s.\n', datestr(now), ...
            count - 1, targetDir);
    end
%end cube writing function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('[%s] Saving output files to %s.\n', datestr(now), targetDir);
%training cubes
files = dataOverview.file(dataOverview.noVoxelLabels > 0 & ...
    dataOverview.usage == 'training');
bbox = dataOverview.bboxBig(dataOverview.noVoxelLabels > 0 & ...
    dataOverview.usage == 'training');
trDir = [targetDir, 'train', filesep];
if ~exist('trDir','dir')
    mkdir(trDir);
end
stacksT = writeToCubes(files, trDir, bbox); %#ok<NASGU>

%validation cubes
files = dataOverview.file(dataOverview.noVoxelLabels > 0 & ...
    (dataOverview.usage == 'unused' | dataOverview.usage == 'validation'));
bbox = dataOverview.bboxBig(dataOverview.noVoxelLabels > 0 & ...
    (dataOverview.usage == 'unused' | dataOverview.usage == 'validation'));
valDir = [targetDir, 'val', filesep];
if ~exist('trDir','dir')
    mkdir(valDir);
end
stacksV = writeToCubes(files, valDir, bbox); %#ok<NASGU>

if ~ilastikExport
    save([targetDir 'psdTrainingData.mat'], ...
        'stacksT', 'stacksV');
end

end

