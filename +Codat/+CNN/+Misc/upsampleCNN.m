function cnet_up = upsampleCNN( cnet, cnet_up )
%UPSAMPLECNN Upsample the weights of a cnn to the shape of the weights of
% another cnn.
% INPUT cnet: Codat.CNN.cnn object
%           The original CNN which weights will be upsampled.
%       cnet_up: Codat.CNN.cnn object
%           The cnn that stores the upsampled weights. This CNN should have
%           exactly the same number of feature maps but can have weight
%           kernels that are larger in the spatial dimensions.
% OUTPUT cnet_up: Codat.CNN.cnn object
%           The upsampled CNN with the interpolated weights. The biases are
%           simply copied from cnet.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

cnet_up.b = cnet.b;

for i = 2:cnet.layer
    cnet_up.W{i} = Codat.CNN.Misc.upsampleWeights(cnet.W{i}, ...
        cnet_up.mSize(cnet_up.W{i}, 1:3));
end

end
