function cnet = setUpCNN( arch, opt, noOutputChannels, lr, hiddenNL, ...
    outputNL, loss)
%SETUPCNN Return some predefined CNN architectures.
% INPUT arch: string
%           Architecture name (see below).
%       opt: (Optional) string
%           Optimizer name (Standard sgd).
%       lr: (Optional) [Nx1] double
%           Vecor of learning rates. If the vector has more than one
%           element than a cell array of cnets with the specified learning
%           rates is returned.
%           (Default: see below)
%       noOutputChannels: (Optional) int
%           Number of output channels
%       	(Options. Default is 1).
%       hiddenNL: (Optional) string or [Nx1] cell of strings
%           Hidden non-linearities for all or separely for each layer.
%           Options are 'tanh', 'sigmoid', 'relu', 'elu' and 'linear'.
%           (Default: 'tanh')
%       outputNL: (Optional) string
%           Output non-linearity. Options are 'tanh', 'sigmoid', 'relu',
%           'elu', 'linear' and 'softmax'.
%           (Default: 'tanh')
%       loss: (Optional) string
%           Loss-function. Options are 'squared','cross-entropy',
%           'softmax'.
%           (Default: 'squared')
% OUTPUT cnet: Codat.CNN.cnn object or [Nx1] cell of Codat.CNN.cnn object
%           The resulting cnet objects.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('opt','var') || isempty(opt)
    opt = 'sgd';
    disp('Setting optimizer to sgd.');
end
if ~exist('noOutputChannels','var')
    noOutputChannels = [];
end

switch arch
    case 'test'
        cLayer = 3;
        featureMaps = [1 10 10 1];
        filterSize = {[3 3 3],[3 3 3],[1 1 1]};
        dropout = [0.1 0.25 0.25 0];
        stride = {[2 2 2],[1 1 1],[1 1 1]};
        maxPool = [false, false, false];
        batchNorm = false;

    case 'shallow'
        cLayer = 3;
        featureMaps = [1 16 16 1];
        filterSize = {[8 8 4],[7 7 3],[7 7 3]};
        dropout = 0.2;
        stride = {[1 1 1]};
        maxPool = [true,true,false];
        batchNorm = false;

    case '2d'
        cLayer = 9;
        featureMaps = [1 64 64 64 64 64 64 128 128 1];
        filterSize = {[5 5 1],[5 5 1],[5 5 1],[5 5 1],[5 5 1], ...
                      [5 5 1],[3 3 1],[3 3 1],[3 3 1]};
        dropout = 0.2;
        stride = {[1 1 1],[1 1 1],[2 2 2],[1 1 1],[1 1 1], ...
                  [2 2 2],[1 1 1],[1 1 1],[1 1 1]};
        maxPool = false;
        batchNorm = false;

    case 'cnn1'
        cLayer = 9;
        featureMaps = [1 16 16 16 16 16 16 32 32 1];
        filterSize = {[6 6 4],[5 5 2],[5 5 2],[5 5 2],[5 5 2], ...
                      [5 5 2],[3 3 2],[3 3 2],[3 3 2]};
        dropout = 0.2;
        stride = {[1 1 1],[1 1 1],[1 1 1],[1 1 1],[1 1 1], ...
                  [1 1 1],[1 1 1],[1 1 1],[1 1 1]};
        maxPool = [false,false,true,false,false,true,false,false,false];
        batchNorm = false;

    case 'cnn1_large'
        cLayer = 9;
        featureMaps = [1 16 16 32 32 32 48 48 48 1];
        filterSize = {[6 6 4],[5 5 2],[5 5 2],[5 5 2],[5 5 2], ...
                      [5 5 2],[3 3 2],[3 3 2],[3 3 2]};
        dropout = 0.2;
        stride = {[1 1 1]};
        maxPool = logical([0,0,1,0,0,1,0,0,0]);
        batchNorm = false;

    case 'cnn1_large_enn'
        cLayer = 10;
        featureMaps = [1 16 16 16 32 32 32 48 48 48 2];
        filterSize = {[6 6 4],[5 5 2],[5 5 2],[5 5 2],[5 5 2], ...
                      [5 5 2],[3 3 2],[3 3 2],[3 3 2],[1 1 1]};
        dropout = 0.2;
        stride = {[1 1 1]};
        maxPool = logical([0,0,1,0,0,1,0,0,0,0]);
        batchNorm = false;

    case 'cnn2'
        cLayer = 9;
        featureMaps = [1 16 16 24 24 24 32 32 32 1];
        filterSize = {[7 7 3],[5 5 3],[4 4 2],[5 5 3],[5 5 3], ...
                      [5 5 2],[5 5 2],[3 3 2],[3 3 2]};
        dropout = 0.25;
        stride = {[1 1 1]};
        shortcut = [0 0 0 0 0 0 0 0 0 0];
        batchNorm = false;
        maxPool = logical([0, 0, 0, 1, 0, 0, 0, 0, 0]);

    case 'cnn2_skip'
        cLayer = 9;
        featureMaps = [1 16 16 24 24 24 32 32 32 1];
        filterSize = {[7 7 3],[5 5 3],[4 4 2],[5 5 3],[5 5 3], ...
                      [5 5 2],[5 5 2],[3 3 2],[3 3 2]};
        dropout = 0.25;
        stride = {[1 1 1]};
        shortcut = [0 0 0 0 0 0 4 0 2 0];
        batchNorm = false;
        maxPool = logical([0, 0, 0, 1, 0, 0, 0, 0, 0]);

    case 'cnn2_large'
        cLayer = 9;
        featureMaps = [1 24 24 24 32 32 32 64 64 1];
        filterSize = {[7 7 3],[5 5 3],[4 4 2],[5 5 3],[5 5 3], ...
                      [5 5 2],[5 5 2],[3 3 2],[3 3 2]};
        dropout = 0.25;
        stride = {[1 1 1]};
        shortcut = [0 0 0 0 0 0 0 0 0 0];
        batchNorm = false;
        maxPool = logical([0, 0, 0, 1, 0, 0, 0, 0, 0]);

    case 'cnn2_large_benenet'
        % NOTE(amotta): This is the architecture dubbed as "BeneNet. It is
        % a modified version of `cnn2_large` with fewer feature maps and
        % lower dropout.
        % NOTE(amotta): Contrary to the "BeneNet" trained by Benedikt, this
        % architecture has dropout in all but the last layer. Also, the
        % hyper-parameters of the optimizer are slightly different.
        cLayer = 9;
        featureMaps = [1 24 24 24 24 32 32 32 32 1];
        filterSize = {[7 7 3],[5 5 3],[4 4 2],[5 5 3],[5 5 3], ...
                      [5 5 2],[5 5 2],[3 3 2],[3 3 2]};
        dropout = 0.2;
        stride = {[1 1 1]};
        shortcut = [0 0 0 0 0 0 0 0 0 0];
        batchNorm = false;
        maxPool = logical([0, 0, 0, 1, 0, 0, 0, 0, 0]);

    case 'cnn2_large_up'
        cLayer = 9;
        featureMaps = [1 24 24 24 24 32 32 32 32 1];
        filterSize = {[13 13 5],[9 9 5],[8 8 4],[9 9 5],[9 9 5], ...
                      [9 9 3],[9 9 3],[5 5 3],[5 5 3]};
        dropout = 0.2;
        stride = {[1 1 1]};
        shortcut = [0 0 0 0 0 0 0 0 0 0];
        batchNorm = false;
        maxPool = logical([0, 0, 0, 1, 0, 0, 0, 0, 0]);

    case 'cnn2_huge'
        cLayer = 9;
        featureMaps = [1 32 32 32 48 48 48 64 64 1];
        filterSize = {[7 7 3],[5 5 3],[4 4 2],[5 5 3],[5 5 3], ...
                      [5 5 2],[5 5 2],[3 3 2],[3 3 2]};
        dropout = 0.25;
        stride = {[1 1 1]};
        shortcut = [0 0 0 0 0 0 0 0 0 0];
        batchNorm = false;
        maxPool = logical([0, 0, 0, 1, 0, 0, 0, 0, 0]);

    case 'cnn3'
        cLayer = 14;
        featureMaps = [1 16 16 16 24 24 24 24 32 32 32 32 32 32 1];
        filterSize = {[7 7 3],[5 5 3],[5 5 3],[4 4 2],[5 5 2], ...
                      [3 3 2],[3 3 2],[3 3 2],[3 3 1],[3 3 2], ...
                      [3 3 2],[3 3 2],[3 3 2],[3 3 1]};
        dropout = 0.25;
        stride = {[1 1 1]};
        batchNorm = false;
        maxPool = false(1,15);
        maxPool(10) = true;
        shortcut = [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];

    case 'cnn4'
        cLayer = 23;
        featureMaps = [1 16 16 16 16 16 16 16 24 24 24 24 24 24 24 32 32 32 32 32 32 32 32 1];
        filterSize = {[7 7 2],[3 3 2],[3 3 1],[3 3 2],[3 3 2], ...
                      [3 3 2],[3 3 2],[3 3 2],[3 3 1],[3 3 2], ...
                      [3 3 2],[3 3 2],[3 3 2],[3 3 2],[3 3 2], ...
                      [3 3 2],[3 3 2],[3 3 2],[3 3 2],[3 3 2], ...
                      [3 3 2],[3 3 2],[3 3 1]};
%         shortcut = [0 0 0 2 0 4 0 6 0 8 0 10 0 12 0 14 0 16 0 18 0 20 0 0];
        shortcut = 0;
        batchNorm = false;
        dropout = 0.25;
        stride = {[1 1 1]};
        maxPool = false;

    case 'cnn4res'
        cLayer = 23;
        featureMaps = [ 1 16 16 16 16 16 16 16 24 24 ...
                       24 24 24 24 24 32 32 32 32 32 ...
                       32 32 32 1];
        filterSize = {[7 7 2],[3 3 2],[3 3 1],[3 3 2],[3 3 2], ...
                      [3 3 2],[3 3 2],[3 3 2],[3 3 1],[3 3 2], ...
                      [3 3 2],[3 3 2],[3 3 2],[3 3 2],[3 3 2], ...
                      [3 3 2],[3 3 2],[3 3 2],[3 3 2],[3 3 2], ...
                      [3 3 2],[3 3 2],[3 3 1]};
        shortcut = [0 0 1 0 3 0 5 0 7 0 9 0 11 0 13 0 15 0 17 0 19 0 21 0];
        batchNorm = false;
        dropout = 0.25;
        stride = {[1 1 1]};
        maxPool = false;

    case 'cnn5'
        cLayer = 5;
        featureMaps = [1 16 16 16 16 1];
        filterSize = {[8 8 5],[7 7 4],[7 7 3],[7 7 3],[7 7 3]};
        dropout = 0;
        stride = {[1 1 1]};
        maxPool = [false, true, false, false, false];
        batchNorm = false;

    case 'cnn5l'
        cLayer = 5;
        featureMaps = [1 16 16 32 32 1];
        filterSize = {[8 8 5],[7 7 4],[7 7 3],[7 7 3],[7 7 3]};
        dropout = 0;
        stride = {[1 1 1]};
        maxPool = [false, true, false, false, false];
        batchNorm = false;

    case 'cnn6'
        cLayer = 9;
        featureMaps = [1 16 16 16 16 32 32 32 32 1];
        filterSize = {[9 9 5],[7 7 3],[7 7 3],[7 7 3],[7 7 3], ...
                      [7 7 3],[5 5 3],[5 5 3],[5 5 3]};
        dropout = 0.25;
        stride = {[1 1 1]};
        maxPool = false;
        batchNorm = false;

    case 'cnn7'
        cLayer = 10;
        featureMaps = [1 16 16 16 16 16 16 32 32 32 1];
        filterSize = {[9 9 3],[7 7 3],[7 7 3],[7 7 3],[5 5 3], ...
                      [5 5 3],[5 5 3],[5 5 3],[5 5 3],[5 5 3]};
        dropout = 0.2;
        stride = {[1 1 1]};
        maxPool = false;
        batchNorm = false;

    case 'cnn8'
        cLayer = 30;
        featureMaps = [1 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 ...
                         32 32 32 32 32 32 32 32 32 32 32 32 32 32 1];
        filterSize = {[3 3 2], [3 3 2], [3 3 2], [3 3 2], [3 3 1], ...
                      [3 3 2], [3 3 2], [3 3 2], [3 3 2], [3 3 1], ...
                      [3 3 2], [3 3 2], [3 3 2], [3 3 2], [3 3 1], ...
                      [3 3 2], [3 3 2], [3 3 2], [3 3 2], [3 3 1], ...
                      [3 3 2], [3 3 2], [3 3 2], [3 3 2], [3 3 1], ...
                      [3 3 2], [3 3 2], [3 3 2], [3 3 2], [3 3 1]};
        dropout = 0.2;
        maxPool = false;
        batchNorm = false;
        stride = {[1, 1, 1]};

    case 'cnn9'
        cLayer = 4;
        featureMaps = [1 16 32 64 1];
        filterSize = {[12 12 6], [9 9 3], [7 7 3], [6 6 4]};
        dropout = 0.2;
        maxPool = logical([1 0 0 0]);
        stride = {[1 1 1]};

    case 'cnn10'
        cLayer = 6;
        featureMaps = [1 16 32 32 32 64 1];
        filterSize = {[11 11 5],[8 8 2],[5 5 3],[5 5 3],[5 5 3],[5 5 2]};
        dropout = 0.2;
        maxPool = logical([0 1 0 0 0 0]);
        stride = {[1 1 1]};

    case 'cnn11'
        cLayer = 12;
        featureMaps = [1 16 16 16 16 16 32 32 32 32 64 64 1];
        filterSize = {[7 7 3],[7 7 3],[5 5 3],[5 5 3],[5 5 3], ...
                      [5 5 3],[5 5 3],[5 5 3],[5 5 2],[5 5 2], ...
                      [5 5 2],[3 3 2]};
        dropout = 0.2;
        maxPool = false;
        stride = {[1 1 1]};

    case 'cnn12'
        cLayer = 3;
        featureMaps = [1 32 32 1];
        filterSize = {[12 12 6], [11 11 5], [10 10 4]};
        dropout = 0.2;
        maxPool = logical([1 0 0]);
        stride = {[1 1 1]};

    case 'cnn13'
        cLayer = 9;
        featureMaps = [1 24 24 24 32 32 32 64 64 1];
        filterSize = {[7 7 3],[5 5 3],[4 4 2],[5 5 2],[5 5 2], ...
                      [5 5 2],[5 5 2],[3 3 2],[3 3 2]};
        dropout = 0.25;
        stride = {[1 1 1]};
        shortcut = [0 0 0 0 0 0 0 0 0 0];
        batchNorm = false;
        maxPool = logical([0, 0, 1, 0, 0, 0, 1, 0, 0]);

    case 'cnn14'
        cLayer = 4;
        featureMaps = [1 16 24 32 1];
        filterSize = {[8 8 4], [7 7 3], [5 5 2], [5 5 2]};
        dropout = 0.25;
        stride = {[1 1 1]};
        batchNorm = false;
        shortcut = 0;
        maxPool = logical([1 0 1 0]);

    case 'cnn15'
        cLayer = 6;
        featureMaps = [1 16 16 32 32 64 1];
        filterSize = {[10 10 6], [9 9 3], [7 7 3], [5 5 3], [5 5 2], ...
                      [3 3 2]};
        dropout = 0.25;
        stride = {[1, 1, 1]};
        batchNorm = false;
        shortcut = 0;
        maxPool = logical([0 1 0 1 0 0]);

    case 'cnn16'
        cLayer = 7 ;
        featureMaps = [1 12 24 36 36 48 48 4];
        filterSize = {[6 6 2], [5 5 3], [3 3 3], [3 3 3], [3 3 3], ...
                      [3 3 2], [3 3 2]};
        dropout = 0;
        stride = {[1, 1, 1]};
        batchNorm = false;
        shortcut = 0;
        maxPool = [2 2 1; 2 2 2; 2 2 2; 1 1 1; 1 1 1; ...
                   1 1 1; 1 1 1];

    case 'cnn17'
        cLayer = 8;
        featureMaps = [1 12 12 24 24 48 48 48 4];
        filterSize = {[8 8 4], [5 5 3], [5 5 3], [3 3 3], [3 3 3], ...
                      [3 3 3], [3 3 2], [3 3 2]};
        dropout = 0*0.25;
        stride = {[1, 1, 1]};
        batchNorm = false;
        shortcut = 0;
        maxPool = [1 1 1; 2 2 1; 1 1 1; 2 2 2; 1 1 1; ...
                   2 2 2; 1 1 1; 1 1 1];
               
               
    case 'cnn17l'
        cLayer = 8;
        featureMaps = [1 16 16 32 32 64 64 64 1];
        filterSize = {[8 8 4], [5 5 3], [5 5 3], [3 3 3], [3 3 3], ...
                      [3 3 3], [3 3 2], [3 3 2]};
        dropout = 0*0.25;
        stride = {[1, 1, 1]};
        batchNorm = false;
        shortcut = 0;
        maxPool = [1 1 1; 2 2 1; 1 1 1; 2 2 2; 1 1 1; ...
                   2 2 2; 1 1 1; 1 1 1];

    case 'cnn17_skip'
        cLayer = 9;
        featureMaps = [1 12 12 24 24 48 48 48 48 4];
        filterSize = {[8 8 4], [5 5 3], [5 5 3], [3 3 3], [3 3 3], ...
                      [3 3 3], [3 3 2], [3 3 2], [1 1 1]};
        dropout = 0*0.25;
        stride = {[1, 1, 1]};
        batchNorm = false;
        shortcut = [0 0 1 0 1 0 1 0 0 0];
        maxPool = [1 1 1; 2 2 1; 1 1 1; 2 2 2; 1 1 1; ...
                   2 2 2; 1 1 1; 1 1 1; 1 1 1];

   case 'cnn17_enn'
        cLayer = 9;
        featureMaps = [1 12 12 24 24 48 48 48 48 4];
        filterSize = {[8 8 4], [5 5 3], [5 5 3], [3 3 3], [3 3 3], ...
                      [3 3 3], [3 3 2], [3 3 2], [1 1 1]};
        dropout = 0*0.25;
        stride = {[1, 1, 1]};
        batchNorm = false;
        shortcut = 0;
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        maxPool = [1 1 1; 2 2 1; 1 1 1; 2 2 2; 1 1 1; ...
                   2 2 2; 1 1 1; 1 1 1; 1 1 1];

   case 'cnn17_large_enn'
        cLayer = 9;
        featureMaps = [1 16 16 32 32 48 48 64 64 4];
        filterSize = {[8 8 4], [5 5 3], [5 5 3], [3 3 3], [3 3 3], ...
                      [3 3 3], [3 3 2], [3 3 2], [1 1 1]};
        dropout = 0*0.25;
        stride = {[1, 1, 1]};
        batchNorm = false;
        shortcut = 0;
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        maxPool = [1 1 1; 2 2 1; 1 1 1; 2 2 2; 1 1 1; ...
                   2 2 2; 1 1 1; 1 1 1; 1 1 1];

   case 'cnn17r_enn'
        cLayer = 9;
        featureMaps = [4 12 12 24 24 48 48 48 48 4];
        filterSize = {[8 8 4], [5 5 3], [5 5 3], [3 3 3], [3 3 3], ...
                      [3 3 3], [3 3 2], [3 3 2], [1 1 1]};
        dropout = 0*0.25;
        stride = {[1, 1, 1]};
        batchNorm = false;
        shortcut = 0;
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        maxPool = [1 1 1; 2 2 1; 1 1 1; 2 2 2; 1 1 1; ...
                   2 2 2; 1 1 1; 1 1 1; 1 1 1];
    case 'cnn18'
        cLayer = 9;
        featureMaps = [1 12 12 24 24 36 36 48 48 1];
        filterSize = {[6 6 3], [5 5 2], [5 5 2], [5 5 2], [3 3 3], ...
                      [3 3 2], [3 3 2], [3 3 2], [3 3 2]};
        dropout = 0.25;
        stride = {[1, 1, 1]};
        batchNorm = false;
        shortcut = 0;
        maxPool = [1 1 1; 2 2 1; 1 1 1; 2 2 2; 1 1 1; ...
                   2 2 2; 1 1 1; 2 2 2; 1 1 1];
%         maxPool = logical([0 1 0 1 0 1 0 1 0]);

    case 'cnn19'
        % architecture for upsampled data
        cLayer = 9;
        featureMaps = [1 12 24 24 36 36 48 48 48 1];
        filterSize = {[11 11 5], [8 8 4], [6 6 3], [6 6 3], [5 5 2], ...
                      [5 5 3], [5 5 2], [3 3 2], [1 1 1]};
        dropout = 0.2;
        stride = {[1, 1, 1]};
        batchNorm = false;
        shortcut = [0 0 1 0 1 0 1 0 0 0];
        maxPool = [1 1 1; 2 2 2; 1 1 1; 2 2 2; 1 1 1; ...
                   2 2 2; 1 1 1; 1 1 1; 1 1 1];

    case 'ChallengerDeep'
        cLayer = 14;
        featureMaps = [1 16 16 16 16 16 16 16 16 16 32 32 32 32 1];
        filterSize = {[5 5 3],[5 5 3],[5 5 3],[5 5 3],[5 5 3], ...
                      [5 5 3],[5 5 3],[5 5 3],[5 5 2],[5 5 2], ...
                      [5 5 2],[5 5 2],[5 5 2],[5 5 2]};
        dropout = 0.2;
        stride = {[1,1,1]};
        maxPool = false;
        batchNorm = false;
        
    case 'ChallengerDeep_ce'
        cLayer = 14;
        featureMaps = [1 16 16 16 16 16 16 16 16 16 32 32 32 32 2];
        filterSize = {[5 5 3],[5 5 3],[5 5 3],[5 5 3],[5 5 3], ...
                      [5 5 3],[5 5 3],[5 5 3],[5 5 2],[5 5 2], ...
                      [5 5 2],[5 5 2],[5 5 2],[5 5 2]};
        nonLinearity = repmat({'tanh'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        loss = 'softmax';
        dropout = 0.2;
        stride = {[1,1,1]};
        maxPool = false;
        batchNorm = false;

    case 'ChallengerDeep_enn'
        cLayer = 15;
        featureMaps = [1 12 12 12 12 12 18 18 18 18 18 24 24 24 24 4];
        filterSize = {[5 5 3],[5 5 3],[5 5 3],[5 5 3],[5 5 3], ...
                      [5 5 3],[5 5 3],[5 5 3],[5 5 2],[5 5 2], ...
                      [5 5 2],[5 5 2],[5 5 2],[5 5 2],[1 1 1]};
        dropout = 0.2;
        stride = {[1,1,1]};
        maxPool = false;
        batchNorm = false;
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';

    case 'ChallengerDeep_res'
        cLayer = 14;
        featureMaps = [1 16 16 16 16 16 16 16 16 16 32 32 32 32 1];
        filterSize = {[5 5 3],[5 5 3],[5 5 3],[5 5 3],[5 5 3], ...
                      [5 5 3],[5 5 3],[5 5 3],[5 5 2],[5 5 2], ...
                      [5 5 2],[5 5 2],[5 5 2],[5 5 2]};
        shortcut = [0 0 1 0 3 0 5 0 7 0 9 0 11 0 0];
        dropout = 0.2;
        stride = {[1,1,1]};
        maxPool = false;
        batchNorm = false;

    case 'ChallengerDeep2'
        cLayer = 16;
        featureMaps = [1 16 16 16 16 16 24 24 24 24 24 32 32 32 32 32 1];
        filterSize = {[7 7 3],[5 5 3],[5 5 3],[5 5 3],[5 5 3], ...
                      [5 5 3],[5 5 3],[5 5 3],[5 5 3],[5 5 3], ...
                      [5,5,2],[5 5 2],[5 5 2],[5 5 2],[5 5 2], ...
                      [5 5 2]};
        dropout = 0.2;
        stride = {[1,1,1]};
        maxPool = false;
        batchNorm = false;

    case 'ChallengerDeep2_ce'
        cLayer = 16;
        featureMaps = [1 16 16 16 16 16 24 24 24 24 24 32 32 32 32 32 2];
        filterSize = {[7 7 3],[5 5 3],[5 5 3],[5 5 3],[5 5 3], ...
                      [5 5 3],[5 5 3],[5 5 3],[5 5 3],[5 5 3], ...
                      [5,5,2],[5 5 2],[5 5 2],[5 5 2],[5 5 2], ...
                      [5 5 2]};
        nonLinearity = repmat({'tanh'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        loss = 'softmax';
        dropout = 0.2;
        stride = {[1,1,1]};
        maxPool = false;
        batchNorm = false;
        
    case 'ChallengerDeep3'
        cLayer = 20;
        featureMaps = [1 16 16 16 16 16 ...
                         16 16 24 24 24 ...
                         24 24 24 32 32 ...
                         32 32 32 32 1];
        filterSize = {[7 7 3],[5 5 3],[5 5 3],[5 5 3],[5 5 3], ...
                      [5 5 3],[5 5 3],[5 5 3],[5 5 3],[5 5 3], ...
                      [5,5,2],[5 5 2],[5 5 2],[5 5 2],[5 5 2], ...
                      [5 5 2],[3 3 2],[3 3 2],[3 3 2],[3 3 2]};
        dropout = 0.2;
        stride = {[1,1,1]};
        maxPool = false;
        batchNorm = false;
        
    case 'ChallengerDeep3_ce'
        cLayer = 20;
        featureMaps = [1 16 16 16 16 16 ...
                         16 16 24 24 24 ...
                         24 24 24 32 32 ...
                         32 32 32 32 2];
        filterSize = {[7 7 3],[5 5 3],[5 5 3],[5 5 3],[5 5 3], ...
                      [5 5 3],[5 5 3],[5 5 3],[5 5 3],[5 5 3], ...
                      [5,5,2],[5 5 2],[5 5 2],[5 5 2],[5 5 2], ...
                      [5 5 2],[3 3 2],[3 3 2],[3 3 2],[3 3 2]};
        nonLinearity = repmat({'tanh'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        loss = 'softmax';
        dropout = 0.2;
        stride = {[1,1,1]};
        maxPool = false;
        batchNorm = false;

    case 'ChallengerDeep2res'
        cLayer = 16;
        featureMaps = [1 16 16 16 16 16 16 16 16 16 16 16 32 32 32 32 1];
        filterSize = {[7 7 3],[5 5 3],[5 5 3],[5 5 3],[5 5 3], ...
                      [5 5 3],[5 5 3],[5 5 3],[5 5 3],[5 5 3], ...
                      [5,5,2],[5 5 2],[5 5 2],[5 5 2],[5 5 2], ...
                      [5 5 2]};
        dropout = 0.2;
        shortcut = [0 0 1 0 3 0 5 0 7 0 9 0 11 0 13 0 0];
        stride = {[1,1,1]};
        maxPool = false;
        batchNorm = false;

    case 'SegEM'
        cLayer = 5;
        featureMaps = [1 10 10 10 10 1];
        filterSize = {[11 11 5],[11 11 5],[11 11 5],[11 11 5],[11 11 5]};
        dropout = 0;
        stride = {[1 1 1]};
        maxPool = false;
        batchNorm = false;

    case 'VDS'
        cLayer = 40;
        featureMaps = [1 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 ...
                         16 16 16 16 16 16 16 16 16 16 16 16 16 16 16, ...
                         16 16 16 16 16 16 16 16 16 1];
        filterSize = {[3 3 2], [3 3 2], [3 3 2],[3 3 2], [3 3 1], ...
                      [3 3 2], [3 3 2], [3 3 2],[3 3 2], [3 3 1], ...
                      [3 3 2], [3 3 2], [3 3 2],[3 3 2], [3 3 1], ...
                      [3 3 2], [3 3 2], [3 3 2],[3 3 2], [3 3 1], ...
                      [3 3 2], [3 3 2], [3 3 2],[3 3 2], [3 3 1], ...
                      [3 3 2], [3 3 2], [3 3 2],[3 3 2], [3 3 1], ...
                      [3 3 2], [3 3 2], [3 3 2],[3 3 2], [3 3 1], ...
                      [3 3 2], [3 3 2], [3 3 2],[3 3 2], [3 3 1]};
        dropout = 0.2;
        maxPool = false;
        stride = {[1, 1, 1]};

    case 'dk_syn_m'
        cLayer = 6;
        featureMaps = [1, 12, 24, 36, 48, 48, 4];
        filterSize = {[6 6 1], [4 4 1], [4 4 4], [4 4 2], [4 4 2], ...
                      [1 1 1]};
        maxPool = [2 2 1; 2 2 1; 2 2 2; 2 2 2; 1 1 1; 1 1 1];
        stride = {[1, 1, 1]};
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        dropout = 0;

    case 'dk_syn_m_r'
        % recursive
        cLayer = 6;
        featureMaps = [4, 12, 24, 36, 48, 48, 4];
        filterSize = {[6 6 1], [4 4 1], [4 4 4], [4 4 2], [4 4 2], ...
                      [1 1 1]};
        maxPool = [2 2 1; 2 2 1; 2 2 2; 2 2 2; 1 1 1; 1 1 1];
        stride = {[1, 1, 1]};
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        dropout = 0;

    case 'vesicle_cnn'
        % version with stride 1 pooling (requires to set 'd' variable
        % to ones(6, 3) in constructor because this is not supported)
        % local normalization response layer omitted

        cLayer = 5;
        featureMaps = [1 48 48 48 200 2];
        filterSize = {[5 5 1], [5 5 1], [5 5 1], [50 50 1], [1 1 1]};
        maxPool = [2 2 1; 2 2 1; 2 2 1; 1 1 1; 1 1 1];
        stride = {[1, 1, 1]};
        nonLinearity = {'relu', 'relu', 'relu', 'relu', 'softmax'};
        lossA = 'softmax';
        dropout = 0;

    case 'vesicle_cnn_strided'
        % version as probably done by ciresan et al with stride 2 max
        % pooling layer (could not find the n3 architecture from the
        % ciresan paper anywhere)
        % local normalization layer omitted

        % Note: Since the border using stride 2 violates the asymmetry
        % assumptions of this framework it is not easily possible to
        % implement the strided version here (would require to consider
        % only every sixth output voxel)

        cLayer = 5;
        featureMaps = [1 48 48 48 200 2];
        filterSize = {[5 5 1], [5 5 1], [5 5 1], [4 4 1], [1 1 1]};
        maxPool = [2 2 1; 2 2 1; 2 2 1; 1 1 1; 1 1 1];
        stride = {[1, 1, 1]};
        nonLinearity = {'relu', 'relu', 'relu', 'relu', 'softmax'};
        loss = 'softmax';
        dropout = 0;

    case 'svm_1'
        cLayer = 4;
        featureMaps = [1, 16, 32, 48, 4];
        filterSize = {[12 12 6], [7 7 3], [5 5 3], [1 1 1]};
        maxPool = [2 2 1; 2 2 2; 2 2 2; 1 1 1];
        stride = {[1, 1, 1]};
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        dropout = 0;

    case 'svm_1_rec'
        cLayer = 4;
        featureMaps = [4, 16, 32, 48, 4];
        filterSize = {[12 12 6], [7 7 3], [5 5 3], [1 1 1]};
        maxPool = [2 2 1; 2 2 2; 2 2 2; 1 1 1];
        stride = {[1, 1, 1]};
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        dropout = 0;

    case 'svm_2a'
        cLayer = 7;
        featureMaps = [1, 16, 16, 32, 48, 64, 64, 4];
        filterSize = {[8 8 2], [5 5 2], [3 3 2], [3 3 2], [3 3 3], ...
                      [3 3 3], [1 1 1]};
        maxPool = [1 1 1; 2 2 1; 2 2 2; 2 2 2; 1 1 1; 1 1 1; ...
                   1 1 1];
        stride = {[1, 1, 1]};
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        dropout = 0;

    case 'svm_2a_rec'
        cLayer = 7;
        featureMaps = [4, 16, 16, 32, 48, 64, 64, 4];
        filterSize = {[8 8 2], [5 5 2], [3 3 2], [3 3 2], [3 3 3], ...
                      [3 3 3], [1 1 1]};
        maxPool = [1 1 1; 2 2 1; 2 2 2; 2 2 2; 1 1 1; 1 1 1; ...
                   1 1 1];
        stride = {[1, 1, 1]};
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        dropout = 0;

    case 'svm_2b'
        cLayer = 7;
        featureMaps = [1, 16, 16, 32, 48, 64, 64, 4];
        filterSize = {[8 8 2], [5 5 2], [3 3 2], [3 3 2], [3 3 3], ...
                      [3 3 3], [1 1 1]};
        maxPool = [1 1 1; 2 2 1; 2 2 2; 2 2 2; 2 2 2; 1 1 1; ...
                   1 1 1];
        stride = {[1, 1, 1]};
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        dropout = 0;

    case 'svm_2b_rec'
        cLayer = 7;
        featureMaps = [4, 16, 16, 32, 48, 64, 64, 4];
        filterSize = {[8 8 2], [5 5 2], [3 3 2], [3 3 2], [3 3 3], ...
                      [3 3 3], [1 1 1]};
        maxPool = [1 1 1; 2 2 1; 2 2 2; 2 2 2; 2 2 2; 1 1 1; ...
                   1 1 1];
        stride = {[1, 1, 1]};
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        dropout = 0;

    case 'svm_2c'
        cLayer = 7;
        featureMaps = [1, 16, 16, 32, 48, 64, 64, 4];
        filterSize = {[8 8 3], [5 5 2], [3 3 2], [3 3 2], [3 3 3], ...
                      [3 3 3], [1 1 1]};
        maxPool = [2 2 1; 2 2 2; 2 2 2; 1 1 1; 2 2 2; 1 1 1; ...
                   1 1 1];
        stride = {[1, 1, 1]};
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        dropout = 0;

    case 'svm_2c_rec'
        cLayer = 7;
        featureMaps = [4, 16, 16, 32, 48, 64, 64, 4];
        filterSize = {[8 8 3], [5 5 2], [3 3 2], [3 3 2], [3 3 3], ...
                      [3 3 3], [1 1 1]};
        maxPool = [2 2 1; 2 2 2; 2 2 2; 1 1 1; 2 2 2; 1 1 1; ...
                   1 1 1];
        stride = {[1, 1, 1]};
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        dropout = 0;

    case 'svm_3a'
        cLayer = 10;
        featureMaps = [1, 12, 12, 24, 24, 24, 48, 48, 48, 48, 4];
        filterSize = {[8 8 2], [3 3 2], [3 3 2], [3 3 2], [3 3 2], ...
                      [3 3 2], [3 3 3], [3 3 3], [3 3 3], [1 1 1]};
        maxPool = [1 1 1; 2 2 1; 1 1 1; 1 1 1; 2 2 2; ...
                   1 1 1; 1 1 1; 2 2 2; 1 1 1; 1 1 1];
        stride = {[1, 1, 1]};
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        dropout = 0;

    case 'svm_3a_rec'
        cLayer = 10;
        featureMaps = [4, 12, 12, 24, 24, 24, 48, 48, 48, 48, 4];
        filterSize = {[8 8 2], [3 3 2], [3 3 2], [3 3 2], [3 3 2], ...
                      [3 3 2], [3 3 3], [3 3 3], [3 3 3], [1 1 1]};
        maxPool = [1 1 1; 2 2 1; 1 1 1; 1 1 1; 2 2 2; ...
                   1 1 1; 1 1 1; 2 2 2; 1 1 1; 1 1 1];
        stride = {[1, 1, 1]};
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        dropout = 0;

    case 'svm_3b'
        cLayer = 10;
        featureMaps = [1, 12, 12, 24, 24, 24, 48, 48, 48, 48, 4];
        filterSize = {[8 8 3], [3 3 2], [3 3 2], [3 3 2], [3 3 2], ...
                      [3 3 2], [3 3 3], [3 3 3], [3 3 3], [1 1 1]};
        maxPool = [1 1 1; 2 2 1; 1 1 1; 2 2 2; 1 1 1; ...
                   1 1 1; 2 2 2; 1 1 1; 1 1 1; 1 1 1];
        stride = {[1, 1, 1]};
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        dropout = 0;

    case 'svm_3b_rec'
        cLayer = 10;
        featureMaps = [4, 12, 12, 24, 24, 24, 48, 48, 48, 48, 4];
        filterSize = {[8 8 3], [3 3 2], [3 3 2], [3 3 2], [3 3 2], ...
                      [3 3 2], [3 3 3], [3 3 3], [3 3 3], [1 1 1]};
        maxPool = [1 1 1; 2 2 1; 1 1 1; 2 2 2; 1 1 1; ...
                   1 1 1; 2 2 2; 1 1 1; 1 1 1; 1 1 1];
        stride = {[1, 1, 1]};
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        dropout = 0;

    case 'svm_3c'
        cLayer = 10;
        featureMaps = [1, 12, 12, 24, 24, 36, 36, 48, 48, 48, 4];
        filterSize = {[8 8 3], [3 3 2], [3 3 2], [3 3 2], [3 3 2], ...
                      [3 3 2], [3 3 3], [3 3 3], [3 3 3], [1 1 1]};
        maxPool = [1 1 1; 2 2 1; 1 1 1; 2 2 2; 1 1 1; ...
                   2 2 2; 1 1 1; 2 2 2; 1 1 1; 1 1 1];
        stride = {[1, 1, 1]};
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        dropout = 0;

    case 'svm_3c_rec'
        cLayer = 10;
        featureMaps = [4, 12, 12, 24, 24, 36, 36, 48, 48, 48, 4];
        filterSize = {[8 8 3], [3 3 2], [3 3 2], [3 3 2], [3 3 2], ...
                      [3 3 2], [3 3 3], [3 3 3], [3 3 3], [1 1 1]};
        maxPool = [1 1 1; 2 2 1; 1 1 1; 2 2 2; 1 1 1; ...
                   2 2 2; 1 1 1; 2 2 2; 1 1 1; 1 1 1];
        stride = {[1, 1, 1]};
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        dropout = 0;

    case 'svm_4a'
        cLayer = 13;
        featureMaps = [1, 12, 12, 12, 24, 24, 24, 48, 48, 48, 48, 48, 48, 4];
        filterSize = {[8 8 2], [3 3 1], [3 3 2], [3 3 2], [3 3 2], ...
                      [3 3 2], [3 3 3], [3 3 3], [3 3 3], [3 3 3], ...
                      [3 3 3], [3 3 3], [1 1 1]};
        maxPool = [1 1 1; 1 1 1; 2 2 1; 1 1 1; 1 1 1; ...
                   2 2 2; 1 1 1; 1 1 1; 1 1 1; 1 1 1; ...
                   1 1 1; 1 1 1; 1 1 1];
        stride = {[1, 1, 1]};
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        dropout = 0;

    case 'svm_4a_rec'
        cLayer = 13;
        featureMaps = [4, 12, 12, 12, 24, 24, 24, 48, 48, 48, 48, 48, 48, 4];
        filterSize = {[8 8 2], [3 3 1], [3 3 2], [3 3 2], [3 3 2], ...
                      [3 3 2], [3 3 3], [3 3 3], [3 3 3], [3 3 3], ...
                      [3 3 3], [3 3 3], [1 1 1]};
        maxPool = [1 1 1; 1 1 1; 2 2 1; 1 1 1; 1 1 1; ...
                   2 2 2; 1 1 1; 1 1 1; 1 1 1; 1 1 1; ...
                   1 1 1; 1 1 1; 1 1 1];
        stride = {[1, 1, 1]};
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        dropout = 0;

    case 'svm_4b'
        cLayer = 13;
        featureMaps = [1, 12, 12, 12, 24, 24, 24, 48, 48, 48, 48, 48, 48, 4];
        filterSize = {[8 8 2], [3 3 1], [3 3 2], [3 3 2], [3 3 2], ...
                      [3 3 2], [3 3 3], [3 3 3], [3 3 3], [3 3 3], ...
                      [3 3 3], [3 3 3], [1 1 1]};
        maxPool = [1 1 1; 1 1 1; 2 2 1; 1 1 1; 1 1 1; ...
                   2 2 2; 1 1 1; 1 1 1; 2 2 2; 1 1 1; ...
                   1 1 1; 1 1 1; 1 1 1];
        stride = {[1, 1, 1]};
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        dropout = 0;

    case 'svm_4b_rec'
        cLayer = 13;
        featureMaps = [4, 12, 12, 12, 24, 24, 24, 48, 48, 48, 48, 48, 48, 4];
        filterSize = {[8 8 2], [3 3 1], [3 3 2], [3 3 2], [3 3 2], ...
                      [3 3 2], [3 3 3], [3 3 3], [3 3 3], [3 3 3], ...
                      [3 3 3], [3 3 3], [1 1 1]};
        maxPool = [1 1 1; 1 1 1; 2 2 1; 1 1 1; 1 1 1; ...
                   2 2 2; 1 1 1; 1 1 1; 2 2 2; 1 1 1; ...
                   1 1 1; 1 1 1; 1 1 1];
        stride = {[1, 1, 1]};
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        dropout = 0;

    case 'svm_5a'
        cLayer = 16;
        featureMaps = [1, 12, 12, 12, 12, 12, ...
                          24, 24, 24, 24, 24, ...
                          48, 48, 48, 48, 48, ...
                          4];
        filterSize = {[8 8 3], [3 3 2], [3 3 2], [3 3 2], [3 3 2], ...
                      [3 3 2], [3 3 3], [3 3 3], [3 3 2], [3 3 2], ...
                      [3 3 3], [3 3 3], [3 3 2], [3 3 3], [3 3 3], ...
                      [1 1 1]};
        maxPool = [1 1 1; 1 1 1; 1 1 1; 1 1 1; 2 2 1; ...
                   1 1 1; 1 1 1; 1 1 1; 1 1 1; 2 2 2; ...
                   1 1 1; 1 1 1; 1 1 1; 1 1 1; 1 1 1; ...
                   1 1 1];
        stride = {[1, 1, 1]};
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        dropout = 0;

    case 'svm_5a_rec'
        cLayer = 16;
        featureMaps = [4, 12, 12, 12, 12, 12, ...
                          24, 24, 24, 24, 24, ...
                          48, 48, 48, 48, 48, ...
                          4];
        filterSize = {[8 8 3], [3 3 2], [3 3 2], [3 3 2], [3 3 2], ...
                      [3 3 2], [3 3 3], [3 3 3], [3 3 2], [3 3 2], ...
                      [3 3 3], [3 3 3], [3 3 2], [3 3 3], [3 3 3], ...
                      [1 1 1]};
        maxPool = [1 1 1; 1 1 1; 1 1 1; 1 1 1; 2 2 1; ...
                   1 1 1; 1 1 1; 1 1 1; 1 1 1; 2 2 2; ...
                   1 1 1; 1 1 1; 1 1 1; 1 1 1; 1 1 1; ...
                   1 1 1];
        stride = {[1, 1, 1]};
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        dropout = 0;

    case 'svm_5b'
        cLayer = 16;
        featureMaps = [1, 12, 12, 12, 12, 18, ...
                          18, 18, 18, 24, 24, ...
                          24, 24, 36, 36, 36, ...
                          4];
        filterSize = {[8 8 3], [3 3 2], [3 3 2], [3 3 2], [3 3 2], ...
                      [3 3 2], [3 3 3], [3 3 3], [3 3 2], [3 3 2], ...
                      [3 3 3], [3 3 3], [3 3 2], [3 3 3], [3 3 3], ...
                      [1 1 1]};
        maxPool = [1 1 1; 1 1 1; 1 1 1; 2 2 1; 1 1 1; ...
                   1 1 1; 1 1 1; 2 2 2; 1 1 1; 1 1 1; ...
                   1 1 1; 2 2 2; 1 1 1; 1 1 1; 1 1 1; ...
                   1 1 1];
        stride = {[1, 1, 1]};
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        dropout = 0;

    case 'svm_5b_rec'
        cLayer = 16;
        featureMaps = [4, 12, 12, 12, 12, 18, ...
                          18, 18, 18, 24, 24, ...
                          24, 24, 36, 36, 36, ...
                          4];
        filterSize = {[8 8 3], [3 3 2], [3 3 2], [3 3 2], [3 3 2], ...
                      [3 3 2], [3 3 3], [3 3 3], [3 3 2], [3 3 2], ...
                      [3 3 3], [3 3 3], [3 3 2], [3 3 3], [3 3 3], ...
                      [1 1 1]};
        maxPool = [1 1 1; 1 1 1; 1 1 1; 2 2 1; 1 1 1; ...
                   1 1 1; 1 1 1; 2 2 2; 1 1 1; 1 1 1; ...
                   1 1 1; 2 2 2; 1 1 1; 1 1 1; 1 1 1; ...
                   1 1 1];
        stride = {[1, 1, 1]};
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        dropout = 0;

    case 'svm_segEM'
        cLayer = 6;
        featureMaps = [1 10 10 10 10 10 4];
        filterSize = {[11 11 5],[11 11 5],[11 11 5],[11 11 5],[11 11 5], ...
                      [ 1  1  1]};
        stride = {[1 1 1]};
        maxPool = false;
        batchNorm = false;
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        dropout = 0;
    case 'svm_segEM_rec'
        cLayer = 6;
        featureMaps = [4 10 10 10 10 10 4];
        filterSize = {[11 11 5],[11 11 5],[11 11 5],[11 11 5],[11 11 5], ...
                      [ 1  1  1]};
        stride = {[1 1 1]};
        maxPool = false;
        batchNorm = false;
        nonLinearity = repmat({'tanh2'}, cLayer, 1);
        nonLinearity{end} = 'softmax';
        lossA = 'softmax';
        dropout = 0;

    otherwise
        error('Architecture not implemented');
end

%set non-linearities
if exist('hiddenNL','var') && ~isempty(hiddenNL)
    if ischar(hiddenNL)
        nonLinearity = repmat({hiddenNL}, 1, cLayer - 1);
    elseif iscell(hiddenNL) && length(hiddenNL) == cLayer - 1
        nonLinearity = hiddenNL;
    else
        error('Wrong hiddenNL input specified.');
    end
elseif (~exist('hiddenNL','var') || isempty(hiddenNL)) ...
        && ~exist('nonLinearity', 'var')
    nonLinearity = repmat({'tanh'}, 1, cLayer - 1);
end

if exist('outputNL','var') && ~isempty(outputNL)
    nonLinearity = [nonLinearity, {outputNL}];
elseif ~exist('outputNL','var') && length(nonLinearity) == (cLayer - 1)
    nonLinearity = [nonLinearity, {'tanh'}];
end

if ~exist('loss','var') || isempty(loss)
    if exist('lossA', 'var')
        % loss defined in architecture
        loss = lossA;
    else
        % default loss
        loss = 'squared';
    end
end

if ~isempty(noOutputChannels)
    featureMaps(end) = noOutputChannels;
end

switch opt
    case 'sgd'
        optimizer = Codat.Optimizer.gradientDescent(1e-5,0.9);
    case 'rmsprop'
        optimizer = Codat.Optimizer.rmsProp(1e-5,0.9);
    case 'adam'
        optimizer = Codat.Optimizer.adam(1e-5);
    otherwise
        disp('opt not found. Setting sgd as optimizer');
        optimizer = Codat.Optimizer.gradientDescent(1e-5,0.9);
end

if ~exist('shortcut','var') || isempty(shortcut)
    shortcut = 0;
end
if ~exist('batchNorm','var') || isempty(batchNorm)
    batchNorm = false;
end

cnet = Codat.CNN.cnn(cLayer, featureMaps, filterSize, stride, maxPool, ...
    dropout, shortcut, batchNorm, nonLinearity, loss, optimizer);

if exist('lr','var') && ~isempty(lr)
    cnets = repmat({cnet},length(lr),1);
    for i = 1:length(cnets)
        cnets{i}.optimizer.learningRate = lr(i);
    end
    if length(cnets) == 1
        cnet = cnets{1};
    else
        cnet = cnets;
    end
end

end
