function cnet = codatToSegEM(codat)
%CODATTOSEGEM Convert a Codat.CNN.cnn object to a SegEM cnn object.
% INPUT codat: Codat.CNN.cnn object
% OUTPUT cnet: A SegEM cnn object.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>
%         Sahil Loomba <sahil.loomba@brain.mpg.de>

numHiddenLayer = codat.layer - 2;
numFeature = codat.featureMaps(2:end-1);
filterSize = codat.filterSize{2}; % first is filter size for input layer

% training parameters: dont think this is needed to convert myelin codat cnn to segEM cnn
inputSize = [100,100,100]; % input cube size
savingPath = '/gaba/u/sahilloo/myelinCNN/results/';
maxIter = 1e4;
wStart =  [1e-9 1e-9 1e-9 1e-9 1e-9];
bStart = [1e-9 1e-9 1e-9 1e-9 1e-9];
runSetting = train(inputSize, savingPath, maxIter, wStart, bStart);
runSetting.actvtClass = @gsingle; % gets changed to @single for inference

% initialize segEM cnn
cnet = cnn(numHiddenLayer, numFeature, filterSize, runSetting);
cnet = cnet.init();

% set weights of segEM cnn to trained codat cnn for inference
codat.W = cellfun(@(x)flip(x,4),codat.W,'UniformOutput',false);
for l=2:length(cnet.layer)
    cnet.layer{l}.W = cell(cnet.layer{l}.numFeature, 1);
    for prevFm=1:cnet.layer{l-1}.numFeature
        for fm=1:cnet.layer{l}.numFeature
            cnet.layer{l}.W{prevFm,fm} = codat.W{l}(:,:,:,prevFm,fm);
        end
    end
    cnet.layer{l}.B = codat.b{l}';
end


end
