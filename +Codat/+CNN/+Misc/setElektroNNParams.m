function cnet = setElektroNNParams( cnet, w, b )
%SETELEKTRONNPARAMS Summary of this function goes here
% INPUT cnet: Codat.cnn object
%           A codat cnn object with the correct architectur.
%       w: [Nx1] cell
%           The weights of the elektroNN cnet.
%           (see python.synem.syconn_com.get_params)
%       b: [Nx1] cell
%           The biases of the elektroNN cnet.
%           (see python.synem.syconn_com.get_params)
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

assert(cnet.numParams == ...
    (sum(cellfun(@numel,w)) + sum(cellfun(@numel, b))), ...
    'The specified parameters do not match the number of parameters in cnet.');

for i = 1:length(w)
    cnet.W{i+1} = permute(w{i}, [4 5 2 3 1]);
    cnet.W{i+1} = cnet.W{i+1}(:,:,:,end:-1:1,:);
    cnet.b{i+1} = b{i}(:);
end
    


end