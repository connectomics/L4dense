function pred = predictCube( cnet, raw, options )
%PREDICTCUBE Predict for input cube.
% INPUT cnet: A Codat.CNN object.
%       raw: 3D input array.
%       options: Options struct with field
%       	gpuDev: (Optional) logical
%               (Default: false)
%            	see cnet.train
%       	target_size: 3x1 array.
%           	Input cube is tiled such that each forward
%           	pass produces an output of target_size.
%           	Targets for one cube are stiched together
%           	afterwards.
%           	(Default is the whole ROI).
%        	convAlg: (Default is current convAlg of cnet)
%       	display: (Optional) Positive integer specifying the
%           	printout frequency to track how many tiles have
%           	been processed so far.
%           	(Default: No printout).
%       	rotPred: (Optional) Logical flag indicating whether
%           	rotPredict is used for prediction.
%           	(Default: false)
%           fmL: (Optional) [Nx1] int
%               Specify which feature map layers are added to the output.
%               (see also Codat.CNN.cnn.getFMs)
%               (Note: This currently does not work for rotPred = true)
%               (Default: output layer only)
%           fmL_idx: (Optional) [Nx1] cell
%               The indices within a fm layer that are kept.
%               (Default: the whole fm layer)
%           padBorder: (Optional) logical
%               Symmetric padding of the input data.
%               (Default: false)
% OUTPUT pred: Prediction for raw.
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

defOpts = defaultOptions(cnet.layer);

if ~exist('options', 'var') || isempty(options)
    options = defOpts;
else
    options = Util.setUserOptions(defOpts, options);
end

cnet = cnet.setConvMode(options.convAlg);
if options.gpuDev
    cnet = cnet.setParamsToActvtClass(@gpuArray);
end

if options.rotPred && (options.fmL ~= cnet.layer)
    error('rotPred does currently not support fmL.');
end

% padding
if options.padBorder
    raw = padarray(raw, cnet.border./2, 'symmetric');
end

% shorter names
b = cnet.border;
ts = options.target_size;

% make tiles
[y,x,z] = meshgrid(1:ts(2):size(raw,2) - b(2), ...
                   1:ts(1):size(raw,1) - b(1), ...
                   1:ts(3):size(raw,3) - b(3));
% predict each tile
if options.display
    Util.log('Starting prediction for %d tiles.', numel(x));
end

sz = size(raw);
sz(end+1:4) = 1;
pred = zeros([sz(1:3) - b(:)', sz(4)], 'single');
for tile = 1:numel(x)
    bboxRaw = [x(tile), min(x(tile) + b(1) + ts(1) - 1,size(raw,1)); ...
               y(tile), min(y(tile) + b(2) + ts(2) - 1,size(raw,2)); ...
               z(tile), min(z(tile) + b(3) + ts(3) - 1,size(raw,3)); ...
               1, size(raw,4)];
    bboxPred = bboxRaw;
    bboxPred(1:3,2) = bboxPred(1:3,2) - b(:);
    currTile = Util.getBbox(raw, bboxRaw);
    if options.gpuDev
        currTile = gpuArray(currTile);
    end
    if options.rotPred
        thisPred = gather(cnet.rotPredict(currTile));
    elseif (length(options.fmL) == 1) && (options.fmL == cnet.layer)
        thisPred = gather(cnet.predict(currTile));
        if ~isempty(options.fmL_idx)
            thisPred = thisPred(:,:,:,options.fmL_idx{1});
        end
    else
        thisPred = gather(cnet.getFMs(currTile, options.fmL, ...
            cnet.mSize(currTile, 1:3) - cnet.border, options.fmL_idx));
    end
    bboxPred(4,:) = [1, size(thisPred, 4)];
    pred = Util.setBBoxInArray(pred, bboxPred, thisPred);
    if mod(tile, options.display) == 0
        Util.log('Finished tile %d out of %d.', tile, numel(x));
    end
end

end

function options = defaultOptions(numLayer)
options.gpuDev = false;
options.convAlg = 'fft2';
options.target_size = [128 128 128];
options.rotPred = false;
options.display = 0;
options.fmL = numLayer;
options.padBorder = false;
options.fmL_idx = [];
end
