function pred = predict( rcnn, X )
%PREDICT Prediction for rcnn.
% INPUT X: nd float array
%           The input to the rcnn. The first channel of X is added to the
%           output of each cnn step.
% OUTPUT pred: nd float array
%           The rcnn prediction.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

for i = 1:length(rcnn.cnets)
    tmp = rcnn.cnets{i}.predict(X);
    if ~isempty(rcnn.postprocFunc) && ~isempty(rcnn.postprocFunc{i})
        f_postProc = str2func(rcnn.postprocFunc{i});
        tmp = f_postProc(tmp);
    end
    sz = size(tmp);
    X = X(:,:,:,1); % only keep original data (for multiple recurrence steps)
    X = Util.cropArray(X, sz(1:ndims(X)));
    X = cat(4, X, tmp);
end

pred = tmp;
end