function getFMs(obj, input, fmL, sizOut, fmL_idx)
%GETFMS Similar to predict but can also output intermediate FMs of the last
% CNN.
% INPUT input: 3d or 4d single
%           Input to cnet.
%       fmL: [Nx1] int
%           Layers for which the featureMaps will be concatenated and
%           returned (w.r.t. last cnn).
%       sizOut: (Optional) [3x1] int
%           Size of the output fms.
%           (Default: all fms are cropped to the smallest requested fm
%           size).
%       fmL_idx: (Optional) [Nx1] cell
%           The indices of the feature maps for the corresponding layer
%           in fmL that are kep (w.r.t. last cnn).
%           (Default: all feature maps of a layer)
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

for i = 1:length(rcnn.cnets)
    if i < length(rcnn.cnets)
        % regular predict for the first cnet
        tmp = rcnn.cnets{i}.predict(X);
    else
        % get fms for last cnn
        tmp = rcnn.cnets{i}.getFMs(X, fmL, sizOut, fmL_idx);
    end
    if ~isempty(rcnn.postprocFunc) && ~isempty(rcnn.postprocFunc{i})
        tmp = rcnn.postprocFunc{i}(tmp);
    end
    sz = size(tmp);
    X = X(:,:,:,1); % only keep original data (for multiple recurrence steps)
    X = Util.cropArray(X, sz(1:ndims(X)));
    X = cat(4, X, tmp);
end

pred = tmp;
end
