function rcnn = setConvMode( rcnn, mode )
%SETCONVMODE Call setConvMode for each cnet.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

rcnn.cnets = cellfun(@(x)x.setConvMode(mode), rcnn.cnets, ...
    'UniformOutput', false);

end

