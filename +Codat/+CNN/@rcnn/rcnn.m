classdef rcnn
    %RCNN Recursive cnn wrapper class.
    % Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>
    
    properties
        cnets
        postprocFunc = [];
        border
        layer % number of layers of last cnn
    end
    
    methods
        function obj = rcnn(cnets, postprocFunc)
            obj.cnets = cnets(:);
            obj.border = sum(cell2mat(cellfun(@(x)x.border, obj.cnets, ...
                'uni', 0)), 1);
            if exist('postprocFunc', 'var')
                if ~iscell(postprocFunc)
                    postprocFunc = {postprocFunc};
                end
                for i = 1:length(postprocFunc)
                    if isa(postprocFunc{i}, 'function_handle')
                        postprocFunc{i} = func2str(postprocFunc{i});
                    end
                end
                obj.postprocFunc = postprocFunc;
                obj.postprocFunc(end+1:length(obj.cnets)) = {[]};
            end
            obj.layer = cnets{end}.layer;
        end
    end
    
end

