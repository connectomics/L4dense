function job = quickPred( cnet, stackIdx, rotPred, useTraining )
%QUICKPRED Predict raw data on GABA
% INPUT cnet: A Codat.CNN object.
%       stackIdx: (Optional) int
%           Index of stack from training set to predict.
%           (Default: 19)
%       rotPred: (Optional) logical
%           Flag to set rotation invariant prediction.
%           (Default: false).
%       useTest: (Optional) logical
%           Flag indicating that the training set should be used.
%           (Default: false)
% OUTPUT job: job object
%           The job object
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('stackIdx','var') || isempty(stackIdx)
    stackIdx = 19;
end
if ~exist('rotPred','var') || isempty(rotPred)
    rotPred = false;
end
if ~exist('useTraining','var') || isempty(useTraining)
    useTraining = false;
end
cluster = getCluster('gpu');
job = startJob(cluster, @jobWrapper, ...
    {cnet, stackIdx, rotPred, useTraining}, 1);
end

function pred = jobWrapper(cnet, stackIdx, rotPred, useTraining)
options.gpuDev = 1;
options.target_size = [50 50 50]; %avoid gpu fft bug
options.rotPred = rotPred;
if useTraining
    m = load('cortexTrainingDataParameter.mat');
    m2 = load(m.stacks(stackIdx).stackFile);
else
    m = load('cortexTestDataParameter.mat');
    m2 = load(m.stacks(stackIdx).targetFile);
end
raw = m2.raw;
raw = (single(raw) - 122)./22;
cnet = cnet.setConvMode('fft2');
pred = Codat.CNN.Misc.predictCube(cnet, raw, options);
end


