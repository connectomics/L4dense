function job = predictDataset( cnet, bbox, datIn, datOut, options, cluster)
%PREDICTDATASET CNN prediction for WkDatasets.
% INPUT cnet: Codat.CNN.cnn object
%       bbox: [3x2] int specifying the output bounding box. The input
%           bounding box will be larger by cnet.border.
%       datIn: WkDataset object of the input data.
%       datOut: WkDataset or McWkwDataset object for the output data.
%       options: (Optional) Options struct
%           'gpuDev': (Default: true)
%           'target_size_job': The target size that is processed in a
%               single job.
%               (Default: [64 64 64])
%           'target_size': See Codat.CNN.Misc.predictROI (i.e. the tiling
%               of the target size within a job).
%               (Default: [512 512 256])
%           'convAlg': See Codat.CNN.Misc.predictROI
%               (Default: 'fft2')
%           'f_norm': See Codat.CNN.Misc.predictROI
%               (Default: func2str(@(x)(single(x) - 122)./22))
%           'fmL': [Nx1] int
%               Indices of the cnet.layer outputs to use
%               (Default: cnet.layer, i.e. output layer only)
%           'fmL_idx': [Nx1] cell
%               The indices of the feature maps in the corresponding layer.
%               If empty than all feature maps in fmL will be calculated.
%               (Default: [])
%       cluster: (Optional) Cluster object to use.
% OUTPUT job: parallel.job object
%           The created job object.
%
% NOTE It could happen that several jobs write to the same cube in case the
%      bbox + [cnet.border'./2, - cnet.border'./2] is not aligned with the
%      knossos/32 wkwrap-cubes or options.target_size is not equal to
%      pOut.cubesize(1:3)
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

optDef = defaultOptions(cnet.layer);

% user options
if exist('options', 'var') && ~isempty(options)
    options = Util.setUserOptions(optDef, options);
else
    options = optDef;
end

% check bbox alignment
if any(bbox ~= Util.alignBboxWithKnossosCubes(bbox, [32 32 32]))
    warning('Bbox is not aligned with [32 32 32] cubes.');
end

if ~exist('cluster','var') || isempty(cluster)
    if options.gpuDev
        cluster = getCluster('gpu');
    else
        cluster = getCluster('cpu');
    end
end

% job inputs
inputCell = cell(0,1);
for x = bbox(1,1):options.target_size_job(1):bbox(1,2)
    for y = bbox(2,1):options.target_size_job(2):bbox(2,2)
        for z = bbox(3,1):options.target_size_job(3):bbox(3,2)
            thisBbox = [x x; y y; z z] + ...
                [[0;0;0],options.target_size_job(1:3)' - 1];
            thisBbox(:,2) = min(thisBbox(:,2), bbox(:,2));
            inputCell{end+1} = {thisBbox}; %#ok<AGROW>
        end
    end
end

job = Cluster.startJob(@jobWrapper, inputCell, 'cluster', cluster, ...
    'sharedInputs', {cnet, datIn, datOut, options});

end


function defOpts = defaultOptions(numLayer)
defOpts.gpuDev = true;
defOpts.f_norm = func2str(@(x)(single(x) - 122)./22);
defOpts.convAlg = 'fft2';
defOpts.target_size = [64 64 64];
defOpts.target_size_job = [512 512 256];
defOpts.fmL = numLayer;
defOpts.fmL_idx = [];
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function jobWrapper(cnet, datIn, datOut, options, bbox)
raw = datIn.readRoi(Util.addBorder(bbox, cnet.border./2));

if ischar(options.f_norm)
    f_norm = str2func(options.f_norm);
else
    f_norm = options.f_norm;
end

% remove options that were not for predictCube
options = rmfield(options, 'f_norm');
options = rmfield(options, 'target_size_job');

raw = f_norm(raw);
if options.gpuDev
    selectEmptyGPUDev();
end
pred = Codat.CNN.Misc.predictCube(cnet, raw, options);
datOut.writeRoi(bbox, pred);
end
