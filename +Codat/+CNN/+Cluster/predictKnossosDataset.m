function [job, delTmp] = predictKnossosDataset( cnet, pIn, pOut, bbox, options, cluster )
%PREDICTKNOSSOSDATASET Predict data from a knossos dataset.
% INPUT cnet: A Codat.CNN object.
%       pIn: KnossosDataset object of the input data.
%       pOut: KnossosDataset object for the output data.
%       bbox: [3x2] int specifying the output bounding box. The input
%           bounding box will be larger by cnet.border.
%       options: (Optional) Options struct
%           'gpuDev': (Default: true)
%           'target_size_job': The target size that is processed in a
%               single job.
%               (Default: pOut.cubesize)
%           'target_size': See Codat.CNN.Misc.predictROI (i.e. the tiling
%               of the target size within a job).
%               (Default: pOut.cubesize)
%           'convAlg': See Codat.CNN.Misc.predictROI
%           'f_norm': See Codat.CNN.Misc.predictROI
%       cluster: (Optional) Cluster object to use.
%
% NOTE It could happen that several jobs write to the same cube in case the
%      bbox + [cnet.border'./2, - cnet.border'./2] is not aligned with the
%      knossos or options.target_size is not equal to pOut.cubesize(1:3)
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

warning('DEPRECATED - use predictDataset instead.');

if ~exist('cluster','var') || isempty(cluster)
    if options.gpuDev
        cluster = getCluster('gpu');
    else
        cluster = getCluster('cpu');
    end
end

info = Util.runInfo();
info.param.cluster = cluster.IndependentSubmitFcn(2:end);

if any(mod(bbox(:,1),pIn.cubesize(1:3)') ~= 1)
    warning('Bbox not aligned with standard knossos cubes.');
end

if isa(pOut, 'Datasets.WkDataset')
    optDef = defaultOptions([64 64 64]);
else
    optDef = defaultOptions(pOut.cubesize);
end
if exist('options', 'var') && ~isempty(options)
    options = Util.setUserOptions(optDef, options);
else
    options = optDef;
end

options.target_size = options.target_size(:)';
options.target_size_job = options.target_size_job(:)';

% write common files to tmp path
[tmpPath, delTmp] = Util.tmpFile(cluster.JobStorageLocation, ...
    'cnet', cnet, 'pIn', pIn, 'pOut', pOut, 'options', options);

% job inputs
inputCell = cell(0,1);
Util.log('Submitting jobs to cluster.');
for x = bbox(1,1):options.target_size_job(1):bbox(1,2)
    for y = bbox(2,1):options.target_size_job(2):bbox(2,2)
        for z = bbox(3,1):options.target_size_job(3):bbox(3,2)
            %input bbox to cnn
            thisBbox = [x x; y y; z z] + [[0;0;0],options.target_size_job(1:3)' - 1];
            thisBbox(:,2) = min(thisBbox(:,2), bbox(:,2));
            inputCell{end+1} = {tmpPath, thisBbox}; %#ok<AGROW>
        end
    end
end
job = startJob(cluster, @jobWrapper, inputCell, 0);
Util.log('Submitted job %d.', job.Id);
infoFile = fullfile(pOut.root, 'RunInfo.mat');
Util.save(infoFile, info);
Util.log('Saving run info to %s.', infoFile);
%wait(job);
%Util.log('Deleting temporary files.');
%delTmp();
end

function jobWrapper(tmpPath, bbox)

% load the data
m = load(tmpPath);

% select gpu dev if necessary
if m.options.gpuDev
    selectEmptyGPUDev();
end

pred = Codat.CNN.Misc.predictROI(m.cnet, bbox, m.options, m.pIn);

% save output
p = m.pOut;
p.writeRoi(bbox(:,1), pred);
end

function defOpts = defaultOptions(target_size)
defOpts.gpuDev = true;
defOpts.f_norm = func2str(@(x)(single(x) - 122)./22);
defOpts.convAlg = 'fft2';
defOpts.target_size = target_size(1:3);
defOpts.target_size_job = target_size(1:3);
end
