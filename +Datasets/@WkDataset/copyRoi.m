function copyRoi( obj, datOut, bbox, cubesize )
%COPYROI Copy the data in the specified ROI to the target dataset.
% INPUT datOut: Datasets.WkDataset
%           The output dataset.
%       bbox: [3x2] int
%           The bounding box to copy.
%       cubesize: (Optional) [3x1] int
%           Single cubes of this size are read and written sequentially.
%           (Default: [128, 128, 128];
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>


if ~exist('cubesize', 'var') || isempty(cubesize)
    cubesize = [128, 128, 128];
end

for x = bbox(1,1):cubesize(1):bbox(1,2)
    for y = bbox(2,1):cubesize(2):bbox(2,2)
        for z = bbox(3,1):cubesize(3):bbox(3,2)
            from = [x; y; z];
            to = from + cubesize(:) - 1;
            to = bsxfun(@min, to, bbox(:,2));
            thisBbox = [from, to];
            dat = obj.readRoi(thisBbox);
            datOut.writeRoi(thisBbox, dat);
        end
    end
end

end