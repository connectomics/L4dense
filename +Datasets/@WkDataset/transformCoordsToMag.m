function coords = transformCoordsToMag( coords, magFrom, magTo )
%TRANSFORMCOORDSTOMAG Transform coordinates to a different magnification.
% INPUT coords: [Nx3] int
%           List of 3d coordinates.
%       magFrom: int
%           Current magnification of coords.
%       magTo: int
%           Output magnification of coords.
% OUTPUT coords: [Nx3] int
%           The input coordinates in the magTo magnification.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if magFrom < magTo
    coords = coords./(magTo - magFrom + 1);
else
    coords = coords.*(magFrom - magTo + 1);
end
coords = round(coords);

end

