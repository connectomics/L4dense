function writeRoi(obj, bbox, data)
% WRITEROI Wrapper for writeKnossosRoi.
% INPUT bbox: [3x2] int or [3x1] int
%           Bounding box of the form [x_min; y_min; z_min]
%           In case a [3x2] bounding box is supplied only the
%           first column is used.
%       data: 3d or 4d raw data of obj.p.dtype
% see also writeKnossosRoi
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>


switch obj.backend
    case 'wkcube'
        %check input data
        if ~isa(data, obj.dtype)
            error('Data type needs to be %s.', obj.dtype);
        end

        %adapt bbox input for writeKnossosRoi
        if iscolumn(bbox)
            bbox = bbox';
        elseif all(size(bbox) == [3, 2])
            sz = size(data);
            sz(end+1:4) = 1;
            if ~all(([diff(bbox, [], 2)' + 1, obj.numChannels]) == sz)
                error(['The specified bounding box does not fit' ...
                    ' the size of the data.']);
            end
            bbox = bbox(:,1)';
        end

        writeKnossosRoi(obj.root, obj.p.prefix, bbox, ...
            data, obj.p.dtype, obj.p.suffix, [], obj.p.cubesize);
    case 'wkwrap'
        if ~isa(data, obj.dtype)
            error('Data type needs to be %s.', obj.dtype);
        end
        if obj.p.xyzcOrder && obj.p.numChannels > 1
            data = permute(data, [4 1 2 3]);
        end
        wkwSaveRoi(obj.root, bbox(1:3), data);
end
end