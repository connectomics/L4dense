function datOut = singleToUint8Dataset(obj, outputFolder, dataRange, ...
    bbox, channel, cubeSize)
%SINGLETOUINT8DATASET Convert a dataset with single datatype to a uint8
% dataset.
% INPUT outputFolder: string or WkDataset or [Nx1] cell of string/WkDatset
%           The path for the output dataset (will be a wkwrap dataset). If
%           If it is a cell array then the channels are written to the
%           corresponding dataset. The function creates new wkwrap datasets
%           with default settings if they do not exist.
%       dataRange: [1x2] double
%           Numerical range of the data. The data is normalized to [0, 255]
%           using this range and then converted to uint8.
%       bbox: [3x2] or [6x1] int
%           The bounding box that is converted.
%       channel: (Optional) int or [Nx1] int
%           The channel that is written to the corresponding output dataset.
%           (Default: 1:(number of outputFolders specified)
%       cubeSize: (Optional) [3x1] int
%           Size of the individual cubes that are processed.
%           (Default: [1024; 1024; 1024])
% OUTPUT datOut: WkDataset object or [Nx1] cell of WkDatsets.
%           The output WkDatasets.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~iscell(outputFolder)
    outputFolder = {outputFolder};
end

if ~exist('channel', 'var') || isempty(channel)
    channel = 1:length(outputFolder);
else
    assert(length(channel) == length(outputFolder), ...
        'Specify a channel for each outputFolder');
end

datOut = cell(length(outputFolder), 1);
for i = 1:length(outputFolder)
    if ischar(outputFolder{i})
        % create dataset if it does not exist
        if ~exist(fullfile(outputFolder{i}, 'header.wkw'), 'file')
            wkwInit('new', outputFolder{i}, 32, 32, 'uint8', 1);
        end
        datOut{i} = Datasets.WkDataset(outputFolder{i});
    else
        datOut{i} = outputFolder{i};
    end
    Util.log('Storing channel %d at %s.', channel(i), datOut{i}.root);
end

bbox = reshape(bbox(:), 3, 2);
if ~exist('cubeSize', 'var') || isempty(cubeSize)
    cubeSize = [1024; 1024; 1024];
else
    cubeSize = cubeSize(:);
end

Util.log('Starting conversion.');
for x = bbox(1,1):cubeSize(1):bbox(1,2)
    for y = bbox(2,1):cubeSize(2):bbox(2,2)
        for z = bbox(3,1):cubeSize(3):bbox(3,2)
            this_bbox = [[x;y;z], [x;y;z] + cubeSize - 1];
            this_bbox(:,2) = min(this_bbox(:,2), bbox(:,2));
            data = obj.readRoi(this_bbox);
            data = (data - dataRange(1))./(dataRange(2) - dataRange(1));
            data = uint8(255*data);
            for i = 1:length(datOut)
                datOut{i}.writeRoi(this_bbox, data(:,:,:,channel(i)));
            end
        end
    end
end
Util.log('Finished conversion.');

end
