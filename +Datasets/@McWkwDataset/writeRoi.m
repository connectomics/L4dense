function writeRoi( obj, bbox, data, channels )
%WRITEROI Write data.
% INPUT bbox: [3x2] int or [3x1] int
%           Bounding box of the form [x_min; y_min; z_min]
%           In case a [3x2] bounding box is supplied only the
%           first column is used.
%       data: 3d or 4d data to write
%       channels: (Optional) [Nx1] int
%           Linear indices of the channels to write.
%           (Default: all channels)
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if nargin < 4
    assert(size(data, 4) == obj.numChannels);
    channels = 1:size(data, 4);
else
    assert(size(data, 4) == length(channels));
end

for c = channels(:)'
    obj.datChannels{c}.writeRoi(bbox, data(:,:,:,c));
end


end

