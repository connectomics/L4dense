% Written by
%   Manuel Berning <manuel.berning@brain.mpg.de>
%   Alessandro Motta <alessandro.motta@brain.mpg.de>
thisDir = fileparts(mfilename('fullpath'));

% Add current directory and its sub-directories to path. But remove
% "hidden" directories whose name starts with a dot.
pathsToAdd = strsplit(genpath(thisDir), pathsep);
[~, isVisible] = cellfun(@fileparts, pathsToAdd, 'UniformOutput', false);
isVisible = not(cellfun(@(n) numel(n) > 0 && n(1) == '.', isVisible));
addpath(strjoin(pathsToAdd(isVisible), pathsep));

SynEM.setup();

% Mark as ready
global PIPELINE_READY;
PIPELINE_READY = true;
clear;
