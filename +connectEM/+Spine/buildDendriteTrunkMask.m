function trunkMask = buildDendriteTrunkMask(param, dendAgglos)
    % trunkMask = buildDendriteTrunkMask(param, dendAgglos)
    %   Makes a set of agglomerates or super-agglomerates and returns a
    %   logical mask (`candMask`) which indicates the dendrite trunks to
    %   which spine necks may attach.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    if isstruct(dendAgglos)
        dendAgglos = arrayfun( ...
            @Agglo.fromSuperAgglo, ...
            dendAgglos, 'UniformOutput', false);
    end
    
    assert(iscell(dendAgglos));
    trunkMask = Agglo.calculateVolume(param, dendAgglos);
    trunkMask = reshape(trunkMask > (10 ^ 5.5), [], 1);
end
