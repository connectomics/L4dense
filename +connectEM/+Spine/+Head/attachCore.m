function [edges, attached] = attachCore(attachParam, data, shAgglos)
    % Based on code by
    %   Manuel Berning <manuel.berning@brain.mpg.de>
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    maxNrSteps = attachParam.maxNumSteps;
    minEdgeProb = attachParam.minEdgeProb;
    
    dendLUT = data.dendLUT;
    exclLUT = data.exclLUT;
    graph = data.graph;
    
    % actually do the work
   [edges, attached] = cellfun( ...
       @forSpineHead, shAgglos, 'UniformOutput', false);
   
    % make output a vector
    attached = cell2mat(attached);
    
    function [edges, attached] = forSpineHead(shSegIds)
        % prepare output
        edges = zeros(maxNrSteps, 2);
        attached = zeros(1, 'like', dendLUT);
        
        % check if spine head is empty
        % HACK(amotta): The conversion from the SegEM- to the BeneNet-based
        % segmentation might produce empty spine heads...
        if isempty(shSegIds); return; end;
        
        % check if spine is in agglomerate
        attached = max(dendLUT(shSegIds));
        if attached; return; end;
        
        % prepare working set
        segIds = zeros(maxNrSteps, 1);
        segIds = cat(1, segIds, shSegIds(:));
        
        % find candidates
       [neighSegIds, neighProbs] = arrayfun( ...
           @getNeighbours, shSegIds(:), 'UniformOutput', false);
        neighSrcIds = repelem(shSegIds, cellfun(@numel, neighProbs));
        
        neighProbs = cell2mat(neighProbs);
        neighSegIds = cell2mat(neighSegIds);
        neighSrcIds = reshape(neighSrcIds, [], 1);
        
        for stepIdx = 1:maxNrSteps
            neighExcl = ...
                exclLUT(neighSegIds) ...
              | ismember(neighSegIds, segIds);
          
            neighProbs(neighExcl) = [];
            neighSegIds(neighExcl) = [];
            neighSrcIds(neighExcl) = [];
            
            % find most probable edge
           [edgeProb, idx] = max(neighProbs);
           
            % check candidate
            if isempty(neighSegIds); break; end;
            if edgeProb < minEdgeProb; break; end;
            
            segId = neighSegIds(idx);
            edges(stepIdx, :) = [neighSrcIds(idx), segId];
            segIds(stepIdx) = segId;
            
            % are we done yet?
            attached = dendLUT(segId);
            if attached; break; end;
            
            % get new neighbours
           [newNeighSegIds, newNeighProbs] = getNeighbours(segId);
            newNeighSrcIds = repmat(segId, numel(newNeighSegIds), 1);
           
            % update neighbours
            neighProbs = [neighProbs; newNeighProbs]; %#ok
            neighSegIds = [neighSegIds; newNeighSegIds]; %#ok
            neighSrcIds = [neighSrcIds; newNeighSrcIds]; %#ok
        end
    end

    function [segIds, probs] = getNeighbours(segId)
        segIds = reshape(graph.neighbours{segId}, [], 1);
        probs = reshape(graph.neighProb{segId}, [], 1);
        assert(all(size(segIds) == size(probs)));
    end
end

