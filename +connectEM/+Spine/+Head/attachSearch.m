function [gridParams, results] = attachSearch(param, graph, aggloFile)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    rootDir = param.saveFolder;
    maxSegId = Seg.Global.getMaxSegId(param);
    
    % configuration
    attachParam = struct;
  % attachParam.maxAstroProb = linspace(0, 1, 11);
  % attachParam.maxAxonProb  = linspace(0, 1, 11);
  % attachParam.minDendProb  = linspace(0, 1, 11);
    attachParam.minEdgeProb  = linspace(0, 1, 41);
    attachParam.maxNumSteps  = 10;
    
    % build all combinations
    fieldNames = fieldnames(attachParam);
    gridParams = cellfun(@(n) {attachParam.(n)}, fieldNames);
    
   [gridParams{:}] = ndgrid(gridParams{:});
    gridParams = cellfun(@(v) {reshape(v, [], 1)}, gridParams);
    gridParams = cat(2, gridParams{:});
    
    %% build data structure
    data = struct;
    data.graph = graph;
    data.dendLUT = buildDendLUT(param, aggloFile);
    
    %% prepare for exclusion LUT
    useTypeEM = ...
        isfield(attachParam, 'maxAstroProb') ...
      | isfield(attachParam, 'maxAxonProb') ...
      | isfield(attachParam, 'minDendProb');
  
    if useTypeEM
        predsFile = fullfile(rootDir, 'segmentAggloPredictions.mat');
        preds = load(predsFile);

        % find non-dendritic segments
        astroProbs = preds.probs(:, preds.classes == 'astrocyte');
        axonProbs  = preds.probs(:, preds.classes == 'axon');
        dendProbs  = preds.probs(:, preds.classes == 'dendrite');
    end
    
    %% load ground truth
    spines = connectEM.Spine.loadGroundTruth(param);
    
    dendAggloIds = cellfun( ...
        @(ids) {setdiff(data.dendLUT(ids), 0)}, spines(:, 3));
    
    % HACK(amotta): This is to avoid an error in case the target segments
    % were not part of the dendrite trunk agglomerates.
    dendAggloIds = cellfun(@(ids) max(cat(1, -1, ids(:))), dendAggloIds);
    
    %% run for different parameter sets
    numParams = size(gridParams, 1);
    results = nan(numParams, 2);
    
    for curIdx = 1:numParams
        % build current parameter set
        curAttachParams = cell2struct( ...
            num2cell(gridParams(curIdx, :)), fieldNames, 2);
        
        curData = data;
        curData.exclLUT = false(maxSegId, 1);
        
        if useTypeEM
            % build exclusion LUT
            curExclSegIds = preds.segId( ...
                astroProbs > curAttachParams.maxAstroProb ...
              | axonProbs  > curAttachParams.maxAxonProb ...
              | dendProbs  < curAttachParams.minDendProb);

            % build LUT
            curData.exclLUT(curExclSegIds) = true;
        end
        
        [~, curAttached] = connectEM.Spine.Head.attachCore( ...
            curAttachParams, curData, spines(:, 1));
        
        results(curIdx, 1) = mean(curAttached ~= 0);
        results(curIdx, 2) = mean(curAttached == dendAggloIds);
        
        fprintf('%d of %d evaluations done\n', curIdx, numParams);
    end
end

function dendLUT = buildDendLUT(param, aggloFile);
    % load dendrite agglomerates
   [aggloCands, aggloIds] = ...
       connectEM.Spine.loadDendriteAgglos(param, aggloFile);
    
    % build LUT
    maxSegId = Seg.Global.getMaxSegId(param);
    dendLUT = Agglo.buildLUT(maxSegId, aggloCands);
    
    % translate back agglomerate IDs
    aggloIds(end + 1) = 0;
    dendLUT(~dendLUT) = numel(aggloIds);
    dendLUT = aggloIds(dendLUT);
end
