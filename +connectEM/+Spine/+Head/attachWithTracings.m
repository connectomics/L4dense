function [superAgglos, trT] = ...
        attachWithTracings(param, superAgglos, shT, trT)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    agglos = Agglo.fromSuperAgglo(superAgglos);
    trunkMask = connectEM.Spine.buildDendriteTrunkMask(param, agglos);

    %% Build look-up tables
    Util.log('Building look-up tables');

    maxSegId = Seg.Global.getMaxSegId(param);
    shLUT = Agglo.buildLUT(maxSegId, shT.segIds);

    % Terminology
    % * dendrite trunk agglomerates are ones to which spine heads may attach
    % * other dendrite agglomerates are the ones which may be picked-up
    trunkLUT = Agglo.buildLUT(maxSegId, agglos(trunkMask), find(trunkMask)); %#ok
    otherLUT = Agglo.buildLUT(maxSegId, agglos(~trunkMask), find(~trunkMask)); %#ok

    %% Preprocess tracings
    Util.log('Preprocessing tracings');

    % Remove tracings that contain a comment
    trT(~cellfun(@isempty, trT.comment), :) = [];

    % Remove tracings without nodes
    trT(cellfun(@isempty, trT.nodes), :) = [];

    % Remove tracings that do not start in spine head
    trT.shId = cellfun(@(s) s(1, 1), trT.segIds);
    trT(~trT.shId, :) = [];

    trT.shId = shLUT(trT.shId);
    trT(~trT.shId, :) = [];

    % Remove tracings starting from same spine head
   [dupShIds, ~, dupShCount] = unique(trT.shId);
    dupShIds = dupShIds(accumarray(dupShCount, 1) > 1);
    trT(ismember(trT.shId, dupShIds), :) = [];

    % Remove tracings that do not reach dendrite trunk
    trT.trunkId = cellfun( ...
        @(segIds) nonzeros(segIds(end, :)), ...
        trT.segIds, 'UniformOutput', false);
    trT(cellfun(@isempty, trT.trunkId), :) = [];

    trT.trunkId = cellfun(@(segIds) mode( ...
        nonzeros(trunkLUT(segIds))), trT.trunkId);
    trT(isnan(trT.trunkId), :) = [];

    % A note on segments collected by spine head tracings
    %
    % Spine heads agglomerates generated independently from the dendrite
    % agglomerates. As a result, we could pursue one of the following
    % strategies for segment collection:
    %
    % * Patch in the spine head agglomerates and cut out the corresponding
    %   segments from the dendrite agglomerates.
    % * Find the dendrite agglomerate which corresponds best to the spine
    %   head agglomerate. Then patch in the dendrite agglomerate.
    %
    % For now, let's use the latter method due to its simplicity.

    % Translate spine heads to dendrite agglomerate.
    trT.otherId = cellfun( ...
        @(segIds) nonzeros(otherLUT(segIds(1, 1))), ...
        trT.segIds, 'UniformOutput', false);

    % Ignore spine heads which overlap with multiple dendrite agglomerate.
    trT(cellfun(@isempty, trT.otherId), :) = [];
    trT.otherId = cell2mat(trT.otherId);

    % Ignore agglomerates with conflicting trunk attachments.
    dupOtherIds = unique(trT(:, {'trunkId', 'otherId'}), 'rows');
   [dupOtherIds, ~, dupOtherCount] = unique(dupOtherIds.otherId);
    dupOtherIds = dupOtherIds(accumarray(dupOtherCount, 1) > 1);
    trT(ismember(trT.otherId, dupOtherIds), :) = [];

    %% Build super-agglomerate representation
    Util.log('Building super-agglomerates');

    for curIdx = 1:size(trT, 1)
        % Overlap between nodes and trunk agglomerates is established using
        % the 26-neighbourhood of the last node. As a result, the last node
        % itself might not be placed in the trunk . Let's fake the ID in
        % order to guarantee attachment with `SuperAgglo.merge`.
        curSegIds = trT.segIds{curIdx};
        curTrunkMask = arrayfun(@(id) id && ...
            trunkLUT(id) == trT.trunkId(curIdx), curSegIds);

        curLastSegId = find(curTrunkMask(end, :), 1, 'first');
        curSegIds(end, 1) = curSegIds(end, curLastSegId);
        curSegIds = curSegIds(:, 1);

        % Build proper masks
        curOtherMask = arrayfun(@(id) id && ...
            otherLUT(id) == trT.otherId(curIdx), curSegIds);
        curTrunkMask = arrayfun(@(id) id && ...
            trunkLUT(id) == trT.trunkId(curIdx), curSegIds);

        % Sanity checks
        assert(curOtherMask(1));
        assert(curTrunkMask(end));

        % Restrict to stretch from spine head to trunk
        % Please note that this assumes linear tracings!
        curEndId = find(curTrunkMask, 1, 'first');
        curStartId = find(curOtherMask(1:curEndId), 1, 'last');
        curNodeIds = curStartId:curEndId;
        assert(numel(curNodeIds) > 1);

        % Remove segment label for all nodes which are not part of either
        % the spine head or the dendrite trunk. This essentially means that
        % we're not trusting the segment pick-up along the spine neck.
        curSegIds = double(curSegIds);
        curSegIds(~curOtherMask & ~curTrunkMask) = nan;

        curSuperAgglo = struct;
        curSuperAgglo.nodes = [trT.nodes{curIdx}, curSegIds];
        curSuperAgglo.edges =  trT.edges{curIdx};

        curSuperAgglo.nodes = curSuperAgglo.nodes(curNodeIds, :);
       [~, curSuperAgglo.edges] = ismember(curSuperAgglo.edges, curNodeIds);
        curSuperAgglo.edges(~all(curSuperAgglo.edges, 2), :) = [];

        % Merge spine head and dendrite trunk agglomerates
        curSuperAgglo = SuperAgglo.merge( ...
            superAgglos(trT.otherId(curIdx)), curSuperAgglo);
        superAgglos(trT.trunkId(curIdx)) = SuperAgglo.merge( ...
            superAgglos(trT.trunkId(curIdx)), curSuperAgglo);
    end
end
