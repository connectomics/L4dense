function [edges, attached, dendOut] = ...
        attach(param, attachParam, graph, sh, dendIn)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    %   Sahil Loomba <sahil.loomba@brain.mpg.de>
    
    % mask for spine attachment
    % NOTE(amotta): In a meeting with MH we've decided to use all
    % postsynaptic processes (including AIS) as spine attachment targets.
    dendMask = true(numel(dendIn.dendrites), 1);
    
    % prepare data
    superAgglos = dendIn.dendrites(dendMask);
    agglos = Agglo.fromSuperAgglo(superAgglos);
    trunkMask = connectEM.Spine.buildDendriteTrunkMask(param, agglos);
    
    data = struct;
    data.graph = graph;
    data.exclLUT = buildExclLUT(param, attachParam);
   [data.dendLUT, otherLUT] = buildDendLUT(param, agglos, trunkMask);
    
    % Try to attach spine heads
   [edges, attached] = ...
       connectEM.Spine.Head.attachCore(attachParam, data, sh.agglos);
   [shAgglos, shEdges] = ...
       connectEM.Spine.Head.mergeNeck(sh.agglos, sh.edges, edges);
   
    % Convert spines to super-agglomerates
    segPoints = Seg.Global.getSegToPointMap(param);
    shSuperAgglos = connectEM.Spine.toSuperAgglo(segPoints, shAgglos, shEdges);
    
    % Initialize state
    usedOtherLUT = false(size(superAgglos));

    % Build new dendrite agglos.
    for curIdx = 1:numel(shSuperAgglos)
        curDendIdx = attached(curIdx);
        if ~curDendIdx; continue; end
        
        % To avoid duplication of segments, each non-candidate agglomerate
        % can be picked up at most once. For now we're using a first-come
        % first-serve approach.
        %
        % As a result, the segment-pickup is dependent on the somewhat
        % arbitrary order of spine heads. Maybe we will need to improve
        % this in the future...
        curOtherIds = setdiff(otherLUT(shAgglos{curIdx}), 0);
        curOtherIds(usedOtherLUT(curOtherIds)) = [];
        curOtherIds = reshape(curOtherIds, 1, []);
        
        curShSuperAgglo = shSuperAgglos(curIdx);
        
        % Incorporate overlapping agglomerates into spine.
        for curMergeIdx = curOtherIds
            curShSuperAgglo = SuperAgglo.merge( ...
                curShSuperAgglo, superAgglos(curMergeIdx));
        end
        
        % Mark picked-up agglomerates to avoid reuse.
        usedOtherLUT(curOtherIds) = true;
        
        % Incorporate spine into dendrite trunk.
        superAgglos(curDendIdx) = SuperAgglo.merge( ...
            superAgglos(curDendIdx), curShSuperAgglo);
    end
    
    % Remove picked-up agglomerates
    superAgglos(usedOtherLUT) = [];
    
    % Translate dendrite indices
    superAggloIds = (1:numel(usedOtherLUT))';
    superAggloIds(usedOtherLUT) = [];
    
    % Adding AIS back in
    dendIds = find(dendMask);
    otherIds = find(~dendMask);
    
   [~, attached] = ismember(attached, superAggloIds);
    attached(attached > 0) = dendIds(attached(attached > 0));
    
    superAgglos = [superAgglos; dendIn.dendrites(otherIds)];
    superAggloIds = [dendIds(superAggloIds); otherIds];
   
    % Build output
    dendOut = struct;
    dendOut.dendrites = superAgglos;
    dendOut.parentIds = superAggloIds;
end

function exclLUT = buildExclLUT(param, attachParam)
    maxSegId = Seg.Global.getMaxSegId(param);
    exclLUT = false(maxSegId, 1);
    
    % skip rest, if there are no type thresholds
    fieldNames = {'maxAstroProb', 'maxAxonProb', 'minDendProb'};
    if ~any(ismember(fieldNames, fieldnames(attachParam))); return; end
    
    % load segment classifications
    predsFile = fullfile(param.saveFolder, 'segmentAggloPredictions.mat');
    preds = load(predsFile);
    
    % find non-dendritic segments
    astroProbs = preds.probs(:, preds.classes == 'astrocyte');
    axonProbs  = preds.probs(:, preds.classes == 'axon');
    dendProbs  = preds.probs(:, preds.classes == 'dendrite');
    
    exclSegIds = preds.segId( ...
        astroProbs > attachParam.maxAstroProb ...
      | axonProbs  > attachParam.maxAxonProb ...
      | dendProbs  < attachParam.minDendProb);
    
    % build LUT
    exclLUT(exclSegIds) = true;
end

function [trunkLUT, otherLUT] = buildDendLUT(param, agglos, trunkMask)
    maxSegId = Seg.Global.getMaxSegId(param);
    
    % build LUT
    trunkIds = find(trunkMask);
    trunkLUT = Agglo.buildLUT(maxSegId, agglos(trunkMask));
    trunkLUT(trunkLUT ~= 0) = trunkIds(trunkLUT(trunkLUT ~= 0));
    
    otherIds = find(~trunkMask);
    otherLUT = Agglo.buildLUT(maxSegId, agglos(~trunkMask));
    otherLUT(otherLUT ~= 0) = otherIds(otherLUT(otherLUT ~= 0));
end
