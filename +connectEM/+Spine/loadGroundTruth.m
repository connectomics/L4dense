function spines = loadGroundTruth(param)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    
    tracingDir = fileparts(mfilename('fullpath'));
    tracingDir = fullfile(tracingDir, '+Data', 'attachment-ground-truth');
    nmlFile = fullfile(tracingDir, 'test-set.nml');
    
    nml = slurpNml(nmlFile);
    nodes = NML.buildNodeTable(nml);
    trees = NML.buildTreeTable(nml);
    comments = NML.buildCommentTable(nml);
    
    % verify comments
    outColumns = {'sh', 'neck', 'dend'};
    assert(all(ismember(comments.comment, outColumns)));
    
    % add comments to node table
   [~, rows] = ismember(comments.node, nodes.id);
    nodes.comment = repmat({'neck'}, height(nodes), 1);
    nodes.comment(rows) = comments.comment;
    nodes.comment = categorical(nodes.comment);
    
    % load segment IDs
    nodes.segId = Seg.Global.getSegIds(param, 1 + nodes.coord);
    
    if ~all(nodes.segId > 0)
        % verify merger mode tracing
        warning('Removing nodes placed on border');
        nodes(~nodes.segId, :) = [];
    end
    
    % determine output columns / rows
   [~, nodes.outCol] = ismember(nodes.comment, outColumns);
   [uniTreeIds, ~, nodes.outRow] = unique(nodes.treeId);
   
    % this is for debugging only...
    % uniTrees = tree
    trees = sortrows(trees, 'id', 'ascend');
    uniTrees = trees(ismember(trees.id, uniTreeIds), :);
    
    outSize = [numel(uniTreeIds), numel(outColumns)];
    nodes.outIdx = sub2ind(outSize, nodes.outRow, nodes.outCol);
    
    % build output
    spines = reshape(1:prod(outSize), outSize);
    spines = arrayfun(@(idx) ...
        unique(nodes.segId(nodes.outIdx == idx)), ...
        spines, 'UniformOutput', false);
    
    % each tree must contain at least one spine head node
    shMissing = cellfun(@isempty, spines(:, 1));
    
    if any(shMissing)
        errMsg = strjoin(uniTrees.name(shMissing), ', ');
        errMsg = ['The following trees are missing spine heads: ', errMsg];

        error(errMsg);
    end
end
